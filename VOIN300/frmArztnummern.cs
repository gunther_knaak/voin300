using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace VOIN10
{
  public partial class frmArztnummern : Form
  {
    private bool bApply;
    public bool ApplyClicked
    {
      get { return bApply; }
    }
    private string strNr;
    public string Arztnummer
    {
      get { return strNr; }
    }

    public frmArztnummern()
    {
      InitializeComponent();
      bApply = false;
      strNr = "";
      lstArztnr.Focus();
    }

    public void Reinit()
    {
      this.bApply = false;
      this.strNr = "";
    }

    public void SetInfo(string Info)
    {
      lblInfo.Text = Info;
    }

    public void SetSelected(string ArztNr)
    {
      int i = 0;
      if (lstArztnr.Items.Count > 0 && ArztNr.Length == 9)
      {
        i = lstArztnr.FindString(ArztNr);
        if (i < 0)
          i = 0;
      }
      lstArztnr.SelectedIndex = i;
    }

    public void ClearArztNr()
    {
      lstArztnr.Items.Clear();
    }

    public void AddArztNr(string Nr)
    {
      lstArztnr.Items.Add(Nr);
    }

    private void btnOk_Click(object sender, EventArgs e)
    {
      if (lstArztnr.SelectedItem == null)
        MessageBox.Show("Bitte w�hlen zuerst eine Arztnummer aus der Liste.", "Keine Arztnummer gew�hlt");
      else
      {
        this.strNr = lstArztnr.SelectedItem.ToString();
        this.bApply = true;
        this.Close();
      }
    }

    private void lstArztnr_DoubleClick(object sender, EventArgs e)
    {
      if (lstArztnr.SelectedItem != null)
        btnOk_Click(sender, e);
    }

    private void frmArztnummern_Shown(object sender, EventArgs e)
    {
      Reinit();
      lstArztnr.Focus();
      //if (lstArztnr.Items.Count > 0)
      //  lstArztnr.SelectedIndex = 0;
    }

    private void lstArztnr_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (e.KeyChar == '\r')
        lstArztnr_DoubleClick(sender, null);
    }

    /*
     * Author: rstem
     * Datum: 17.12.2014
     * L�schen einer Verbindung zwischen arzt und betriebsst�tte
     */
    private void btnDel_Click(object sender, EventArgs e)
    {
        if (lstArztnr.SelectedItem != null)
        {
            int tmpSelIndex = lstArztnr.SelectedIndex;
            MainForm m = (MainForm)this.Owner;
            m.delArtznummerBetriebBerbindung(lstArztnr.SelectedItem.ToString());
            lstArztnr.Items.Remove(lstArztnr.SelectedItem);

            if (lstArztnr.Items.Count == 1)
            {
                lstArztnr.SelectedIndex = 0;
                btnOk_Click(sender, e);
            }
            else
            {
                lstArztnr.SelectedIndex = tmpSelIndex - 1;
            }

        }

    }
  }
}