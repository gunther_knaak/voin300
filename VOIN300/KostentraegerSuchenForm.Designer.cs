﻿namespace VOIN10
{
    partial class KostentraegerSuchenForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
          this.panel1 = new System.Windows.Forms.Panel();
          this.cancelBtn = new System.Windows.Forms.Button();
          this.okBtn = new System.Windows.Forms.Button();
          this.ikTB = new System.Windows.Forms.TextBox();
          this.dataGridView1 = new System.Windows.Forms.DataGridView();
          this.IKColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
          this.Name1Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
          this.Name2Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
          this.Name3Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
          this.Name4Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
          this.kurzbezeichnungColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
          this.ans_OrtColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
          this.dataGridTextBoxColumn5 = new System.Windows.Forms.DataGridTextBoxColumn();
          this.dataGridTextBoxColumn4 = new System.Windows.Forms.DataGridTextBoxColumn();
          this.dataGridTextBoxColumn3 = new System.Windows.Forms.DataGridTextBoxColumn();
          this.dataGridTextBoxColumn2 = new System.Windows.Forms.DataGridTextBoxColumn();
          this.dataGridTextBoxColumn1 = new System.Windows.Forms.DataGridTextBoxColumn();
          this.panel1.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
          this.SuspendLayout();
          // 
          // panel1
          // 
          this.panel1.Controls.Add(this.cancelBtn);
          this.panel1.Controls.Add(this.okBtn);
          this.panel1.Controls.Add(this.ikTB);
          this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
          this.panel1.Location = new System.Drawing.Point(0, 0);
          this.panel1.Name = "panel1";
          this.panel1.Size = new System.Drawing.Size(138, 279);
          this.panel1.TabIndex = 0;
          // 
          // cancelBtn
          // 
          this.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
          this.cancelBtn.Location = new System.Drawing.Point(3, 58);
          this.cancelBtn.Name = "cancelBtn";
          this.cancelBtn.Size = new System.Drawing.Size(132, 23);
          this.cancelBtn.TabIndex = 5;
          this.cancelBtn.Text = "&Abbrechen";
          this.cancelBtn.UseVisualStyleBackColor = true;
          // 
          // okBtn
          // 
          this.okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
          this.okBtn.Location = new System.Drawing.Point(3, 29);
          this.okBtn.Name = "okBtn";
          this.okBtn.Size = new System.Drawing.Size(132, 23);
          this.okBtn.TabIndex = 4;
          this.okBtn.Text = "&Übernehmen";
          this.okBtn.UseVisualStyleBackColor = true;
          // 
          // ikTB
          // 
          this.ikTB.Location = new System.Drawing.Point(3, 3);
          this.ikTB.Name = "ikTB";
          this.ikTB.Size = new System.Drawing.Size(132, 20);
          this.ikTB.TabIndex = 0;
          this.ikTB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ikTB_KeyDown);
          // 
          // dataGridView1
          // 
          this.dataGridView1.AllowUserToAddRows = false;
          this.dataGridView1.AllowUserToDeleteRows = false;
          this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
          this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
          this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IKColumn,
            this.Name1Column,
            this.Name2Column,
            this.Name3Column,
            this.Name4Column,
            this.kurzbezeichnungColumn,
            this.ans_OrtColumn});
          this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
          this.dataGridView1.Location = new System.Drawing.Point(138, 0);
          this.dataGridView1.MultiSelect = false;
          this.dataGridView1.Name = "dataGridView1";
          this.dataGridView1.ReadOnly = true;
          this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
          this.dataGridView1.Size = new System.Drawing.Size(559, 279);
          this.dataGridView1.TabIndex = 2;
          this.dataGridView1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseDoubleClick);
          // 
          // IKColumn
          // 
          this.IKColumn.DataPropertyName = "IK";
          this.IKColumn.HeaderText = "IK";
          this.IKColumn.Name = "IKColumn";
          this.IKColumn.ReadOnly = true;
          // 
          // Name1Column
          // 
          this.Name1Column.DataPropertyName = "Name1";
          this.Name1Column.HeaderText = "Name1";
          this.Name1Column.Name = "Name1Column";
          this.Name1Column.ReadOnly = true;
          // 
          // Name2Column
          // 
          this.Name2Column.DataPropertyName = "Name2";
          this.Name2Column.HeaderText = "Name2";
          this.Name2Column.Name = "Name2Column";
          this.Name2Column.ReadOnly = true;
          // 
          // Name3Column
          // 
          this.Name3Column.DataPropertyName = "Name3";
          this.Name3Column.HeaderText = "Name3";
          this.Name3Column.Name = "Name3Column";
          this.Name3Column.ReadOnly = true;
          // 
          // Name4Column
          // 
          this.Name4Column.DataPropertyName = "Name4";
          this.Name4Column.HeaderText = "Name4";
          this.Name4Column.Name = "Name4Column";
          this.Name4Column.ReadOnly = true;
          // 
          // kurzbezeichnungColumn
          // 
          this.kurzbezeichnungColumn.DataPropertyName = "Kurzbezeichnung";
          this.kurzbezeichnungColumn.HeaderText = "Kurzbezeichnung";
          this.kurzbezeichnungColumn.Name = "kurzbezeichnungColumn";
          this.kurzbezeichnungColumn.ReadOnly = true;
          // 
          // ans_OrtColumn
          // 
          this.ans_OrtColumn.DataPropertyName = "Ans_Ort";
          this.ans_OrtColumn.HeaderText = "Ort";
          this.ans_OrtColumn.Name = "ans_OrtColumn";
          this.ans_OrtColumn.ReadOnly = true;
          // 
          // dataGridTextBoxColumn5
          // 
          this.dataGridTextBoxColumn5.Format = "";
          this.dataGridTextBoxColumn5.FormatInfo = null;
          this.dataGridTextBoxColumn5.HeaderText = "Name4";
          this.dataGridTextBoxColumn5.MappingName = "Name4";
          this.dataGridTextBoxColumn5.ReadOnly = true;
          this.dataGridTextBoxColumn5.Width = 108;
          // 
          // dataGridTextBoxColumn4
          // 
          this.dataGridTextBoxColumn4.Format = "";
          this.dataGridTextBoxColumn4.FormatInfo = null;
          this.dataGridTextBoxColumn4.HeaderText = "Name3";
          this.dataGridTextBoxColumn4.MappingName = "Name3";
          this.dataGridTextBoxColumn4.ReadOnly = true;
          this.dataGridTextBoxColumn4.Width = 108;
          // 
          // dataGridTextBoxColumn3
          // 
          this.dataGridTextBoxColumn3.Format = "";
          this.dataGridTextBoxColumn3.FormatInfo = null;
          this.dataGridTextBoxColumn3.HeaderText = "Name2";
          this.dataGridTextBoxColumn3.MappingName = "Name2";
          this.dataGridTextBoxColumn3.ReadOnly = true;
          this.dataGridTextBoxColumn3.Width = 108;
          // 
          // dataGridTextBoxColumn2
          // 
          this.dataGridTextBoxColumn2.Format = "";
          this.dataGridTextBoxColumn2.FormatInfo = null;
          this.dataGridTextBoxColumn2.HeaderText = "Name1";
          this.dataGridTextBoxColumn2.MappingName = "Name1";
          this.dataGridTextBoxColumn2.ReadOnly = true;
          this.dataGridTextBoxColumn2.Width = 108;
          // 
          // dataGridTextBoxColumn1
          // 
          this.dataGridTextBoxColumn1.Format = "";
          this.dataGridTextBoxColumn1.FormatInfo = null;
          this.dataGridTextBoxColumn1.HeaderText = "IK";
          this.dataGridTextBoxColumn1.MappingName = "IK";
          this.dataGridTextBoxColumn1.ReadOnly = true;
          this.dataGridTextBoxColumn1.Width = 65;
          // 
          // KostentraegerSuchenForm
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(697, 279);
          this.Controls.Add(this.dataGridView1);
          this.Controls.Add(this.panel1);
          this.KeyPreview = true;
          this.Name = "KostentraegerSuchenForm";
          this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
          this.Text = "Kostenträger suchen";
          this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
          this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KostentraegerSuchenForm_KeyDown);
          this.panel1.ResumeLayout(false);
          this.panel1.PerformLayout();
          ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
          this.ResumeLayout(false);

        }

        #endregion

      private System.Windows.Forms.Panel panel1;
      private System.Windows.Forms.TextBox ikTB;
        private System.Windows.Forms.Button cancelBtn;
      private System.Windows.Forms.Button okBtn;
      private System.Windows.Forms.DataGridView dataGridView1;
      private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn5;
      private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn4;
      private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn3;
      private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn2;
      private System.Windows.Forms.DataGridTextBoxColumn dataGridTextBoxColumn1;
      private System.Windows.Forms.DataGridViewTextBoxColumn IKColumn;
      private System.Windows.Forms.DataGridViewTextBoxColumn Name1Column;
      private System.Windows.Forms.DataGridViewTextBoxColumn Name2Column;
      private System.Windows.Forms.DataGridViewTextBoxColumn Name3Column;
      private System.Windows.Forms.DataGridViewTextBoxColumn Name4Column;
      private System.Windows.Forms.DataGridViewTextBoxColumn kurzbezeichnungColumn;
      private System.Windows.Forms.DataGridViewTextBoxColumn ans_OrtColumn;
    }
}