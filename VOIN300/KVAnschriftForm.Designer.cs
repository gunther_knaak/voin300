﻿namespace VOIN10
{
  partial class KVAnschriftForm
  {
    /// <summary>
    /// Erforderliche Designervariable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Verwendete Ressourcen bereinigen.
    /// </summary>
    /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Vom Windows Form-Designer generierter Code

    /// <summary>
    /// Erforderliche Methode für die Designerunterstützung.
    /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
    /// </summary>
    private void InitializeComponent()
    {
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.name1TB = new System.Windows.Forms.TextBox();
      this.name2TB = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.name3TB = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.name4TB = new System.Windows.Forms.TextBox();
      this.label5 = new System.Windows.Forms.Label();
      this.strasseTB = new System.Windows.Forms.TextBox();
      this.label6 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.ortTB = new System.Windows.Forms.TextBox();
      this.plzMTB = new System.Windows.Forms.MaskedTextBox();
      this.okBtn = new System.Windows.Forms.Button();
      this.cancelBtn = new System.Windows.Forms.Button();
      this.suchenBtn = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(12, 9);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(356, 16);
      this.label1.TabIndex = 0;
      this.label1.Text = "Der Kostenvoranschlag geht an folgende Anschrift:";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(12, 31);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(44, 13);
      this.label2.TabIndex = 1;
      this.label2.Text = "Name1:";
      // 
      // name1TB
      // 
      this.name1TB.Location = new System.Drawing.Point(62, 28);
      this.name1TB.Name = "name1TB";
      this.name1TB.Size = new System.Drawing.Size(306, 20);
      this.name1TB.TabIndex = 2;
      // 
      // name2TB
      // 
      this.name2TB.Location = new System.Drawing.Point(62, 54);
      this.name2TB.Name = "name2TB";
      this.name2TB.Size = new System.Drawing.Size(306, 20);
      this.name2TB.TabIndex = 4;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(12, 57);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(44, 13);
      this.label3.TabIndex = 3;
      this.label3.Text = "Name2:";
      // 
      // name3TB
      // 
      this.name3TB.Location = new System.Drawing.Point(62, 80);
      this.name3TB.Name = "name3TB";
      this.name3TB.Size = new System.Drawing.Size(306, 20);
      this.name3TB.TabIndex = 6;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(12, 83);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(44, 13);
      this.label4.TabIndex = 5;
      this.label4.Text = "Name3:";
      // 
      // name4TB
      // 
      this.name4TB.Location = new System.Drawing.Point(62, 106);
      this.name4TB.Name = "name4TB";
      this.name4TB.Size = new System.Drawing.Size(306, 20);
      this.name4TB.TabIndex = 8;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(12, 109);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(44, 13);
      this.label5.TabIndex = 7;
      this.label5.Text = "Name4:";
      // 
      // strasseTB
      // 
      this.strasseTB.Location = new System.Drawing.Point(62, 132);
      this.strasseTB.Name = "strasseTB";
      this.strasseTB.Size = new System.Drawing.Size(306, 20);
      this.strasseTB.TabIndex = 10;
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(12, 135);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(41, 13);
      this.label6.TabIndex = 9;
      this.label6.Text = "Straße:";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(12, 161);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(30, 13);
      this.label7.TabIndex = 11;
      this.label7.Text = "PLZ:";
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(118, 161);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(24, 13);
      this.label8.TabIndex = 13;
      this.label8.Text = "Ort:";
      // 
      // ortTB
      // 
      this.ortTB.Location = new System.Drawing.Point(148, 158);
      this.ortTB.Name = "ortTB";
      this.ortTB.Size = new System.Drawing.Size(220, 20);
      this.ortTB.TabIndex = 14;
      // 
      // plzMTB
      // 
      this.plzMTB.Location = new System.Drawing.Point(62, 158);
      this.plzMTB.Mask = "00000";
      this.plzMTB.Name = "plzMTB";
      this.plzMTB.Size = new System.Drawing.Size(50, 20);
      this.plzMTB.TabIndex = 12;
      // 
      // okBtn
      // 
      this.okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.okBtn.Location = new System.Drawing.Point(212, 184);
      this.okBtn.Name = "okBtn";
      this.okBtn.Size = new System.Drawing.Size(75, 23);
      this.okBtn.TabIndex = 16;
      this.okBtn.Text = "OK";
      this.okBtn.UseVisualStyleBackColor = true;
      // 
      // cancelBtn
      // 
      this.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.cancelBtn.Location = new System.Drawing.Point(293, 184);
      this.cancelBtn.Name = "cancelBtn";
      this.cancelBtn.Size = new System.Drawing.Size(75, 23);
      this.cancelBtn.TabIndex = 17;
      this.cancelBtn.Text = "Abbrechen";
      this.cancelBtn.UseVisualStyleBackColor = true;
      // 
      // suchenBtn
      // 
      this.suchenBtn.Location = new System.Drawing.Point(131, 184);
      this.suchenBtn.Name = "suchenBtn";
      this.suchenBtn.Size = new System.Drawing.Size(75, 23);
      this.suchenBtn.TabIndex = 15;
      this.suchenBtn.TabStop = false;
      this.suchenBtn.Text = "Suchen F2";
      this.suchenBtn.UseVisualStyleBackColor = true;
      this.suchenBtn.Click += new System.EventHandler(this.suchenBtn_Click);
      // 
      // KVAnschriftForm
      // 
      this.AcceptButton = this.okBtn;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.cancelBtn;
      this.ClientSize = new System.Drawing.Size(378, 217);
      this.Controls.Add(this.suchenBtn);
      this.Controls.Add(this.cancelBtn);
      this.Controls.Add(this.okBtn);
      this.Controls.Add(this.plzMTB);
      this.Controls.Add(this.ortTB);
      this.Controls.Add(this.label8);
      this.Controls.Add(this.label7);
      this.Controls.Add(this.strasseTB);
      this.Controls.Add(this.label6);
      this.Controls.Add(this.name4TB);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.name3TB);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.name2TB);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.name1TB);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "KVAnschriftForm";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Kostenvoranschlag";
      this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KVAnschriftForm_KeyDown);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox name1TB;
    private System.Windows.Forms.TextBox name2TB;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox name3TB;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox name4TB;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.TextBox strasseTB;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.TextBox ortTB;
    private System.Windows.Forms.MaskedTextBox plzMTB;
    private System.Windows.Forms.Button okBtn;
    private System.Windows.Forms.Button cancelBtn;
    private System.Windows.Forms.Button suchenBtn;
  }
}