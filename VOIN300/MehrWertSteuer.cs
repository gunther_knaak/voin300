﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace VOIN10
{
  class MehrWertSteuer
  {
    private static ArrayList mwstList;

    public static ArrayList MWStList
    {
      get { return mwstList; }
    }

    static MehrWertSteuer()
    {
      mwstList = new ArrayList();
      mwstList.Add(new MehrWertSteuer(0, 0));
      mwstList.Add(new MehrWertSteuer(1, 19));
      mwstList.Add(new MehrWertSteuer(2, 7));
    }

    private MehrWertSteuer(short mwstCode, decimal mwst)
    {
      this.mwstCode = mwstCode;
      this.mwst = mwst;
    }

    private short mwstCode;
    public short MWStCode
    {
      get { return mwstCode; }
    }

    private decimal mwst;
    public decimal MWSt
    {
      get { return mwst; }
    }

    public string MWStName
    {
      get 
      {
        return MWStCode.ToString() + " - " + (MWSt == 0 ? "keine" : MWSt.ToString() + "%");
      }
    }
  }
}
