﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VOIN10
{
  class Rezept
  {
    public Rezept(string nachname, string vorname, int id)
    {
      this.nachname = nachname;
      this.vorname = vorname;
      this.id = id;
    }

    private string nachname;

    public string Nachname
    {
      get { return nachname; }
      set { nachname = value; }
    }

    private string vorname;

    public string Vorname
    {
      get { return vorname; }
      set { vorname = value; }
    }

    public string Name
    {
      get
      {
        return Nachname + " " + Vorname;
      }
    }

    private int id;

    public int ID
    {
      get { return id; }
      set { id = value; }
    }

    public override string ToString()
    {
      return Name;
    }
  }
}
