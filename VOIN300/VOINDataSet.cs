﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace VOIN10 {


  partial class VOINDataSet
  {
      partial class Kostentraeger_300DataTable
      {
      }
  
    partial class tab_HMPBezeichnungDataTable
    {
   }
  
    partial class RezepteDataTable
    {
      protected override void OnTableNewRow(System.Data.DataTableNewRowEventArgs e)
      {
        e.Row["RezArt"] = 1;
        e.Row["Betriebsstaettennummer"] = "999999999";
        e.Row["ArztNr"] = "999999999";
        e.Row["Befreit"] = false;
        e.Row["Vorgang"] = 300;
        e.Row["Status"] = "10001";
        e.Row["Erfasst"] = DateTime.Now;
        e.Row["Hilfsmittel"] = 0;
        e.Row["RechNr_Kasse"] = -1;
        e.Row["RechNr_Mandant"] = -1;
        e.Row["Pauschale"] = 0m;
        e.Row["Unfall"] = false;
        e.Row["GebDat"] = new DateTime(1900, 1, 1);
        e.Row["VersNr"] = "0";
        //e.Row["Besonderheit"] = Environment.UserName;
        base.OnTableNewRow(e);
      }
    }

  }
}
