using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace VOIN10
{
  public partial class KVAnschriftForm : Form
  {
    public string Name1
    {
      get { return name1TB.Text; }
      set { name1TB.Text = value; }
    }

    public string Name2
    {
      get { return name2TB.Text; }
      set { name2TB.Text = value; }
    }

    public string Name3
    {
      get { return name3TB.Text; }
      set { name3TB.Text = value; }
    }

    public string Name4
    {
      get { return name4TB.Text; }
      set { name4TB.Text = value; }
    }

    public string Strasse
    {
      get { return strasseTB.Text; }
      set { strasseTB.Text = value; }
    }

    public string PLZ
    {
      get { return plzMTB.Text; }
      set { plzMTB.Text = value; }
    }

    public string Ort
    {
      get { return ortTB.Text; }
      set { ortTB.Text = value; }
    }

    private string ik = string.Empty;
    public string IK
    {
      get { return ik; }
      set { ik = value; }
    }

    private VOINDataSet dataSet = null;
    public VOINDataSet DataSet
    {
      get { return dataSet; }
      set { dataSet = value; }
    }

    public KVAnschriftForm()
    {
      InitializeComponent();
    }

    private void suchenBtn_Click(object sender, EventArgs e)
    {
      using (KostentraegerSuchenForm form = new KostentraegerSuchenForm())
      {
        form.DataSet = DataSet;
        if (form.ShowDialog() == DialogResult.OK)
        {
          VOINDataSet.Kostentraeger_300Row r = form.Row;
          if (r != null)
          {
            Name1 = (r.IsName1Null() ? string.Empty : r.Name1);
            Name2 = (r.IsName2Null() ? string.Empty : r.Name2);
            Name3 = (r.IsName3Null() ? string.Empty : r.Name3);
            Name4 = (r.IsName4Null() ? string.Empty : r.Name4);
            Strasse = (r.IsAns_StrasseNull() ? string.Empty : r.Ans_Strasse);
            PLZ = (r.IsAns_plzNull() ? string.Empty : r.Ans_plz);
            Ort = (r.IsAns_OrtNull() ? string.Empty : r.Ans_Ort);
            IK = r.IK.Trim();
          }
        }
      }
    }

    private void KVAnschriftForm_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.F2)
      {
        suchenBtn_Click(suchenBtn, EventArgs.Empty);
        e.Handled = true;
      }
    }
  }
}