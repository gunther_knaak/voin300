﻿namespace VOIN10
{
  partial class KorrekturTypeForm
  {
    /// <summary>
    /// Erforderliche Designervariable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Verwendete Ressourcen bereinigen.
    /// </summary>
    /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Vom Windows Form-Designer generierter Code

    /// <summary>
    /// Erforderliche Methode für die Designerunterstützung.
    /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
    /// </summary>
    private void InitializeComponent()
    {
      this.zuruecksetzenRB = new System.Windows.Forms.RadioButton();
      this.nachkorrekturRB = new System.Windows.Forms.RadioButton();
      this.vollkorrekturRB = new System.Windows.Forms.RadioButton();
      this.okBtn = new System.Windows.Forms.Button();
      this.cancelBtn = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // zuruecksetzenRB
      // 
      this.zuruecksetzenRB.AutoSize = true;
      this.zuruecksetzenRB.Checked = true;
      this.zuruecksetzenRB.Location = new System.Drawing.Point(12, 12);
      this.zuruecksetzenRB.Name = "zuruecksetzenRB";
      this.zuruecksetzenRB.Size = new System.Drawing.Size(90, 17);
      this.zuruecksetzenRB.TabIndex = 0;
      this.zuruecksetzenRB.TabStop = true;
      this.zuruecksetzenRB.Text = "Zurücksetzen";
      this.zuruecksetzenRB.UseVisualStyleBackColor = true;
      // 
      // nachkorrekturRB
      // 
      this.nachkorrekturRB.AutoSize = true;
      this.nachkorrekturRB.Enabled = false;
      this.nachkorrekturRB.Location = new System.Drawing.Point(12, 35);
      this.nachkorrekturRB.Name = "nachkorrekturRB";
      this.nachkorrekturRB.Size = new System.Drawing.Size(93, 17);
      this.nachkorrekturRB.TabIndex = 1;
      this.nachkorrekturRB.Text = "Nachkorrektur";
      this.nachkorrekturRB.UseVisualStyleBackColor = true;
      // 
      // vollkorrekturRB
      // 
      this.vollkorrekturRB.AutoSize = true;
      this.vollkorrekturRB.Enabled = false;
      this.vollkorrekturRB.Location = new System.Drawing.Point(12, 58);
      this.vollkorrekturRB.Name = "vollkorrekturRB";
      this.vollkorrekturRB.Size = new System.Drawing.Size(84, 17);
      this.vollkorrekturRB.TabIndex = 2;
      this.vollkorrekturRB.Text = "Vollkorrektur";
      this.vollkorrekturRB.UseVisualStyleBackColor = true;
      // 
      // okBtn
      // 
      this.okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.okBtn.Location = new System.Drawing.Point(12, 81);
      this.okBtn.Name = "okBtn";
      this.okBtn.Size = new System.Drawing.Size(75, 23);
      this.okBtn.TabIndex = 3;
      this.okBtn.Text = "OK";
      this.okBtn.UseVisualStyleBackColor = true;
      // 
      // cancelBtn
      // 
      this.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.cancelBtn.Location = new System.Drawing.Point(93, 81);
      this.cancelBtn.Name = "cancelBtn";
      this.cancelBtn.Size = new System.Drawing.Size(75, 23);
      this.cancelBtn.TabIndex = 4;
      this.cancelBtn.Text = "Abbrechen";
      this.cancelBtn.UseVisualStyleBackColor = true;
      // 
      // KorrekturTypeForm
      // 
      this.AcceptButton = this.okBtn;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.cancelBtn;
      this.ClientSize = new System.Drawing.Size(180, 114);
      this.Controls.Add(this.cancelBtn);
      this.Controls.Add(this.okBtn);
      this.Controls.Add(this.vollkorrekturRB);
      this.Controls.Add(this.nachkorrekturRB);
      this.Controls.Add(this.zuruecksetzenRB);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "KorrekturTypeForm";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Korrektur";
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.RadioButton zuruecksetzenRB;
    private System.Windows.Forms.RadioButton nachkorrekturRB;
    private System.Windows.Forms.RadioButton vollkorrekturRB;
    private System.Windows.Forms.Button okBtn;
    private System.Windows.Forms.Button cancelBtn;
  }
}