using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace VOIN10.PatientenSuchen
{
  public partial class PatientenSuchenForm : Form
  {
    private int year = DateTime.Today.Year;
    public int Year
    {
      get { return year; }
      set { year = value; }
    }

    public PatientenSuchenDataSet.RezepteRow Rezept
    {
      get 
      {
        if (patientenSuchenBS.Count > 0 && patientenSuchenBS.Position >= 0)
        {
          return (PatientenSuchenDataSet.RezepteRow)
            ((DataRowView)patientenSuchenBS.Current).Row;
        }
        return null;
      }
    }

    public string ConnectionString
    {
      get { return sqlConnection.ConnectionString; }
      set { sqlConnection.ConnectionString = value; }
    }

    public string SuchParam
    {
      get { return textBox1.Text; }
      set { textBox1.Text = value; }
    }

    public PatientenSuchenForm()
    {
      InitializeComponent();
    }

    private void FindPatienten()
    {
      textBox1.Text = textBox1.Text.Trim();
      try
      {
        patientenSuchenDS.Rezepte.BeginLoadData();
        patientenSuchenDS.Rezepte.Clear();
        for (int i = 0; i < 3; i++)
        {
          searchDA.SelectCommand.CommandText = "select  Name, Vorname, GebDat, " +
            "Strasse, Plz, Ort, VersNr, Status, ArztNr from [" + (year - i).ToString() +
            "_Rezepte] where Name like '%" + textBox1.Text + "%' or Vorname like '%" +
            textBox1.Text + "%'" + " GROUP BY  Name, Vorname, GebDat, Strasse, " +
            "Plz, Ort, VersNr, Status, ArztNr";
          searchDA.Fill(patientenSuchenDS.Rezepte);
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      finally
      {
        if (sqlConnection.State != ConnectionState.Closed)
          sqlConnection.Close();
        patientenSuchenDS.Rezepte.EndLoadData();
      }
      this.BeginInvoke((MethodInvoker)delegate()
      {
        resultsDGV.Focus();
      });
    }

    private void resultsDGV_MouseDoubleClick(object sender, MouseEventArgs e)
    {
      DataGridView.HitTestInfo hitTest = resultsDGV.HitTest(e.X, e.Y);
      if (patientenSuchenBS.Count > 0 && patientenSuchenBS.Position >= 0 &&
        (hitTest.Type == DataGridViewHitTestType.Cell ||
        hitTest.Type == DataGridViewHitTestType.RowHeader))
      {
        DialogResult = DialogResult.OK;
      }
    }

    private void PatientenSuchenForm_KeyDown(object sender, KeyEventArgs e)
    {
      if (textBox1.Focused && e.KeyCode == Keys.Enter)
      {
        FindPatienten();
      }
      else if (resultsDGV.Focused && patientenSuchenBS.Count > 0 &&
        patientenSuchenBS.Position >= 0 && e.KeyCode == Keys.Enter)
      {
        DialogResult = DialogResult.OK;
        e.Handled = true;
      }
    }
  }
}