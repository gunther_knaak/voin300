﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Globalization;
using System.Media;
using System.Collections;
using System.Diagnostics;
using System.IO;
using ListLabel;
using SwS.Data;
using NLog;

namespace VOIN10
{
   
  public partial class MainForm : Form
  {
      private static Logger logger = LogManager.GetCurrentClassLogger();

    int zuzGen = 2;
    private List<string> orte = new List<string>();
    private string lastIKConnectionString = "Data Source=.\\SQLEXPRESS;AttachDbFilename=C:\\660310038.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True";
    private string stamm4ConnectionString = "Data Source=.\\SQLEXPRESS;AttachDbFilename=C:\\stamm4.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True";
    private bool currentChanging = false;
    private bool rezeptValidating = true;
    private uint lastIK = 660310038;
    private string dbServer = string.Empty;
    private string mandantenPath = string.Empty;
    private bool initialization = false;
    private string abrechnungsJahr = "2006";
    private bool error = false;
    private string versorgungText = "";
    private string strScanOrdner = "";
    private int flag = 0;
    private bool f6pressed = false;
    private string tempRezeptDatum;
    private string temparztNummerTB;
    private string tempvkBisTB;
    private bool newstart = true;
   
      

    public string AbrechnungsJahr
    {
      get
      {
        return abrechnungsJahr;
      }
      set
      {
        if (abrechnungsJahr != value)
        {
          SetAbrechnungsJahr(value);
        }
      }
    }

    private void SetAbrechnungsJahrStart(string value)
    {
      if (rezepteBindingSource.Count == 0 || (ValidatePosition() && ValidateRezept(false)))
      {
        UpdateDataSet(false);
        if (value == jahr1MenuItem.Text)
        {
          jahr1MenuItem.Checked = true;
          jahr2MenuItem.Checked = false;
        }
        else
        {
          jahr2MenuItem.Checked = true;
          jahr1MenuItem.Checked = false;
        }
        positionenDA.SelectCommand.CommandText =
          positionenDA.SelectCommand.CommandText.Replace(AbrechnungsJahr +
          "_Positionen", value + "_Positionen");
        positionenDA.InsertCommand.CommandText =
          positionenDA.InsertCommand.CommandText.Replace(AbrechnungsJahr +
          "_Positionen", value + "_Positionen");
        positionenDA.UpdateCommand.CommandText =
          positionenDA.UpdateCommand.CommandText.Replace(AbrechnungsJahr +
          "_Positionen", value + "_Positionen");
        positionenDA.DeleteCommand.CommandText =
          positionenDA.DeleteCommand.CommandText.Replace(AbrechnungsJahr +
          "_Positionen", value + "_Positionen");

        rezepteDA.SelectCommand.CommandText =
          rezepteDA.SelectCommand.CommandText.Replace(AbrechnungsJahr +
          "_Rezepte", value + "_Rezepte");
        rezepteDA.InsertCommand.CommandText =
          rezepteDA.InsertCommand.CommandText.Replace(AbrechnungsJahr +
          "_Rezepte", value + "_Rezepte");
        rezepteDA.UpdateCommand.CommandText =
          rezepteDA.UpdateCommand.CommandText.Replace(AbrechnungsJahr +
          "_Rezepte", value + "_Rezepte");
        rezepteDA.DeleteCommand.CommandText =
          rezepteDA.DeleteCommand.CommandText.Replace(AbrechnungsJahr +
          "_Rezepte", value + "_Rezepte");

        voinDataSet.AcceptChanges();
        abrechnungsJahr = value;
      }
    }

    private void SetAbrechnungsJahr(string value)
    {
      if (rezepteBindingSource.Count == 0 || (ValidatePosition() && ValidateRezept(false)))
      {
        UpdateDataSet(false);
        if (value == jahr1MenuItem.Text)
        {
          jahr1MenuItem.Checked = true;
          jahr2MenuItem.Checked = false;
        }
        else
        {
          jahr2MenuItem.Checked = true;
          jahr1MenuItem.Checked = false;
        }
        positionenDA.SelectCommand.CommandText =
          positionenDA.SelectCommand.CommandText.Replace(AbrechnungsJahr +
          "_Positionen", value + "_Positionen");
        positionenDA.InsertCommand.CommandText =
          positionenDA.InsertCommand.CommandText.Replace(AbrechnungsJahr +
          "_Positionen", value + "_Positionen");
        positionenDA.UpdateCommand.CommandText =
          positionenDA.UpdateCommand.CommandText.Replace(AbrechnungsJahr +
          "_Positionen", value + "_Positionen");
        positionenDA.DeleteCommand.CommandText =
          positionenDA.DeleteCommand.CommandText.Replace(AbrechnungsJahr +
          "_Positionen", value + "_Positionen");

        rezepteDA.SelectCommand.CommandText =
          rezepteDA.SelectCommand.CommandText.Replace(AbrechnungsJahr +
          "_Rezepte", value + "_Rezepte");
        rezepteDA.InsertCommand.CommandText =
          rezepteDA.InsertCommand.CommandText.Replace(AbrechnungsJahr +
          "_Rezepte", value + "_Rezepte");
        rezepteDA.UpdateCommand.CommandText =
          rezepteDA.UpdateCommand.CommandText.Replace(AbrechnungsJahr +
          "_Rezepte", value + "_Rezepte");
        rezepteDA.DeleteCommand.CommandText =
          rezepteDA.DeleteCommand.CommandText.Replace(AbrechnungsJahr +
          "_Rezepte", value + "_Rezepte");

        voinDataSet.AcceptChanges();
        abrechnungsJahr = value;
        UpdateDataSet(true);
        FillListBox();
        SetDataBindings();
        FillJahrList();
      }
    }

    private bool formCreated = false;

    public MainForm()
    {
        ///MessageBox.Show(ModuloCheck("01798968").ToString());

      using (StartForm form = new StartForm())
      {
        form.Show();
        Application.DoEvents();

        InitializeComponent();

        voinDataSet.Rezepte.ColumnChanged += new DataColumnChangeEventHandler(Rezepte_ColumnChanged);
        voinDataSet.Rezepte.RowDeleting += new DataRowChangeEventHandler(Rezepte_RowDeleting);
        voinDataSet.Rezepte.TableNewRow += new DataTableNewRowEventHandler(Rezepte_TableNewRow);
        voinDataSet.Rezepte.ColumnChanging += new DataColumnChangeEventHandler(Rezepte_ColumnChanging);
        voinDataSet.Positionen.TableNewRow += new DataTableNewRowEventHandler(Positionen_TableNewRow);

        Initialize();
      }
      formCreated = true;
    }

    void Positionen_TableNewRow(object sender, DataTableNewRowEventArgs e)
    {
      e.Row["Verbrauch"] = (verbrauchLabel.Visible ? (short)1 : (short)0);
      e.Row["Tarifkennzeichen"] = tarifSchlusselAbrechnCodeTB.Text;
    }

    private void SelectIni()
    {
      folderBrowserDialog1.SelectedPath = VOIN10.Properties.Settings.Default.IniPath;
      if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
      {
        if (VOIN10.Properties.Settings.Default.IniPath !=
          folderBrowserDialog1.SelectedPath)
        {
          VOIN10.Properties.Settings.Default.IniPath =
            folderBrowserDialog1.SelectedPath;
        }
      }
    }

        /// <summary>
        /// Baut die Listbox mit den Bilddateien aus dem ScanOrdner Pfad in der Ini auf
        /// </summary>
        private void UpdateImageList()
        {
            lstImages.Items.Clear();
            try
            {
                using (DataSetImages ds = new DataSetImages())
                {
                    if (strScanOrdner.Length > 0)
                    {
                        if (Directory.Exists(strScanOrdner))
                        {
                            string[] fileEntries = Directory.GetFiles(strScanOrdner, "*.tif");
                            foreach (string fileName in fileEntries)
                            {
                                
                                FileInfo fi = new FileInfo(fileName);
                              
                                ds.Images.AddImagesRow(Path.GetFileName(fileName), fi.CreationTime);
                            }

                            fileEntries = Directory.GetFiles(strScanOrdner, "*.jpg");
                            foreach (string fileName in fileEntries)
                            {
                                FileInfo fi = new FileInfo(fileName);
                                ds.Images.AddImagesRow(Path.GetFileName(fileName), fi.CreationTime);
                            }

                            DataRow[] rows = ds.Images.Select("", "Datum DESC");
                            if (rows != null && rows.Length > 0)
                            {
                                foreach (DataRow row in rows)
                                    lstImages.Items.Add(row["Name"].ToString());
                            }
                            

                        }
                        else
                            lstImages.Enabled = false;
                    }


                }

                /*if (strScanOrdner.Length > 0)
                {
                    if (Directory.Exists(strScanOrdner))
                    {
                        string[] fileEntries = Directory.GetFiles(strScanOrdner);
                        foreach (string fileName in fileEntries)
                        lstImages.Items.Add(Path.GetFileName(fileName));
                    }
                    else
                        lstImages.Enabled = false;
                }*/
            }
            catch (Exception ex)
            {
                lstImages.Enabled = false;
                //MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


    private void Initialize()
    {
      initialization = true;
      mwstCB.DataSource = MehrWertSteuer.MWStList;
      mwstCB.DisplayMember = "MWStName";
      mwstCB.ValueMember = "MWStCode";
      mwstCB.SelectedValue = (short)1;
      initialization = false;
      hilfsMittelKennZeichenCB.DataSource = HilfsMittel.HilfsMittelList;
      hilfsMittelKennZeichenCB.DisplayMember = "Name";
      hilfsMittelKennZeichenCB.ValueMember = "HilfsMittel_KZ";

      if (VOIN10.Properties.Settings.Default.IniPath == string.Empty)
      {
        VOIN10.Properties.Settings.Default.IniPath = Application.StartupPath;
        SelectIni();
      }
      StringBuilder dbStr = new StringBuilder(256);
      StringBuilder uidStr = new StringBuilder(256);
      StringBuilder pwdStr = new StringBuilder(256);
      StringBuilder mPath = new StringBuilder(256);
      StringBuilder stbScanOrdner = new StringBuilder(256);

      // dp 08-06-2010
      Utils.GetPrivateProfileString("Olav4", "ScanOrdner", "", stbScanOrdner, 256,
        Path.Combine(VOIN10.Properties.Settings.Default.IniPath, "Olav4.ini"));
      strScanOrdner = stbScanOrdner.ToString();
      if (strScanOrdner == string.Empty)
      {
        Utils.GetPrivateProfileString("Olav4", "ScannOrdner", "", stbScanOrdner, 256,
          Path.Combine(VOIN10.Properties.Settings.Default.IniPath, "Olav4.ini"));
        strScanOrdner = stbScanOrdner.ToString();
      }

      Utils.GetPrivateProfileString("Olav4", "DBServer", "SWS2", dbStr, 256,
        Path.Combine(VOIN10.Properties.Settings.Default.IniPath, "Olav4.ini"));
      dbServer = dbStr.ToString();
      Utils.GetPrivateProfileString("Olav4", "DBUID", "sa", uidStr, 256,
        Path.Combine(VOIN10.Properties.Settings.Default.IniPath, "Olav4.ini"));
      Utils.GetPrivateProfileString("Olav4", "DBPWD", "SWS", pwdStr, 256,
      Path.Combine(VOIN10.Properties.Settings.Default.IniPath, "Olav4.ini"));
   
      lastIK = Utils.GetPrivateProfileInt("Olav4", "LastIK", 660310038,
        Path.Combine(VOIN10.Properties.Settings.Default.IniPath, "Olav4.ini"));

      Utils.GetPrivateProfileString("Pfade", "Mandanten",
        "H:\\olav10\\660310038\\Mandanten", mPath, 256,
        Path.Combine(VOIN10.Properties.Settings.Default.IniPath, lastIK.ToString() + ".ini"));
      mandantenPath = mPath.ToString();

      stamm4ConnectionString = "Data Source=" + dbStr.ToString() + ";" +
        "Database=Stamm4;" + "User ID=" + uidStr.ToString() + ";" + "Password=" + pwdStr.ToString() + ";";

      lastIKConnectionString = "Data Source=" + dbStr.ToString() + ";" +
        "Database=" + lastIK.ToString() + ";" + "User ID=" + uidStr.ToString() + ";" + "Password=" + pwdStr.ToString() + ";";

      lastIKsqlConnection.ConnectionString = lastIKConnectionString;
      positionenSqlConnection.ConnectionString = lastIKConnectionString;

      mandantenBesSqlConnection.ConnectionString = stamm4ConnectionString;
      stamm4SqlConnection.ConnectionString = stamm4ConnectionString;

      geburtsDatumMTB.TextChanged += new EventHandler(maskedTextBox_TextChanged);
      rezeptDatumMTB.TextChanged += new EventHandler(maskedTextBox_TextChanged);

      uint abrYear = Utils.GetPrivateProfileInt("Olav4", "AbrechnungsJahr",
        DateTime.Today.Year, Path.Combine(VOIN10.Properties.Settings.Default.IniPath,
        lastIK.ToString() + ".ini"));
      jahr2MenuItem.Text = abrYear.ToString();
      jahr1MenuItem.Text = (abrYear - 1).ToString();

      this.SetAbrechnungsJahrStart(abrYear.ToString());
      SetAppTitel();
      FillDataSet();

      FillJahrList();
      //voinDataSet._2006_Positionen.ColumnChanging += new DataColumnChangeEventHandler(_2006_Positionen_ColumnChanging);
     
      //AbrechnungsJahr = abrYear.ToString();
      PrepeateToStart();

      UpdateImageList();
    }

    //void _2006_Positionen_ColumnChanging(object sender, DataColumnChangeEventArgs e)
    //{
    //  MessageBox.Show(e.Column.ColumnName + " " + e.ProposedValue.ToString() + " " + e.Row[e.Column].ToString());
    //}

    void Rezepte_ColumnChanging(object sender, DataColumnChangeEventArgs e)
    {
      //if (e.Column.ColumnName == "ID")
      //{
      //  foreach (Rezept r in namesListBox.Items)
      //    if (r.ID == (int)e.Row["ID"])
      //    {
      //      r.ID = (int)e.ProposedValue;
      //      break;
      //    }
      //}
    }

    private void FillDataSet()
    {      
      leitZahlenDA.Fill(voinDataSet.Leitzahlen);
      kostentraegerDA.Fill(voinDataSet.Kostentraeger_300);
      kostentraeder_TarifeDA.Fill(voinDataSet.Kostentraeger_Tarife);
      tab_PositionenDA.Fill(voinDataSet.Tab_Positionen);
      mandantenDA.Fill(voinDataSet.Mandanten);
      mandantenBesDA.Fill(voinDataSet.Mandanten_Bes);
      gruppeDA.Fill(voinDataSet._10_GRUPPE);
      ortDA.Fill(voinDataSet._10_ORT);
      unterGruppeDA.Fill(voinDataSet._10_UNTER);
      artDA.Fill(voinDataSet._10_ART);
      produktDA.Fill(voinDataSet._10_PRODUKT);
      tab_HMPBezeichnungDA.Fill(voinDataSet.tab_HMPBezeichnung);
      tabprimKKassenDA.Fill(voinDataSet.Tab_primKKassen);
      kostenTraegerDTADA.Fill(voinDataSet.Kostentraeger_dta);
      
      voinDataSet.DefaultViewManager.DataViewSettings["Rezepte"].Sort = "Name, Vorname";
      voinDataSet.DefaultViewManager.DataViewSettings["Positionen"].Sort = "BehDat";
    }

    void Rezepte_TableNewRow(object sender, DataTableNewRowEventArgs e)
    {
      if (mandantenBindingSource.Count > 0 && mandantenBindingSource.Current != null)
      {
        DataRowView drv = (DataRowView)mandantenBindingSource.Current;
        string tkz = (string)drv["Tarifkennzeichen"];
        if (tkz.Length >= 2)
          e.Row["AbrCode"] = tkz.Substring(0, 2);
        if (tkz.Length > 2)
          e.Row["Tarif"] = tkz.Substring(2, tkz.Length - 2);
      }
      InsertIntoNamesList(e.Row);
      bool r = TestIK();
      if (pnlRezeptEingabe.Enabled != r)
      {
        positionenPanel.Enabled = erfasstTB.Enabled = pnlRezeptEingabe.Enabled = r;
      }
    }

    private void InsertIntoNamesList(DataRow row)
    {
      //namesListBox.BeginUpdate();
      //namesListBox.Items.Add(new Rezept(string.Empty, string.Empty, (int)row["ID"]));
      //namesListBox.EndUpdate();
    }

    void Rezepte_RowDeleting(object sender, DataRowChangeEventArgs e)
    {
      DeleteFromNamesList((int)e.Row["ID"]);
    }

    private void DeleteFromNamesList(int id)
    {
      //foreach (Rezept r in namesListBox.Items)
      //  if (r.ID == id)
      //  {
      //    namesListBox.BeginUpdate();
      //    namesListBox.Items.Remove(r);
      //    namesListBox.EndUpdate();
      //    break;
      //  }

    }

    void Rezepte_ColumnChanged(object sender, DataColumnChangeEventArgs e)
    {
      if (e.Column.ColumnName == "Name" || e.Column.ColumnName == "Vorname")
        UpdateNamesList(e.Column.ColumnName, (int)e.Row["ID"], (string)e.ProposedValue);
    }

    private void UpdateNamesList(string columnName, int id, string value)
    {
      //foreach (Rezept r in namesListBox.Items)
      //  if (r.ID == id)
      //  {
      //    bool selected = false;
      //    if (columnName == "Name")
      //      r.Nachname = value;
      //    else
      //      r.Vorname = value;
      //    namesListBox.BeginUpdate();
      //    selected = namesListBox.SelectedItems.Contains(r);
      //    namesListBox.Items.Remove(r);
      //    namesListBox.Items.Add(r);
      //    if (selected) namesListBox.SelectedItems.Add(r);
      //    namesListBox.EndUpdate();
      //    break;
      //  }
    }

    private void SetAppTitel()
    {
      this.Text = lastIK + "/" + AbrechnungsJahr + " Server " + dbServer + " " +
        " Version " + Application.ProductVersion;
    }

    private void PrepeateToStart()
    {
      protokollBtn.Enabled = abrechnenButton.Enabled = true;
      nrComboBox.Enabled = true;
      pnlKostenTaeger.Enabled = true;
      neuRechTSBtn.Enabled = true;
      FillListBox();
      SetDataBindings();
      nrComboBox.Focus();
    }

    private void SaveCurrent(bool savePosition)
    {
      if (rezepteBindingSource.Count > 0 && rezepteBindingSource.Current != null)
      {
        DataRowView drv = (DataRowView)rezepteBindingSource.Current;
        DataRow row = drv.Row;
        if (drv.IsEdit || drv.IsNew)
        {

          // dp 07-06-2010
          // scanordner aus der ini zufügen
          imgTB.Text = imgTB.Text.Trim();
          if (imgTB.Text != string.Empty)
          {
            string strFile = imgTB.Text;
            if (strFile.IndexOf("\\") < 0)
            {
              //imgTB.DataBindings.Clear();
              drv["Image"] = Path.Combine(strScanOrdner, strFile);
              drv["Vorderseite"] = Path.Combine(strScanOrdner, strFile);
                //empty.tif
              drv["Hinterseite"] = Path.Combine(strScanOrdner, "empty.tif");
              // drv["Vorderseite"] = Path.Combine(strScanOrdner, strFile);
            }
          } // if

          drv["Name"] = "N";
          drv["Vorname"] = "N";

          rezepteBindingSource.EndEdit();

          /*if (imgTB.DataBindings.Count == 0)
            imgTB.DataBindings.Add("Text", rezepteBindingSource, "Image");*/
          
          if (savePosition && row.RowState != DataRowState.Detached)
           {
            int index = rezepteBindingSource.Find("ID", row["ID"]);
            if (index >= 0)
              rezepteBindingSource.Position = index;
          }
        }
      }
    }

    private void SetDataBindings()
    {
      SetAppTitel();
      if (nrComboBox.Enabled)
      {
        nrComboBox.DataSource = mandantenBindingSource;
        nrComboBox.DisplayMember = "Mandanten.Nr";
        name1Label.DataBindings.Clear();
        name1Label.DataBindings.Add("Text", mandantenBindingSource, "Name1");
        name2Label.DataBindings.Clear();
        name2Label.DataBindings.Add("Text", mandantenBindingSource, "Name2");

        (rezepteBindingSource as ISupportInitialize).BeginInit();
        if (rezepteBindingSource.DataMember != null) rezepteBindingSource.DataSource = null;
        rezepteBindingSource.DataMember = "FK_Mandanten_Rezepte";
        rezepteBindingSource.DataSource = mandantenBindingSource;
        (rezepteBindingSource as ISupportInitialize).EndInit();
        rezepteBindingNavigator.Enabled = true;

        imageTB.DataBindings.Clear();
        imageTB.Text = string.Empty;
        imageTB.DataBindings.Add("Text", rezepteBindingSource, "KD_Auftrag");
        

        rechNrTB.DataBindings.Clear();
        rechNrTB.Text = string.Empty;
        rechNrTB.DataBindings.Add("Text", rezepteBindingSource, "KD_RechNr");

        rechTB.DataBindings.Clear();
        rechTB.Text = string.Empty;
        rechTB.DataBindings.Add("Text", rezepteBindingSource, "KD_RechNr");

        nameTB.DataBindings.Clear();
        nameTB.Text = string.Empty;
        nameTB.DataBindings.Add("Text", rezepteBindingSource, "Name");

        vornameTB.DataBindings.Clear();
        vornameTB.Text = string.Empty;
        vornameTB.DataBindings.Add("Text", rezepteBindingSource, "Vorname");

        versichertenNummerMTB.DataBindings.Clear();
        versichertenNummerMTB.Text = string.Empty;
        versichertenNummerMTB.DataBindings.Add("Text", rezepteBindingSource, "VersNr");

        geburtsDatumMTB.DataBindings.Clear();
        geburtsDatumMTB.Text = string.Empty;
        geburtsDatumMTB.DataBindings.Add("Text", rezepteBindingSource, "GebDat");

        strasseTB.DataBindings.Clear();
        strasseTB.Text = string.Empty;
        strasseTB.DataBindings.Add("Text", rezepteBindingSource, "Strasse");

        plzMTB.DataBindings.Clear();
        plzMTB.Text = string.Empty;
        plzMTB.DataBindings.Add("Text", rezepteBindingSource, "PLZ");

        ortTB.DataBindings.Clear();
        ortTB.Text = string.Empty;
        ortTB.DataBindings.Add("Text", rezepteBindingSource, "Ort");

        indikationTB.DataBindings.Clear();
        indikationTB.Text = string.Empty;
        indikationTB.DataBindings.Add("Text", rezepteBindingSource, "Indikation");

        vkBisTB.DataBindings.Clear();
        vkBisTB.Text = string.Empty;
        vkBisTB.DataBindings.Add("Text", rezepteBindingSource, "ArztNr");
        tempvkBisTB = vkBisTB.Text;

        befreitCheckBox.DataBindings.Clear();
        befreitCheckBox.Checked = true;
        befreitCheckBox.DataBindings.Add("Checked", rezepteBindingSource, "Befreit");

        arztNummerTB.DataBindings.Clear();
        arztNummerTB.Text = string.Empty;
        arztNummerTB.DataBindings.Add("Text", rezepteBindingSource, "Betriebsstaettennummer");
        temparztNummerTB = arztNummerTB.Text;

        Binding bng = new Binding("Text", rezepteBindingSource, "Genehmigung_Datum");
        bng.Format += new ConvertEventHandler(bng_Format);
        bng.Parse += new ConvertEventHandler(bng_Parse);
        genVonMTB.DataBindings.Clear();
        genVonMTB.Text = string.Empty;
        genVonMTB.DataBindings.Add(bng);

        bng = new Binding("Text", rezepteBindingSource, "Erfasst");
        bng.Format += new ConvertEventHandler(bng_Format);
        bng.Parse += new ConvertEventHandler(bng_Parse);
        erfasstTB.DataBindings.Clear();
        erfasstTB.Text = string.Empty;
        erfasstTB.DataBindings.Add(bng);

        genKennzTB.DataBindings.Clear();
        genKennzTB.Text = string.Empty;
        genKennzTB.DataBindings.Add("Text", rezepteBindingSource, "Genehmigung_KZ");

        rezeptDatumMTB.DataBindings.Clear();
        rezeptDatumMTB.Text = string.Empty;
        rezeptDatumMTB.DataBindings.Add("Text", rezepteBindingSource, "RezDat");


        rezeptArtCB.DataBindings.Clear();
        rezeptArtCB.DataSource = RezeptzArt.RezeptArtList;
        rezeptArtCB.ValueMember = "RezeptArt";
        rezeptArtCB.DisplayMember = "RezeptArtName";
        rezeptArtCB.DataBindings.Add("SelectedValue", rezepteBindingSource, "RezArt");

        namesDGV.DataSource = rezepteBindingSource;
        foreach (DataGridViewColumn c in namesDGV.Columns)
          if (c != NameVorname)
            c.Visible = false;
        CurrencyManager bm = rezepteBindingSource.CurrencyManager;
        //bm.CurrentChanged += new EventHandler(bm_CurrentChanged);
        sortCB.SelectedIndex = 0;
        bm_CurrentChanged(bm, EventArgs.Empty);
      }
    }

    void bm_CurrentChanged(object sender, EventArgs e)
    {
      //MessageBox.Show("bm_CurrentChanged" + " " +
      //  ((rezepteBindingSource.Current as DataRowView).Row["Name"] == DBNull.Value ? "" :
      //  (string)(rezepteBindingSource.Current as DataRowView).Row["Name"]));
      if (!currentChanging)
        try
        {
          currentChanging = true;
          neuePositionBtn.Text = "&Neu";
          positionenDGV.DataSource = null;
          BuildPositionenDGVColumns();
          ((ISupportInitialize)positionenBindingSource).BeginInit();
          positionenBindingSource.DataSource = null;
          positionenBindingSource.DataMember = string.Empty;
          ((ISupportInitialize)positionenBindingSource).EndInit();
          status1TB.Text = string.Empty;
          status2TB.Text = string.Empty;
          BindingManagerBase bm = sender as BindingManagerBase;
          positionenPanel.Enabled = erfasstTB.Enabled = pnlRezeptEingabe.Enabled = bm.Count > 0 && TestIK();
          int position = bm.Position;
          UpdateDataSet(false);
          if (bm.Position != position) bm.Position = position;
          if (bm.Count > 0 && bm.Current != null)
          {
            DataRowView drv = (DataRowView)bm.Current;
            if (drv["ArztNr"] != DBNull.Value)
            {
              string s = ConvertToNumber(drv["ArztNr"].ToString());
              if ((string)drv["ArztNr"] != s)
              {
                drv["ArztNr"] = s;
                if (arztNummerTB.DataBindings.Count > 0)
                  arztNummerTB.DataBindings[0].ReadValue();
              }
            }
            if (drv["VersNr"] != DBNull.Value)
            {
              string s = ConvertToNumber(drv["VersNr"].ToString());
              if ((string)drv["VersNr"] != s)
              {
                drv["VersNr"] = s;
                if (versichertenNummerMTB.DataBindings.Count > 0)
                  versichertenNummerMTB.DataBindings[0].ReadValue();
              }
            }
            string status = drv["Status"].ToString();
            if (status.Length > 0)
            {
              status1TB.Text = new String(status[0], 1);
              string s5 = new String(status[status.Length - 1], 1);
              if (s5.Length > 0 && !char.IsDigit(s5[0]))
                s5 = "1";
              if ((string)drv["Status"] != status1TB.Text + "000" + s5)
                drv["Status"] = status1TB.Text + "000" + s5;
              status2TB.Text = s5;
            }
            if (drv["IK"].ToString() == string.Empty && TestIK())
            {
                drv["IK"] = ikTB.Text;
            }
            else
                /* 
                 * Author: stem
                 * Datum: 26.11.2014
                 * Kassennummer soll stehen bleiben und selektiert werden bis auf die  10
                 * wenn F7 gedrückt wird
                 */
                //ikTB.Text = drv["IK"].ToString();

                if (drv["IK"].ToString() != "10")
                {
                    ikTB.Text = drv["IK"].ToString();
                    ikTB_TextChanged(sender, e);
                    ikTB_Validating(sender, null);
                }

                if (ikTB.Text == "")
                    ikTB.Text = drv["IK"].ToString();
                else
                    drv["IK"] = ikTB.Text;

                if (ikTB.Text.Length >= 2)
                {
                    ikTB.SelectionLength = ikTB.TextLength - 2;
                    ikTB.SelectionStart = 2;
                }
            focusPosition = false;
            LoadPositionen();
            copyTSBtn.Enabled = true;
          }
          else
            copyTSBtn.Enabled = false;
        }
        finally
        {
          focusPosition = true;
          currentChanging = false;
        }
    }

    private string ConvertToNumber(string p)
    {
      string res = string.Empty;
      for (int i = 0; i < p.Length; i++)
      {
        if (char.IsDigit(p[i]))
          res += p[i];
      }
      return res;
    }

    void bng_Parse(object sender, ConvertEventArgs e)
    {
      if (e.DesiredType != typeof(DateTime)) return;
      if ((e.Value as string) != string.Empty)
        try
        {
          e.Value = DateTime.Parse((string)e.Value);
        }
        catch
        {
          e.Value = DBNull.Value;
        }
      else
        e.Value = DBNull.Value;
    }

    void bng_Format(object sender, ConvertEventArgs e)
    {
      if (e.DesiredType != typeof(string)) return;
      if (e.Value.GetType() != typeof(DBNull))
        e.Value = ((DateTime)e.Value).ToString("dd.MM.yyyy");
      else
        e.Value = string.Empty;
    }

    private void searchBtn_Click(object sender, EventArgs e)
    {
      try
      {
        rezeptValidating = false;
        using (KostentraegerSuchenForm form = new KostentraegerSuchenForm())
        {
          form.DataSet = voinDataSet;
          if (form.ShowDialog() == DialogResult.OK)
          {
            ikTB.Text = form.IK.Trim();
            //nameLabel.Text = form.KTName;
            nameTB.Focus();
          }
        }
      }
      finally
      {
        rezeptValidating = true;
      }
    }

    private void jahrMenuItem_Click(object sender, EventArgs e)
    {
      AbrechnungsJahr = (sender as MenuItem).Text;
    }

    private void status1TB_TextChanged(object sender, EventArgs e)
    {
      TextBox box = sender as TextBox;
      if (box.Text.Length > 0)
        if (box.Text != "1" && box.Text != "3" && box.Text != "5")
        {
          MessageBox.Show("Status ist falsch", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
          box.SelectAll();
        }
        else
        {
          BindingManagerBase bm = rezepteBindingSource.CurrencyManager;
          if (bm.Count > 0)
          {
            DataRowView drv = (DataRowView)bm.Current;
            if (drv["Status"] == DBNull.Value || ((string)drv["Status"])[0] != status1TB.Text[0])
              drv["Status"] = status1TB.Text + "000" + status2TB.Text;
            if (box.Focused)
              status2TB.Focus();
          }
        }
    }

    private void status2TB_TextChanged(object sender, EventArgs e)
    {
      TextBox box = sender as TextBox;
      if (box.Text != box.Text.ToUpper())
        box.Text = box.Text.ToUpper();
      else
        if (box.Text.Length > 0)
          if (!"146789".Contains(box.Text))//MXACKLENDFSP
          {
            MessageBox.Show("Status ist falsch", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            box.SelectAll();
          }
          else
          {
            BindingManagerBase bm = rezepteBindingSource.CurrencyManager;
            if (bm.Count > 0)
            {
              DataRowView drv = (DataRowView)bm.Current;
              if (drv["Status"] == DBNull.Value || ((string)drv["Status"])[((string)drv["Status"]).Length - 1] != status2TB.Text[status2TB.Text.Length - 1])
                drv["Status"] = status1TB.Text + "000" + status2TB.Text;
              if (box.Focused)
                box.Parent.SelectNextControl(box, true, true, true, true);
            }
          }

    }

    private void status1TB_Validating(object sender, CancelEventArgs e)
    {
      TextBox box = sender as TextBox;
      if (box.Text != "1" && box.Text != "3" && box.Text != "5")
      {
        MessageBox.Show("Status ist falsch", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        box.SelectAll();
        e.Cancel = true;
      }
    }

    private void status2TB_Validating(object sender, CancelEventArgs e)
    {
      TextBox box = sender as TextBox;
      if (!"146789".Contains(box.Text) || box.Text == string.Empty)//MXACKLENDFSP
      {
        MessageBox.Show("Status ist falsch", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        box.SelectAll();
        e.Cancel = true;
      }

    }

    private void genVonTB_Validating(object sender, CancelEventArgs e)
    {
      MaskedTextBox tb = sender as MaskedTextBox;
      if (tb.Text != string.Empty && tb.Text != "  .  .")
      {
        if (tb.Text == "  .  ." + AbrechnungsJahr)
        {
          tb.Text = string.Empty;
          return;
        }
        try
        {
          DateTime.Parse(tb.Text);
        }
        catch (Exception ex)
        {
          MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
          e.Cancel = true;
        }
      }
    }

    private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
    {
      VOIN10.Properties.Settings.Default.Save();
      if (ValidatePosition() && ValidateRezept(true))
      {
        if (voinDataSet.HasChanges())
        {
          UpdateDataSet(false);
          //DialogResult res = MessageBox.Show("Sollen die Änderungen gespeichert werden?", this.Text, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
          //if (res == DialogResult.Cancel)
          //  e.Cancel = true;
          //else
          //  if (res == DialogResult.Yes)
          //  {
          //    UpdateDataSet(false);
          //  }
        }
      }
      else
        e.Cancel = true;
    }

    private void UpdateTarifSchlusselAbbrCode(bool updTKZ)
    {
      if (mandantenBindingSource.Count > 0 && mandantenBindingSource.Current != null)
      {
        DataRow[] rows = voinDataSet.Kostentraeger_Tarife.Select("IK = '" + ikTB.Text + "'");
        if (rows.Length != 0)
        {
          string ts = rows[0]["TarifSchluessel"].ToString().Substring(0, 4);
          //while (ts.Length > 0 && ts[0] == '0') ts = ts.Substring(1);
          DataRowView drv = mandantenBindingSource.Current as DataRowView;
          string newVal = ts + drv.Row["Tarifkennzeichen"];
          string oldVal = tarifSchlusselAbrechnCodeTB.Text;
          tarifSchlusselAbrechnCodeTB.Text = newVal;
          if (updTKZ)
          {
            UpdateTKZ(oldVal, newVal);
          }
        }
        else
        {
          string oldVal = tarifSchlusselAbrechnCodeTB.Text;
          tarifSchlusselAbrechnCodeTB.Text = string.Empty;
          if(updTKZ)
            UpdateTKZ(oldVal, tarifSchlusselAbrechnCodeTB.Text);
        }
      }
    }

    private void UpdateTKZ(string oldVal, string newVal)
    {
      if (rezepteBindingSource.Count > 0 && rezepteBindingSource.Position >= 0)
      {
        VOINDataSet.RezepteRow rez = (VOINDataSet.RezepteRow)
          ((DataRowView)rezepteBindingSource.Current).Row;
        VOINDataSet.PositionenRow[] prows = (VOINDataSet.PositionenRow[])
          voinDataSet.Positionen.Select("ID = " + rez.ID.ToString());
        foreach (VOINDataSet.PositionenRow pr in prows)
        {
          if (pr.IsNull("Tarifkennzeichen") || pr.Tarifkennzeichen.Trim() == oldVal.Trim())
          {
            pr.Tarifkennzeichen = newVal;
            if (positionenBindingSource.Position >= 0 && (VOINDataSet.PositionenRow)
              ((DataRowView)positionenBindingSource.Current).Row == pr)
              posTKZTB.Text = newVal;
          }
        }
      }
    }

    private void UpdateNameLabel()
    {
      nameLabel.Text = "";
      DataRow[] rows = voinDataSet.Kostentraeger_300.Select("IK = '" + ikTB.Text + "'");
      if (rows.Length != 0)
        nameLabel.Text = rows[0]["Name1"] + " " + rows[0]["Name2"] + " " + rows[0]["Name3"] + " " + rows[0]["Name4"];
    }

    private void ikTB_Validating(object sender, CancelEventArgs e)
    {
      UpdateNameLabel();
      if (ikTB.Text == "100000000" && rezepteBindingSource.Position >= 0 && !befreitCheckBox.Checked)
      {
        befreitCheckBox.Checked = !befreitCheckBox.Checked;
        befreitCheckBox_Click(befreitCheckBox, EventArgs.Empty);
      }
    }

    private bool TestIK()
    {
      DataRow[] rows = voinDataSet.Kostentraeger_300.Select("IK = '" + ikTB.Text + "'");
      bool res = rows.Length != 0;
      bindingNavigatorAddNewItem.Enabled = res;
      return res;
    }

    private void ikTB_TextChanged(object sender, EventArgs e)
    {
      UpdateNameLabel();
      UpdateTarifSchlusselAbbrCode(true);
      BindingManagerBase bm = rezepteBindingSource.CurrencyManager;
      if (TestIK())
      {
          //string hik = GetMainIK(ikTB.Text);
          //if (ikTB.Text != hik)
          //    ikTB.Text = hik;
        if (bm.Count > 0)
        {
          DataRowView drv = (DataRowView)bm.Current;
          if (drv["IK"] == DBNull.Value || ((string)drv["IK"] != ikTB.Text.Trim())) drv["IK"] = ikTB.Text.Trim();
        }
        positionenPanel.Enabled = erfasstTB.Enabled = pnlRezeptEingabe.Enabled = bm.Count > 0;
        nameTB.Focus();

      }
      else
      {
        ikTB.Focus();
        positionenPanel.Enabled = erfasstTB.Enabled = pnlRezeptEingabe.Enabled = false;
      }

    }

    private string GetMainIK(string ik)
    {
      string res = ik.Trim();
      VOINDataSet.Kostentraeger_dtaRow[] rows = (VOINDataSet.Kostentraeger_dtaRow[])
        voinDataSet.Kostentraeger_dta.Select("ik = '" + ik.Trim() + "'");
      if (rows.Length > 0)
      {
        string fkt_ik = (rows[0].Isfkt_ikNull() ? string.Empty : rows[0].fkt_ik.Trim());
        if(fkt_ik != string.Empty && fkt_ik != ik)
          res = GetMainIK(fkt_ik);
      }
      return res;
    }

    private void FillListBox()
    {
      //namesListBox.Items.Clear();
      //if (nrComboBox.Enabled && mandantenBindingSource.Count > 0 && mandantenBindingSource.Current != null)
      //{
      //  DataRowView current = mandantenBindingSource.Current as DataRowView;
      //  if (current != null)
      //  {
      //    DataRow[] rows = current.Row.GetChildRows("FK_Mandanten_Rezepte");
      //    foreach (DataRow row in rows)
      //      namesListBox.Items.Add(new Rezept(row["Name"] == DBNull.Value ? string.Empty : (string)row["Name"],
      //        row["Vorname"] == DBNull.Value ? string.Empty : (string)row["Vorname"], (int)row["ID"]));
      //  }
      //}
    }

    private void namesListBox_SelectedIndexChanged(object sender, EventArgs e)
    {
      //if (namesListBox.SelectedIndex != -1 && rezepteBindingSource.CurrencyManager.Count != 0)
      //{
      //  int index = rezepteBindingSource.Find("ID", (namesListBox.Items[namesListBox.SelectedIndex] as Rezept).ID);
      //  rezepteBindingSource.Position = index;
      //}
    }

    private void MainForm_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.F2)
      {
        if (gruppeMTB.Enabled)
        {
          SearchPosition();
          e.Handled = true;
        }
        else if (searchBtn.Enabled && pnlSuchen.ContainsFocus)
        {
            searchBtn_Click(searchBtn, EventArgs.Empty);
            e.Handled = true;
        }
        else if (nameTB.Enabled && nameTB.ContainsFocus)
        {
          patientenSuchenBtn_Click(patientenSuchenBtn, EventArgs.Empty);
          e.Handled = true;
        }
      }
      if (e.KeyCode == Keys.F5 && befreitCheckBox.Enabled)
      {
        befreitCheckBox.Checked = !befreitCheckBox.Checked;
        befreitCheckBox_Click(befreitCheckBox, EventArgs.Empty);
        e.Handled = true;
      }
      if (e.KeyCode == Keys.F6)
      {
        bindingNavigatorAddNewItem_Click(bindingNavigatorAddNewItem, EventArgs.Empty);
        arztNummerTB.Text = temparztNummerTB;
        vkBisTB.Text = tempvkBisTB;
        befreitCheckBox.Checked = true;
        e.Handled = true;       
      }

        // MWu 13.05.2014
      if (e.KeyCode == Keys.F6)
      {

          if (f6pressed)
          {
              rezeptDatumMTB.Text = tempRezeptDatum;
             arztNummerTB.Text= temparztNummerTB;
             vkBisTB.Text = tempvkBisTB;
             befreitCheckBox.Checked = true;
          }

          else
          {
              f6pressed = true;
          }

      }
      

      if (e.KeyCode == Keys.F7)
      {
          UpdateImageList();
          f6pressed = true; //MWu
        neuRechTSBtn_Click(neuRechTSBtn, EventArgs.Empty);
        e.Handled = true;
        
        lstImages.SelectedIndex = 0;
        newstart = true;
        
        befreitCheckBox.Checked = true;
       

      }
      if (e.KeyCode == Keys.F9)
      {
        copyTSBtn_Click(copyTSBtn, EventArgs.Empty);
        e.Handled = true;
      }
      if (e.KeyCode == Keys.F10 && verbrauchBtn.Enabled)
      {
        verbrauchBtn_Click(verbrauchBtn, EventArgs.Empty);
        e.Handled = true;
      }
      if (e.KeyCode == Keys.Enter)
      {
        Control c = this;
        Control oc = null;
        while (c != null && c.HasChildren && c != oc)
        {
          oc = c;
          foreach (Control ch in c.Controls)
            if (ch.ContainsFocus)
            {
              c = ch;
              break;
            }
        }
        if (c == rechNrTB)
        {
          nameTB.Focus();
          e.Handled = true;
        }
        else if (c == tagMonatMTB)
        {
          anzahlNUD.Focus();
          e.Handled = true;
        }


        else if (anzahlNUD.ContainsFocus && anzahlNUD.Text == string.Empty && neuePositionBtn.Text == "&Abbrechen")
        {
          e.Handled = true;
          anzahlNUD.Text = anzahlNUD.Value.ToString("N0");
          neuePositionBtn.Text = "&Neu";
          if (positionenBindingSource.Count != 0 && positionenBindingSource.Current != null)
            positionenBindingSource.CancelEdit();
          nrComboBox.Focus();
          bindingNavigatorAddNewItem_Click(bindingNavigatorAddNewItem, EventArgs.Empty);
        }

        // in positionen springen, neue anlegen wenn noch keine vorhanden
        else if (c == genKennzTB || c == imgTB || c == vkBisTB || c == lstImages)
        {
          bool canceled1 = false;
          e.Handled = true;
          if (ValidateRezept(true, out canceled1) && !canceled1)
          {
            this.BeginInvoke((MethodInvoker)delegate()
            {
              if (positionenBindingSource.Count == 0)
                neuePositionBtn_Click(neuePositionBtn, EventArgs.Empty);
              else
                anzahlNUD.Focus();
            });
          }
        }



        else if (c != null)
        {
          this.SelectNextControl(c, true, true, true, true);
          e.Handled = true;
        }
      } // if (e.KeyCode == Keys.Enter)
    }

    private void StandartKKasseSuchen()
    {
      PrimKassenForm form = null;
      try
      {
        form = new PrimKassenForm();
        form.DataSet = voinDataSet;
        if (form.ShowDialog() == DialogResult.OK)
        {
          ikTB.Text = form.IK;
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      finally
      {
        if (form != null)
          form.Dispose();
      }
    }

    private void plzTB_Validating(object sender, CancelEventArgs e)
    {
      DataRow[] rows = voinDataSet.Leitzahlen.Select("Leitzahl = '" + plzMTB.Text + "'");
      orte.Clear();
      foreach (DataRow row in rows)
        orte.Add((string)row["Bezeichnung"]);
      orte.Sort();
      if (orte.Count != 0)
      {
        ortTB.Text = orte[0];
      }
    }

    private int GetIndexOfOrt(string ort)
    {
      ort = ort.ToUpper();
      for (int i = 0; i < orte.Count; i++)
        if (ort == orte[i].ToUpper())
          return i;
      return -1;
    }

    private void ortTB_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Down)
      {
        int index = GetIndexOfOrt(ortTB.Text);
        if (index != -1 && index < orte.Count - 1)
          ortTB.Text = orte[index + 1];
        else if (orte.Count > 0)
          ortTB.Text = orte[0];
        e.SuppressKeyPress = true;
      }
      if (e.KeyCode == Keys.Up)
      {
        int index = GetIndexOfOrt(ortTB.Text);
        if (index != -1 && index > 0)
          ortTB.Text = orte[index - 1];
        else if (orte.Count > 0)
          ortTB.Text = orte[orte.Count - 1];
        e.SuppressKeyPress = true;
      }
    }

    private void menuItem3_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void copyTSBtn_Click(object sender, EventArgs e)
    {
      ToolStripButton btn = sender as ToolStripButton;
      BindingNavigator nav = btn.Owner as BindingNavigator;
      bool canceled = false;
      if (btn.Enabled && nav != null && nav.BindingSource != null &&
        nav.BindingSource.AllowNew && nav.BindingSource.Current != null &&
        ValidatePosition() && ValidateRezept(true, out canceled) && canceled == false)
      {
        nav.BindingSource.EndEdit();
        DataRow oldr = (nav.BindingSource.Current as DataRowView).Row;
        UpdateDataSet(false);
        nav.BindingSource.AddNew();
        DataRow newr = (nav.BindingSource.Current as DataRowView).Row;
        for (int i = 0; i < oldr.ItemArray.Length; i++)
          if (oldr.Table.Columns[i].ColumnName != "ID" &&
            oldr.Table.Columns[i].ColumnName != "Indikation" &&
            oldr.Table.Columns[i].ColumnName != "RezArt" &&
            oldr.Table.Columns[i].ColumnName != "Status" &&
            /*oldr.Table.Columns[i].ColumnName != "RezDat" &&
            oldr.Table.Columns[i].ColumnName != "ArztNr" &&*/
            oldr.Table.Columns[i].ColumnName != "Genehmigung_Datum" &&
            oldr.Table.Columns[i].ColumnName != "Genehmigung_KZ")
            newr[i] = oldr[i];
        nav.BindingSource.ResetCurrentItem();
        ikTB.Text = (string)newr["IK"];
        indikationTB.Focus();
      }
    }

    private void neuRechTSBtn_Click(object sender, EventArgs e)
    {
      if ((sender as ToolStripButton).Enabled && ValidatePosition() && ValidateRezept(false))
      {
        int auftrag = 0;
        int rech = 0;
        if (mitRechNrCB.Checked)
        {
          if(rechNrTB.Text.Length > 0)
          {
            rech = Convert.ToInt32(rechNrTB.Text);
          }
          if(imageTB.Text.Length > 0)
            auftrag = Convert.ToInt32(imageTB.Text);
        }
        if (einzelnCB.Checked && rechTB.Text.Length > 0)
          rech = Convert.ToInt32(rechTB.Text);
        ((BindingNavigator)((ToolStripButton)(sender)).Owner).BindingSource.EndEdit();
        UpdateDataSet(false);
        ((BindingNavigator)((ToolStripButton)(sender)).Owner).BindingSource.AddNew();
        //ikTB.Text = "10";
        ikTB.Focus();
        ikTB.SelectionStart = 2;
        //ikTB.SelectionLength = 0;
        if (nameTB.DataBindings.Count > 0 && !nameTB.DataBindings[0].IsBinding)
          rezepteBindingSource.ResumeBinding();

        if (mitRechNrCB.Checked)
        {
          imageTB.Text = auftrag.ToString();
          rechNrTB.Text = rech.ToString();
        }
        if (einzelnCB.Checked)
        {
          rech++;
          rechTB.Text = rech.ToString();
        }

        tmrNextImage.Start();
      }
    }

    bool addingNew = false;

    private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
    {
      try
      {
        if ((sender as ToolStripButton).Enabled && ValidatePosition() && ValidateRezept(false))
        {
          int auftrag = 0;
          int rech = 0;
          int im = 0;
          if (mitRechNrCB.Checked && rechNrTB.Text.Length > 0)
          {
            auftrag = Convert.ToInt32(imageTB.Text);
            rech = Convert.ToInt32(rechNrTB.Text);
          }
          if (einzelnCB.Checked && rechTB.Text.Length > 0)          
            rech = Convert.ToInt32(rechTB.Text);
          
          /*if (imgCB.Checked && imgTB.Text.Length > 0)
            im = Convert.ToInt32(imgTB.Text);*/
          
          string ik = ikTB.Text;
          addingNew = true;
          string img = imageTB.Text;
          string rechNr = rechNrTB.Text;
          rezepteBindingSource.EndEdit();
          UpdateDataSet(false);
          rezepteBindingSource.AddNew();
          if (mitRechNrCB.Checked)
          {
            imageTB.Text = img;
            rechNrTB.Text = rechNr;
          }
          if (nameTB.Enabled) nameTB.Focus();
          if (nameTB.DataBindings.Count > 0 && !nameTB.DataBindings[0].IsBinding)
            rezepteBindingSource.ResumeBinding();
          if (ik != string.Empty)
          {
            ikTB.Text = ik;
            if (rezepteBindingSource.Position >= 0)
            {
              DataRowView drv = (DataRowView)rezepteBindingSource.Current;
              if (drv["IK"] == DBNull.Value || (string)drv["IK"] != ik)
                drv["IK"] = ik;
              this.BeginInvoke((MethodInvoker)delegate()
              {
                //nameTB.Focus();
                rezeptDatumMTB.Focus();
              });
            }
          }
          if (mitRechNrCB.Checked)
          {
            imageTB.Text = auftrag.ToString();
            rechNrTB.Text = rech.ToString();
          }
          if (einzelnCB.Checked)
          {
            rech++;
            rechTB.Text = rech.ToString();
          }
          
          /*if (imgCB.Checked)
          {
              im++;
              imgTB.Text = im.ToString();
          }*/

          // dp 07-06-2010
          if (rezepteBindingSource.Current != null)
          {
            DataRowView drv2 = (DataRowView)rezepteBindingSource.Current;
            drv2["Name"] = "N";
            drv2["Vorname"] = "N";
            drv2["Befreit"] = 1;
          }
        }
      }
      finally
      {
        this.BeginInvoke((MethodInvoker)delegate()
        {
          addingNew = false;
        });
      }
      tmrNextImage.Start();
    }

    private void ClearDataBindings()
    {
      rezepteBindingNavigator.Enabled = false;

      imageTB.DataBindings.Clear();

      rechNrTB.DataBindings.Clear();

      nameTB.DataBindings.Clear();

      vornameTB.DataBindings.Clear();

      versichertenNummerMTB.DataBindings.Clear();

      geburtsDatumMTB.DataBindings.Clear();

      strasseTB.DataBindings.Clear();

      plzMTB.DataBindings.Clear();

      ortTB.DataBindings.Clear();

      indikationTB.DataBindings.Clear();

      befreitCheckBox.DataBindings.Clear();

      arztNummerTB.DataBindings.Clear();

      genVonMTB.DataBindings.Clear();

      erfasstTB.DataBindings.Clear();

      genKennzTB.DataBindings.Clear();

      rezeptDatumMTB.DataBindings.Clear();


      rezeptArtCB.DataBindings.Clear();
      rezeptArtCB.DataSource = null;
      rezeptArtCB.ValueMember = string.Empty;
      rezeptArtCB.DisplayMember = string.Empty;
    }

    private void mitRechNrCB_Click(object sender, EventArgs e)
    {
      CheckBox cb = (sender as CheckBox);
      label16.Visible = label17.Visible = rechNrTB.Visible = imageTB.Visible = cb.TabStop = cb.Checked;
      if (cb.Checked)
      {
        einzelnCB.Checked = label41.Visible = rechTB.Visible = cb.TabStop = false;
        if (imageTB.Text.Length == 0)
        {
          imageTB.Text = "1";
        }
        if (rechNrTB.Text.Length == 0)
        {
          rechNrTB.Text = "1";
        }
      }
    }

    private void maskedTextBoxDate_Validating(object sender, CancelEventArgs e)
    {
      MaskedTextBox tb = sender as MaskedTextBox;
      if (tb.Focused)
        try
        {
          DateTime.Parse(tb.Text);
        }
        catch (Exception ex)
        {
          if (tb != rezeptDatumMTB)
          {
            MessageBox.Show(ex.Message + " " + tb.Text, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            e.Cancel = true;
          }
          else
          {
            if (tb.Text == "  .  ." + DateTime.Today.Year)
              tb.Text = "01.01.1900";
            else
            {
              MessageBox.Show(ex.Message + " " + tb.Text, this.Text, MessageBoxButtons.OK,
                MessageBoxIcon.Error);
              e.Cancel = true;
            }
          }
        }
    }

    private void jahrVorgabeMTB_Enter(object sender, EventArgs e)
    {
      MaskedTextBox tb = sender as MaskedTextBox;
      if (tb != null && tb.Text == "  .  .")
      {
        tb.Text = "  .  ." + AbrechnungsJahr;
        this.BeginInvoke((MethodInvoker)delegate()
        {
          tb.SelectionStart = 0;
          tb.SelectionLength = 5;
        });
      }
      else
      {
        this.BeginInvoke((MethodInvoker)delegate()
        {
          tb.SelectAll();
        });
      }
    }

    private void geburtsDatumMTB_Validated(object sender, EventArgs e)
    {
      try
      {
        DateTime gd = DateTime.Parse(geburtsDatumMTB.Text);
        DateTime rd = DateTime.Parse(rezeptDatumMTB.Text);
        if (rd.Year - gd.Year < 18)
        {
          status1TB.Text = "3";
          if (rezepteBindingSource.Count > 0)
          {
            DataRowView drv = (DataRowView)rezepteBindingSource.Current;
            if (drv["Status"] == DBNull.Value || ((string)drv["Status"])[0] != status1TB.Text[0])
              drv["Status"] = status1TB.Text + "000" + status2TB.Text;
          }
          befreitCheckBox.Checked = true;
          befreitCheckBox_Click(befreitCheckBox, EventArgs.Empty);
        }
        if (rd.Year - gd.Year == 18 && rd.Month < gd.Month)
        {
          status1TB.Text = "3";
          if (rezepteBindingSource.Count > 0)
          {
            DataRowView drv = (DataRowView)rezepteBindingSource.Current;
            if (drv["Status"] == DBNull.Value || ((string)drv["Status"])[0] != status1TB.Text[0])
              drv["Status"] = status1TB.Text + "000" + status2TB.Text;
          }
          befreitCheckBox.Checked = true;
          befreitCheckBox_Click(befreitCheckBox, EventArgs.Empty);
        }
        if (rd.Year - gd.Year == 18 && rd.Month == gd.Month && rd.Day < gd.Day)
        {
          status1TB.Text = "3";
          if (rezepteBindingSource.Count > 0)
          {
            DataRowView drv = (DataRowView)rezepteBindingSource.Current;
            if (drv["Status"] == DBNull.Value || ((string)drv["Status"])[0] != status1TB.Text[0])
              drv["Status"] = status1TB.Text + "000" + status2TB.Text;
          }
          befreitCheckBox.Checked = true;
          befreitCheckBox_Click(befreitCheckBox, EventArgs.Empty);
        }

      }
      catch
      {
      }
    }

    private void ikTB_Enter(object sender, EventArgs e)
    {
      TextBox tb = sender as TextBox;
      if (tb.Text == string.Empty)
      {
        this.BeginInvoke((MethodInvoker)delegate()
        {
          tb.Text = "10";
          tb.SelectionStart = tb.Text.Length;
          tb.SelectionLength = 0;
        });
      }
      else
      {
        this.BeginInvoke((MethodInvoker)delegate()
        {
          tb.SelectionStart = Math.Min(2, tb.Text.Length);
          tb.SelectionLength = tb.Text.Length - Math.Min(2, tb.Text.Length);
        });
      }
    }

    private void datumMTB_TextChanged(object sender, EventArgs e)
    {
      MaskedTextBox tb = sender as MaskedTextBox;
      if (tb.Text.Length == 10 && !tb.Text.Contains(" ") && tb.Focused)
        try
        {
          DateTime.Parse(tb.Text);
          tb.Parent.SelectNextControl(tb, true, true, true, true);
        }
        catch
        {
        }
    }

    private void plzMTB_TextChanged(object sender, EventArgs e)
    {
      if (plzMTB.Text.Length == 5 && plzMTB.Focused)
        plzMTB.Parent.SelectNextControl(plzMTB, true, true, true, true);
    }

    private void menuItem4_Click(object sender, EventArgs e)
    {
      if (ValidatePosition() && ValidateRezept(true))
      {
        UpdateDataSet(true);
        FillListBox();
      }
    }

    private void mandantenBindingSource_CurrentChanged(object sender, EventArgs e)
    {
      if (mandantenBindingSource.Count != 0 && mandantenBindingSource.Current != null)
      {
        DataRowView drv = mandantenBindingSource.Current as DataRowView;
        imgCB.Checked = (bool)(drv.Row["Image"].ToString().Length > 0);
        mitRechNrCB.Checked = false;
        einzelnCB.Checked = false;
        this.imgCB_Click(imgCB, null);
        this.mitRechNrCB_Click(mitRechNrCB, null);
        this.einzelnCB_Click(einzelnCB, null);
        if (rezepteDA.SelectCommand.Parameters["@Mandant"].Value == null ||
          (int)rezepteDA.SelectCommand.Parameters["@Mandant"].Value != (int)drv.Row["Nr"])
        {
          LoadMandanten((int)drv.Row["Nr"]);
          if (rezepteBindingSource.Count > 0)
            ikTB.Focus();
        }
      }
    }

    private void LoadMandanten(int nr)
    {
      rezepteDA.SelectCommand.Parameters["@Mandant"].Value = nr;
      UpdateDataSet(true);
      FillListBox();
      UpdateTarifSchlusselAbbrCode(false);
    }

    #region UpdateDataSet

    bool updating = false;

    private void UpdateDataSet(bool reload)
    {
      if (!updating)
      {
        try
        {
          updating = true;
          if (voinDataSet.HasChanges())
          {
            SqlConnection con = null;
            SqlTransaction tran = null;
            DataRow[] rows = null;
            try
            {
              con = new SqlConnection(lastIKConnectionString);
              con.Open();
              tran = con.BeginTransaction();

              rows = voinDataSet.Rezepte.Select("", "", DataViewRowState.Added);
              SqlDBUpdater.Update(rows, con, tran, rezepteDA, AbrechnungsJahr +
                "_Rezepte", "ID");

              rows = voinDataSet.Positionen.Select("", "", DataViewRowState.Added |
                DataViewRowState.Deleted | DataViewRowState.ModifiedCurrent);
              SqlDBUpdater.Update(rows, con, tran, positionenDA, AbrechnungsJahr +
                "_Posirionen", "AUTO");

              rows = voinDataSet.Rezepte.Select("", "", DataViewRowState.Deleted |
                DataViewRowState.ModifiedCurrent);
              SqlDBUpdater.Update(rows, con, tran, rezepteDA, AbrechnungsJahr +
                "_Rezepte", "ID");

              tran.Commit();
              rows = voinDataSet.Rezepte.Select("", "",
                DataViewRowState.ModifiedCurrent | DataViewRowState.Added |
                DataViewRowState.Deleted);
              for (int i = 0; i < rows.Length; i++)
                rows[i].AcceptChanges();
              //voinDataSet.Rezepte.AcceptChanges();
              rows = voinDataSet.Positionen.Select("", "",
                DataViewRowState.ModifiedCurrent | DataViewRowState.Added |
                DataViewRowState.Deleted);
              for (int i = 0; i < rows.Length; i++)
                rows[i].AcceptChanges();
              //voinDataSet.Rezepte.AcceptChanges();
              //voinDataSet.Positionen.AcceptChanges();
            }
            catch (Exception ex)
            {
              reload = true;
              MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
              try
              {
                if (tran != null)
                  tran.Rollback();
                voinDataSet.Rezepte.RejectChanges();
                voinDataSet.Positionen.RejectChanges();
              }
              catch (Exception ex1)
              {
                MessageBox.Show(ex1.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
              }
            }
            finally
            {
              if (tran != null)
                tran.Dispose();
              if (con != null)
                con.Dispose();
            }

            if (!reload && rezepteBindingSource.Count > 0 &&
              rezepteBindingSource.Position >= 0)
              bm_CurrentChanged(rezepteBindingSource.CurrencyManager, EventArgs.Empty);

          }
          if (reload)
          {
            currentChanging = true;
            //namesListBox.Items.Clear();
            voinDataSet.Positionen.BeginLoadData();
            voinDataSet.Rezepte.BeginLoadData();
            voinDataSet.Positionen.Clear();
            voinDataSet.Rezepte.Clear();
            rezepteDA.Fill(voinDataSet.Rezepte);
            voinDataSet.Rezepte.EndLoadData();
            voinDataSet.Positionen.EndLoadData();
            FillListBox();
          }
        }
        catch (Exception ex)
        {
          MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
          if (jahr1MenuItem.Checked)
          {
              error = true;
          }          
        }
        finally
        {
          currentChanging = false;
          updating = false;
          if (lastIKsqlConnection.State != ConnectionState.Closed)
            lastIKsqlConnection.Close();
          if (reload)
            rezepteBindingSource_PositionChanged(rezepteBindingSource, EventArgs.Empty);
            if (error)
            {
                error = false;
                AbrechnungsJahr = jahr2MenuItem.Text;
            }
        }

      }
    }

    private void rezepte_RowUpdated(object sender, SqlRowUpdatedEventArgs e)
    {
      if (e.RecordsAffected == 0)
      {
        e.Row.RowError = "Datensatz wurde geändert.";
        e.Status = UpdateStatus.SkipCurrentRow;
      }
    }

    #endregion

    private void befreitCheckBox_Click(object sender, EventArgs e)
    {
      if (rezepteBindingSource.Count > 0 && rezepteBindingSource.Current != null)
      {
        DataRow row = ((DataRowView)rezepteBindingSource.Current).Row;
        if (row.RowState != DataRowState.Detached && row.RowState != DataRowState.Deleted)
          rezepteBindingSource.EndEdit();
        UpdatePreis(true);
        SetBefreit();
      }
    }

    private void nrComboBox_Enter(object sender, EventArgs e)
    {
      ValidateRezept(true);
    }

    private bool ValidateRezept(bool savePosition)
    {
      bool canceled;
      return ValidateRezept(savePosition, out canceled);
    }

    private bool ValidateRezept(bool savePosition, out bool canceled)
    {
      bool res = true;
      canceled = false;

      if (rezeptValidating)
      {
          
        if (rezeptDatumMTB.Text == "  .  .")
        {
          rezeptDatumMTB.Text = "01.01.1900";
        }
      
          
        try
        {
          errorProvider.Clear();
          SaveCurrent(savePosition);

          //dpre 23.06.2010 Arztnummer in dbo.ArztBetrNr speichern
          SaveArztnummer();
        }
        catch (Exception ex)
        {
          if (MessageBox.Show(ex.Message + "\nSoll der aktuelle Bearbeitungsvorgang abgebrochen werden?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
          {
            if (rezepteBindingSource.Current != null)
            {
              canceled = true;
              DeleteFromNamesList((int)(rezepteBindingSource.Current as DataRowView).Row["id"]);
              rezepteBindingSource.CancelEdit();
            }
          }
          else
          {
            bool f = true;
            res = false;
            if (ex.Message.IndexOf("'") != -1)
            {
              string spalte = ex.Message.Substring(ex.Message.IndexOf("'") + 1, ex.Message.IndexOf("'", ex.Message.IndexOf("'") + 1) - ex.Message.IndexOf("'") - 1);
              for (int i = 0; i < pnlRezeptEingabe.Controls.Count && f; i++)
              {
                Control c = pnlRezeptEingabe.Controls[i];
                foreach (Binding b in c.DataBindings)
                {
                  if (b.BindingMemberInfo.BindingField == spalte && c.Enabled)
                  {
                    errorProvider.SetError(c, ex.Message);
                    f = false;
                    c.Focus();
                    break;
                  }
                }
              }
              if (f && spalte == "Status")
                if (status1TB.Text == string.Empty && status1TB.Enabled)
                {
                  errorProvider.SetError(status1TB, ex.Message);
                  f = false;
                  status1TB.Focus();
                }
                else if (status2TB.Text == string.Empty && status2TB.Enabled)
                {
                  errorProvider.SetError(status2TB, ex.Message);
                  f = false;
                  status2TB.Focus();
                }
            }
            if (f && nameTB.Enabled) nameTB.Focus();
            else if (f && ikTB.Enabled) ikTB.Focus();
          }
        }
      }
      tempRezeptDatum = rezeptDatumMTB.Text;
      tempvkBisTB= vkBisTB.Text;
      temparztNummerTB = arztNummerTB.Text;
      return res;
      
    }

    private void positionenPanel_Enter(object sender, EventArgs e)
    {
      if (!addingNew)
        ValidateRezept(true);
    }

    private void splitContainer1_Panel1_Enter(object sender, EventArgs e)
    {
      ValidateRezept(true);
    }

    private void rezepteBindingSource_ListChanged(object sender, ListChangedEventArgs e)
    {
      UpdateBindingNavigator(sender as BindingSource);
    }

    private void UpdateBindingNavigator(BindingSource bs)
    {
      if (bs != null)
      {
        if (bs.Count > 0)
        {
          if (bs.Position > 0)
          {
            bindingNavigatorMoveFirstItem.Enabled = true;
            bindingNavigatorMovePreviousItem.Enabled = true;
          }
          else
          {
            bindingNavigatorMoveFirstItem.Enabled = false;
            bindingNavigatorMovePreviousItem.Enabled = false;
          }
          bindingNavigatorPositionItem.Text = (bs.Position + 1).ToString();
          bindingNavigatorPositionItem.Enabled = true;
          if (bs.Position < bs.Count - 1)
          {
            bindingNavigatorMoveNextItem.Enabled = true;
            bindingNavigatorMoveLastItem.Enabled = true;
          }
          else
          {
            bindingNavigatorMoveNextItem.Enabled = false;
            bindingNavigatorMoveLastItem.Enabled = false;
          }
          bindingNavigatorDeleteItem.Enabled = true;
        }
        else
        {
          bindingNavigatorMoveFirstItem.Enabled = false;
          bindingNavigatorMovePreviousItem.Enabled = false;
          bindingNavigatorPositionItem.Enabled = false;
          bindingNavigatorPositionItem.Text = "0";
          bindingNavigatorMoveNextItem.Enabled = false;
          bindingNavigatorMoveLastItem.Enabled = false;
          bindingNavigatorDeleteItem.Enabled = false;
        }
      }
    }

    bool positionChanging = false;

    object lastRezept = null;

    private void rezepteBindingSource_PositionChanged(object sender, EventArgs e)
    {
      if (formCreated)
      {
        this.BeginInvoke((MethodInvoker)delegate()
        {
          if (!positionChanging)
            try
            {
              positionChanging = true;
              //MessageBox.Show("PositionChanged");
              object newRezept = null;
              if (rezepteBindingSource.Count > 0 && rezepteBindingSource.Position >= 0)
                newRezept = rezepteBindingSource.Current;
              if (newRezept != lastRezept)
              {
                lastRezept = newRezept;
                bm_CurrentChanged(((BindingSource)sender).CurrencyManager, e);
              }
              UpdateBindingNavigator(sender as BindingSource);
            }
            finally
            {
              positionChanging = false;
            }
        });
      }
      else
      {
        if (!positionChanging)
          try
          {
            positionChanging = true;
            //MessageBox.Show("PositionChanged");
            bm_CurrentChanged(((BindingSource)sender).CurrencyManager, e);
            UpdateBindingNavigator(sender as BindingSource);
          }
          finally
          {
            positionChanging = false;
          }
      }
    }

    private void bindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
    {
      if (ValidatePosition() && ValidateRezept(true))
      {
        rezepteBindingSource.Position = 0;
      }
    }

    private void bindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
    {
      if (ValidatePosition() && ValidateRezept(true))
      {
        rezepteBindingSource.Position--;
      }
    }

    private void bindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
    {
      if (ValidatePosition() && ValidateRezept(true))
      {
        rezepteBindingSource.Position++;
      }
    }

    private void bindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
    {
      if (ValidatePosition() && ValidateRezept(true))
      {
        rezepteBindingSource.Position = rezepteBindingSource.Count - 1;
      }
    }

    private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
    {
      bool firePositionChanged = false;
      if (rezepteBindingSource.Count > 0 && rezepteBindingSource.Current != null &&
        rezepteBindingSource.Position != rezepteBindingSource.Count - 1)
        firePositionChanged = true;
      if (rezepteBindingSource.Count > 0 && rezepteBindingSource.Current != null &&
        (rezepteBindingSource.Current as DataRowView).Row.RowState == DataRowState.Detached)
      {
        errorProvider.Clear();
        DeleteFromNamesList((int)(rezepteBindingSource.Current as DataRowView).Row["id"]);
        rezepteBindingSource.CancelEdit();
      }
      else if (ValidatePosition() && ValidateRezept(true))
      {
        errorProvider.Clear();
        rezepteBindingSource.RemoveCurrent();
      }
      if (firePositionChanged)
        rezepteBindingSource_PositionChanged(rezepteBindingSource, EventArgs.Empty);
    }

    private void bindingNavigatorPositionItem_Validating(object sender, CancelEventArgs e)
    {
      try
      {
        int pos = int.Parse(bindingNavigatorPositionItem.Text) - 1;
        if (rezepteBindingSource.Position != pos && ValidatePosition() && ValidateRezept(true))
          rezepteBindingSource.Position = pos;
      }
      catch
      {
      }
      bindingNavigatorPositionItem.Text = (rezepteBindingSource.Position + 1).ToString();
    }

    private void abrechnenButton_Click(object sender, EventArgs e)
    {
      if (namesDGV.SelectedRows.Count > 0 && ValidateRezept(false) &&
        MessageBox.Show("Sollen die ausgewählten Rezepte abgerechnet werden?", this.Text,
        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
      {
        //foreach (Rezept rezept in namesListBox.SelectedItems)
        //{
        //  DataRow[] rows = voinDataSet.Rezepte.Select("ID = " + rezept.ID.ToString());
        //  if (rows.Length > 0 && rows[0].RowState != DataRowState.Deleted &&
        //    rows[0].RowState != DataRowState.Detached)
        //    rows[0]["Vorgang"] = 0;
        //}
       foreach (DataGridViewRow row in namesDGV.SelectedRows)
        {
          VOINDataSet.RezepteRow r = (VOINDataSet.RezepteRow)
            ((DataRowView)row.DataBoundItem).Row;
          if (r.RowState != DataRowState.Detached && r.RowState != DataRowState.Deleted)
            r.Vorgang = 0;

        } 
        UpdateDataSet(true);
      }
    }

    private void BesonderheitenBtn_Click(object sender, EventArgs e)
    {
      if (mandantenBindingSource.Count > 0 && mandantenBindingSource.Current != null)
        using (BesonderheitenForm form = new BesonderheitenForm())
        {
          form.Mandant = ((DataRowView)mandantenBindingSource.Current).Row;
          form.DataSet = voinDataSet;
          form.DataAdapter = mandantenBesDA;
          form.ShowDialog();
        }
    }

    private void maskedTextBox_TextChanged(object sender, EventArgs e)
    {
      //  back:
      MaskedTextBox mtb = sender as MaskedTextBox;
      int selStart = mtb.SelectionStart;
      int selLen = mtb.SelectionLength;
      mtb.Text = mtb.Lines[0];
      mtb.SelectionStart = selStart;
      mtb.SelectionLength = selLen;
    }

    private void positionsnummerMTBs_Validating(object sender, CancelEventArgs e)
    {
        MaskedTextBox mtb = (MaskedTextBox)sender;

        if (mtb.Text == string.Empty)
        {
            DataRowView drv = mandantenBindingSource.Current as DataRowView;

            if (drv.Row["PosDummy"].ToString() != "")
            {
                mtb.Text = drv.Row["PosDummy"].ToString();
            }
            else
            {
                mtb.Focus();
                MessageBox.Show("Positionsnummer is empty");
                Console.Beep(5000, 4000);
                flag = 1;
            }
        }
        else
            flag = 0;

        //hhh 13/03/2014

        if (flag != 1)
        {
            mtb.Text.Replace(" ", string.Empty);
            if (string.IsNullOrWhiteSpace(mtb.Text))
                MessageBox.Show("Write Correct Value");
            else
            {
                if (mtb.Text.Length == 8)
                {

                    if (ModuloCheck(mtb.Text))
                        UpdatePositionsnummer((MaskedTextBox)sender);
                    else
                    {
                        MessageBox.Show("Write Correct Value");
                        mtb.Text = string.Empty;
                        mtb.Focus();
                        for (int i = 0; i < 10; i++)
                        {
                            Console.Beep();
                        }

                    }
                }
                else
                {
                    MessageBox.Show("Length Must be 8 digit");
                    mtb.Text = string.Empty;
                    mtb.Focus();

                }
            }
        }
    }

    private void UpdatePositionsnummer(MaskedTextBox mtb)
    {
        
      DataRow[] rows = new DataRow[0];
      while (mtb.Text.Length < mtb.Mask.Length && mtb.Text.Length != 0)
        mtb.Text = "0" + mtb.Text;
      if (mtb == gruppeMTB)
      {
        gruppeLabel.Text = string.Empty;
        if (gruppeMTB.Text != string.Empty)
        {
          rows = voinDataSet._10_GRUPPE.Select("GRUPPE = " + gruppeMTB.Text);
          if (rows.Length != 0)
            gruppeLabel.Text = (string)rows[0]["Bezeichnung"];
        }
      }
    
    }

    private void SearchPosition()
    {
      if (mandantenBindingSource.Count > 0 && mandantenBindingSource.Current != null)
        using (PositionsSearchForm form = new PositionsSearchForm())
        {
          form.DataSet = voinDataSet;
          form.Mandant = (int)((DataRowView)mandantenBindingSource.Current).Row["Nr"];
          form.TarifKZ = tarifSchlusselAbrechnCodeTB.Text;
          if (form.ShowDialog(this) == DialogResult.OK)
            if (form.HMP != string.Empty && form.SelTarifKZ != string.Empty)
            {
              string hmp = form.HMP;
              gruppeMTB.Text = Substring(hmp, 0, 2); // subhi Substring(hmp, 0, 2)
              string tkz = form.SelTarifKZ.Trim();
              if (tkz.Length == 11)
              {
                tkz = tkz.Substring(7, 4) + tkz.Substring(0, 7);
                posTKZTB.Text = tkz;
                if (positionenBindingSource.Count > 0 && positionenBindingSource.Position >= 0)
                {
                  DataRow r = ((DataRowView)positionenBindingSource.Current).Row;
                  r["Tarifkennzeichen"] = tkz;
                }
              }
              else
                MessageBox.Show("Tarifkennzeichen " + tkz + " hat das falsche Format.",
                  this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
              if (form.EPreis != DBNull.Value)
              {
                decimal p = (decimal)form.EPreis;
                ePreisTB.Text = p.ToString("N2");
                ePreisTB.Focus();
              }
              UpdatePositionsnummer(gruppeMTB);
           
            }
        }
    }

    private string Substring(string str, int pos, int len)
    {
      string res = string.Empty;
      if (str != null && str.Length > pos)
      {
        int l = str.Length >= pos + len ? len : str.Length - pos;
        res = str.Substring(pos, l);
      }
      return res;
    }

    private void positionenMTBs_TextChanged(object sender, EventArgs e)
    {
      MaskedTextBox mtb = (MaskedTextBox)sender;
      if (mtb.Text.Length == mtb.Mask.Length && mtb.Focused)
        mtb.Parent.SelectNextControl(mtb, true, true, true, true);
    }

    private void rechnerBtn_Click(object sender, EventArgs e)
    {
      Process p = new Process();
      try
      {
        p.StartInfo.FileName = "calc.exe";
        p.Start();
      }
      catch (Win32Exception ex)
      {
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void UpdatePreis(bool calcZuzalung)
    {
      decimal preis;
      decimal anteil;
      decimal st;
      decimal forderung;
      decimal zuzahlung;
      MehrWertSteuer mwst;
      bool befreit = true;
      DataRow currentRezept = null;
      short zuzahlungsArt = 0;
      bool verbrauch = false;

      if (positionenBindingSource.Count > 0 && positionenBindingSource.Current != null)
      {
        DataRow row = ((DataRowView)positionenBindingSource.Current).Row;
        if (row["Verbrauch"] == DBNull.Value || (short)row["Verbrauch"] == 0)
        {
          verbrauch = false;
          zuzahlungsArt = 1;
        }
        else
        {
          verbrauch = true;
          zuzahlungsArt = 3;
        }
      }

      if (rezepteBindingSource.Count != 0 && rezepteBindingSource.Current != null)
      {
        currentRezept = ((DataRowView)rezepteBindingSource.Current).Row;
        befreit = (bool)currentRezept["Befreit"];
      }

      try { preis = Math.Round(decimal.Parse(ePreisTB.Text), 2, MidpointRounding.AwayFromZero); }
      catch { preis = 0; }

      try 
      {
        if (eigenanteilCB.Checked)
          anteil = Math.Round(decimal.Parse(eigenAnteilTB.Text), 2, MidpointRounding.AwayFromZero);
        else
          anteil = preis - Math.Round(decimal.Parse(eigenAnteilTB.Text), 2, MidpointRounding.AwayFromZero);
      }
      catch { anteil = 0; }
      anteil = 0;

      mwst = (MehrWertSteuer)mwstCB.SelectedItem;
      if (nettoCB.Checked)
        st = Math.Round(preis * (mwst.MWSt / 100m), 2, MidpointRounding.AwayFromZero);
      else
        st = Math.Round(preis * (mwst.MWSt / 100m) / (1m + (mwst.MWSt / 100m)), 2, MidpointRounding.AwayFromZero);
      mwstTB.Text = st.ToString("N2");
      if (calcZuzalung)
      {
        zuzahlung = Math.Round(CalcZuzahlung(preis - anteil + (nettoCB.Checked ?
          st - anteil * mwst.MWSt / 100m : 0), befreit, verbrauch, out zuzahlungsArt), zuzGen, MidpointRounding.AwayFromZero);
        zuzahlung = 0;
        zuzahlungsArt = 0;
      }
      else
      {
        try { zuzahlung = Math.Round(decimal.Parse(zuzahlungTB.Text), zuzGen, MidpointRounding.AwayFromZero); }
        catch { zuzahlung = 0; }
        short art1;
        decimal zuzahlung1 = Math.Round(CalcZuzahlung(preis - anteil + (nettoCB.Checked ?
          st - anteil * mwst.MWSt / 100m : 0), befreit, verbrauch, out art1), zuzGen, MidpointRounding.AwayFromZero);
        if (nettoCB.Checked)
        {
          zuzahlung1 = Math.Round(zuzahlung1 / (1m + (mwst.MWSt / 100m)), zuzGen, MidpointRounding.AwayFromZero);
        }
        if (zuzahlung == zuzahlung1) zuzahlungsArt = art1;
        if (zuzahlung == 0) zuzahlungsArt = 0;
      }
      if (calcZuzalung && nettoCB.Checked)
      {
        zuzahlung = Math.Round(zuzahlung / (1m + (mwst.MWSt / 100m)), zuzGen, MidpointRounding.AwayFromZero);
      }
      zuzahlung = 0;
      zuzahlungsArt = 0;
      zuzahlungTB.Text = zuzahlung.ToString("N" + zuzGen.ToString());
      ShowZuzahlungsArt(zuzahlungsArt);
      forderung = preis - anteil - zuzahlung;
      //if (bruttoCB.Checked) forderung += st;
      forderung *= anzahlNUD.Value;
      forderungLabel.Text = Math.Round(forderung, 2, MidpointRounding.AwayFromZero).ToString("N2");
    }

    private void SetBefreit()
    {
      if (rezepteBindingSource.Count > 0 && rezepteBindingSource.Position >= 0 &&
        positionenBindingSource.Count > 0 && positionenBindingSource.Position >= 0)
      {
        DataRow rezept = ((DataRowView)rezepteBindingSource.Current).Row;
        decimal preis;
        decimal anteil;
        decimal st;
        //decimal forderung;
        decimal zuzahlung;
        bool befreit = true;
        short zuzahlungsArt = 0;
        bool verbrauch = false;
        DataRow[] pos = rezept.GetChildRows("FK_Rezepte_Positionen");
        befreit = (bool)rezept["Befreit"];
        foreach (DataRow p in pos)
        {
          if (p["Verbrauch"] == DBNull.Value || (short)p["Verbrauch"] == 0)
            verbrauch = false;
          else
            verbrauch = true;
          try { preis = (decimal)p["EPreis"]; }
          catch { preis = 0; }

          try { anteil = (decimal)p["Eigenanteil"]; }
          catch { anteil = 0; }
          // dpre 08.10.2010 - eigenanteil immer 0
          anteil = 0;

          try { st = (decimal)p["Mwst"]; }
          catch { st = 0; }

          zuzahlung = Math.Round(CalcZuzahlung(preis - anteil + st, befreit, verbrauch, 
            out zuzahlungsArt), zuzGen, MidpointRounding.AwayFromZero);
          p["Zuzahlung"] = zuzahlung;
          p["Zuzahlungsart"] = zuzahlungsArt;
        }
      }
      CalcGesForderung();
      positionenDGV.Refresh();
      //positionenBindingSource.ResetBindings(false);
    }

    private void ShowZuzahlungsArt(short zuzahlungsArt)
    {
      switch (zuzahlungsArt)
      {
        case 0:
          zuzahlungsArtLabel.Text = string.Empty;
          zuzahlungsArtLabel.Tag = 0;
          zuzahlungsArtTT.SetToolTip(zuzahlungsArtLabel, string.Empty);
          break;
        case 1:
          zuzahlungsArtLabel.Text = "1 - Prozentuale Zzg §61";
          zuzahlungsArtLabel.Tag = 1;
          zuzahlungsArtTT.SetToolTip(zuzahlungsArtLabel, "Prozentuale Zuzahlung gemäß §61 Satz 1 SGB V");
          break;
        case 2:
          zuzahlungsArtLabel.Text = "2 - Zuzahlungsgrenzübertrag";
          zuzahlungsArtLabel.Tag = 2;
          zuzahlungsArtTT.SetToolTip(zuzahlungsArtLabel, "Grenzübertrag");
          break;
        case 3:
          zuzahlungsArtLabel.Text = "3 - Prozentuale Zuzg. Verbrauchszeitraum";
          zuzahlungsArtLabel.Tag = 3;
          zuzahlungsArtTT.SetToolTip(zuzahlungsArtLabel, "Prozentuale Zuzahlung für " +
            "den Verbrauchszeitraum gem. §33 Abs. 2, Satz 4, letzter Halbsatz SGB V " +
            "falls das Heilmittel zum Verbrauch bestimmt ist.");
          break;
        case 4:
          zuzahlungsArtLabel.Text = "4 - Max. Zuzg. Verbrauchszeitraum";
          zuzahlungsArtLabel.Tag = 4;
          zuzahlungsArtTT.SetToolTip(zuzahlungsArtLabel, string.Empty);
          break;
      }
      //if(positionenBindingSource.Count != 0 && positionenBindingSource.Current != null && 
      //  positionenBindingSource.AllowEdit)
      //{
      //  DataRow row = ((DataRowView)positionenBindingSource.Current).Row;
      //  if (row["Zuzahlungsart"] == DBNull.Value || (short)row["Zuzahlungsart"] != (int)zuzahlungsArtLabel.Tag)
      //    row["Zuzahlungsart"] = (short)(int)zuzahlungsArtLabel.Tag;
      //}
    }

    private decimal CalcZuzahlung(decimal preis, bool befreit, bool verbrauch, out short zuzahlungsArt)
    {
      decimal res = 0;
      if (verbrauch)
      {
        if (preis / 10 > 10) { res = 10; zuzahlungsArt = 4; }
        else { res = preis / 10; zuzahlungsArt = 3; }
      }
      else
      {
        if (preis / 10 < 5) { res = 5; zuzahlungsArt = 2; }
        else if (preis / 10 > 10) { res = 10; zuzahlungsArt = 2; }
        else { res = preis / 10; zuzahlungsArt = 1; }
      }
      if (res > preis) res = preis;
      if (befreit) { res = 0; zuzahlungsArt = 0; }
      return res;
    }

    private void nettoCB_CheckedChanged(object sender, EventArgs e)
    {
      if (nettoCB.Checked) nettoCB.Text = "Netto";
      else nettoCB.Text = "Brutto";
      decimal ePreis = 0;
      decimal eigenAnteil = 0;
      decimal zuzahlung = 0;
      MehrWertSteuer mwst = (MehrWertSteuer)mwstCB.SelectedItem;
      try { ePreis = decimal.Parse(ePreisTB.Text); }
      catch { }
      try 
      { 
        if(eigenanteilCB.Checked)
          eigenAnteil = decimal.Parse(eigenAnteilTB.Text); 
        else
          eigenAnteil = ePreis - decimal.Parse(eigenAnteilTB.Text);
      // dpre 08.10.2010 - eigenanteil immer 0
        eigenAnteil = 0;
      }
      catch { }
      try { zuzahlung = decimal.Parse(zuzahlungTB.Text); }
      catch { }
      if (nettoCB.Checked)
      {
        ePreis /= 1 + mwst.MWSt / 100m;
        eigenAnteil /= 1 + mwst.MWSt / 100m;
        zuzahlung /= 1 + mwst.MWSt / 100m;
      }
      else
      {
        ePreis *= 1 + mwst.MWSt / 100m;
        eigenAnteil *= 1 + mwst.MWSt / 100m;
        zuzahlung *= 1 + mwst.MWSt / 100m;
      }
      ePreisTB.Text = ePreis.ToString("N2");
      eigenAnteilTB.Text = ((decimal)0).ToString("N2");
      // dpre 08.10.2010 - eigenanteil immer 0

      /*if (eigenanteilCB.Checked)
        eigenAnteilTB.Text = eigenAnteil.ToString("N2");
      else
        eigenAnteilTB.Text = (ePreis - eigenAnteil).ToString("N2");*/
      zuzahlungTB.Text = zuzahlung.ToString("N" + zuzGen.ToString());
      UpdatePreis(false);
    }

    private void mwstCB_SelectedValueChanged(object sender, EventArgs e)
    {
      if(!initialization)
        UpdatePreis(true);
    }

    private void eigenAntailTB_Validating(object sender, CancelEventArgs e)
    {
      try
      {
          // dpre 08.10.2010 - eigenanteil immer 0
          eigenAnteilTB.Text = "0,00";
        //eigenAnteilTB.Text = Math.Round(decimal.Parse(eigenAnteilTB.Text), 2, MidpointRounding.AwayFromZero).ToString("N2");
      }
      catch
      {
        eigenAnteilTB.Text = "0,00";
      }
      UpdatePreis(true);
    }

    private void selectAllTB_Enter(object sender, EventArgs e)
    {
      TextBoxBase tb = (TextBoxBase)sender;
      this.BeginInvoke((MethodInvoker)delegate()
      {
        tb.SelectAll();
      });
    }

    private void zeitraumTB_Enter(object sender, EventArgs e)
    {
       /* if (!versorgungBisMTB.Text.Equals("  .  ."))
        {
            int count = 0;
            int yearVon = Convert.ToInt32(versorgungVonMTB.Text.Substring(6, 4));
            int yearBis = Convert.ToInt32(versorgungBisMTB.Text.Substring(6, 4));
            int monthVon = Convert.ToInt32(versorgungVonMTB.Text.Substring(3, 2));
            int monthBis = Convert.ToInt32(versorgungBisMTB.Text.Substring(3, 2));
            while (yearBis != yearVon || monthBis != monthVon)
            {
                count++;
                monthVon++;
                if (monthVon == 13)
                {
                    yearVon++;
                    monthVon = 1;
                }
            }
            zeitraumTB.Text = count.ToString();
        }*/
    }

    private void zeitraumTB_Leave(object sender, EventArgs e)
    {
       /* if (zeitraumTB.Text.Length > 0)
        {
            int yearVon = Convert.ToInt32(versorgungVonMTB.Text.Substring(6, 4));
            int monthVon = Convert.ToInt32(versorgungVonMTB.Text.Substring(3, 2));
            int dayVon = Convert.ToInt32(versorgungVonMTB.Text.Substring(0, 2));
            int count = Convert.ToInt32(zeitraumTB.Text);

            monthVon += count;
            while (monthVon > 12)
            {
                yearVon++;
                monthVon -= 12;
            }
            dayVon--;
            if (dayVon < 1)
            {
                monthVon--;
                switch (monthVon)
                {

                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                    case 12:
                        dayVon = 31;
                        break;
                    case 4:
                    case 6:
                    case 9:
                    case 11:
                        dayVon = 30;
                        break;
                    case 2:
                        dayVon = 28;
                        if ((yearVon % 4 == 0) && ((yearVon % 100 != 0) || (yearVon % 400 == 0)))
                            dayVon++;
                        break;
                }
            }
            versorgungBisMTB.Text = dayVon.ToString("00") + "." + monthVon.ToString("00") + "." + yearVon.ToString("0000");
        }*/
    }

    private void versorgungVonMTB_Enter(object sender, EventArgs e)
    {
        string year = jahrCB.SelectedItem.ToString();
        versorgungText = versorgungVonMTB.Text;
        versorgungVonMTB.Text = "01.  ." + year;
        this.BeginInvoke((MethodInvoker)delegate()
        {
            versorgungVonMTB.SelectionStart = 3;
            versorgungVonMTB.SelectionLength = 0;
        });
    }
    private void versorgungVonMTB_Leave(object sender, EventArgs e)
    {
        string year = jahrCB.SelectedItem.ToString();
        if (versorgungVonMTB.Text.Equals("01.  ." + year))
            versorgungVonMTB.Text = versorgungText;
        
    }
    private void versorgungBisMTB_Enter(object sender, EventArgs e)
    {
        string year = jahrCB.SelectedItem.ToString();
        versorgungText = versorgungBisMTB.Text;
        versorgungBisMTB.Text = "31.  ." + year;        
        this.BeginInvoke((MethodInvoker)delegate()
        {
            versorgungBisMTB.SelectionStart = 3;
            versorgungBisMTB.SelectionLength = 0;
        });
    }
    private void versorgungBisMTB_Leave(object sender, EventArgs e)
    {
        string year = jahrCB.SelectedItem.ToString();        
        if (versorgungBisMTB.Text.Equals("31.  ." + year))
            versorgungBisMTB.Text = versorgungText;
        else
        {
            int month = Convert.ToInt32(versorgungBisMTB.Text.Substring(3, 2));
            int yearBis = Convert.ToInt32(versorgungBisMTB.Text.Substring(6, 4));
            switch (month)
            {
                case 4:
                case 6:
                case 9:
                case 11:
                    versorgungBisMTB.Text = "30." + month.ToString("00") + "." + year.ToString();
                    break;
                case 2:
                    versorgungBisMTB.Text = "28." + month.ToString("00") + "." + year.ToString();
                    if ((yearBis % 4 == 0) && ((yearBis % 100 != 0) || (yearBis % 400 == 0)))
                        versorgungBisMTB.Text = "29." + month.ToString("00") + "." + year.ToString();
                    break;
            }
        }
    }

    private void zeitVonMTB_Enter(object sender, EventArgs e)
    {        
        this.BeginInvoke((MethodInvoker)delegate()
        {
            versorgungBisMTB.SelectionStart = 0;
            versorgungBisMTB.SelectionLength = 0;
        });
    }

    private void zeitBisMTB_Leave(object sender, EventArgs e)
    {
        if (!zeitBisMTB.Text.Equals("  :"))
        {
            int vonHour = Convert.ToInt32(zeitVonMTB.Text.Substring(0, 2));
            int vonMin = Convert.ToInt32(zeitVonMTB.Text.Substring(3, 2));
            int bisHour = Convert.ToInt32(zeitBisMTB.Text.Substring(0, 2));
            int bisMin = Convert.ToInt32(zeitBisMTB.Text.Substring(3, 2));

            int minutes = bisMin + 60 - vonMin;
            vonHour++;
            while (vonHour != bisHour)
            {
                minutes += 60;
                vonHour++;
                if (vonHour == 24)
                    vonHour = 0;
            }
            dauerTB.Text = minutes.ToString();
        }
    }

    private void dauerTB_Leave(object sender, EventArgs e)
    {
      if (zeitBisMTB.Text.Equals("  :") && dauerTB.Text.Length > 0)
      {
          int minutes = Convert.ToInt32(dauerTB.Text);
          int hours = minutes / 60;
          minutes -= (hours * 60);
          int vonHour = Convert.ToInt32(zeitVonMTB.Text.Substring(0, 2));
          int vonMin = Convert.ToInt32(zeitVonMTB.Text.Substring(3, 2));
          vonMin += minutes;
          if (vonMin > 60)
          {
              vonMin -= 60;
              hours++;
          }
          vonHour += hours;
          while (vonHour > 24)
              vonHour -= 24;
          zeitBisMTB.Text = vonHour.ToString("00") + ":" + vonMin.ToString("00");
      }
    }

    private void ePreisTB_Validating(object sender, CancelEventArgs e)
    {
      decimal ePreis = 0;
      try 
      { 
        ePreisTB.Text = Math.Round(decimal.Parse(ePreisTB.Text), 2, MidpointRounding.AwayFromZero).ToString("N2");
        ePreis = decimal.Parse(ePreisTB.Text);
      }
      catch { ePreisTB.Text = "0,00"; }
      
      if (!eigenanteilCB.Checked)
      {
          // dpre 08.10.2010 - eigenanteil immer 0
          eigenAnteilTB.Text = "0,00"; // ePreisTB.Text;
        //decimal eigenAnteil = 0;
        //try { eigenAnteil = decimal.Parse(eigenAnteilTB.Text); }
        //catch { }
        //if (eigenAnteil == 0)
        //  eigenAnteilTB.Text = ePreisTB.Text;
        //if (ePreis < eigenAnteil)
        //  eigenAnteilTB.Text = ePreisTB.Text;
      }
      else
      {
        eigenAnteilTB.Text = "0,00";
        //decimal eigenAnteil = 0;
        //try { eigenAnteil = decimal.Parse(eigenAnteilTB.Text); }
        //catch { }
        //if (ePreis < eigenAnteil)
        //  eigenAnteilTB.Text = ePreisTB.Text;
      }

      UpdatePreis(true);
    }

    private void FillJahrList()
    {
      jahrCB.Items.Clear();
      int cy = 0;
      if (jahr2MenuItem.Checked)
        cy = int.Parse(jahr2MenuItem.Text);
      else
        cy = int.Parse(jahr1MenuItem.Text);
      jahrCB.Items.Add(cy.ToString());
      jahrCB.Items.Add((cy - 1).ToString());
      jahrCB.Items.Add((cy - 2).ToString());
      jahrCB.SelectedIndex = 0;
    }

    private void neuePositionBtn_Click(object sender, EventArgs e)
    {
      if (neuePositionBtn.Text == "&Neu")
      {
        neuePositionBtn.Text = "&Abbrechen";
        AddNeuePosition();
        //int size = IdentnrCB.Items.Count;
        //if (size == 0)
        //  IdentnrCB.Items.Add("1");
        //else
        //  IdentnrCB.Items.Add(Convert.ToInt32(IdentnrCB.Items[size - 1].ToString()) + 1);
        IdentnrCB.SelectedIndex = this.getNextFree();
      }
      else
      {
        neuePositionBtn.Text = "&Neu";
        if (positionenBindingSource.Count != 0 && positionenBindingSource.Current != null)
        {
          //positionenBindingSource.RemoveCurrent();
          //IdentnrCB.Items.RemoveAt(IdentnrCB.Items.Count - 1);
        }
      }
    }

    private void AddNeuePosition()
    {
      if (positionenBindingSource.AllowNew && positionenBindingSource.DataSource != null &&
        ValidatePosition())
      {
        positionenBindingSource.AddNew();
        if (positionenBindingSource.Count == 1)
          tagMonatMTB.Focus();
        else
          anzahlNUD.Focus();
        mwstCB.SelectedIndex = 0;
      }
    }

    bool changingCurrentPosition = false;
    private void positionenBindingSource_CurrentChanged(object sender, EventArgs e)
    {
      if (!changingCurrentPosition)
      {
        changingCurrentPosition = true;
        try
        {
          ShowCurrentPosition();
        }
        finally
        {
          CalcGesForderung();
          changingCurrentPosition = false;
        }
      }
    }

    private void ShowCurrentPosition()
    {
      try
      {
        
        if (positionenBindingSource.Count != 0 && positionenBindingSource.Current != null)
        {
          IdentnrCB.Items.Clear();
          for (int i = 0; i < positionenBindingSource.Count; i++)
            IdentnrCB.Items.Add((i+1).ToString());          
          EnabelPositionenPanel();
          DataRow row = ((DataRowView)positionenBindingSource.Current).Row;
          DataRow rezept = ((DataRowView)rezepteBindingSource.Current).Row;
          if (row["LeistungID"] != DBNull.Value)
          {
            IdentnrCB.SelectedIndex = (int)row["LeistungID"] - 1;
          }
          else
          {
            DataRow[] pos = rezept.GetChildRows("FK_Rezepte_Positionen");
            int i = 0;
            foreach (DataRow p in pos)
            {
              if (p["LeistungID"] != DBNull.Value)
              {
                i++;
              }
            }
            IdentnrCB.SelectedIndex = i;
          }

          if (row["Verbrauch"] == DBNull.Value || (short)row["Verbrauch"] == 0)
            verbrauchLabel.Visible = false;
          else
            verbrauchLabel.Visible = true;
          DateTime d = (DateTime)rezept["RezDat"];
          if (row["BehDat"] != DBNull.Value)
            d = (DateTime)row["BehDat"];
          else
          {
            DataRow[] pos = voinDataSet.Positionen.Select("", "", DataViewRowState.CurrentRows);
            if (pos.Length != 0)
              d = (DateTime)pos[0]["BehDat"];
          }
          tagMonatMTB.Text = d.ToString("dd.MM.yyyy").Substring(0, 6);
          int idx = jahrCB.Items.IndexOf(d.ToString("dd.MM.yyyy").Substring(6, 4));
          if (idx != -1)
            jahrCB.SelectedIndex = idx;
          else
            jahrCB.SelectedIndex = jahrCB.Items.Count - 1;
          anzahlNUD.Value = (row["Anzahl"] != DBNull.Value ? (decimal)row["Anzahl"] : 1m);
          anzahlEigenanteilLabel.Text = anzahlNUD.Value.ToString("N0") + " X";
          anzahlZuzahlungLabel.Text = anzahlNUD.Value.ToString("N0") + " X";
          string buPos = (row["BuPos"] != DBNull.Value ? (string)row["BuPos"] : string.Empty);
          gruppeMTB.Text = Substring(buPos, 0, 8); //Autor:subhi, Datum:23.09.2014 Substring(buPos, 0, 2)
         
          if(row["Mwst_KZ"] == DBNull.Value)
          {
            if (mwstCB.SelectedIndex == -1)
              mwstCB.SelectedValue = (short)1;
          }
          else
          {
            mwstCB.SelectedValue = (short)row["Mwst_KZ"];
          }
          
          // gleiche wie für epreis
          mwstTB.Text = (row["Mwst"] == DBNull.Value ? "0,00" : Math.Round((decimal)row["Mwst"] * anzahlNUD.Value, 2, MidpointRounding.AwayFromZero).ToString("N2"));
          
          // dpre 06.08.2010 - epreis wird in voin jetzt als gesamtpreis eingegeben (epreis * anzahl), zeige den gesamtpreis wieder an
          decimal ePreis = (row["EPreis"] != DBNull.Value ? (decimal)row["EPreis"] : 0m) * (decimal)anzahlNUD.Value;
          
          if (!nettoCB.Checked) ePreis += decimal.Parse(mwstTB.Text);
          ePreisTB.Text = ePreis.ToString("N2");

            // dpre 08.10.2010 - eigenanteil immer 0
          decimal eigenAnteil = 0; // row["Eigenanteil"] == DBNull.Value ? 0 : (decimal)row["Eigenanteil"];
          if (nettoCB.Checked) eigenAnteil /= (1m + ((MehrWertSteuer)mwstCB.SelectedItem).MWSt /
             100m);
          if (eigenanteilCB.Checked)
              eigenAnteilTB.Text = ((decimal)0).ToString("N2"); // eigenAnteil.ToString("N2");
          else
              eigenAnteilTB.Text = ((decimal)0).ToString("N2"); // (ePreis - eigenAnteil).ToString("N2");
          decimal zuzahlung = row["Zuzahlung"] == DBNull.Value ? 0 : (decimal)row["Zuzahlung"];
          if (nettoCB.Checked) zuzahlung /= (1m + ((MehrWertSteuer)mwstCB.SelectedItem).MWSt /
             100m);
          zuzahlungTB.Text = zuzahlung.ToString("N" + zuzGen.ToString());
          hilfsMittelKennZeichenCB.SelectedValue = (row["Hilfsmittel_KZ"] == DBNull.Value ?
            (short)0 : (short)row["Hilfsmittel_KZ"]);
          inventarNummerTB.Text = (row["Hilfsmittel_InvNr"] == DBNull.Value ?
            string.Empty : (string)row["Hilfsmittel_InvNr"]);
          zeitVonMTB.Text = (row["Uhrzeit_von"] == DBNull.Value ? string.Empty :
            Substring((string)row["Uhrzeit_von"], 0, 2) + ":" +
            Substring((string)row["Uhrzeit_von"], 2, 2));
          zeitBisMTB.Text = (row["Uhrzeit_bis"] == DBNull.Value ? string.Empty :
            Substring((string)row["Uhrzeit_bis"], 0, 2) + ":" +
            Substring((string)row["Uhrzeit_bis"], 2, 2));
          versorgungVonMTB.Text = (row["Versorgung_von"] == DBNull.Value ? string.Empty :
            ((DateTime)row["Versorgung_von"]).ToString("dd.MM.yyyy"));
          versorgungBisMTB.Text = (row["Versorgung_bis"] == DBNull.Value ? string.Empty :
            ((DateTime)row["Versorgung_bis"]).ToString("dd.MM.yyyy"));
          kilometerTB.Text = (row["Kilometer"] == DBNull.Value ? string.Empty :
            Math.Round((decimal)row["Kilometer"], 2, MidpointRounding.AwayFromZero).ToString());
          dauerTB.Text = (row["Dauer_Min"] == DBNull.Value ? string.Empty :
            ((int)row["Dauer_Min"]).ToString());
          zeitraumTB.Text = (row["VZeitraum"] == DBNull.Value ? string.Empty :
            ((short)row["VZeitraum"]).ToString());
          begruendungTB.Text = (row["Text"] == DBNull.Value ? string.Empty :
            (string)row["Text"]);
          if (focusPosition)
            this.BeginInvoke((MethodInvoker)delegate()
            {
              if (positionenBindingSource.Count == 1)
                tagMonatMTB.Focus();
              else
                anzahlNUD.Focus();
            });
          if (row.IsNull("Tarifkennzeichen"))
          {
            posTKZTB.Text = string.Empty;
          }
          else
          {
            posTKZTB.Text = (string)row["Tarifkennzeichen"];
          }
        }
        else
          DisabelPositionenPanel();
      }
      finally
      {
        UpdatePositionsnummer(gruppeMTB);
        
        UpdatePreis(false);
      }
    }

    private void EnabelPositionenPanel()
    {
      foreach (Control c in positionenPanel.Controls)
        if (c != positionenDGV && c != neuePositionBtn)
          c.Enabled = true;
    }

    private void DisabelPositionenPanel()
    {
      if (pnlRezeptEingabe.Enabled)
        nameTB.Focus();
      else
        ikTB.Focus();
      forderungLabel.Text = string.Empty;
      //09.02.2007
      //verbrauchLabel.Visible = false;
      gruppeLabel.Text = string.Empty;
      ortLabel.Text = string.Empty;
      unterGruppeLabel.Text = string.Empty;
      artLabel.Text = string.Empty;
      produktLabel.Text = string.Empty;
      anzahlEigenanteilLabel.Text = string.Empty;
      anzahlZuzahlungLabel.Text = string.Empty;
      zuzahlungsArtLabel.Text = string.Empty;
      gesamteForderungLabel.Text = "Forderung:";
      gesamteZuzahlungLabel.Text = "Zuzahlung:";
      gesamteSummeLabel.Text = "Ges.:";
      foreach (Control c in positionenPanel.Controls)
        if (c != positionenDGV && c != neuePositionBtn)
        {
          c.Enabled = false;
          if (c is TextBoxBase) c.Text = string.Empty;
        }
    }

    private void CalcGesForderung()
    {
      decimal forderung = 0;
      decimal zuzahlung = 0;
      if (positionenBindingSource.DataSource != null)
        foreach (DataRow row in voinDataSet.Positionen.Rows)
          if (row.RowState != DataRowState.Deleted && row.RowState != DataRowState.Detached)
          {
            forderung += CalcForderung(row);
            zuzahlung += (decimal)row["Zuzahlung"];
          }
      forderung = Math.Round(forderung, 2, MidpointRounding.AwayFromZero);
      zuzahlung = Math.Round(zuzahlung, 2, MidpointRounding.AwayFromZero);
      gesamteForderungLabel.Text = "Forderung: " + forderung.ToString("N2");
      gesamteZuzahlungLabel.Text = "Zuzahlung: " + zuzahlung.ToString("N2");
      gesamteSummeLabel.Text = "Ges.: " + (forderung + zuzahlung).ToString("N2");
    }

    private decimal CalcForderung(DataRow row)
    {
      //ibot 14.05.2007 Zuzahlung 4 Stellen nach ,
      return Math.Round(((row["EPreis"] != DBNull.Value ? (decimal)row["EPreis"] : 0m) +
        (row["Mwst"] != DBNull.Value ? (decimal)row["Mwst"] : 0m)) * (decimal)row["Anzahl"], 2, MidpointRounding.AwayFromZero);
    }

    bool focusPosition = true;

    private void LoadPositionen()
    {
      voinDataSet.Positionen.Clear();
      if (rezepteBindingSource.Count > 0 && rezepteBindingSource.Current != null &&
        rezepteBindingSource.DataMember != string.Empty)
      {
        DataRowView drv = (DataRowView)rezepteBindingSource.Current;
        if (drv.Row.RowState != DataRowState.Deleted && drv.Row.RowState != DataRowState.Detached)
        {
            //hier
          positionenDA.SelectCommand.Parameters["@ID"].Value = drv.Row["ID"];
          positionenDA.Fill(voinDataSet.Positionen);

          //dpre 23.06.2010 Arztnummern Suche
          LoadArztnummern(drv.Row["Betriebsstaettennummer"].ToString(), false, true);
        }
        ((ISupportInitialize)positionenBindingSource).BeginInit();
        positionenBindingSource.DataSource = voinDataSet;
        positionenBindingSource.DataMember = "Positionen";
        ((ISupportInitialize)positionenBindingSource).EndInit();
        positionenDGV.DataSource = positionenBindingSource;
        BuildPositionenDGVColumns();
        //SetPositionenBindings();
        if (ePreisTB.DataBindings.Count > 0 && !ePreisTB.DataBindings[0].IsBinding)
          positionenBindingSource.ResumeBinding();
      }

    }

    private void BuildPositionenDGVColumns()
    {
      //foreach (DataGridViewColumn c in positionenDGV.Columns)
      //{
      //  c.Visible = false;
      //  if (c.DataPropertyName.Trim().ToUpper() == "BEHDAT")
      //  {
      //    c.Visible = true;
      //    c.DisplayIndex = 0;
      //  }
      //  if (c.DataPropertyName.Trim().ToUpper() == "BUPOS")
      //  {
      //    c.Visible = true;
      //    c.DisplayIndex = 1;
      //  }
      //  if (c.DataPropertyName.Trim().ToUpper() == "ANZAHL")
      //  {
      //    c.Visible = true;
      //    c.DisplayIndex = 2;
      //  }
      //}
      //positionenDGV.Columns.Add(this.forderungColumn);
      //positionenDGV.Columns.Add(this.ePreisColumn);

      positionenDGV.Columns.Clear();
      positionenDGV.Columns.Add(this.forderungColumn);
      positionenDGV.Columns.Add(this.ePreisColumn);
      positionenDGV.Columns.Add(this.anzahlColumn);
      positionenDGV.Columns.Add(this.buPosColumn);
      positionenDGV.Columns.Add(this.datumColumn);
      positionenDGV.Columns["datumColumn"].DisplayIndex = 0;
      positionenDGV.Columns["buPosColumn"].DisplayIndex = 1;
      positionenDGV.Columns["anzahlColumn"].DisplayIndex = 2;
      positionenDGV.Columns["ePreisColumn"].DisplayIndex = 3;
      positionenDGV.Columns["forderungColumn"].DisplayIndex = 4;

    }

    //private void SetPositionenBindings()
    //{
    //  Binding bng;
    //  bng = new Binding("Text", positionenBindingSource, "EPreis");
    //  bng.FormatString = "N2";
    //  bng.FormattingEnabled = true;
    //  ePreisTB.DataBindings.Add(bng);
    //  //anzahlNUD.DataBindings.Add("Value", positionenBindingSource, "Anzahl");
    //  bng = new Binding("Text", positionenBindingSource, "Eigenanteil");
    //  bng.FormatString = "N2";
    //  bng.FormattingEnabled = true;
    //  eigenAnteilTB.DataBindings.Add(bng);
    //  bng = new Binding("Text", positionenBindingSource, "Zuzahlung");
    //  bng.FormatString = "N2";
    //  bng.FormattingEnabled = true;
    //  zuzahlungTB.DataBindings.Add(bng);
    //  mwstCB.DataSource = MehrWertSteuer.MWStList;
    //  mwstCB.ValueMember = "MWStCode";
    //  mwstCB.DisplayMember = "MWStName";
    //  mwstCB.DataBindings.Add("SelectedValue", positionenBindingSource, "Mwst_KZ");
    //}

    //private void ClearPositionenBindings()
    //{
    //  if (positionenBindingSource.Count != 0 && positionenBindingSource.Current != null)
    //    positionenBindingSource.EndEdit();
    //  ePreisTB.DataBindings.Clear();
    //  //anzahlNUD.DataBindings.Clear();
    //  eigenAnteilTB.DataBindings.Clear();
    //  zuzahlungTB.DataBindings.Clear();
    //  mwstCB.DataBindings.Clear();
    //}

    private void positionenDGV_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
    {
      //if (positionenDGV.Columns[e.ColumnIndex].HeaderText == "E-Preis" &&
      //  positionenDGV.Rows.Count > e.RowIndex)
      //{
      //  try
      //  {
      //    DataRow row = ((DataRowView)positionenDGV.Rows[e.RowIndex].DataBoundItem).Row;
      //    decimal ePreis = (row["EPreis"] != DBNull.Value ? (decimal)row["EPreis"] : 0m) +
      //      (row["Mwst"] != DBNull.Value ? (decimal)row["Mwst"] : 0m);
      //    e.Value = Math.Round(ePreis, 2);
      //  }
      //  catch { }
      //}
      if (positionenDGV.Columns[e.ColumnIndex].HeaderText == "Forderung" &&
        positionenDGV.Rows.Count > e.RowIndex)
      {
        try
        {
          DataRow row = ((DataRowView)positionenDGV.Rows[e.RowIndex].DataBoundItem).Row;
          decimal forderung = CalcForderung(row);
          e.Value = Math.Round(forderung, 2, MidpointRounding.AwayFromZero);
        }
        catch { }
      }
      if (positionenDGV.Columns[e.ColumnIndex].HeaderText == "BuPos")
      {
        if (e.Value != DBNull.Value)
        {
          string val = (string)e.Value;
          string res = string.Empty;
          if (Substring(val, 0, 2) != string.Empty)
            res = Substring(val, 0, 2);
          if (Substring(val, 2, 2) != string.Empty)
            res += "." + Substring(val, 2, 2);
          if (Substring(val, 4, 2) != string.Empty)
            res += "." + Substring(val, 4, 2);
          if (Substring(val, 6, 2) != string.Empty)
            res += "." + Substring(val, 6, 2);
          if (Substring(val, 7, 3) != string.Empty)
            res += "." + Substring(val, 7, 3);
          e.Value = res;
        }
        else
          e.Value = string.Empty;
      }
    }

    bool showPositionError = true;

    private bool ValidatePosition()
    {
      bool res = true;
      errorProvider.Clear();
      if (positionenBindingSource.Count != 0 && positionenBindingSource.Current != null)
      {
        DataRow row = ((DataRowView)positionenBindingSource.Current).Row;
        DataRow rezept = ((DataRowView)rezepteBindingSource.Current).Row;
        if (row.RowState == DataRowState.Detached && IsDefaultPosition())
        {
          positionenBindingSource.CancelEdit();
          neuePositionBtn.Text = "&Neu";
          return true;
        }
        if (row["ID"] == DBNull.Value)
          row["ID"] = rezept["ID"];
        if (mandantenBindingSource.Count != 0 && mandantenBindingSource.Current != null)
        {
          //string tkz = (string)((DataRowView)mandantenBindingSource.Current)["Tarifkennzeichen"];
          if (row["Tarifkennzeichen"] == DBNull.Value)// ||
            //(string)row["Tarifkennzeichen"] != tarifSchlusselAbrechnCodeTB.Text)
            row["Tarifkennzeichen"] = tarifSchlusselAbrechnCodeTB.Text;
        }
        if (row["Faktor"] == DBNull.Value) row["Faktor"] = 1m;
        if (row["GebArt"] == DBNull.Value) row["GebArt"] = 1;
        if (row["Gebuehr"] == DBNull.Value) row["Gebuehr"] = 10;
        if (row["Verbrauch"] == DBNull.Value) row["Verbrauch"] = 0;
        if (eigenAnteilTB.Focused || ePreisTB.Focused) UpdatePreis(true);
        if (zuzahlungTB.Focused || anzahlNUD.Focused) UpdatePreis(false);
        try
        {
          MehrWertSteuer steuer = (MehrWertSteuer)mwstCB.SelectedItem;
          //BehDat
          try
          {
            string sd = tagMonatMTB.Text + jahrCB.SelectedItem;
            DateTime d = DateTime.Parse(sd);
            if (row["BehDat"] == DBNull.Value || (DateTime)row["BehDat"] != d)
              row["BehDat"] = d;
            UpdatePositionenDaten(d);
          }
          catch (Exception ex)
          {
            throw new Exception("BehDat", ex);
          }
          //Anzahl
          if (row["Anzahl"] == DBNull.Value || (decimal)row["Anzahl"] != anzahlNUD.Value)
            row["Anzahl"] = anzahlNUD.Value;
          //BuPos
          string buPos = gruppeMTB.Text;
          if (row["BuPos"] == DBNull.Value || (string)row["BuPos"] != buPos)
            row["BuPos"] = buPos;

          row["BuPos_Sonder"] = "2";
            if(existsSonderPZN((string)row["BuPos"]))
                row["BuPos_Sonder"] = "3";

            //Mwst
          decimal mwst = 0;
          if (mwstTB.Text != string.Empty)
            mwst = decimal.Parse(mwstTB.Text);
          // dpre 06.08.2010 - mwst für den epreis berechnen, in voin wird jetzt immer der gesamtbetrag (epreis * anzahl) eingegeben
            mwst = mwst / (decimal)row["Anzahl"];

          if (row["Mwst"] == DBNull.Value || (decimal)row["Mwst"] != mwst)
            row["Mwst"] = mwst;
          
            //EPreis
          decimal ePreis = 0;
          decimal ePreisBrutto = 0;
          try
          {
            ePreisBrutto = ePreis = (decimal.Parse(ePreisTB.Text) / (decimal)row["Anzahl"]);
            //            if (!bruttoCB.Checked) ePreis -= mwst;
            if (!nettoCB.Checked) ePreis -= mwst;
            else ePreisBrutto += mwst;

            if (ePreis <= 0m) throw new Exception("Der eingegebene Preis ist falsch.");
            if (row["EPreis"] == DBNull.Value || (decimal)row["EPreis"] != ePreis)
              row["EPreis"] = ePreis;
          }
          catch (Exception ex)
          {
            throw new Exception("EPreis", ex);
          }
          //Mwst_KZ
          if (row["Mwst_KZ"] == DBNull.Value || (short)row["Mwst_KZ"] !=
            (short)mwstCB.SelectedValue) row["Mwst_KZ"] = (short)mwstCB.SelectedValue;
          //Eigenanteil
          try
          {
              // dpre 08.10.2010 - eigenanteil immer 0
            decimal eigenAnteil = 0;
            /*if (eigenAnteilTB.Text != string.Empty)
            {
              eigenAnteil = decimal.Parse(eigenAnteilTB.Text);
            }
            if (nettoCB.Checked)
              eigenAnteil = Math.Round(eigenAnteil * (1 + steuer.MWSt / 100m), 2, MidpointRounding.AwayFromZero);
            if (!eigenanteilCB.Checked)
              eigenAnteil = ePreisBrutto - eigenAnteil;*/
            if (row["Eigenanteil"] == DBNull.Value || (decimal)row["Eigenanteil"] != eigenAnteil)
              row["Eigenanteil"] = eigenAnteil;
          }
          catch (Exception ex)
          {
            throw new Exception("Eigenanteil", ex);
          }
          //Zuzahlung
          try
          {
            decimal zuzahlung = 0;
            if (zuzahlungTB.Text != string.Empty)
              zuzahlung = decimal.Parse(zuzahlungTB.Text);
            if (nettoCB.Checked)
              zuzahlung = Math.Round(zuzahlung * (1 + steuer.MWSt / 100m), zuzGen, MidpointRounding.AwayFromZero);
            if (row["Zuzahlung"] == DBNull.Value || (decimal)row["Zuzahlung"] != zuzahlung)
              row["Zuzahlung"] = zuzahlung;
          }
          catch (Exception ex)
          {
            throw new Exception("Zuzahlung", ex);
          }
          //Zuzahlungsart
          if (zuzahlungsArtLabel.Tag != null)
          {
            short zuzahlungsArt = (short)(int)zuzahlungsArtLabel.Tag;
            if (row["Zuzahlungsart"] == DBNull.Value || (short)row["Zuzahlungsart"] != zuzahlungsArt)
              row["Zuzahlungsart"] = zuzahlungsArt;

          }
          //Hilfsmittel_KZ
          short hilfsmittel_KZ = (short)hilfsMittelKennZeichenCB.SelectedValue;
          if (row["Hilfsmittel_KZ"] == DBNull.Value || (short)row["Hilfsmittel_KZ"] != hilfsmittel_KZ)
            row["Hilfsmittel_KZ"] = hilfsmittel_KZ;
          //Hilfsmittel_InvNr
          if ((row["Hilfsmittel_InvNr"] == DBNull.Value && inventarNummerTB.Text != string.Empty) ||
            (row["Hilfsmittel_InvNr"] != DBNull.Value && (string)row["Hilfsmittel_InvNr"] !=
            inventarNummerTB.Text))
            row["Hilfsmittel_InvNr"] = inventarNummerTB.Text;
          //Uhrzeit_von
          if (zeitVonMTB.Text == "  :" && row["Uhrzeit_von"] != DBNull.Value)
            row["Uhrzeit_von"] = DBNull.Value;
          if (zeitVonMTB.Text != "  :")
          {
            string uhrzeit_von = Substring(zeitVonMTB.Text, 0, 2) +
              Substring(zeitVonMTB.Text, 3, 2);
            if (row["Uhrzeit_von"] == DBNull.Value || (string)row["Uhrzeit_von"] != uhrzeit_von)
              row["Uhrzeit_von"] = uhrzeit_von;
          }
          //Uhrzeit_bis
          if (zeitVonMTB.Text == "  :" && row["Uhrzeit_bis"] != DBNull.Value)
            row["Uhrzeit_bis"] = DBNull.Value;
          if (zeitVonMTB.Text != "  :")
          {
            string uhrzeit_bis = Substring(zeitBisMTB.Text, 0, 2) +
              Substring(zeitBisMTB.Text, 3, 2);
            if (row["Uhrzeit_bis"] == DBNull.Value || (string)row["Uhrzeit_bis"] != uhrzeit_bis)
              row["Uhrzeit_bis"] = uhrzeit_bis;
          }
          //Versorgung_von
          try
          {
            if (versorgungVonMTB.Text == "  .  ." && row["Versorgung_von"] != DBNull.Value)
              row["Versorgung_von"] = DBNull.Value;
            if (versorgungVonMTB.Text != "  .  .")
            {
              DateTime versorgung_von = DateTime.Parse(versorgungVonMTB.Text);
              if (row["Versorgung_von"] == DBNull.Value || (DateTime)row["Versorgung_von"] !=
                versorgung_von) row["Versorgung_von"] = versorgung_von;
            }
          }
          catch (Exception ex)
          {
            throw new Exception("Versorgung_von", ex);
          }
          //Verorgung_bis
          try
          {
            if (versorgungBisMTB.Text == "  .  ." && row["Versorgung_bis"] != DBNull.Value)
              row["Versorgung_bis"] = DBNull.Value;
            if (versorgungBisMTB.Text != "  .  .")
            {
              DateTime versorgung_bis = DateTime.Parse(versorgungBisMTB.Text);
              if (row["Versorgung_bis"] == DBNull.Value || (DateTime)row["Versorgung_bis"] !=
                versorgung_bis) row["Versorgung_bis"] = versorgung_bis;
            }
          }
          catch (Exception ex)
          {
            throw new Exception("Versorgung_bis", ex);
          }
          //Kilometer
          try
          {
            decimal kilometer = 0;
            if (kilometerTB.Text != string.Empty)
              kilometer = Math.Round(decimal.Parse(kilometerTB.Text), 2, MidpointRounding.AwayFromZero);
            if (kilometerTB.Text == string.Empty && row["Kilometer"] != DBNull.Value)
              row["Kilometer"] = DBNull.Value;
            if (kilometerTB.Text != string.Empty && (row["Kilometer"] == DBNull.Value ||
              (decimal)row["Kilometer"] != kilometer)) row["Kilometer"] = kilometer;
          }
          catch (Exception ex)
          {
            throw new Exception("Kilometer", ex);
          }
          //Dauer_Min
          try
          {
            int dauer_Min = 0;
            if (dauerTB.Text != string.Empty) dauer_Min = int.Parse(dauerTB.Text);
            if (dauerTB.Text == string.Empty && row["Dauer_Min"] != DBNull.Value)
              row["Dauer_Min"] = DBNull.Value;
            if (dauerTB.Text != string.Empty && (row["Dauer_Min"] == DBNull.Value ||
              (int)row["Dauer_Min"] != dauer_Min)) row["Dauer_Min"] = dauer_Min;
          }
          catch (Exception ex)
          {
            throw new Exception("Dauer_Min", ex);
          }

          //Identifikationsnr
            row["LeistungID"] = Convert.ToInt32(IdentnrCB.SelectedItem.ToString());
          //VZeitraum
          try
          {
            short vZeitraum = 0;
            if (zeitraumTB.Text != string.Empty) vZeitraum = short.Parse(zeitraumTB.Text);
            if (zeitraumTB.Text == string.Empty && row["VZeitraum"] != DBNull.Value)
              row["VZeitraum"] = DBNull.Value;
            if (zeitraumTB.Text != string.Empty && (row["VZeitraum"] == DBNull.Value ||
              (short)row["VZeitraum"] != vZeitraum)) row["VZeitraum"] = vZeitraum;
          }
          catch (Exception ex)
          {
            throw new Exception("VZeitraum", ex);
          }
          //Text
          if (begruendungTB.Text == string.Empty && row["Text"] != DBNull.Value)
            row["Text"] = DBNull.Value;
          if (begruendungTB.Text != string.Empty && (row["Text"] == DBNull.Value ||
            (string)row["Text"] != begruendungTB.Text)) row["Text"] = begruendungTB.Text;
          positionenBindingSource.EndEdit();
          positionenDGV.Invalidate();
          //Tarifkennzeichen
          if (!row["Tarifkennzeichen"].Equals(posTKZTB.Text))
            row["Tarifkennzeichen"] = posTKZTB.Text;
        }
        catch (Exception ex)
        {
          string message = string.Empty;
          switch (ex.Message)
          {
            case "BehDat":
              message = ex.InnerException.Message;
              break;
            case "EPreis":
              message = ex.InnerException.Message;
              break;
            case "Eigenanteil":
              message = ex.InnerException.Message;
              break;
            case "Zuzahlung":
              message = ex.InnerException.Message;
              break;
            case "Versorgung_von":
              message = ex.InnerException.Message;
              break;
            case "Versorgung_bis":
              message = ex.InnerException.Message;
              break;
            case "Kilometer":
              message = ex.InnerException.Message;
              break;
            case "Dauer_Min":
              message = ex.InnerException.Message;
              break;
            case "VZeitraum":
              message = ex.InnerException.Message;
              break;
            default:
              message = ex.Message;
              break;
          }
          if (showPositionError && MessageBox.Show(message + "\nSoll der aktuelle Bearbeitungsvorgang abgebrochen werden?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
          {
            neuePositionBtn.Text = "&Neu";
            positionenBindingSource.CancelEdit();
            ShowCurrentPosition();
          }
          else
          {
            res = false;
            switch (ex.Message)
            {
              case "BehDat":
                tagMonatMTB.Focus();
                errorProvider.SetError(tagMonatMTB, ex.InnerException.Message);
                break;
              case "EPreis":
                ePreisTB.Focus();
                errorProvider.SetError(ePreisTB, ex.InnerException.Message);
                break;
              case "Eigenanteil":
                eigenAnteilTB.Focus();
                errorProvider.SetError(eigenAnteilTB, ex.InnerException.Message);
                break;
              case "Zuzahlung":
                zuzahlungTB.Focus();
                errorProvider.SetError(zuzahlungTB, ex.InnerException.Message);
                break;
              case "Versorgung_von":
                versorgungVonMTB.Focus();
                errorProvider.SetError(versorgungVonMTB, ex.InnerException.Message);
                break;
              case "Versorgung_bis":
                versorgungBisMTB.Focus();
                errorProvider.SetError(versorgungBisMTB, ex.InnerException.Message);
                break;
              case "Kilometer":
                kilometerTB.Focus();
                errorProvider.SetError(kilometerTB, ex.InnerException.Message);
                break;
              case "Dauer_Min":
                dauerTB.Focus();
                errorProvider.SetError(dauerTB, ex.InnerException.Message);
                break;
              case "VZeitraum":
                zeitraumTB.Focus();
                errorProvider.SetError(zeitraumTB, ex.InnerException.Message);
                break;
              default:
                if (positionenBindingSource.Count == 1)
                  tagMonatMTB.Focus();
                else
                  anzahlNUD.Focus();
                break;
            }
          }

        }
      }
      return res;
    }

    private bool IsDefaultPosition()
    {
      if (anzahlNUD.Value != 1) return false;
      if (gruppeMTB.Text != string.Empty) return false;
      if (ePreisTB.Text != string.Empty && ePreisTB.Text != (0m).ToString("N2"))
        return false;
      //if ((short)mwstCB.SelectedValue != 1) return false;
      if (eigenAnteilTB.Text != string.Empty && eigenAnteilTB.Text != (0m).ToString("N2"))
        return false;
      if (zuzahlungTB.Text != string.Empty && zuzahlungTB.Text != (0m).ToString("N" + zuzGen.ToString()))
        return false;
      //if (verbrauchLabel.Visible) return false;
      if ((short)hilfsMittelKennZeichenCB.SelectedValue != 0) return false;
      if (begruendungTB.Text != string.Empty) return false;
      if (inventarNummerTB.Text != string.Empty) return false;
      if (zeitVonMTB.Text != "  :") return false;
      if (zeitBisMTB.Text != "  :") return false;
      if (versorgungVonMTB.Text != "  .  .") return false;
      if (versorgungBisMTB.Text != "  .  .") return false;
      if (kilometerTB.Text != string.Empty) return false;
      if (dauerTB.Text != string.Empty) return false;
      if (zeitraumTB.Text != string.Empty) return false;
      return true;
    }

    private void UpdatePositionenDaten(DateTime date)
    {
      DataRow[] rows = voinDataSet.Positionen.Select("", "",
        DataViewRowState.CurrentRows);
      foreach (DataRow row in rows)
        if (row["BehDat"] == DBNull.Value || (DateTime)row["BehDat"] != date)
          row["BehDat"] = date;
    }

    private void positionenDGV_Enter(object sender, EventArgs e)
    {
      if (ValidatePosition())
        neuePositionBtn.Text = "&Neu";
    }

    private void positionenPanel_Validating(object sender, CancelEventArgs e)
    {
      if (ValidatePosition())
        neuePositionBtn.Text = "&Neu";
    }

    private void positionDeleteBtn_Click(object sender, EventArgs e)
    {
      neuePositionBtn.Text = "&Neu";
      if (positionenBindingSource.Count != 0 && positionenBindingSource.Current != null)
      {
        positionenBindingSource.RemoveCurrent();
        //IdentnrCB.Items.RemoveAt(IdentnrCB.Items.Count - 1);
        //IdentnrCB.SelectedIndex = IdentnrCB.Items.Count - 1;
      }
    }

    private void tagMonatMTB_Validating(object sender, CancelEventArgs e)
    {
      string ds = tagMonatMTB.Text + jahrCB.SelectedItem;
      DateTime rezDat = DateTime.Today;
      bool f = true;
      try { rezDat = DateTime.Parse(rezeptDatumMTB.Text); }
      catch { f = false; }
      if (f)
        try
        {
          DateTime behDat = DateTime.Parse(ds);
          if (behDat < rezDat)
            MessageBox.Show(ds + " ist kleiner als das Rezeptdatum");
        }
        catch (Exception ex)
        {
          this.BeginInvoke((MethodInvoker)delegate()
          {
            MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
          });
          e.Cancel = true;
        }
    }

    private void zuzahlungTB_Validating(object sender, CancelEventArgs e)
    {
      try { zuzahlungTB.Text = Math.Round(decimal.Parse(zuzahlungTB.Text), zuzGen, 
        MidpointRounding.AwayFromZero).ToString("N" + zuzGen.ToString()); }
      catch { zuzahlungTB.Text = 0m.ToString("N"+zuzGen.ToString()); }
      UpdatePreis(false);
    }

    private void versorgungDatum_Validating(object sender, CancelEventArgs e)
    {
      MaskedTextBox mtb = (MaskedTextBox)sender;
      try
      {
        if (mtb.Text != "  .  .")
          mtb.Text = DateTime.Parse(mtb.Text).ToString("dd.MM.yyyy");
      }
      catch (Exception ex)
      {
        e.Cancel = true;
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        this.BeginInvoke((MethodInvoker)delegate()
        {
          mtb.SelectAll();
        });
      }
    }

    private void uhrzeit_Validating(object sender, CancelEventArgs e)
    {
      MaskedTextBox mtb = (MaskedTextBox)sender;
      try
      {
        if (mtb.Text != "  :")
        {
          int h = int.Parse(Substring(mtb.Text, 0, 2));
          if (h < 0 || h > 23) throw new Exception();
          int m = int.Parse(Substring(mtb.Text, 3, 2));
          if (m < 0 || m > 59) throw new Exception();
          mtb.Text = (h < 10 ? "0" + h.ToString() : h.ToString()) + ":" + (m < 10 ?
            "0" + m.ToString() : m.ToString());
        }
      }
      catch
      {
        e.Cancel = true;
        MessageBox.Show("Die Zeichenfolge wurde nicht als gültige Uhrzeit erkannt.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        this.BeginInvoke((MethodInvoker)delegate()
        {
          mtb.SelectAll();
        });
      }
    }

    private void kilometerTB_Validating(object sender, CancelEventArgs e)
    {
      try
      {
        if (kilometerTB.Text != string.Empty)
          kilometerTB.Text = Math.Round(decimal.Parse(kilometerTB.Text), 2, MidpointRounding.AwayFromZero).ToString();
      }
      catch (Exception ex)
      {
        e.Cancel = true;
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        this.BeginInvoke((MethodInvoker)delegate()
        {
          kilometerTB.SelectAll();
        });
      }
    }

    private void dauerTB_Validating(object sender, CancelEventArgs e)
    {
      try
      {
        if (dauerTB.Text != string.Empty)
          dauerTB.Text = int.Parse(dauerTB.Text).ToString();
      }
      catch (Exception ex)
      {
        e.Cancel = true;
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        this.BeginInvoke((MethodInvoker)delegate()
        {
          dauerTB.SelectAll();
        });
      }
    }

    private void zeitraumTB_Validating(object sender, CancelEventArgs e)
    {
      try
      {
        if (zeitraumTB.Text != string.Empty)
          zeitraumTB.Text = short.Parse(zeitraumTB.Text).ToString();
      }
      catch (Exception ex)
      {
        e.Cancel = true;
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        this.BeginInvoke((MethodInvoker)delegate()
        {
          zeitraumTB.SelectAll();
        });
      }
    }

    private void posBezeichnungBtn_Click(object sender, EventArgs e)
    {
      string msg = "Gruppe: " + gruppeLabel.Text + "\nOrt: " + ortLabel.Text +
        "\nUntergruppe: " + unterGruppeLabel.Text + "\nArt: " + artLabel.Text +
        "\nProdukt: " + produktLabel.Text + "\nPos.Bez: ";
      string hmp = gruppeMTB.Text;
      hmp = hmp.Trim();
      DataRow[] rows = voinDataSet.tab_HMPBezeichnung.Select("HMP LIKE '" + hmp + "'");
      if (rows.Length != 0)
        msg += (string)rows[0]["Bezeichnung"];
      MessageBox.Show(msg, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
    }

    private void anzahlNUD_Enter(object sender, EventArgs e)
    {
      this.BeginInvoke((MethodInvoker)delegate()
      {
        anzahlNUD.Select(0, anzahlNUD.Text.Length);
      });
    }

    private void bindingNavigatorPositionItem_Enter(object sender, EventArgs e)
    {
      ValidatePosition();
      ValidateRezept(true);
    }

    private void verbrauchBtn_Click(object sender, EventArgs e)
    {
      if (positionenBindingSource.Count > 0 && positionenBindingSource.Current != null)
      {
        DataRow row = ((DataRowView)positionenBindingSource.Current).Row;
        if (row["Verbrauch"] == DBNull.Value || (short)row["Verbrauch"] == 0)
        {
          verbrauchLabel.Visible = true;
          row["Verbrauch"] = 1;
        }
        else
        {
          verbrauchLabel.Visible = false;
          row["Verbrauch"] = 0;
        }
        UpdatePreis(true);
      }
    }

    private void anzahlNUD_Validating(object sender, CancelEventArgs e)
    {
      //if (anzahlNUD.Text == string.Empty)
      //{
      //  anzahlNUD.Text = anzahlNUD.Value.ToString("N0");
      //  if (neuePositionBtn.Text == "&Abbrechen")
      //  {
      //    neuePositionBtn.Text = "&Neu";
      //    if (positionenBindingSource.Count != 0 && positionenBindingSource.Current != null)
      //      positionenBindingSource.CancelEdit();
      //    nameTB.Focus();
      //    bindingNavigatorAddNewItem_Click(bindingNavigatorAddNewItem, EventArgs.Empty);
      //  }
      //  else
      //    UpdatePreis(false);
      //}
      //else
      anzahlNUD.Text = anzahlNUD.Value.ToString("N0");
      UpdatePreis(false);
    }

    private void anzahlNUD_ValueChanged(object sender, EventArgs e)
    {
      anzahlEigenanteilLabel.Text = anzahlNUD.Value.ToString("N0") + " X";
      anzahlZuzahlungLabel.Text = anzahlNUD.Value.ToString("N0") + " X";
    }

    private void begruendungTB_Validating(object sender, CancelEventArgs e)
    {
      bool newPos = (neuePositionBtn.Text == "&Abbrechen" ? true : false);
      this.BeginInvoke((MethodInvoker)delegate()
      {
        FocusPosition(newPos);
      });
    }

    private void FocusPosition(bool newPos)
    {
      if (newPos)
      {
        showPositionError = false;
        try
        {
          AddNeuePosition();
          neuePositionBtn.Text = "&Abbrechen";
          //int size = IdentnrCB.Items.Count;
          //if (size == 0)
          //  IdentnrCB.Items.Add("1");
          //else
          //  IdentnrCB.Items.Add(Convert.ToInt32(IdentnrCB.Items[size - 1].ToString()) + 1);
          IdentnrCB.SelectedIndex = getNextFree();
        }
        finally { showPositionError = true; }
      }
      else if(ValidatePosition())
      {
        CalcGesForderung();
        if (positionenBindingSource.Count != 0)
          if (positionenBindingSource.Count - 1 > positionenBindingSource.Position)
            positionenBindingSource.Position++;
          else
            positionenBindingSource.Position = 0;
      }

      if (positionenBindingSource.Count == 1)
        tagMonatMTB.Focus();
      else
        anzahlNUD.Focus();
    }

    private void geburtsDatumMTB_Validating(object sender, CancelEventArgs e)
    {
      MaskedTextBox tb = sender as MaskedTextBox;
      if (tb.Focused)
        try
        {
          DateTime d = DateTime.Parse(tb.Text);
          if (d > DateTime.Today && tb.Text.Length < 10 && d.AddYears(-100) < DateTime.Today)
          {
            d = d.AddYears(-100);
            tb.Text = d.ToString("dd.MM.yyyy");
          }
          if (d > DateTime.Today)
            throw new Exception("Das Datum ist größer als heute.");
        }
        catch (Exception ex)
        {
          MessageBox.Show(ex.Message + " " + tb.Text, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
          e.Cancel = true;
        }
    }

    private void protokollBtn_Click(object sender, EventArgs e)
    {
      if (ValidatePosition() && ValidateRezept(false))
      {
        UpdateDataSet(false);
        if (mandantenBindingSource.Count != 0 && mandantenBindingSource.Current != null)
        {
          DataRow mandant = ((DataRowView)mandantenBindingSource.Current).Row;
          string rezepteCmd = "SELECT ID, IK, Name, Vorname, GebDat, Befreit, RezDat, Betriebsstaettennummer, ArztNr  FROM [" +
            AbrechnungsJahr + "_Rezepte] WHERE (Mandant = @Mandant) AND (Vorgang = 300) " +
            "ORDER BY IK, Name, Vorname";
          string positionenCmd = "SELECT AUTO, Faktor, EPreis, Gebuehr, Zuzahlung, " +
            "BuPos, BehDat, Eigenanteil, Anzahl, Mwst, Mwst_KZ, 0 as PosNrProtokoll FROM [" + AbrechnungsJahr +
            "_Positionen] WHERE (ID = @ID)";
          SqlDataAdapter rezepteDA = new SqlDataAdapter(rezepteCmd, lastIKConnectionString);
          rezepteDA.SelectCommand.Parameters.Add("@Mandant", SqlDbType.VarChar, 50);
          rezepteDA.SelectCommand.Parameters["@Mandant"].Value = mandant["Nr"].ToString();
          SqlDataAdapter positionenDA = new SqlDataAdapter(positionenCmd, lastIKConnectionString);
          positionenDA.SelectCommand.Parameters.Add("@ID", SqlDbType.Int);
          ReportDataSet dataSet = new ReportDataSet();
          //ListLabel ll = null;
          int PosNr = 1;

          try
          {
            rezepteDA.Fill(dataSet.Rezepte);

            foreach (DataRow rezept in dataSet.Rezepte.Rows)
            {
              DataRow[] kostenTrs = voinDataSet.Kostentraeger_300.Select("IK = '" + ((int)rezept["IK"]).ToString() + "'");
              decimal forderung = 0;
              decimal zuzahlung = 0;
              decimal forderungNetto = 0;
              decimal zuzahlungNetto = 0;
              decimal eigenanteil = 0;
              decimal eigenanteilNetto = 0;

              rezept["Mandant"] = mandant["Nr"];
              rezept["MandantenName"] = (string)mandant["Name1"] + " " +
                (string)mandant["Name2"];
              if (kostenTrs.Length != 0)
              {
                rezept["KassenName"] = (kostenTrs[0]["Name1"] != DBNull.Value ?
                  (string)kostenTrs[0]["Name1"] : string.Empty) + " " +
                  (kostenTrs[0]["Name2"] != DBNull.Value ?
                  (string)kostenTrs[0]["Name2"] : string.Empty) + " " +
                  (kostenTrs[0]["Name3"] != DBNull.Value ?
                  (string)kostenTrs[0]["Name3"] : string.Empty) + " " +
                  (kostenTrs[0]["Name4"] != DBNull.Value ?
                  (string)kostenTrs[0]["Name4"] : string.Empty);
              }
              positionenDA.SelectCommand.Parameters["@ID"].Value = (int)rezept["ID"];
              positionenDA.Fill(dataSet.Positionen);
              foreach (DataRow position in dataSet.Positionen.Rows)
              {
                decimal pf = CalcForderung(position);
                decimal pz = (decimal)position["Zuzahlung"] * (decimal)position["Anzahl"];
                decimal ea = 0;
                bool befreit = true;
                if (!rezept.IsNull("Befreit") && (bool)rezept["Befreit"])
                {
                  pf += pz;
                  pz = 0;
                  befreit = true;
                }
                decimal mwst = GetMwst(position);
                if (position["Eigenanteil"] != DBNull.Value)
                  ea = (decimal)position["Eigenanteil"] * (decimal)position["Anzahl"];
                forderung += Math.Round(pf, 2, MidpointRounding.AwayFromZero);
                zuzahlung += Math.Round(pz, 2, MidpointRounding.AwayFromZero);
                eigenanteil += Math.Round(ea, 2, MidpointRounding.AwayFromZero);
                forderungNetto += CalcForderungNetto(position, mwst, befreit);//Math.Round(pf / (1m + mwst / 100m), 2, MidpointRounding.AwayFromZero);
                zuzahlungNetto += Math.Round((decimal)position["Anzahl"] * Math.Round((decimal)position["Zuzahlung"] / (1m + mwst / 100m), 2, MidpointRounding.AwayFromZero),
                  2, MidpointRounding.AwayFromZero);
                eigenanteilNetto += Math.Round((decimal)position["Anzahl"] * 
                  Math.Round((position["Eigenanteil"] == DBNull.Value ? 0m : (decimal)position["Eigenanteil"]) / (1m + mwst / 100m), 2, MidpointRounding.AwayFromZero), 
                  2, MidpointRounding.AwayFromZero);
                    
              }
              rezept["Zuzahlung"] = zuzahlung;
              rezept["Forderung"] = forderung;
              rezept["ZuzahlungNetto"] = zuzahlungNetto;
              rezept["ForderungNetto"] = forderungNetto;
                // dpre 08.10.2010 - eigenanteil immer 0
              rezept["Eigenanteil"] = 0m; // eigenanteil;
              rezept["EigenanteilNetto"] = 0m; // eigenanteilNetto;
              /*
               * Author: rstem
               * Datum: 26.11.2014
               * PosNr zum Protokoll hinzugefügt
               */
              rezept["PosNrProtokoll"] = PosNr;
              PosNr++;
              dataSet.Positionen.Clear();
            }
            //ll = new ListLabel();
            //ll.DataSource = dataSet;
            //ll.DataMember = "Rezepte";
            //ll.AutoShowSelectFile = false;
            //ll.AutoDesignerFile = "Protokol.lst";
            //ll.AutoDestination = LlPrintMode.Export;
            //ll.Print();
            PrintProtokoll(dataSet);
          }
          //catch (LL_User_Aborted_Exception)
          //{
          //}
          catch (Exception ex)
          {
            MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
          }
          finally
          {
            if (rezepteDA != null) rezepteDA.Dispose();
            if (dataSet != null) dataSet.Dispose();
            if (positionenDA != null) positionenDA.Dispose();
            //if (ll != null) ll.Dispose();
          }
        }
      }
    }

    private decimal CalcForderungNetto(DataRow row, decimal mwst, bool befreit)
    {
      return Math.Round(((row["EPreis"] != DBNull.Value ? (decimal)row["EPreis"] : 0m) -
        Math.Round((row["Eigenanteil"] != DBNull.Value ? 
        (decimal)row["Eigenanteil"] : 0m) / (1m + mwst / 100m), 2, 
        MidpointRounding.AwayFromZero) -
        Math.Round((row["Zuzahlung"] != DBNull.Value && !befreit ? (decimal)row["Zuzahlung"] : 0m) /
        (1m + mwst / 100m), zuzGen, MidpointRounding.AwayFromZero)) *
        (row["Anzahl"] != DBNull.Value ? (decimal)row["Anzahl"] : 0m), 2, MidpointRounding.AwayFromZero);
    }

    private void PrintProtokoll(ReportDataSet dataSet)
    {
      int err = 0;
      axListLabel1.LlDefineVariableStart();
      axListLabel1.LlDefineFieldStart();
      err = axListLabel1.LlDefineFieldExt("Mandant", "70000",
        LlFieldTypeConstants.LL_NUMERIC);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("MandantenName", "Mandant",
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("IK", "103677438",
        LlFieldTypeConstants.LL_NUMERIC);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("KassenName", "Kasse",
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("Name", "Bauer",
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("Vorname", "Herbert",
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("GebDat", "12.04.1967",
        LlFieldTypeConstants.LL_DATE_DMY);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("RezDat", "12.10.2006",
        LlFieldTypeConstants.LL_DATE_DMY);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("ZuzahlungNetto", "8.55",
        LlFieldTypeConstants.LL_NUMERIC);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("ForderungNetto", "18.55",
        LlFieldTypeConstants.LL_NUMERIC);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("Zuzahlung", "10.55",
        LlFieldTypeConstants.LL_NUMERIC);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("Forderung", "23.55",
        LlFieldTypeConstants.LL_NUMERIC);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("Eigenanteil", "23.55",
        LlFieldTypeConstants.LL_NUMERIC);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("EigenanteilNetto", "23.55",
        LlFieldTypeConstants.LL_NUMERIC);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

      /*
           * Author: rstem
           * Datum: 26.11.2014
           */
      err = axListLabel1.LlDefineFieldExt("ArztNr", "999999999",
      LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

      err = axListLabel1.LlDefineFieldExt("Betriebsstaettennummer", "999999999",
      LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

      err = axListLabel1.LlDefineFieldExt("PosNr", "1",
          LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");


      err = axListLabel1.LlPrintWithBoxStart(LlProjectConstants.LL_PROJECT_LIST,
        Path.Combine(System.Windows.Forms.Application.StartupPath, "Protokoll.lst"),
        LlPrintModeConstants.LL_PRINT_USERSELECT,
        LlBoxTypeConstants.LL_BOXTYPE_BRIDGEMETER, this.Handle.ToInt32(), "");
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlPrintOptionsDialog(this.Handle.ToInt32(), "");
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      int dest = axListLabel1.LlPrintGetOption(
        LlPrintOptionConstants.LL_PRNOPT_PRINTDLG_DEST);
      err = axListLabel1.LlPrint();
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

      for (int i = 0; i < dataSet.Rezepte.Rows.Count; i++)
      {
        ReportDataSet.RezepteRow r = (ReportDataSet.RezepteRow)
          dataSet.Rezepte.Rows[i];

        err = axListLabel1.LlDefineFieldExt("Mandant", r.Mandant.ToString(),
          LlFieldTypeConstants.LL_NUMERIC);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("MandantenName", r.MandantenName,
          LlFieldTypeConstants.LL_TEXT);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("IK", r.IK.ToString(),
          LlFieldTypeConstants.LL_NUMERIC);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("KassenName", r.KassenName,
          LlFieldTypeConstants.LL_TEXT);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("Name", r.Name,
          LlFieldTypeConstants.LL_TEXT);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("Vorname", r.Vorname,
          LlFieldTypeConstants.LL_TEXT);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("GebDat", r.GebDat.ToString("dd.MM.yyyy"),
          LlFieldTypeConstants.LL_DATE_DMY);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("RezDat", r.RezDat.ToString("dd.MM.yyyy"),
          LlFieldTypeConstants.LL_DATE_DMY);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("ZuzahlungNetto", r.ZuzahlungNetto.ToString(),
          LlFieldTypeConstants.LL_NUMERIC);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("ForderungNetto", r.ForderungNetto.ToString(),
          LlFieldTypeConstants.LL_NUMERIC);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("Zuzahlung", r.Zuzahlung.ToString(),
          LlFieldTypeConstants.LL_NUMERIC);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("Forderung", r.Forderung.ToString(),
          LlFieldTypeConstants.LL_NUMERIC);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("Eigenanteil", r.Eigenanteil.ToString(),
          LlFieldTypeConstants.LL_NUMERIC);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("EigenanteilNetto", r.EigenanteilNetto.ToString(),
          LlFieldTypeConstants.LL_NUMERIC);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

        /*
           * Author: rstem
           * Datum: 26.11.2014
           */
        err = axListLabel1.LlDefineFieldExt("ArztNr", r.ArztNr.ToString(),
        LlFieldTypeConstants.LL_TEXT);
        if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

        err = axListLabel1.LlDefineFieldExt("Betriebsstaettennummer", r.Betriebsstaettennummer.ToString(),
        LlFieldTypeConstants.LL_TEXT);
        if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

        err = axListLabel1.LlDefineFieldExt("PosNr", r.PosNrProtokoll.ToString(),
      LlFieldTypeConstants.LL_TEXT);
        if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");


        while (axListLabel1.LlPrintFields() != 0)
          err = axListLabel1.LlPrint();
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlPrintSetBoxText("Drucken", (i + 1) * 100 /
          dataSet.Rezepte.Rows.Count);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      }

      while (axListLabel1.LlPrintFieldsEnd() != 0) ;
      err = axListLabel1.LlPrintEnd(0);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      if (dest == (int)LlDestinationConstants.LL_DESTINATION_PRV)
      {
        err = axListLabel1.LlPreviewDisplay("Protokoll.lst",
          System.Windows.Forms.Application.StartupPath, this.Handle.ToInt32());
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        axListLabel1.LlPreviewDeleteFiles("Protokoll.lst",
          System.Windows.Forms.Application.StartupPath);
      }
    }

    private void protokollMI_Click(object sender, EventArgs e)
    {
      if (ValidatePosition() && ValidateRezept(false))
      {
        UpdateDataSet(false);
        if (mandantenBindingSource.Count != 0 && mandantenBindingSource.Current != null)
        {
          DataRow mandant = ((DataRowView)mandantenBindingSource.Current).Row;
          string rezepteCmd = "SELECT ID, IK, Name, Vorname, GebDat, Befreit, RezDat, Betriebsstaettennummer, ArztNr  FROM [" +
            AbrechnungsJahr + "_Rezepte] WHERE (Mandant = @Mandant) AND (Vorgang = 300) " +
            "ORDER BY IK, Name, Vorname";
          string positionenCmd = "SELECT AUTO, Faktor, EPreis, Gebuehr, Zuzahlung, " +
            "BuPos, BehDat, Eigenanteil, Anzahl, Mwst, Mwst_KZ, 0 as PosNrProtokoll FROM [" + AbrechnungsJahr +
            "_Positionen] WHERE (ID = @ID)";
          SqlDataAdapter rezepteDA = new SqlDataAdapter(rezepteCmd, lastIKConnectionString);
          rezepteDA.SelectCommand.Parameters.Add("@Mandant", SqlDbType.VarChar, 50);
          rezepteDA.SelectCommand.Parameters["@Mandant"].Value = mandant["Nr"].ToString();
          SqlDataAdapter positionenDA = new SqlDataAdapter(positionenCmd, lastIKConnectionString);
          positionenDA.SelectCommand.Parameters.Add("@ID", SqlDbType.Int);
          ReportDataSet dataSet = new ReportDataSet();
          //ListLabel ll = null;
          int err = 0;
          try
          {
            rezepteDA.Fill(dataSet.Rezepte);

            foreach (DataRow rezept in dataSet.Rezepte.Rows)
            {
              DataRow[] kostenTrs = voinDataSet.Kostentraeger_300.Select("IK = '" + ((int)rezept["IK"]).ToString() + "'");
              decimal forderung = 0;
              decimal zuzahlung = 0;
              decimal eigenanteil = 0;
              decimal forderungNetto = 0;
              decimal zuzahlungNetto = 0;
              decimal eigenanteilNetto = 0;
              int PosNr = 1;

              rezept["Mandant"] = mandant["Nr"];
              rezept["MandantenName"] = (string)mandant["Name1"] + " " +
                (string)mandant["Name2"];
              if (kostenTrs.Length != 0)
              {
                rezept["KassenName"] = (kostenTrs[0]["Name1"] != DBNull.Value ?
                  (string)kostenTrs[0]["Name1"] : string.Empty) + " " +
                  (kostenTrs[0]["Name2"] != DBNull.Value ?
                  (string)kostenTrs[0]["Name2"] : string.Empty) + " " +
                  (kostenTrs[0]["Name3"] != DBNull.Value ?
                  (string)kostenTrs[0]["Name3"] : string.Empty) + " " +
                  (kostenTrs[0]["Name4"] != DBNull.Value ?
                  (string)kostenTrs[0]["Name4"] : string.Empty);
              }
              positionenDA.SelectCommand.Parameters["@ID"].Value = (int)rezept["ID"];
              positionenDA.Fill(dataSet.Positionen);
              foreach (DataRow position in dataSet.Positionen.Rows)
              {
                decimal pf = CalcForderung(position);
                decimal pz = (decimal)position["Zuzahlung"] * (decimal)position["Anzahl"];
                decimal ea = 0;
                if (position["Eigenanteil"] != DBNull.Value)
                  ea = (decimal)position["Eigenanteil"] * (decimal)position["Anzahl"];
                decimal mwst = GetMwst(position);
                forderung += pf;
                zuzahlung += pz;
                eigenanteil += ea;
                forderungNetto += pf / (1m + mwst / 100m);
                zuzahlungNetto += pz / (1m + mwst / 100m);
                eigenanteilNetto += ea / (1m + mwst / 100m);
                
              }
                // dpre 08.10.2010 - eigenanteil immer 0
              rezept["Zuzahlung"] = zuzahlung;
              rezept["Forderung"] = forderung;
              rezept["Eigenanteil"] = 0m; // eigenanteil;
              rezept["ZuzahlungNetto"] = zuzahlungNetto;
              rezept["ForderungNetto"] = forderungNetto;
              rezept["EigenanteilNetto"] = 0m; // eigenanteilNetto;
              /*
             * Author: rstem
             * Datum: 26.11.2014
             * PosNr zum Protokoll hinzugefügt
             */
              rezept["PosNrProtokoll"] = PosNr;
              PosNr++;
              dataSet.Positionen.Clear();
            }
            //ll = new ListLabel();
            //ll.DataSource = dataSet;
            //ll.DataMember = "Rezepte";
            //ll.AutoShowSelectFile = false;
            //ll.AutoDesignerFile = "Protokol.lst";
            //ll.Design();
            axListLabel1.LlDefineVariableStart();
            axListLabel1.LlDefineFieldStart();
            err = axListLabel1.LlDefineFieldExt("Mandant", "70000",
              LlFieldTypeConstants.LL_NUMERIC);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("MandantenName", "Mandant",
              LlFieldTypeConstants.LL_TEXT);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("IK", "103677438",
              LlFieldTypeConstants.LL_NUMERIC);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("KassenName", "Kasse",
              LlFieldTypeConstants.LL_TEXT);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("Name", "Bauer",
              LlFieldTypeConstants.LL_TEXT);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("Vorname", "Herbert",
              LlFieldTypeConstants.LL_TEXT);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("GebDat", "12.04.1967",
              LlFieldTypeConstants.LL_DATE_DMY);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("RezDat", "12.10.2006",
              LlFieldTypeConstants.LL_DATE_DMY);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("ZuzahlungNetto", "8.55",
              LlFieldTypeConstants.LL_NUMERIC);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("ForderungNetto", "18.55",
              LlFieldTypeConstants.LL_NUMERIC);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("Zuzahlung", "10.55",
              LlFieldTypeConstants.LL_NUMERIC);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("Forderung", "23.55",
              LlFieldTypeConstants.LL_NUMERIC);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("Eigenanteil", "23.55",
              LlFieldTypeConstants.LL_NUMERIC);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("EigenanteilNetto", "23.55",
              LlFieldTypeConstants.LL_NUMERIC);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

            /*
             * Author: rstem
             * Datum: 26.11.2014
             */
            err = axListLabel1.LlDefineFieldExt("ArztNr", "999999999",
            LlFieldTypeConstants.LL_TEXT);
            if (err < 0)
                throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

            err = axListLabel1.LlDefineFieldExt("Betriebsstaettennummer", "999999999",
            LlFieldTypeConstants.LL_TEXT);
            if (err < 0)
                throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

            err = axListLabel1.LlDefineFieldExt("PosNr", "1",
            LlFieldTypeConstants.LL_TEXT);
            if (err < 0)
                throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

            err = axListLabel1.LlDefineLayout(this.Handle.ToInt32(), "", LlProjectConstants.LL_PROJECT_LIST,
              Path.Combine(System.Windows.Forms.Application.StartupPath, "Protokoll.lst"));
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          }
          catch (Exception ex)
          {
            MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
          }
          finally
          {
            if (rezepteDA != null) rezepteDA.Dispose();
            if (dataSet != null) dataSet.Dispose();
            if (positionenDA != null) positionenDA.Dispose();
            //if (ll != null) ll.Dispose();
          }
        }
      }
    }

    private decimal GetMwst(DataRow position)
    {
      if (position["Mwst_KZ"] != DBNull.Value)
      {
        foreach (MehrWertSteuer mwst in MehrWertSteuer.MWStList)
          if (mwst.MWStCode == (short)position["Mwst_KZ"])
            return mwst.MWSt;
      }
      else
      {
        return 0;
      }
      throw new Exception("Mehrwertsteuerkennzeichne: " +
        ((short)position["Mwst_KZ"]).ToString() + "wurde nicht gefunden.\n");
    }

    private void MainForm_Shown(object sender, EventArgs e)
    {
      BuildPositionenDGVColumns();
    }

    private void iniMI_Click(object sender, EventArgs e)
    {
      if (ValidatePosition() && ValidateRezept(true))
      {
        folderBrowserDialog1.SelectedPath = VOIN10.Properties.Settings.Default.IniPath;
        if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
        {
          if (VOIN10.Properties.Settings.Default.IniPath !=
            folderBrowserDialog1.SelectedPath)
          {
            VOIN10.Properties.Settings.Default.IniPath =
              folderBrowserDialog1.SelectedPath;
            voinDataSet.Clear();
            Initialize();
          }
        }
      }

    }

    private void kvBtn_Click(object sender, EventArgs e)
    {
      try
      {
        if (ValidatePosition() && ValidateRezept(false))
        {
          UpdateDataSet(false);
          if (namesDGV.SelectedRows.Count > 0 && MessageBox.Show("Soll " + ((VOINDataSet.RezepteRow)
            ((DataRowView)namesDGV.CurrentRow.DataBoundItem).Row).Name + " einen Kostenvoranschlag erhalten?", this.Text,
            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
          {
            Kostenvoranschlag();
          }
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void Kostenvoranschlag()
    {
      //Rezept r = (Rezept)namesListBox.SelectedItem;
      //VOINDataSet.RezepteRow rr = (VOINDataSet.RezepteRow)
      //  voinDataSet.Rezepte.FindByID(r.ID);
      VOINDataSet.RezepteRow rr = (VOINDataSet.RezepteRow)((DataRowView)namesDGV.CurrentRow.DataBoundItem).Row;
      if (rr == null)
        throw new Exception("Rezept wurde nicht gefunden.");
      DataRow[] rows = voinDataSet.Kostentraeger_300.Select("IK = '" + rr.IK.Trim() + "'");
      if (rows == null || rows.Length == 0)
        throw new Exception("Kostenträger wurde nicht gefunden.");
      VOINDataSet.Kostentraeger_300Row ktr = (VOINDataSet.Kostentraeger_300Row)rows[0];
      rows = voinDataSet.Mandanten.Select("Nr = " + rr.Mandant.ToString());
      if (rows == null || rows.Length == 0)
        throw new Exception("Mandant wurde nicht gefunden.");
      VOINDataSet.MandantenRow mr = (VOINDataSet.MandantenRow)rows[0];
      using (KVAnschriftForm form = new KVAnschriftForm())
      {
        form.Name1 = (ktr.IsName1Null() ? string.Empty : ktr.Name1);
        form.Name2 = (ktr.IsName2Null() ? string.Empty : ktr.Name2);
        form.Name3 = (ktr.IsName3Null() ? string.Empty : ktr.Name3);
        form.Name4 = (ktr.IsName4Null() ? string.Empty : ktr.Name4);
        form.Strasse = (ktr.IsAns_StrasseNull() ? string.Empty : ktr.Ans_Strasse);
        form.PLZ = (ktr.IsAns_plzNull() ? string.Empty : ktr.Ans_plz);
        form.Ort = (ktr.IsAns_OrtNull() ? string.Empty : ktr.Ans_Ort);
        form.IK = ktr.IK.Trim();
        form.DataSet = voinDataSet;
        try
        {
          int.Parse(form.Strasse.Trim());
          form.Strasse = "Postfach " + form.Strasse.Trim(); 
        }
        catch
        {
        }
        if (form.ShowDialog() == DialogResult.OK)
        {
          PrintKostenVoranschlag(form.Name1, form.Name2, form.Name3, form.Name4,
            form.Strasse, form.PLZ, form.Ort, form.IK, rr, mr);
          rr.Vorgang = 150;
          UpdateDataSet(true);
        }
      }
    }

    private void PrintKostenVoranschlag(string name1, string name2, string name3,
      string name4, string strasse, string plz, string ort, string ktIK,
      VOINDataSet.RezepteRow rr, VOINDataSet.MandantenRow mr)
    {
      DataSet dataSet = new DataSet();
      SqlDataAdapter da = new SqlDataAdapter();
      SqlConnection con = new SqlConnection(lastIKConnectionString);
      SqlCommand cmd = new SqlCommand("select * from [" + AbrechnungsJahr +
        "_Positionen] where ID = " + rr.ID.ToString(), con);
      int err = 0;
      da.SelectCommand = cmd;
      try
      {
        da.Fill(dataSet);
      }
      finally
      {
        if (con != null && con.State != ConnectionState.Closed)
          con.Close();
      }
      DataTable table = dataSet.Tables[0];
      if (table.Rows.Count == 0)
        throw new Exception("Rezept enthält keine Positionen.");
      axListLabel1.LlDefineVariableStart();
      axListLabel1.LlDefineFieldStart();

      err = axListLabel1.LlDefineVariableExt("Kopf1", (mr.IsKopf1Null() ? string.Empty :
        mr.Kopf1), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("Kopf2", (mr.IsKopf2Null() ? string.Empty :
        mr.Kopf2), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("Kopf3", (mr.IsKopf3Null() ? string.Empty :
        mr.Kopf3), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("KasName1", name1,
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("KasName2", name2,
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("KasName3", name3,
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("KasName4", name4,
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("KasStrasse", strasse,
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("KasPLZ", plz,
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("KasOrt", ort,
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("Telefon", mr.Tel,
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("Fax", (mr.IsFaxNull() ? string.Empty :
        mr.Fax), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("IK", mr.IK.ToString(),
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("USTIDNR", (mr.IsUStidnrNull() ?
        string.Empty : mr.UStidnr), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("EMail", (mr.IsEMailNull() ?
        string.Empty : mr.EMail), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("HomePage", (mr.IsinetNull() ?
        string.Empty : mr.inet), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("Bank1", (mr.IsBeh_BankNull() ?
        string.Empty : mr.Beh_Bank), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("BLZ1", (mr.IsBeh_BLZNull() ? string.Empty :
        mr.Beh_BLZ), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("KTN1", (mr.IsBeh_KontoNrNull() ?
        string.Empty : mr.Beh_KontoNr), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("Zusatz1", (mr.Iszusatz1Null() ?
        string.Empty : mr.zusatz1), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("Zusatz2", (mr.Iszusatz2Null() ?
        string.Empty : mr.zusatz2), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("RezNr", AbrechnungsJahr + "/" +
        rr.ID.ToString(), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("PazName", rr.Name,
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("PazVorname", rr.Vorname,
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("GebDat", (rr.IsGebDatNull() ?
        string.Empty : rr.GebDat.ToString("dd.MM.yyyy")), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("VersNr", rr.VersNr,
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("PazStrasse", (rr.IsStrasseNull() ?
        string.Empty : rr.Strasse), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("PazPLZ", (rr.IsPlzNull() ? string.Empty :
        rr.Plz), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("PazOrt", (rr.IsOrtNull() ? string.Empty :
        rr.Ort), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("RezDat", rr.RezDat.ToString("dd.MM.yyyy"),
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("ArztNr", (rr.IsArztNrNull() ? string.Empty :
        rr.ArztNr), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("Befreit", (rr.IsBefreitNull() ||
        !rr.Befreit ? "false" : "true"), LlFieldTypeConstants.LL_BOOLEAN);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("FussZeile", (mr.IsFusszeileNull() ?
        string.Empty : mr.Fusszeile), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

      err = axListLabel1.LlDefineFieldExt("Anzahl", "2",
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("PosNr", "24.38.72.2.473",
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("EPreis", "383,55",
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("Zuzahlung", "38,55",
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("Bezeichnung", "Einzelgymnastik",
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");


      string fileName = rr.ID.ToString();
      while (fileName.Length < 8) fileName = "0" + fileName;
      fileName = "A" + fileName + "_2_" + ktIK + "_" +
        DateTime.Today.ToString("yyyyMMdd") + ".pdf";
      string pdfPath = Path.Combine(mandantenPath, mr.Nr.ToString());
      pdfPath = Path.Combine(pdfPath, DateTime.Now.ToString("yyMM"));
      pdfPath = Path.Combine(pdfPath, "Pdf");
      if (!Directory.Exists(pdfPath))
        Directory.CreateDirectory(pdfPath);

      err = axListLabel1.LlXSetParameter(LlExtensionTypeConstants.LL_LLX_EXTENSIONTYPE_EXPORT,
        "PDF", "Export.File", fileName);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlXSetParameter(LlExtensionTypeConstants.LL_LLX_EXTENSIONTYPE_EXPORT, "PDF",
        "Export.Path", pdfPath);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlXSetParameter(LlExtensionTypeConstants.LL_LLX_EXTENSIONTYPE_EXPORT,
        "PDF", "Export.Quiet", "1");
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlPrintWithBoxStart(LlProjectConstants.LL_PROJECT_LIST,
        Path.Combine(System.Windows.Forms.Application.StartupPath, "Kostenvoranschlag.lst"),
        LlPrintModeConstants.LL_PRINT_EXPORT, LlBoxTypeConstants.LL_BOXTYPE_BRIDGEMETER,
        this.Handle.ToInt32(), "Export");
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlPrintSetOptionString(LlPrintOptionStringConstants.LL_PRNOPTSTR_EXPORT, "PDF");
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

      err = axListLabel1.LlPrint();
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

      for (int i = 0; i < table.Rows.Count; i++)
      {
        DataRow r = table.Rows[i];

        decimal mwst = GetMwst(r);
        decimal ePreis = Math.Round((decimal)r["EPreis"] * (1m + mwst / 100m), 2, MidpointRounding.AwayFromZero);

        err = axListLabel1.LlDefineFieldExt("Zuzahlung", ((decimal)r["Zuzahlung"]).ToString(),
          LlFieldTypeConstants.LL_TEXT);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("Anzahl",
          ((decimal)r["Anzahl"]).ToString("N0"), LlFieldTypeConstants.LL_TEXT);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("PosNr", (string)r["BuPos"],
          LlFieldTypeConstants.LL_TEXT);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("EPreis",
          ePreis.ToString(), LlFieldTypeConstants.LL_TEXT);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        DataRow[] rows = voinDataSet.tab_HMPBezeichnung.Select("TRIM(HMP) LIKE '" +
          ((string)r["BuPos"]).Trim() + "' and Mandant = " + mr.Nr.ToString());
        string bez = string.Empty;
        if (rows != null && rows.Length > 0)
          bez = (rows[0]["Bezeichnung"] != DBNull.Value ? (string)rows[0]["Bezeichnung"] :
            string.Empty);
        err = axListLabel1.LlDefineFieldExt("Bezeichnung", bez,
          LlFieldTypeConstants.LL_TEXT);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

        while (axListLabel1.LlPrintFields() != 0)
          err = axListLabel1.LlPrint();
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlPrintSetBoxText("Drucken", (i + 1) * 100 /
          table.Rows.Count);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      }

      while (axListLabel1.LlPrintFieldsEnd() != 0) ;
      err = axListLabel1.LlPrintEnd(0);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      Process.Start(Path.Combine(pdfPath, fileName));
    }

    private void kostenvoranschlagMI_Click(object sender, EventArgs e)
    {
      try
      {
        if (ValidatePosition() && ValidateRezept(false))
        {
          UpdateDataSet(false);
          int err = 0;

          axListLabel1.LlDefineVariableStart();
          axListLabel1.LlDefineFieldStart();

          err = axListLabel1.LlDefineVariableExt("Kopf1", "Allergopharmr",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Kopf2", "Joachim Ganzer KG",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Kopf3", "Reinbek",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("KasName1", "Verband der Angestellten",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("KasName2", "Krankenkassen eV",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("KasName3", "Landesvertretung",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("KasName4", "Mecklenburg-Vorpommern",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("KasStrasse", "Wismarschenstr. 142",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("KasPLZ", "19053",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("KasOrt", "Schwerin",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Telefon", "05144 987243",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Fax", "05144 987239",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("IK", "590330566",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("USTIDNR", "",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("EMail", "info@allergopharma.de",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("HomePage", "www.allergopharma.de",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Bank1", "Commerzbank AG Reinbek",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("BLZ1", "20040000",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("KTN1", "2 640 001",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Zusatz1", "Zusatz1",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Zusatz2", "Zusatz2",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("RezNr", "06/37344",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("PazName", "Maier",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("PazVorname", "Anna",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("GebDat", "12.04.1967",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("VersNr", "238388293",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("PazStrasse", "Oberbuch 1a",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("PazPLZ", "91344",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("PazOrt", "Fuchshof",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("RezDat", "10.10.2006",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("ArztNr", "3443788",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Befreit", "true",
            LlFieldTypeConstants.LL_BOOLEAN);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("FussZeile", "Allergopharma * Joachim Ganzer KG",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

          err = axListLabel1.LlDefineFieldExt("Anzahl", "2",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineFieldExt("Zuzahlung", "20",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineFieldExt("PosNr", "24.38.72.2.473",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineFieldExt("EPreis", "383,55",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineFieldExt("Bezeichnung", "Einzelgymnastik",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

          err = axListLabel1.LlDefineLayout(this.Handle.ToInt32(), "", LlProjectConstants.LL_PROJECT_LIST,
            Path.Combine(System.Windows.Forms.Application.StartupPath, "Kostenvoranschlag.lst"));
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (tabControl1.SelectedTab == kostenvoranschlagTabPage)
      {
        FillKostJahrCB();
      }
    }

    private void FillKostJahrCB()
    {
      try
      {
        int jahr = int.Parse(AbrechnungsJahr);
        kostJahrCB.Items.Clear();
        kostJahrCB.Items.Add(jahr);
        kostJahrCB.Items.Add(jahr - 1);
        kostJahrCB.SelectedIndex = 0;
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    int searchYear = 0;

    private void kostSuchenBtn_Click(object sender, EventArgs e)
    {
      SqlConnection con = new SqlConnection(lastIKConnectionString);
      SqlCommand cmd = new SqlCommand();
      SqlDataAdapter da = new SqlDataAdapter();
      VOINDataSet.RezepteDataTable dt = new VOINDataSet.RezepteDataTable();
      try
      {
        searchYear = (int)kostJahrCB.SelectedItem;
        cmd.CommandText = "SELECT * FROM [" + kostJahrCB.SelectedItem.ToString() +
          "_Rezepte] WHERE Vorgang <> 300 AND Vorgang <> 299";
        if (genehmigungRB.Checked)
          cmd.CommandText += " AND Vorgang = 150";
        if (kostRezNrTB.Text.Trim() != string.Empty)
        {
          if (!cmd.CommandText.ToUpper().Contains("WHERE"))
            cmd.CommandText += " WHERE";
          else
            cmd.CommandText += " AND";
          cmd.CommandText += " ID LIKE '%" + kostRezNrTB.Text.Trim() + "%'";
        }
        if (kostNameTB.Text != string.Empty)
        {
          if (!cmd.CommandText.ToUpper().Contains("WHERE"))
            cmd.CommandText += " WHERE";
          else
            cmd.CommandText += " AND";
          cmd.CommandText += " Name LIKE '%" + kostNameTB.Text.ToUpper().Trim() + "%'";
        }
        if (kostVornameTB.Text != string.Empty)
        {
          if (!cmd.CommandText.ToUpper().Contains("WHERE"))
            cmd.CommandText += " WHERE";
          else
            cmd.CommandText += " AND";
          cmd.CommandText += " Vorname LIKE '%" + kostVornameTB.Text.ToUpper().Trim() + "%'";
        }
        cmd.Connection = con;
        da.SelectCommand = cmd;
        da.Fill(dt);
        DataView dv = new DataView(dt, string.Empty, "Name, Vorname, GebDat",
          DataViewRowState.CurrentRows);
        kostenvoranschlagDGV.DataSource = dv;
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      finally
      {
        if (con != null && con.State != ConnectionState.Closed)
          con.Close();
      }
    }

    private void kostenvoranschlagDGV_MouseDoubleClick(object sender, MouseEventArgs e)
    {
      DataGridView.HitTestInfo info = kostenvoranschlagDGV.HitTest(e.X, e.Y);
      if ((info.Type == DataGridViewHitTestType.Cell || info.Type ==
        DataGridViewHitTestType.RowHeader) &&
        kostenvoranschlagDGV.SelectedRows.Count > 0)
      {
        DataRow row = ((DataRowView)((DataGridViewRow)
          kostenvoranschlagDGV.SelectedRows[0]).DataBoundItem).Row;
        bool doIt = false;
        if (genehmigungRB.Checked && MessageBox.Show("Genehmigung für diesen Patienten eingeben?", this.Text,
          MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
          doIt = true;
        if (korrekturRB.Checked)
        {
          using (KorrekturTypeForm form = new KorrekturTypeForm())
          {
            if (form.ShowDialog() == DialogResult.OK)
              doIt = true;
          }
        }
        if (doIt)
        {
          try
          {
            int id = (int)row["ID"];
            if (int.Parse(AbrechnungsJahr) != searchYear)
            {
              id = CopyRezept((int)row["ID"], searchYear, 300);
            }
            else
            {
              SetVorgang(row, searchYear, 300);
            }
            int p = mandantenBindingSource.Find("Nr", (int)row["Mandant"]);
            if (p < 0)
              throw new Exception("Mandant wurde nicht gefunden.");
            if (mandantenBindingSource.Position != p)
              mandantenBindingSource.Position = p;
            else
              LoadMandanten((int)row["Mandant"]);
            p = rezepteBindingSource.Find("ID", id);
            if (p < 0)
              throw new Exception("Rezept wurde nicht gefunden.");
            rezepteBindingSource.Position = p;
            tabControl1.SelectedTab = rezepteTabPage;
            if (rezepteBindingSource.IsBindingSuspended)
              rezepteBindingSource.ResumeBinding();
            kostenvoranschlagDGV.DataSource = null;
          }
          catch (Exception ex)
          {
            MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK,
              MessageBoxIcon.Error);
          }
        }

      }
    }

    private int CopyRezept(int id, int searchYear, int vorgang)
    {
      int res = 0;
      SqlDataAdapter rezDA = new SqlDataAdapter();
      DataTable rezTbl = new DataTable();
      SqlDataAdapter posDA = new SqlDataAdapter();
      DataTable posTbl = new DataTable();
      rezDA.SelectCommand = new SqlCommand("select * from [" + searchYear.ToString() +
        "_Rezepte] where id = @Original_ID", rezepteDA.SelectCommand.Connection);
      rezDA.SelectCommand.Parameters.Add("@Original_ID", SqlDbType.Int);
      rezDA.SelectCommand.Parameters["@Original_ID"].Value = id;
      rezDA.InsertCommand = rezepteDA.InsertCommand.Clone();
      posDA.SelectCommand = positionenDA.SelectCommand.Clone();
      posDA.SelectCommand.Parameters["@ID"].Value = id;
      posDA.InsertCommand = positionenDA.InsertCommand.Clone();
      posDA.SelectCommand.CommandText =
        posDA.SelectCommand.CommandText.Replace("[" + AbrechnungsJahr + "_",
        "[" + searchYear.ToString() + "_");
      try
      {
        rezDA.Fill(rezTbl);
        posDA.Fill(posTbl);
        if (rezTbl.Rows.Count == 0)
          throw new Exception("Rezept wurde nicht gefunden.");
        rezTbl.Rows[0].SetAdded();
        rezTbl.Rows[0]["Vorgang"] = vorgang;
        rezTbl.Rows[0]["Erfasst"] = DateTime.Today;
        foreach (DataRow r in posTbl.Rows)
          r.SetAdded();
        rezDA.Update(rezTbl);
        int nID = (int)rezTbl.Rows[0]["ID"];
        foreach (DataRow r in posTbl.Rows)
          r["ID"] = nID;
        posDA.Update(posTbl);
        res = (int)rezTbl.Rows[0]["ID"];
      }
      finally
      {
        if (rezDA.SelectCommand.Connection.State != ConnectionState.Closed)
          rezDA.SelectCommand.Connection.Close();
        if (posDA.SelectCommand.Connection.State != ConnectionState.Closed)
          posDA.SelectCommand.Connection.Close();
        rezDA.Dispose();
        rezTbl.Dispose();
        posDA.Dispose();
        posTbl.Dispose();
      }
      return res;
    }

    private void SetVorgang(DataRow row, int year, int vorgang)
    {
      SqlConnection con = new SqlConnection(lastIKConnectionString);
      SqlCommand cmd = new SqlCommand("UPDATE [" + year.ToString() + "_Rezepte] SET " +
        "Vorgang = " + vorgang.ToString() + " WHERE ID = " + ((int)row["ID"]).ToString(),
        con);
      try
      {
        con.Open();
        cmd.ExecuteNonQuery();
      }
      finally
      {
        if (con != null && con.State != ConnectionState.Closed)
          con.Close();
      }
    }

    private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
    {
      if (e.TabPage != rezepteTabPage && (!ValidatePosition() || !ValidateRezept(true)))
      {
        e.Cancel = true;
        splitContainer1.Panel2.Select();
      }
    }

    private void eigenanteilCB_CheckedChanged(object sender, EventArgs e)
    {
      if (eigenanteilCB.Checked) eigenanteilCB.Text = "Eigenanteil";
      else eigenanteilCB.Text = "Übernahme";
      decimal ePreis = 0;
      decimal eigenAnteil = 0;
      try { ePreis = decimal.Parse(ePreisTB.Text); }
      catch { }
      try
      {
        eigenAnteil = decimal.Parse(eigenAnteilTB.Text);
      }
      catch { }
      eigenAnteil = ePreis - eigenAnteil;
      // dpre 08.10.2010 - eigenanteil immer 0
      eigenAnteilTB.Text = ((decimal)0).ToString("N2"); // eigenAnteil.ToString("N2");
    }

    private void sortCB_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (sortCB.SelectedIndex == 0)
      {
        rezepteBindingSource.Sort = "IK";
      }
      else if (sortCB.SelectedIndex == 1)
      {
        rezepteBindingSource.Sort = "Name, Vorname";
      }
      else if (sortCB.SelectedIndex == 2)
      {
        rezepteBindingSource.Sort = "ID";
      }
    }

    private VOINDataSet.RezepteRow currentRezept = null;
    private bool curRezChanging = false;

    private void rezepteBindingSource_CurrentChanged(object sender, EventArgs e)
    {
      if (!curRezChanging)
      {
        curRezChanging = true;
        
        try
        {
          VOINDataSet.RezepteRow newR = null;
          if (rezepteBindingSource.Count > 0 && rezepteBindingSource.Position >= 0)
          {
            newR = (VOINDataSet.RezepteRow)((DataRowView)rezepteBindingSource.Current).Row;
          }
          if (newR != currentRezept)
          {
            currentRezept = newR;
            if (currentRezept != null)
            {
              this.BeginInvoke((MethodInvoker)delegate()
              {
                if (currentRezept != null)
                {
                  if (currentRezept.IsHilfsmittelNull() || currentRezept.Hilfsmittel == -1)
                    currentRezept.Hilfsmittel = 0;
                  if (currentRezept.IsPauschaleNull() || currentRezept.Pauschale != 0)
                    currentRezept.Pauschale = 0;

             

                    // dpre 08.10.2010 - image immer mit empty.tif vorbelegen

                  if (currentRezept.IsImageNull()) //currentRezept.IsImageNull()
                  {
                      //hhh2
                      //currentRezept.Image = Path.Combine(strScanOrdner, "Scannen01128.tif");
                      /* change */
                     
                      int index = lstImages.SelectedIndex;
                      if (!newstart)
                      {
                          index++;
                         
                      }
                      newstart = false;
                      
                      currentRezept.Image = Path.Combine(strScanOrdner, lstImages.GetItemText(lstImages.Items[index]));
                      int tmp = lstImages.SelectedIndex;
                      // currentRezept.Image = Path.Combine(strScanOrdner, "empty2.tif");

                  }
               
                         if (currentRezept.IsVorderseiteNull())
                        currentRezept.Vorderseite = Path.Combine(strScanOrdner, "empty.tif");
                    if (currentRezept.IsHinterseiteNull())
                        currentRezept.Hinterseite = Path.Combine(strScanOrdner, "empty.tif");
                    

                   

                  if (currentRezept.IsImageNull())
                  {
                    imgTB.Text = string.Empty;
                    picImage2.Image = null;
                    lstImages.ClearSelected();
                  }
                  else
                  {
                      //imgTB.Text = lstImages.SelectedItem.ToString();
                      //hhh1
                      //lstImages.SelectedItem = Path.GetFileName(currentRezept.Image);
                      imgTB.Text = Path.GetFileName(currentRezept.Image);
                    try
                    {
                       picImage.Load(Path.Combine(strScanOrdner, imgTB.Text));
                   
                      int currW = picImage.Image.Width;
                      int currH = picImage.Image.Height;
                      int destW = picImage2.Width;
                      int destH = picImage2.Height;
                      double multiplier = 1;

                      if (currH > currW)
                      { // portrait
                        if (destH > destW)
                            multiplier = (double)destW / (double)currW;
                        else
                            multiplier = (double)destH / (double)currH;
                      }
                      else
                      { // landscape
                        if (destH > destW)
                            multiplier = (double)destW / (double)currW;
                        else
                            multiplier = (double)destH / (double)currH;
                      }
                      picImage2.Image = new Bitmap(picImage.Image, (int)(currW * multiplier), (int)(currH * multiplier));
                     // picImage2.Image = new Bitmap(picImage.Image, (int)(currW * multiplier*2), (int)(currH * multiplier*2));
                      //tempPictureBox.Size = new System.Drawing.Size(100, 100);
                     // picImage2.Image = new Bitmap(;
                     
                     lstImages.SelectedItem = imgTB.Text;
                       
                       
                    }
                    catch (Exception ex)
                    {
                      picImage2.Image = null;
                    }
                  }
                }
              });
            }
          }
        }
        catch (Exception ex)
        {
          MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK,
            MessageBoxIcon.Error);
        }
        finally
        {
          curRezChanging = false;
        }
      }
      
    }
     

    private void patientenSuchenBtn_Click(object sender, EventArgs e)
    {
      if (rezepteBindingSource.Count > 0 && rezepteBindingSource.Position >= 0)
      {
        VOINDataSet.RezepteRow crez = (VOINDataSet.RezepteRow)
          ((DataRowView)rezepteBindingSource.Current).Row;
        PatientenSuchen.PatientenSuchenForm form = null;
        try
        {
          form = new VOIN10.PatientenSuchen.PatientenSuchenForm();
          form.ConnectionString = lastIKConnectionString;
          form.Year = int.Parse(AbrechnungsJahr);
          form.SuchParam = nameTB.Text;
          if (form.ShowDialog(this) == DialogResult.OK && form.Rezept != null)
          {
            PatientenSuchen.PatientenSuchenDataSet.RezepteRow rez = form.Rezept;
            crez.Name = rez.Name.ToUpper();
            crez.Vorname = rez.Vorname.ToUpper();
            crez.GebDat = rez.GebDat;
            //crez.VK_Bis = rez.VK_Bis;
            crez.Strasse = rez.Strasse;
            crez.Plz = rez.Plz;
            crez.Ort = rez.Ort;
            crez.VersNr = rez.VersNr;
            crez.Status = rez.Status;
            crez.KD_RechNr = rechNrTB.Text.ToString();
            crez.KD_Auftrag = imageTB.Text.ToString();
            if (rez.Status.Length > 0)
              status1TB.Text = rez.Status.Substring(0, 1);
            if (rez.Status.Length >= 5)
              status2TB.Text = rez.Status.Substring(4, 1);
            crez.ArztNr = rez.ArztNr;
            rezepteBindingSource.ResetCurrentItem();
          }
        }
        catch (Exception ex)
        {
          MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        finally
        {
          if (form != null)
            form.Dispose();
        }
        nameTB.Focus();
      }

    }

    private void befreitCheckBox_CheckedChanged(object sender, EventArgs e)
    {
      zuzahlungTB.ReadOnly = befreitCheckBox.Checked;
    }

    private void versichertenNummerMTB_Validating(object sender, CancelEventArgs e)
    {
      versichertenNummerMTB.Text = ConvertToNumber(versichertenNummerMTB.Text);
    }

    private void arztNummerTB_Validating(object sender, CancelEventArgs e)
    {
      arztNummerTB.Text = ConvertToNumber(arztNummerTB.Text);
        temparztNummerTB=arztNummerTB.Text;
      //dpre 23.06.2010 Arztnr in dbo.ArztBetrNr suchen
      LoadArztnummern(arztNummerTB.Text, false, false);
    }

    private void verordnungMI_Click(object sender, EventArgs e)
    {
      try
      {
        if (ValidatePosition() && ValidateRezept(false))
        {
          UpdateDataSet(false);
          int err = 0;

          axListLabel1.LlDefineVariableStart();
          axListLabel1.LlDefineFieldStart();

          err = axListLabel1.LlDefineVariableExt("IK", "123456789",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Zuzahlung", (5.35m).ToString("N2"),
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("GesBrutto", (53.5m).ToString("N2"),
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("PosNr1", "0000000000",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("PosNr2", "1111111111",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("PosNr3", "2222222222",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Faktor1", "1",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Faktor2", "1",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Faktor3", "1",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Taxe1", (1.78m).ToString("N2"),
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Taxe2", (1.78m).ToString("N2"),
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Taxe3", (1.79m).ToString("N2"),
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

          err = axListLabel1.LlDefineLayout(this.Handle.ToInt32(), "", LlProjectConstants.LL_PROJECT_LABEL,
            Path.Combine(System.Windows.Forms.Application.StartupPath, "Verordnung.lbl"));
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void printVOBtn_Click(object sender, EventArgs e)
    {
      try
      {
        if (ValidatePosition() && ValidateRezept(false) &&
          rezepteBindingSource.Count > 0 && rezepteBindingSource.Position >= 0)
        {
          VOINDataSet.RezepteRow rezept = (VOINDataSet.RezepteRow)
            ((DataRowView)rezepteBindingSource.Current).Row;
          UpdateDataSet(false);
          VOINDataSet.PositionenRow[] prows = (VOINDataSet.PositionenRow[])
            voinDataSet.Positionen.Select("ID = " + rezept.ID.ToString());
          int err = 0;

          axListLabel1.LlDefineVariableStart();
          axListLabel1.LlDefineFieldStart();

          err = axListLabel1.LlDefineVariableExt("IK", rezept.MandantenRow.IK.ToString(),
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          decimal zuzahlung = 0;
          for (int i = 0; i < prows.Length; i++)
            if (!prows[i].IsZuzahlungNull())
              zuzahlung += Math.Round(prows[i].Zuzahlung * prows[i].Anzahl, 2, MidpointRounding.AwayFromZero);
          err = axListLabel1.LlDefineVariableExt("Zuzahlung", zuzahlung.ToString("N2"),
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          decimal gesBrutto = 0;
          for (int i = 0; i < prows.Length; i++)
          {
            if (!prows[i].IsMwstNull())
              gesBrutto += prows[i].Mwst * prows[i].Anzahl;
            gesBrutto += prows[i].EPreis * prows[i].Anzahl;
          }
          err = axListLabel1.LlDefineVariableExt("GesBrutto", gesBrutto.ToString("N2"),
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          string posNr1 = string.Empty;
          if (prows.Length > 0)
            posNr1 = prows[0].BuPos;
          err = axListLabel1.LlDefineVariableExt("PosNr1", posNr1,
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          string posNr2 = string.Empty;
          if (prows.Length > 1)
            posNr2 = prows[1].BuPos;
          err = axListLabel1.LlDefineVariableExt("PosNr2", posNr2,
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          string posNr3 = string.Empty;
          if (prows.Length > 2)
            posNr3 = prows[2].BuPos;
          err = axListLabel1.LlDefineVariableExt("PosNr3", posNr3,
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Faktor1", (prows.Length > 0 ? prows[0].Anzahl.ToString("N0") : string.Empty),
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Faktor2", (prows.Length > 1 ? prows[1].Anzahl.ToString("N0") : string.Empty),
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Faktor3", (prows.Length > 2 ? prows[2].Anzahl.ToString("N0") : string.Empty),
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Taxe1", (prows.Length > 0 ?
            (prows[0].EPreis + (prows[0].IsMwstNull() ? 0 : prows[0].Mwst)).ToString("N2") : string.Empty),
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Taxe2", (prows.Length > 1 ?
            (prows[1].EPreis + (prows[1].IsMwstNull() ? 0 : prows[1].Mwst)).ToString("N2") : string.Empty),
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Taxe3", (prows.Length > 2 ?
            (prows[2].EPreis + (prows[2].IsMwstNull() ? 0 : prows[2].Mwst)).ToString("N2") : string.Empty),
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

          err = axListLabel1.LlPrintWithBoxStart(LlProjectConstants.LL_PROJECT_LABEL,
            Path.Combine(System.Windows.Forms.Application.StartupPath, "Verordnung.lbl"),
            LlPrintModeConstants.LL_PRINT_USERSELECT,
            LlBoxTypeConstants.LL_BOXTYPE_BRIDGEMETER, this.Handle.ToInt32(), "");
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlPrintOptionsDialog(this.Handle.ToInt32(), "");
          if (err < 0)
          {
            if (err != -99)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            else
              throw new UserAbortedException();
          }
          int dest = axListLabel1.LlPrintGetOption(
            LlPrintOptionConstants.LL_PRNOPT_PRINTDLG_DEST);
          err = axListLabel1.LlPrint();
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlPrintEnd(0);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          if (dest == (int)LlDestinationConstants.LL_DESTINATION_PRV)
          {
            err = axListLabel1.LlPreviewDisplay("Verordnung.lbl",
              System.Windows.Forms.Application.StartupPath, this.Handle.ToInt32());
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            axListLabel1.LlPreviewDeleteFiles("Verordnung.lbl",
              System.Windows.Forms.Application.StartupPath);
          }

        }
      }
      catch (UserAbortedException)
      {
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }

    }

    private void posVerwaltungMI_Click(object sender, EventArgs e)
    {
      PosVerwaltung.PosVerwaltungForm form = null;
      if (mandantenBindingSource.Count > 0 && mandantenBindingSource.Position >= 0)
      {
        try
        {
          form = new VOIN10.PosVerwaltung.PosVerwaltungForm();
          form.Mandant = (int)((DataRowView)mandantenBindingSource.Current)["Nr"];
          form.MTarifKZ = (string)((DataRowView)mandantenBindingSource.Current)["Tarifkennzeichen"];
          form.ConnectionString = stamm4ConnectionString;
          DataRow[] rows = voinDataSet.Kostentraeger_Tarife.Select("IK = '" + ikTB.Text + "'");
          if (rows.Length > 0)
          {
            form.KTarif = (rows[0].IsNull("TarifSchluessel") ? string.Empty : 
              ((string)rows[0]["TarifSchluessel"]).Substring(0, 4));
          }
          form.ShowDialog(this);
          voinDataSet.BeginInit();
          voinDataSet.tab_HMPBezeichnung.BeginLoadData();
          voinDataSet.tab_HMPBezeichnung.Clear();
          tab_HMPBezeichnungDA.Fill(voinDataSet.tab_HMPBezeichnung);
        }
        catch (Exception ex)
        {
          MessageBox.Show(ex.Message + "\r\n" + ex.StackTrace, this.Text, MessageBoxButtons.OK,
            MessageBoxIcon.Error);
        }
        finally
        {
          voinDataSet.tab_HMPBezeichnung.EndLoadData();
          voinDataSet.EndInit();
          if (form != null)
            form.Dispose();
          if (stamm4SqlConnection.State != ConnectionState.Closed)
            stamm4SqlConnection.Close();
        }
      }
    }

    private void IdentnrCB_SelectedIndexChanged(object sender, EventArgs e)
    {
    //  DataRow rezept = ((DataRowView)rezepteBindingSource.Current).Row;
    //  DataRow[] pos = rezept.GetChildRows("FK_Rezepte_Positionen");
    //  int[] counter = new int[IdentnrCB.Items.Count];
    //  foreach (DataRow p in pos)
    //  {
    //    //if (p == ((DataRowView)positionenBindingSource.Current).Row)
    //    {
          
    //      String ident = p["LeistungID"].ToString();
    //      for (int i = 0; i < IdentnrCB.Items.Count; i++)
    //      {
    //        if (((String)IdentnrCB.Items[i]).Equals(ident))
    //        {
    //          counter[i]++;
    //        }
    //      }
    //    }
    //  }
    //  bool tmp = false;
    //  for (int i = 0; i < counter.Length; i++)
    //  {
    //    if (counter[i] > 1)
    //      if (tmp)
    //        IdentnrCB.Items.RemoveAt(IdentnrCB.Items.Count - 1);
    //      else
    //        tmp = true;
    //  }
    }

    private int getNextFree()
    {
      DataRow rezept = ((DataRowView)rezepteBindingSource.Current).Row;
      DataRow[] pos = rezept.GetChildRows("FK_Rezepte_Positionen");
      int[] counter = new int[IdentnrCB.Items.Count];
      foreach (DataRow p in pos)
      {
        String ident = p["LeistungID"].ToString();
        for (int i = 0; i < IdentnrCB.Items.Count; i++)
        {
          if (((String)IdentnrCB.Items[i]).Equals(ident))
          {
            counter[i]++;
          }
        }        
      }
      for (int i = 0; i < counter.Length; i++)
      {
        if (counter[i] == 0)
          return i;   
      }

      return counter.Length;
    }

      private void einzelnCB_Click(object sender, EventArgs e)
      {
          CheckBox cb = (sender as CheckBox);
          label41.Visible = rechTB.Visible = cb.TabStop = cb.Checked;
          if (cb.Checked)
          {
            mitRechNrCB.Checked = label16.Visible = label17.Visible = rechNrTB.Visible = imageTB.Visible = cb.TabStop = false;
            if (rechTB.Text.Length == 0)
              rechTB.Text = "1";
          }
      }
      private void dauerCB_Click(object sender, EventArgs e)
      {
          CheckBox cb = (sender as CheckBox);
          zeitVonMTB.TabStop = zeitBisMTB.TabStop = dauerTB.TabStop = cb.Checked;
      }

      private void zeitraumCB_Click(object sender, EventArgs e)
      {
          CheckBox cb = (sender as CheckBox);
          versorgungVonMTB.TabStop = versorgungBisMTB.TabStop = zeitraumTB.TabStop = cb.Checked;
      }
    private void imgCB_Click(object sender, EventArgs e)
    {
      CheckBox cb = (sender as CheckBox);      
      label40.Visible = imgTB.Visible = cb.TabStop = cb.Checked;
    }

    private void lstImages_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        
        if (lstImages.SelectedItem != null)
           
      {
          
        imgTB.Text = lstImages.SelectedItem.ToString();
        
                               
        try
        {
          picImage.Load(Path.Combine(strScanOrdner, imgTB.Text));
          int currW = picImage.Image.Width;
          int currH = picImage.Image.Height;
          int destW = picImage2.Width;
          int destH = picImage2.Height;
          double multiplier = 1;

          if (currH > currW)
          { // portrait
            if (destH > destW)
              multiplier = (double)destW / (double)currW;
            else
              multiplier = (double)destH / (double)currH;
          }
          else
          { // landscape
            if (destH > destW)
              multiplier = (double)destW / (double)currW;
            else
              multiplier = (double)destH / (double)currH;
          }
          picImage2.Image = new Bitmap(picImage.Image, (int)(currW * multiplier), (int)(currH * multiplier));
        }
        catch
        {
          picImage2.Image = null;
        }
        
        //picImage2.Load(Path.Combine(strScanOrdner, imgTB.Text));
      }
      else
        imgTB.Text = "";
    }

    private void btnArtznrSuchen_Click(object sender, EventArgs e)
    {
      try
      {
        if (frmArztnr != null)
        {
          frmArztnr.SetSelected(vkBisTB.Text);
          frmArztnr.ShowDialog(this);
          if (frmArztnr.ApplyClicked)
            vkBisTB.Text = frmArztnr.Arztnummer;
          vkBisTB.Focus();
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(this, ex.Message, "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }


    /// <summary>
    /// Speichert neue Arztnummern in dbo.ArztBetrNr
    /// </summary>
    private void SaveArztnummer()
    {
      try
      {
        if (vkBisTB.Text.Length == 9 && arztNummerTB.Text.Length == 9 && vkBisTB.Text != "999999999" && arztNummerTB.Text != "999999999")
        {
          voinDataSet.ArztBetrNr.Clear();
          arztBetrNrDA.SelectCommand.Parameters.Clear();
          arztBetrNrDA.SelectCommand.Parameters.Add("@Betrnr", SqlDbType.VarChar, 9);
          arztBetrNrDA.SelectCommand.Parameters["@Betrnr"].Value = arztNummerTB.Text;
          arztBetrNrDA.Fill(voinDataSet.ArztBetrNr);
          DataRow[] rows = voinDataSet.ArztBetrNr.Select("Arztnr = " + vkBisTB.Text);
          if (rows.Length == 0)
          { // arztnr / betrnr paar nicht gefunden, neu anlegen
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "INSERT INTO ArztBetrNr (Arztnr, Betrnr) VALUES ('" + vkBisTB.Text + "', '" + arztNummerTB.Text + "')";
            cmd.Connection = lastIKsqlConnection;
            bool bCloseAfter = false;
            if (lastIKsqlConnection.State == ConnectionState.Closed)
            {
              bCloseAfter = true;
              lastIKsqlConnection.Open();
            }
            cmd.ExecuteNonQuery();
            if (bCloseAfter)
              lastIKsqlConnection.Close();
            LoadArztnummern(arztNummerTB.Text, false, true);
          }
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(this, ex.Message, "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    /* 
     * Author: rstem
     * Datum: 17.12.2014
     * Löschen der Verbindung zwischen einem Arzt und einer Betriebsstätte
     */
    public void delArtznummerBetriebBerbindung(String Arztnummer)
    {
        try
        {
            if (Arztnummer != "")
            {

                    System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = "DELETE FROM ArztBetrNr Where Arztnr = '" + Arztnummer + "' and Betrnr = '" + arztNummerTB.Text + "'";
                    cmd.Connection = lastIKsqlConnection;
                    bool bCloseAfter = false;
                    if (lastIKsqlConnection.State == ConnectionState.Closed)
                    {
                        bCloseAfter = true;
                        lastIKsqlConnection.Open();
                    }
                    cmd.ExecuteNonQuery();
                    if (bCloseAfter)
                        lastIKsqlConnection.Close();
                    LoadArztnummern(arztNummerTB.Text, false, false);
            }
        }
        catch (Exception ex)
        {
            MessageBox.Show(this, ex.Message, "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }


    /// <summary>
    /// Läd die Arztnummer für die angegebene Betriebsstättennummer aus dbo.ArztBetrNr und fügt wenn diese in vkBisTB ein,
    /// wenn es ein neues Rezept ist oder keine Arztnummer eingegeben wurde.
    /// </summary>
    /// <param name="Betrnr"></param>
    /// <param name="RezeptIsNew"></param>
    private void LoadArztnummern(string Betrnr, bool RezeptIsNew, bool ShowButtonOnly)
    {
      try
      {
        if (rezepteBindingSource.Count > 0 && rezepteBindingSource.Current != null && rezepteBindingSource.DataMember != string.Empty)
        {
          DataRowView drv = (DataRowView)rezepteBindingSource.Current;
          if (drv.Row.RowState == DataRowState.Detached || drv.Row.RowState == DataRowState.Added)
            RezeptIsNew = true;
        }

        voinDataSet.ArztBetrNr.Clear();
        arztBetrNrDA.SelectCommand.Parameters.Clear();
        arztBetrNrDA.SelectCommand.Parameters.Add("@Betrnr", SqlDbType.VarChar, 9);
        arztBetrNrDA.SelectCommand.Parameters["@Betrnr"].Value = Betrnr;
        arztBetrNrDA.Fill(voinDataSet.ArztBetrNr);

        // bindingsource wird nicht benötigt
        /*((ISupportInitialize)arztBetrNrBindingSource).BeginInit();
        arztBetrNrBindingSource.DataSource = voinDataSet;
        arztBetrNrBindingSource.DataMember = "ArztBetrNr";
        ((ISupportInitialize)arztBetrNrBindingSource).EndInit();*/

        if (voinDataSet.ArztBetrNr.Rows.Count > 1)
        {
          // neue form mit anzeige der arztnummern basteln
          if (frmArztnr == null)
            frmArztnr = new frmArztnummern();
          frmArztnr.ClearArztNr();
          frmArztnr.SetInfo("Folgende Arztnummern wurden für die Bestriebsstätte " + Betrnr + " gefunden");

          foreach (DataRow row in voinDataSet.ArztBetrNr.Rows)
          {
            frmArztnr.AddArztNr(row["Arztnr"].ToString());
          }

          btnArtznrSuchen.Visible = true;
        }
        else
          btnArtznrSuchen.Visible = false;

        if (voinDataSet.ArztBetrNr.Rows.Count > 0 && !ShowButtonOnly)
        {
          if (vkBisTB.Text.Length == 0 || vkBisTB.Text == "999999999")
            vkBisTB.Text = voinDataSet.ArztBetrNr.Rows[0]["Arztnr"].ToString();
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(this, ex.Message, "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }
    frmArztnummern frmArztnr;

      private void tmrNextImage_Tick(object sender, EventArgs e)
      {
          tmrNextImage.Stop();
          try
          {
              if (lstImages.Enabled)
              {
                  int tmp=lstImages.SelectedIndex;
                 // lstImages.SelectedIndex++;

                  lstImages_SelectedIndexChanged(null, EventArgs.Empty);
              }
          }
          catch { }
      }

      /// <summary>
      /// Prüft die Modulo11 Prüfziffer der PZN
      /// </summary>
      private bool ModuloCheck(string PZN)
      {
        
          int intValue = int.Parse(PZN.Substring(0, 1));

          int y;
          for (int i = 2; i <= 7; i++)
          {
              y = int.Parse(PZN.Substring(i - 1, 1));
              intValue = intValue + y * (i);

          }

         
          int check;
          check = intValue % 11;
          int X = int.Parse(PZN.Substring(PZN.Length - 1, 1));
          if (check == X)
              return true;
          return false;
      }

      private void namesDGV_CellContentClick(object sender, DataGridViewCellEventArgs e)
      {

      }

      private void ImgAktualisierenBtn_Click(object sender, EventArgs e)
      {
          UpdateImageList();
      }

      private void pnlRezeptEingabe_Paint(object sender, PaintEventArgs e)
      {

      }

      private void picImage2_Click(object sender, EventArgs e)
      {

      }

      private void positionenPanel_Paint(object sender, PaintEventArgs e)
      {

      }

      /*
       * Author: rstem
       * Datum: 24.11.2014
       * Automatische Suche ab erster Eingabe eines Zeichens
       */
      private void arztNummerTB_KeyUp(object sender, KeyEventArgs e)
      {
          if (e.KeyCode != Keys.Back)
          {
              if (arztNummerTB.Text == "999999999")
              {
                  arztNummerTB.SelectionStart = 0;
                  arztNummerTB.SelectionLength = arztNummerTB.Text.Length;
              }
              else
              {
                  int tmpZeichenAnzahl = arztNummerTB.Text.Length;

                  arztBetrNrSucheDA.SelectCommand.Parameters.Clear();
                  arztBetrNrSucheDA.SelectCommand.Parameters.Add("@Betrnr", SqlDbType.VarChar, 9);
                  if (arztNummerTB.Text.Length < 9)
                      arztBetrNrSucheDA.SelectCommand.Parameters["@Betrnr"].Value = arztNummerTB.Text + "%";
                  else
                      arztBetrNrSucheDA.SelectCommand.Parameters["@Betrnr"].Value = arztNummerTB.Text;
                  voinDataSet.EnforceConstraints = false;
                  arztBetrNrSucheDA.Fill(voinDataSet.ArztBetrNr);

                  DataRow[] rowBetrSt = voinDataSet.ArztBetrNr.Select("Betrnr Like '" + arztNummerTB.Text + "%'");

                  if (rowBetrSt.Length != 0)
                  {
                      arztNummerTB.Text = rowBetrSt[0].ItemArray[2].ToString();
                      arztNummerTB.SelectionStart = tmpZeichenAnzahl;
                      if (arztNummerTB.Text.Length - tmpZeichenAnzahl >= 0)
                      {
                          arztNummerTB.SelectionLength = arztNummerTB.Text.Length - tmpZeichenAnzahl;
                      }
                      else
                      {
                          arztNummerTB.SelectionLength = arztNummerTB.Text.Length;
                      }

                  }
              }
          }

      }

      private bool existsSonderPZN(string buPos)
      {
          bool res = false;

          SqlConnection sqlConnection1 = new SqlConnection(lastIKConnectionString);
          try 
          {              
              SqlCommand cmd = new SqlCommand();
              SqlDataReader reader;

              cmd.CommandText = "Select * from SonderPZN WHERE SPZN = '" + buPos + "';";
              cmd.CommandType = CommandType.Text;
              cmd.Connection = sqlConnection1;
          
              sqlConnection1.Open();

              reader = cmd.ExecuteReader();
              if (reader.HasRows)
                  res = true;
              // Data is accessible through the DataReader object here.
              logger.Debug("Query='{0}', found='{1}'", cmd.CommandText, res);
              sqlConnection1.Close();
          }
          catch (Exception ex)
          {
              logger.Error("error check SonderPZN", ex);
          }
          return res;
      }

  }

  public class UserAbortedException : Exception
  {
    public UserAbortedException()
    {
    }
  }
}