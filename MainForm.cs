﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Globalization;
using System.Media;
using System.Collections;
using System.Diagnostics;
using System.IO;
using ListLabel;

namespace VOIN10
{
  public partial class MainForm : Form
  {
    private List<string> orte = new List<string>();
    private string lastIKConnectionString = "Data Source=.\\SQLEXPRESS;AttachDbFilename=C:\\660310038.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True";
    private string stamm4ConnectionString = "Data Source=.\\SQLEXPRESS;AttachDbFilename=C:\\stamm4.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True";
    private bool currentChanging = false;
    private bool rezeptValidating = true;
    private uint lastIK = 660310038;
    private string dbServer = string.Empty;
    private string mandantenPath = string.Empty;
    private bool initialization = false;

    private string abrechnungsJahr = "2006";
    public string AbrechnungsJahr
    {
      get
      {
        return abrechnungsJahr;
      }
      set
      {
        if (abrechnungsJahr != value)
        {
          SetAbrechnungsJahr(value);
        }
      }
    }

    private void SetAbrechnungsJahr(string value)
    {
      if (rezepteBindingSource.Count == 0 || (ValidatePosition() && ValidateRezept(false)))
      {
        UpdateDataSet(false);
        if (value == jahr1MenuItem.Text)
        {
          jahr1MenuItem.Checked = true;
          jahr2MenuItem.Checked = false;
        }
        else
        {
          jahr2MenuItem.Checked = true;
          jahr1MenuItem.Checked = false;
        }
        positionenDA.SelectCommand.CommandText =
          positionenDA.SelectCommand.CommandText.Replace(AbrechnungsJahr +
          "_Positionen", value + "_Positionen");
        positionenDA.InsertCommand.CommandText =
          positionenDA.InsertCommand.CommandText.Replace(AbrechnungsJahr +
          "_Positionen", value + "_Positionen");
        positionenDA.UpdateCommand.CommandText =
          positionenDA.UpdateCommand.CommandText.Replace(AbrechnungsJahr +
          "_Positionen", value + "_Positionen");
        positionenDA.DeleteCommand.CommandText =
          positionenDA.DeleteCommand.CommandText.Replace(AbrechnungsJahr +
          "_Positionen", value + "_Positionen");

        rezepteDA.SelectCommand.CommandText =
          rezepteDA.SelectCommand.CommandText.Replace(AbrechnungsJahr +
          "_Rezepte", value + "_Rezepte");
        rezepteDA.InsertCommand.CommandText =
          rezepteDA.InsertCommand.CommandText.Replace(AbrechnungsJahr +
          "_Rezepte", value + "_Rezepte");
        rezepteDA.UpdateCommand.CommandText =
          rezepteDA.UpdateCommand.CommandText.Replace(AbrechnungsJahr +
          "_Rezepte", value + "_Rezepte");
        rezepteDA.DeleteCommand.CommandText =
          rezepteDA.DeleteCommand.CommandText.Replace(AbrechnungsJahr +
          "_Rezepte", value + "_Rezepte");

        voinDataSet.AcceptChanges();
        abrechnungsJahr = value;
        UpdateDataSet(true);
        FillListBox();
        SetDataBindings();
        FillJahrList();
      }
    }

    public MainForm()
    {
      using (StartForm form = new StartForm())
      {
        form.Show();
        Application.DoEvents();

        InitializeComponent();

        voinDataSet.Rezepte.ColumnChanged += new DataColumnChangeEventHandler(Rezepte_ColumnChanged);
        voinDataSet.Rezepte.RowDeleting += new DataRowChangeEventHandler(Rezepte_RowDeleting);
        voinDataSet.Rezepte.TableNewRow += new DataTableNewRowEventHandler(Rezepte_TableNewRow);
        voinDataSet.Rezepte.ColumnChanging += new DataColumnChangeEventHandler(Rezepte_ColumnChanging);

        Initialize();
      }
    }

    private void Initialize()
    {
      initialization = true;
      mwstCB.DataSource = MehrWertSteuer.MWStList;
      mwstCB.DisplayMember = "MWStName";
      mwstCB.ValueMember = "MWStCode";
      mwstCB.SelectedValue = (short)1;
      initialization = false;
      hilfsMittelKennZeichenCB.DataSource = HilfsMittel.HilfsMittelList;
      hilfsMittelKennZeichenCB.DisplayMember = "Name";
      hilfsMittelKennZeichenCB.ValueMember = "HilfsMittel_KZ";

      if (VOIN10.Properties.Settings.Default.IniPath == string.Empty)
      {
        VOIN10.Properties.Settings.Default.IniPath = Application.StartupPath;
      }
      StringBuilder dbStr = new StringBuilder(256);
      StringBuilder uidStr = new StringBuilder(256);
      StringBuilder mPath = new StringBuilder(256);
      Utils.GetPrivateProfileString("Olav4", "DBServer", "SWS2", dbStr, 256,
        Path.Combine(VOIN10.Properties.Settings.Default.IniPath, "Olav4.ini"));
      dbServer = dbStr.ToString();
      Utils.GetPrivateProfileString("Olav4", "DBUID", "sa", uidStr, 256,
        Path.Combine(VOIN10.Properties.Settings.Default.IniPath, "Olav4.ini"));
      lastIK = Utils.GetPrivateProfileInt("Olav4", "LastIK", 660310038,
        Path.Combine(VOIN10.Properties.Settings.Default.IniPath, "Olav4.ini"));

      Utils.GetPrivateProfileString("Pfade", "Mandanten",
        "H:\\olav10\\660310038\\Mandanten", mPath, 256,
        Path.Combine(VOIN10.Properties.Settings.Default.IniPath, lastIK.ToString() + ".ini"));
      mandantenPath = mPath.ToString();

      stamm4ConnectionString = "Data Source=" + dbStr.ToString() + ";" +
        "Database=Stamm4;" + "User ID=" + uidStr.ToString() + ";";

      lastIKConnectionString = "Data Source=" + dbStr.ToString() + ";" +
        "Database=" + lastIK.ToString() + ";" + "User ID=" + uidStr.ToString() + ";";

      lastIKsqlConnection.ConnectionString = lastIKConnectionString;
      positionenSqlConnection.ConnectionString = lastIKConnectionString;

      mandantenBesSqlConnection.ConnectionString = stamm4ConnectionString;
      stamm4SqlConnection.ConnectionString = stamm4ConnectionString;

      geburtsDatumMTB.TextChanged += new EventHandler(maskedTextBox_TextChanged);
      rezeptDatumMTB.TextChanged += new EventHandler(maskedTextBox_TextChanged);

      SetAppTitel();
      FillDataSet();

      FillJahrList();
      //voinDataSet._2006_Positionen.ColumnChanging += new DataColumnChangeEventHandler(_2006_Positionen_ColumnChanging);

      uint abrYear = Utils.GetPrivateProfileInt("Olav4", "AbrechnungsJahr",
        DateTime.Today.Year, Path.Combine(VOIN10.Properties.Settings.Default.IniPath,
        lastIK.ToString() + ".ini"));
      jahr2MenuItem.Text = abrYear.ToString();
      jahr1MenuItem.Text = (abrYear - 1).ToString();
      AbrechnungsJahr = abrYear.ToString();
      PrepeateToStart();
    }

    //void _2006_Positionen_ColumnChanging(object sender, DataColumnChangeEventArgs e)
    //{
    //  MessageBox.Show(e.Column.ColumnName + " " + e.ProposedValue.ToString() + " " + e.Row[e.Column].ToString());
    //}

    void Rezepte_ColumnChanging(object sender, DataColumnChangeEventArgs e)
    {
      if (e.Column.ColumnName == "ID")
      {
        foreach (Rezept r in namesListBox.Items)
          if (r.ID == (int)e.Row["ID"])
          {
            r.ID = (int)e.ProposedValue;
            break;
          }
      }
    }

    private void FillDataSet()
    {
      leitZahlenDA.Fill(voinDataSet.Leitzahlen);
      kostentraegerDA.Fill(voinDataSet.Kostentraeger);
      kostentraeder_TarifeDA.Fill(voinDataSet.Kostentraeger_Tarife);
      tab_PositionenDA.Fill(voinDataSet.Tab_Positionen);
      mandantenDA.Fill(voinDataSet.Mandanten);
      mandantenBesDA.Fill(voinDataSet.Mandanten_Bes);
      gruppeDA.Fill(voinDataSet._10_GRUPPE);
      ortDA.Fill(voinDataSet._10_ORT);
      unterGruppeDA.Fill(voinDataSet._10_UNTER);
      artDA.Fill(voinDataSet._10_ART);
      produktDA.Fill(voinDataSet._10_PRODUKT);
      tab_HMPBezeichnungDA.Fill(voinDataSet.tab_HMPBezeichnung);
      tabprimKKassenDA.Fill(voinDataSet.Tab_primKKassen);

      voinDataSet.DefaultViewManager.DataViewSettings["Rezepte"].Sort = "Name, Vorname";
      voinDataSet.DefaultViewManager.DataViewSettings["Positionen"].Sort = "BehDat";
    }

    void Rezepte_TableNewRow(object sender, DataTableNewRowEventArgs e)
    {
      if (mandantenBindingSource.Count > 0 && mandantenBindingSource.Current != null)
      {
        DataRowView drv = (DataRowView)mandantenBindingSource.Current;
        string tkz = (string)drv["Tarifkennzeichen"];
        if (tkz.Length >= 2)
          e.Row["AbrCode"] = tkz.Substring(0, 2);
        if (tkz.Length > 2)
          e.Row["Tarif"] = tkz.Substring(2, tkz.Length - 2);
      }
      InsertIntoNamesList(e.Row);
      bool r = TestIK();
      if (panel4.Enabled != r)
      {
        positionenPanel.Enabled = erfasstTB.Enabled = panel4.Enabled = r;
      }
    }

    private void InsertIntoNamesList(DataRow row)
    {
      namesListBox.BeginUpdate();
      namesListBox.Items.Add(new Rezept(string.Empty, string.Empty, (int)row["ID"]));
      namesListBox.EndUpdate();
    }

    void Rezepte_RowDeleting(object sender, DataRowChangeEventArgs e)
    {
      DeleteFromNamesList((int)e.Row["ID"]);
    }

    private void DeleteFromNamesList(int id)
    {
      foreach (Rezept r in namesListBox.Items)
        if (r.ID == id)
        {
          namesListBox.BeginUpdate();
          namesListBox.Items.Remove(r);
          namesListBox.EndUpdate();
          break;
        }

    }

    void Rezepte_ColumnChanged(object sender, DataColumnChangeEventArgs e)
    {
      if (e.Column.ColumnName == "Name" || e.Column.ColumnName == "Vorname")
        UpdateNamesList(e.Column.ColumnName, (int)e.Row["ID"], (string)e.ProposedValue);
    }

    private void UpdateNamesList(string columnName, int id, string value)
    {
      foreach (Rezept r in namesListBox.Items)
        if (r.ID == id)
        {
          bool selected = false;
          if (columnName == "Name")
            r.Nachname = value;
          else
            r.Vorname = value;
          namesListBox.BeginUpdate();
          selected = namesListBox.SelectedItems.Contains(r);
          namesListBox.Items.Remove(r);
          namesListBox.Items.Add(r);
          if (selected) namesListBox.SelectedItems.Add(r);
          namesListBox.EndUpdate();
          break;
        }
    }

    private void SetAppTitel()
    {
      this.Text = lastIK + "/" + AbrechnungsJahr + " Server " + dbServer + " " +
        " Versoin " + Application.ProductVersion;
    }

    private void PrepeateToStart()
    {
      protokollBtn.Enabled = abrechnenButton.Enabled = true;
      nrComboBox.Enabled = true;
      kostenTaegerPanel.Enabled = true;
      neuRechTSBtn.Enabled = true;
      FillListBox();
      SetDataBindings();
      nrComboBox.Focus();
    }

    private void SaveCurrent(bool savePosition)
    {
      if (rezepteBindingSource.Count > 0 && rezepteBindingSource.Current != null)
      {
        DataRowView drv = (DataRowView)rezepteBindingSource.Current;
        DataRow row = drv.Row;
        if (drv.IsEdit || drv.IsNew)
        {
          rezepteBindingSource.EndEdit();
          if (savePosition && row.RowState != DataRowState.Detached)
          {
            int index = rezepteBindingSource.Find("ID", row["ID"]);
            if (index >= 0)
              rezepteBindingSource.Position = index;
          }
        }
      }
    }

    private void SetDataBindings()
    {
      SetAppTitel();
      if (nrComboBox.Enabled)
      {
        nrComboBox.DataSource = mandantenBindingSource;
        nrComboBox.DisplayMember = "Mandanten.Nr";
        name1Label.DataBindings.Clear();
        name1Label.DataBindings.Add("Text", mandantenBindingSource, "Name1");
        name2Label.DataBindings.Clear();
        name2Label.DataBindings.Add("Text", mandantenBindingSource, "Name2");

        (rezepteBindingSource as ISupportInitialize).BeginInit();
        if (rezepteBindingSource.DataMember != null) rezepteBindingSource.DataSource = null;
        rezepteBindingSource.DataMember = "FK_Mandanten_Rezepte";
        rezepteBindingSource.DataSource = mandantenBindingSource;
        (rezepteBindingSource as ISupportInitialize).EndInit();
        rezepteBindingNavigator.Enabled = true;

        imageTB.DataBindings.Clear();
        imageTB.Text = string.Empty;
        imageTB.DataBindings.Add("Text", rezepteBindingSource, "Image");

        rechNrTB.DataBindings.Clear();
        rechNrTB.Text = string.Empty;
        rechNrTB.DataBindings.Add("Text", rezepteBindingSource, "Diagnose");

        nameTB.DataBindings.Clear();
        nameTB.Text = string.Empty;
        nameTB.DataBindings.Add("Text", rezepteBindingSource, "Name");

        vornameTB.DataBindings.Clear();
        vornameTB.Text = string.Empty;
        vornameTB.DataBindings.Add("Text", rezepteBindingSource, "Vorname");

        versichertenNummerMTB.DataBindings.Clear();
        versichertenNummerMTB.Text = string.Empty;
        versichertenNummerMTB.DataBindings.Add("Text", rezepteBindingSource, "VersNr");

        geburtsDatumMTB.DataBindings.Clear();
        geburtsDatumMTB.Text = string.Empty;
        geburtsDatumMTB.DataBindings.Add("Text", rezepteBindingSource, "GebDat");

        strasseTB.DataBindings.Clear();
        strasseTB.Text = string.Empty;
        strasseTB.DataBindings.Add("Text", rezepteBindingSource, "Strasse");

        plzMTB.DataBindings.Clear();
        plzMTB.Text = string.Empty;
        plzMTB.DataBindings.Add("Text", rezepteBindingSource, "PLZ");

        ortTB.DataBindings.Clear();
        ortTB.Text = string.Empty;
        ortTB.DataBindings.Add("Text", rezepteBindingSource, "Ort");

        indikationTB.DataBindings.Clear();
        indikationTB.Text = string.Empty;
        indikationTB.DataBindings.Add("Text", rezepteBindingSource, "Indikation");

        befreitCheckBox.DataBindings.Clear();
        befreitCheckBox.Checked = false;
        befreitCheckBox.DataBindings.Add("Checked", rezepteBindingSource, "Befreit");

        arztNummerTB.DataBindings.Clear();
        arztNummerTB.Text = string.Empty;
        arztNummerTB.DataBindings.Add("Text", rezepteBindingSource, "ArztNr");

        Binding bng = new Binding("Text", rezepteBindingSource, "Genehmigung_Datum");
        bng.Format += new ConvertEventHandler(bng_Format);
        bng.Parse += new ConvertEventHandler(bng_Parse);
        genVonMTB.DataBindings.Clear();
        genVonMTB.Text = string.Empty;
        genVonMTB.DataBindings.Add(bng);

        bng = new Binding("Text", rezepteBindingSource, "Erfasst");
        bng.Format += new ConvertEventHandler(bng_Format);
        bng.Parse += new ConvertEventHandler(bng_Parse);
        erfasstTB.DataBindings.Clear();
        erfasstTB.Text = string.Empty;
        erfasstTB.DataBindings.Add(bng);

        genKennzTB.DataBindings.Clear();
        genKennzTB.Text = string.Empty;
        genKennzTB.DataBindings.Add("Text", rezepteBindingSource, "Genehmigung_KZ");

        rezeptDatumMTB.DataBindings.Clear();
        rezeptDatumMTB.Text = string.Empty;
        rezeptDatumMTB.DataBindings.Add("Text", rezepteBindingSource, "RezDat");


        rezeptArtCB.DataBindings.Clear();
        rezeptArtCB.DataSource = RezeptzArt.RezeptArtList;
        rezeptArtCB.ValueMember = "RezeptArt";
        rezeptArtCB.DisplayMember = "RezeptArtName";
        rezeptArtCB.DataBindings.Add("SelectedValue", rezepteBindingSource, "RezArt");

        CurrencyManager bm = rezepteBindingSource.CurrencyManager;
        //bm.CurrentChanged += new EventHandler(bm_CurrentChanged);
        bm_CurrentChanged(bm, EventArgs.Empty);
      }
    }

    void bm_CurrentChanged(object sender, EventArgs e)
    {
      //MessageBox.Show("bm_CurrentChanged" + " " +
      //  ((rezepteBindingSource.Current as DataRowView).Row["Name"] == DBNull.Value ? "" :
      //  (string)(rezepteBindingSource.Current as DataRowView).Row["Name"]));
      if (!currentChanging)
        try
        {
          currentChanging = true;
          neuePositionBtn.Text = "&Neu";
          positionenDGV.DataSource = null;
          BuildPositionenDGVColumns();
          ((ISupportInitialize)positionenBindingSource).BeginInit();
          positionenBindingSource.DataSource = null;
          positionenBindingSource.DataMember = string.Empty;
          ((ISupportInitialize)positionenBindingSource).EndInit();
          status1TB.Text = string.Empty;
          status2TB.Text = string.Empty;
          BindingManagerBase bm = sender as BindingManagerBase;
          positionenPanel.Enabled = erfasstTB.Enabled = panel4.Enabled = bm.Count > 0 && TestIK();
          int position = bm.Position;
          UpdateDataSet(false);
          if (bm.Position != position) bm.Position = position;
          if (bm.Count > 0 && bm.Current != null)
          {
            DataRowView drv = (DataRowView)bm.Current;
            string status = drv["Status"].ToString();
            if (status.Length > 0)
            {
              status1TB.Text = new String(status[0], 1);
              status2TB.Text = new String(status[status.Length - 1], 1);
            }
            if (drv["IK"].ToString() == string.Empty && TestIK())
              drv["IK"] = ikTB.Text;
            else
              ikTB.Text = drv["IK"].ToString();
            focusPosition = false;
            LoadPositionen();
            copyTSBtn.Enabled = true;
          }
          else
            copyTSBtn.Enabled = false;
        }
        finally
        {
          focusPosition = true;
          currentChanging = false;
        }
    }

    void bng_Parse(object sender, ConvertEventArgs e)
    {
      if (e.DesiredType != typeof(DateTime)) return;
      if ((e.Value as string) != string.Empty)
        try
        {
          e.Value = DateTime.Parse((string)e.Value);
        }
        catch
        {
          e.Value = DBNull.Value;
        }
      else
        e.Value = DBNull.Value;
    }

    void bng_Format(object sender, ConvertEventArgs e)
    {
      if (e.DesiredType != typeof(string)) return;
      if (e.Value.GetType() != typeof(DBNull))
        e.Value = ((DateTime)e.Value).ToString("dd.MM.yyyy");
      else
        e.Value = string.Empty;
    }

    private void searchBtn_Click(object sender, EventArgs e)
    {
      try
      {
        rezeptValidating = false;
        using (KostentraegerSuchenForm form = new KostentraegerSuchenForm())
        {
          form.DataSet = voinDataSet;
          if (form.ShowDialog() == DialogResult.OK)
          {
            ikTB.Text = form.IK.Trim();
            nameLabel.Text = form.KTName;
            nameTB.Focus();
          }
        }
      }
      finally
      {
        rezeptValidating = true;
      }
    }

    private void jahrMenuItem_Click(object sender, EventArgs e)
    {
      AbrechnungsJahr = (sender as MenuItem).Text;
    }

    private void status1TB_TextChanged(object sender, EventArgs e)
    {
      TextBox box = sender as TextBox;
      if (box.Text.Length > 0)
        if (box.Text != "1" && box.Text != "3" && box.Text != "5")
        {
          MessageBox.Show("Status ist falsch", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
          box.SelectAll();
        }
        else
        {
          BindingManagerBase bm = rezepteBindingSource.CurrencyManager;
          if (bm.Count > 0)
          {
            DataRowView drv = (DataRowView)bm.Current;
            if (drv["Status"] == DBNull.Value || ((string)drv["Status"])[0] != status1TB.Text[0])
              drv["Status"] = status1TB.Text + "000" + status2TB.Text;
            if (box.Focused)
              status2TB.Focus();
          }
        }
    }

    private void status2TB_TextChanged(object sender, EventArgs e)
    {
      TextBox box = sender as TextBox;
      if (box.Text != box.Text.ToUpper())
        box.Text = box.Text.ToUpper();
      else
        if (box.Text.Length > 0)
          if (!"146789MXACKLENDFSP".Contains(box.Text))
          {
            MessageBox.Show("Status ist falsch", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            box.SelectAll();
          }
          else
          {
            BindingManagerBase bm = rezepteBindingSource.CurrencyManager;
            if (bm.Count > 0)
            {
              DataRowView drv = (DataRowView)bm.Current;
              if (drv["Status"] == DBNull.Value || ((string)drv["Status"])[((string)drv["Status"]).Length - 1] != status2TB.Text[status2TB.Text.Length - 1])
                drv["Status"] = status1TB.Text + "000" + status2TB.Text;
              if (box.Focused)
                box.Parent.SelectNextControl(box, true, true, true, true);
            }
          }

    }

    private void status1TB_Validating(object sender, CancelEventArgs e)
    {
      TextBox box = sender as TextBox;
      if (box.Text != "1" && box.Text != "3" && box.Text != "5")
      {
        MessageBox.Show("Status ist falsch", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        box.SelectAll();
        e.Cancel = true;
      }
    }

    private void status2TB_Validating(object sender, CancelEventArgs e)
    {
      TextBox box = sender as TextBox;
      if (!"146789MXACKLENDFSP".Contains(box.Text) || box.Text == string.Empty)
      {
        MessageBox.Show("Status ist falsch", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        box.SelectAll();
        e.Cancel = true;
      }

    }

    private void genVonTB_Validating(object sender, CancelEventArgs e)
    {
      MaskedTextBox tb = sender as MaskedTextBox;
      if (tb.Text != string.Empty && tb.Text != "  .  .")
      {
        if (tb.Text == "  .  ." + AbrechnungsJahr)
        {
          tb.Text = string.Empty;
          return;
        }
        try
        {
          DateTime.Parse(tb.Text);
        }
        catch (Exception ex)
        {
          MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
          e.Cancel = true;
        }
      }
    }

    private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
    {
      VOIN10.Properties.Settings.Default.Save();
      if (ValidatePosition() && ValidateRezept(true))
      {
        if (voinDataSet.HasChanges())
        {
          UpdateDataSet(false);
          //DialogResult res = MessageBox.Show("Sollen die Änderungen gespeichert werden?", this.Text, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
          //if (res == DialogResult.Cancel)
          //  e.Cancel = true;
          //else
          //  if (res == DialogResult.Yes)
          //  {
          //    UpdateDataSet(false);
          //  }
        }
      }
      else
        e.Cancel = true;
    }

    private void UpdateTarifSchlusselAbbrCode()
    {
      if (mandantenBindingSource.Count > 0 && mandantenBindingSource.Current != null)
      {
        DataRow[] rows = voinDataSet.Kostentraeger_Tarife.Select("IK = '" + ikTB.Text + "'");
        if (rows.Length != 0)
        {
          string ts = rows[0]["TarifSchluessel"].ToString().Substring(0, 4);
          //while (ts.Length > 0 && ts[0] == '0') ts = ts.Substring(1);
          DataRowView drv = mandantenBindingSource.Current as DataRowView;
          tarifSchlusselAbrechnCodeTB.Text = ts + drv.Row["Tarifkennzeichen"];
        }
        else
          tarifSchlusselAbrechnCodeTB.Text = string.Empty;
      }
    }

    private void UpdateNameLabel()
    {
      nameLabel.Text = "";
      DataRow[] rows = voinDataSet.Kostentraeger.Select("IK = '" + ikTB.Text + "'");
      if (rows.Length != 0)
        nameLabel.Text = rows[0]["Name1"] + " " + rows[0]["Name2"] + " " + rows[0]["Name3"] + " " + rows[0]["Name4"];
    }

    private void ikTB_Validating(object sender, CancelEventArgs e)
    {
      UpdateNameLabel();
    }

    private bool TestIK()
    {
      DataRow[] rows = voinDataSet.Kostentraeger.Select("IK = '" + ikTB.Text + "'");
      bool res = rows.Length != 0;
      bindingNavigatorAddNewItem.Enabled = res;
      return res;
    }

    private void ikTB_TextChanged(object sender, EventArgs e)
    {
      UpdateNameLabel();
      UpdateTarifSchlusselAbbrCode();
      BindingManagerBase bm = rezepteBindingSource.CurrencyManager;
      if (TestIK())
      {
        if (bm.Count > 0)
        {
          DataRowView drv = (DataRowView)bm.Current;
          if (drv["IK"] == DBNull.Value || ((string)drv["IK"] != ikTB.Text.Trim())) drv["IK"] = ikTB.Text.Trim();
        }
        positionenPanel.Enabled = erfasstTB.Enabled = panel4.Enabled = bm.Count > 0;
        nameTB.Focus();
      }
      else
      {
        ikTB.Focus();
        positionenPanel.Enabled = erfasstTB.Enabled = panel4.Enabled = false;
      }

    }

    private void FillListBox()
    {
      namesListBox.Items.Clear();
      if (nrComboBox.Enabled && mandantenBindingSource.Count > 0 && mandantenBindingSource.Current != null)
      {
        DataRowView current = mandantenBindingSource.Current as DataRowView;
        if (current != null)
        {
          DataRow[] rows = current.Row.GetChildRows("FK_Mandanten_Rezepte");
          foreach (DataRow row in rows)
            namesListBox.Items.Add(new Rezept(row["Name"] == DBNull.Value ? string.Empty : (string)row["Name"],
              row["Vorname"] == DBNull.Value ? string.Empty : (string)row["Vorname"], (int)row["ID"]));
        }
      }
    }

    private void namesListBox_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (namesListBox.SelectedIndex != -1 && rezepteBindingSource.CurrencyManager.Count != 0)
      {
        int index = rezepteBindingSource.Find("ID", (namesListBox.Items[namesListBox.SelectedIndex] as Rezept).ID);
        rezepteBindingSource.Position = index;
      }
    }

    private void MainForm_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.F2)
      {
        if (gruppeMTB.Enabled && (gruppeMTB.Focused || ortMTB.Focused ||
          unterGruppeMTB.Focused || artMTB.Focused || produktMTB.Focused))
        {
          SearchPosition();
          e.Handled = true;
        }
        if (searchBtn.Enabled && panel1.ContainsFocus)
        {
          StandartKKasseSuchen();
          e.Handled = true;
        }
      }
      if (e.KeyCode == Keys.F5 && befreitCheckBox.Enabled)
      {
        befreitCheckBox.Checked = !befreitCheckBox.Checked;
        befreitCheckBox_Click(befreitCheckBox, EventArgs.Empty);
        e.Handled = true;
      }
      if (e.KeyCode == Keys.F6)
      {
        bindingNavigatorAddNewItem_Click(bindingNavigatorAddNewItem, EventArgs.Empty);
        e.Handled = true;
      }
      if (e.KeyCode == Keys.F7)
      {
        neuRechTSBtn_Click(neuRechTSBtn, EventArgs.Empty);
        e.Handled = true;
      }
      if (e.KeyCode == Keys.F9)
      {
        copyTSBtn_Click(copyTSBtn, EventArgs.Empty);
        e.Handled = true;
      }
      if (e.KeyCode == Keys.F10 && verbrauchBtn.Enabled)
      {
        verbrauchBtn_Click(verbrauchBtn, EventArgs.Empty);
        e.Handled = true;
      }
      if (e.KeyCode == Keys.Enter)
      {
        Control c = this;
        Control oc = null;
        while (c != null && c.HasChildren && c != oc)
        {
          oc = c;
          foreach (Control ch in c.Controls)
            if (ch.ContainsFocus)
            {
              c = ch;
              break;
            }
        }
        if (c == rechNrTB)
        {
          nameTB.Focus();
          e.Handled = true;
        }
        else if (c == tagMonatMTB)
        {
          anzahlNUD.Focus();
          e.Handled = true;
        }
        else if (anzahlNUD.ContainsFocus && anzahlNUD.Text == string.Empty &&
          neuePositionBtn.Text == "&Abbrechen")
        {
          e.Handled = true;
          anzahlNUD.Text = anzahlNUD.Value.ToString("N0");
          neuePositionBtn.Text = "&Neu";
          if (positionenBindingSource.Count != 0 && positionenBindingSource.Current != null)
            positionenBindingSource.CancelEdit();
          nrComboBox.Focus();
          bindingNavigatorAddNewItem_Click(bindingNavigatorAddNewItem, EventArgs.Empty);
        }
        else if (c == genKennzTB)
        {
          bool canceled1 = false;
          e.Handled = true;
          if (ValidateRezept(true, out canceled1) && !canceled1)
          {
            if (positionenBindingSource.Count == 0)
              neuePositionBtn_Click(neuePositionBtn, EventArgs.Empty);
            else
              anzahlNUD.Focus();
          }
        }
        else if (c != null)
        {
          this.SelectNextControl(c, true, true, true, true);
          e.Handled = true;
        }
      }
    }

    private void StandartKKasseSuchen()
    {
      PrimKassenForm form = null;
      try
      {
        form = new PrimKassenForm();
        form.DataSet = voinDataSet;
        if (form.ShowDialog() == DialogResult.OK)
        {
          ikTB.Text = form.IK;
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      finally
      {
        if (form != null)
          form.Dispose();
      }
    }

    private void plzTB_Validating(object sender, CancelEventArgs e)
    {
      DataRow[] rows = voinDataSet.Leitzahlen.Select("Leitzahl = '" + plzMTB.Text + "'");
      orte.Clear();
      foreach (DataRow row in rows)
        orte.Add((string)row["Bezeichnung"]);
      orte.Sort();
      if (orte.Count != 0)
      {
        ortTB.Text = orte[0];
      }
    }

    private int GetIndexOfOrt(string ort)
    {
      ort = ort.ToUpper();
      for (int i = 0; i < orte.Count; i++)
        if (ort == orte[i].ToUpper())
          return i;
      return -1;
    }

    private void ortTB_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Down)
      {
        int index = GetIndexOfOrt(ortTB.Text);
        if (index != -1 && index < orte.Count - 1)
          ortTB.Text = orte[index + 1];
        else if (orte.Count > 0)
          ortTB.Text = orte[0];
        e.SuppressKeyPress = true;
      }
      if (e.KeyCode == Keys.Up)
      {
        int index = GetIndexOfOrt(ortTB.Text);
        if (index != -1 && index > 0)
          ortTB.Text = orte[index - 1];
        else if (orte.Count > 0)
          ortTB.Text = orte[orte.Count - 1];
        e.SuppressKeyPress = true;
      }
    }

    private void menuItem3_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void copyTSBtn_Click(object sender, EventArgs e)
    {
      ToolStripButton btn = sender as ToolStripButton;
      BindingNavigator nav = btn.Owner as BindingNavigator;
      bool canceled = false;
      if (btn.Enabled && nav != null && nav.BindingSource != null &&
        nav.BindingSource.AllowNew && nav.BindingSource.Current != null &&
        ValidatePosition() && ValidateRezept(true, out canceled) && canceled == false)
      {
        nav.BindingSource.EndEdit();
        DataRow oldr = (nav.BindingSource.Current as DataRowView).Row;
        UpdateDataSet(false);
        nav.BindingSource.AddNew();
        DataRow newr = (nav.BindingSource.Current as DataRowView).Row;
        for (int i = 0; i < oldr.ItemArray.Length; i++)
          if (oldr.Table.Columns[i].ColumnName != "ID" &&
            oldr.Table.Columns[i].ColumnName != "Indikation" &&
            oldr.Table.Columns[i].ColumnName != "RezArt" &&
            oldr.Table.Columns[i].ColumnName != "Status" &&
            oldr.Table.Columns[i].ColumnName != "RezDat" &&
            oldr.Table.Columns[i].ColumnName != "ArztNr" &&
            oldr.Table.Columns[i].ColumnName != "Genehmigung_Datum" &&
            oldr.Table.Columns[i].ColumnName != "Genehmigung_KZ")
            newr[i] = oldr[i];
        nav.BindingSource.ResetCurrentItem();
        ikTB.Text = (string)newr["IK"];
        indikationTB.Focus();
      }
    }

    private void neuRechTSBtn_Click(object sender, EventArgs e)
    {
      if ((sender as ToolStripButton).Enabled && ValidatePosition() && ValidateRezept(false))
      {
        ((BindingNavigator)((ToolStripButton)(sender)).Owner).BindingSource.EndEdit();
        UpdateDataSet(false);
        ((BindingNavigator)((ToolStripButton)(sender)).Owner).BindingSource.AddNew();
        ikTB.Text = "10";
        ikTB.Focus();
        ikTB.SelectionStart = ikTB.Text.Length;
        ikTB.SelectionLength = 0;
        if (nameTB.DataBindings.Count > 0 && !nameTB.DataBindings[0].IsBinding)
          rezepteBindingSource.ResumeBinding();
      }
    }

    private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
    {
      if ((sender as ToolStripButton).Enabled && ValidatePosition() && ValidateRezept(false))
      {
        string img = imageTB.Text;
        string rechNr = rechNrTB.Text;
        rezepteBindingSource.EndEdit();
        UpdateDataSet(false);
        rezepteBindingSource.AddNew();
        if (mitRechNrCB.Checked)
        {
          imageTB.Text = img;
          rechNrTB.Text = rechNr;
        }
        if (nameTB.Enabled) nameTB.Focus();
        if (nameTB.DataBindings.Count > 0 && !nameTB.DataBindings[0].IsBinding)
          rezepteBindingSource.ResumeBinding();
      }
    }

    private void ClearDataBindings()
    {
      rezepteBindingNavigator.Enabled = false;

      imageTB.DataBindings.Clear();

      rechNrTB.DataBindings.Clear();

      nameTB.DataBindings.Clear();

      vornameTB.DataBindings.Clear();

      versichertenNummerMTB.DataBindings.Clear();

      geburtsDatumMTB.DataBindings.Clear();

      strasseTB.DataBindings.Clear();

      plzMTB.DataBindings.Clear();

      ortTB.DataBindings.Clear();

      indikationTB.DataBindings.Clear();

      befreitCheckBox.DataBindings.Clear();

      arztNummerTB.DataBindings.Clear();

      genVonMTB.DataBindings.Clear();

      erfasstTB.DataBindings.Clear();

      genKennzTB.DataBindings.Clear();

      rezeptDatumMTB.DataBindings.Clear();


      rezeptArtCB.DataBindings.Clear();
      rezeptArtCB.DataSource = null;
      rezeptArtCB.ValueMember = string.Empty;
      rezeptArtCB.DisplayMember = string.Empty;
    }

    private void mitRechNrCB_Click(object sender, EventArgs e)
    {
      CheckBox cb = (sender as CheckBox);
      label16.Visible = label17.Visible = rechNrTB.Visible = imageTB.Visible = cb.TabStop = cb.Checked;

    }

    private void maskedTextBoxDate_Validating(object sender, CancelEventArgs e)
    {
      MaskedTextBox tb = sender as MaskedTextBox;
      if (tb.Focused)
        try
        {
          DateTime.Parse(tb.Text);
        }
        catch (Exception ex)
        {
          MessageBox.Show(ex.Message + " " + tb.Text, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
          e.Cancel = true;
        }
    }

    private void jahrVorgabeMTB_Enter(object sender, EventArgs e)
    {
      MaskedTextBox tb = sender as MaskedTextBox;
      if (tb != null && tb.Text == "  .  .")
      {
        tb.Text = "  .  ." + AbrechnungsJahr;
        this.BeginInvoke((MethodInvoker)delegate()
        {
          tb.SelectionStart = 0;
          tb.SelectionLength = 5;
        });
      }
      else
      {
        this.BeginInvoke((MethodInvoker)delegate()
        {
          tb.SelectAll();
        });
      }
    }

    private void geburtsDatumMTB_Validated(object sender, EventArgs e)
    {
      try
      {
        DateTime gd = DateTime.Parse(geburtsDatumMTB.Text);
        DateTime rd = DateTime.Parse(rezeptDatumMTB.Text);
        if (rd.Year - gd.Year < 19)
          befreitCheckBox.Checked = true;
        if (rd.Year - gd.Year == 19 && rd.Month < gd.Month)
          befreitCheckBox.Checked = true;
        if (rd.Year - gd.Year == 19 && rd.Month == gd.Month && rd.Day < gd.Day)
          befreitCheckBox.Checked = true;
      }
      catch
      {
      }
    }

    private void ikTB_Enter(object sender, EventArgs e)
    {
      TextBox tb = sender as TextBox;
      if (tb.Text == string.Empty)
      {
        this.BeginInvoke((MethodInvoker)delegate()
        {
          tb.Text = "10";
          tb.SelectionStart = tb.Text.Length;
          tb.SelectionLength = 0;
        });
      }
      else
      {
        this.BeginInvoke((MethodInvoker)delegate()
        {
          tb.SelectionStart = Math.Min(2, tb.Text.Length);
          tb.SelectionLength = tb.Text.Length - Math.Min(2, tb.Text.Length);
        });
      }
    }

    private void datumMTB_TextChanged(object sender, EventArgs e)
    {
      MaskedTextBox tb = sender as MaskedTextBox;
      if (tb.Text.Length == 10 && !tb.Text.Contains(" ") && tb.Focused)
        try
        {
          DateTime.Parse(tb.Text);
          tb.Parent.SelectNextControl(tb, true, true, true, true);
        }
        catch
        {
        }
    }

    private void plzMTB_TextChanged(object sender, EventArgs e)
    {
      if (plzMTB.Text.Length == 5 && plzMTB.Focused)
        plzMTB.Parent.SelectNextControl(plzMTB, true, true, true, true);
    }

    private void menuItem4_Click(object sender, EventArgs e)
    {
      if (ValidatePosition() && ValidateRezept(true))
      {
        UpdateDataSet(true);
        FillListBox();
      }
    }

    private void mandantenBindingSource_CurrentChanged(object sender, EventArgs e)
    {
      if (mandantenBindingSource.Count != 0 && mandantenBindingSource.Current != null)
      {
        DataRowView drv = mandantenBindingSource.Current as DataRowView;
        if (rezepteDA.SelectCommand.Parameters["@Mandant"].Value == null ||
          (int)rezepteDA.SelectCommand.Parameters["@Mandant"].Value != (int)drv.Row["Nr"])
        {
          LoadMandanten((int)drv.Row["Nr"]);
        }
      }
    }

    private void LoadMandanten(int nr)
    {
      rezepteDA.SelectCommand.Parameters["@Mandant"].Value = nr;
      UpdateDataSet(true);
      FillListBox();
      UpdateTarifSchlusselAbbrCode();
    }

    #region UpdateDataSet

    bool updating = false;

    private void UpdateDataSet(bool reload)
    {
      if (!updating)
      {
        try
        {
          updating = true;
          if (voinDataSet.HasChanges())
          {
            using (SqlConnection con = new SqlConnection(lastIKConnectionString))
            {
              con.Open();
              SqlTransaction tran = con.BeginTransaction();
              positionenDA.DeleteCommand.Connection =
                positionenDA.InsertCommand.Connection =
                positionenDA.UpdateCommand.Connection =
                positionenDA.SelectCommand.Connection = con;

              positionenDA.DeleteCommand.Transaction =
                positionenDA.InsertCommand.Transaction =
                positionenDA.UpdateCommand.Transaction =
                positionenDA.SelectCommand.Transaction = tran;

              DataTable deletedPositionen =
                voinDataSet.Positionen.GetChanges(DataRowState.Deleted);

              DataTable newPositionen = null;

              DataTable modifiedPositionen = null;

              try
              {
                UpdateResult ur = UpdateResult.Updated;

                if (deletedPositionen != null)
                  positionenDA.Update(deletedPositionen);

                voinDataSet.UpdateRezepte(voinDataSet.Rezepte, con, tran, AbrechnungsJahr + "_Rezepte");

                newPositionen = voinDataSet.Positionen.GetChanges(DataRowState.Added);
                modifiedPositionen = voinDataSet.Positionen.GetChanges(DataRowState.Modified);

                if (newPositionen != null)
                  positionenDA.Update(newPositionen);
                if (modifiedPositionen != null)
                  positionenDA.Update(modifiedPositionen);

                if (voinDataSet.Rezepte.HasErrors && ur == UpdateResult.Updated)
                  ur = CheckErrors(voinDataSet.Rezepte, con, tran);

                if (deletedPositionen != null && deletedPositionen.HasErrors &&
                  ur == UpdateResult.Updated)
                  ur = CheckErrors(deletedPositionen, con, tran);
                if (newPositionen != null && newPositionen.HasErrors &&
                  ur == UpdateResult.Updated)
                  ur = CheckErrors(newPositionen, con, tran);
                if (modifiedPositionen != null && modifiedPositionen.HasErrors &&
                  ur == UpdateResult.Updated)
                  ur = CheckErrors(modifiedPositionen, con, tran);

                if (ur == UpdateResult.Updated) tran.Commit();
                else throw new Exception("Aktualisierungsvorgang wurde abgebrochen.");

              }
              catch (Exception ex)
              {
                reload = true;
                MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                try
                {
                  tran.Rollback();
                  voinDataSet.RejectChanges();
                }
                catch (Exception ex1)
                {
                  MessageBox.Show(ex1.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
              }
              finally
              {
                positionenDA.DeleteCommand.Connection =
                  positionenDA.InsertCommand.Connection =
                  positionenDA.UpdateCommand.Connection =
                  positionenDA.SelectCommand.Connection = positionenSqlConnection;
                positionenDA.DeleteCommand.Transaction =
                  positionenDA.InsertCommand.Transaction =
                  positionenDA.UpdateCommand.Transaction =
                  positionenDA.SelectCommand.Transaction = null;

                if (deletedPositionen != null) deletedPositionen.Dispose();
                if (newPositionen != null) newPositionen.Dispose();
                if (modifiedPositionen != null) modifiedPositionen.Dispose();

                voinDataSet.AcceptChanges();
              }

            }
          }
          if (reload)
          {
            currentChanging = true;
            namesListBox.Items.Clear();
            voinDataSet.Positionen.BeginLoadData();
            voinDataSet.Rezepte.BeginLoadData();
            voinDataSet.Positionen.Clear();
            voinDataSet.Rezepte.Clear();
            rezepteDA.Fill(voinDataSet.Rezepte);
            voinDataSet.Rezepte.EndLoadData();
            voinDataSet.Positionen.EndLoadData();
            FillListBox();
          }
        }
        catch (Exception ex)
        {
          MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        finally
        {
          currentChanging = false;
          updating = false;
          if (lastIKsqlConnection.State != ConnectionState.Closed)
            lastIKsqlConnection.Close();
          if (reload)
            rezepteBindingSource_PositionChanged(rezepteBindingSource, EventArgs.Empty);
        }

      }
    }

    private void rezepte_RowUpdated(object sender, SqlRowUpdatedEventArgs e)
    {
      if (e.RecordsAffected == 0)
      {
        e.Row.RowError = "Datensatz wurde geändert.";
        e.Status = UpdateStatus.SkipCurrentRow;
      }
    }

    internal enum UpdateResult { Updated, Retry, Cancel }

    private object GetValue(DataRow row, string columnName)
    {
      return row.RowState == DataRowState.Deleted ? row[columnName, DataRowVersion.Original] : row[columnName];
    }

    private object GetValue(DataRow row, int columnIndex)
    {
      return row.RowState == DataRowState.Deleted ? row[columnIndex, DataRowVersion.Original] : row[columnIndex];
    }


    private UpdateResult CheckErrors(DataTable t, SqlConnection con, SqlTransaction tran)
    {
      UpdateResult res = UpdateResult.Updated;
      if (t.TableName == "2005_Rezepte" || t.TableName == "2006_Rezepte")
      {
        DataRow[] rows = t.GetErrors();
        foreach (DataRow r in rows)
        {
          do
          {
            res = CheckRezepteRow(r, con, tran);
          } while (res == UpdateResult.Retry);
          if (res == UpdateResult.Cancel) break;
        }
      }
      else if (t.TableName == "2005_Positionen" || t.TableName == "2006_Positionen")
      {
        DataRow[] rows = t.GetErrors();
        foreach (DataRow r in rows)
        {
          do
          {
            res = CheckPositionenRow(r, con, tran);
          } while (res == UpdateResult.Retry);
          if (res == UpdateResult.Cancel) break;
        }
      }
      return res;
    }

    #endregion

    #region CheckRezepteRow

    private SqlCommand GetRezepteInsertCommand(string tableName, DataRow row, SqlTransaction tran)
    {
      SqlCommand cmd = new SqlCommand("INSERT INTO [" + tableName + "] (Mandant, IK," +
        " AbrCode, Tarif, Name, Vorname, GebDat, Strasse, Plz, Ort, VersNr, Status," +
        " RezDat, Befreit, ArztNr, VK_Bis, Unfall, Hilfsmittel, Erfasst, Vorgang," +
        " RechNr_Kasse, RechNr_Mandant, Image, RezArt, Pauschale, Diagnose," +
        " Besonderheit, Genehmigung_KZ, Genehmigung_Datum, Indikation)" +
        " VALUES (@Mandant,@IK,@AbrCode,@Tarif,@Name,@Vorname,@GebDat,@Strasse,@Plz," +
        "@Ort,@VersNr,@Status,@RezDat,@Befreit,@ArztNr,@VK_Bis,@Unfall,@Hilfsmittel," +
        "@Erfasst,@Vorgang,@RechNr_Kasse,@RechNr_Mandant,@Image,@RezArt,@Pauschale," +
        "@Diagnose,@Besonderheit,@Genehmigung_KZ,@Genehmigung_Datum,@Indikation);" +
        "SELECT CAST(scope_identity() AS int)");
      cmd.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Mandant", System.Data.SqlDbType.VarChar, 0, "Mandant"),
            new System.Data.SqlClient.SqlParameter("@IK", System.Data.SqlDbType.VarChar, 0, "IK"),
            new System.Data.SqlClient.SqlParameter("@AbrCode", System.Data.SqlDbType.VarChar, 0, "AbrCode"),
            new System.Data.SqlClient.SqlParameter("@Tarif", System.Data.SqlDbType.VarChar, 0, "Tarif"),
            new System.Data.SqlClient.SqlParameter("@Name", System.Data.SqlDbType.VarChar, 0, "Name"),
            new System.Data.SqlClient.SqlParameter("@Vorname", System.Data.SqlDbType.VarChar, 0, "Vorname"),
            new System.Data.SqlClient.SqlParameter("@GebDat", System.Data.SqlDbType.DateTime, 0, "GebDat"),
            new System.Data.SqlClient.SqlParameter("@Strasse", System.Data.SqlDbType.VarChar, 0, "Strasse"),
            new System.Data.SqlClient.SqlParameter("@Plz", System.Data.SqlDbType.VarChar, 0, "Plz"),
            new System.Data.SqlClient.SqlParameter("@Ort", System.Data.SqlDbType.VarChar, 0, "Ort"),
            new System.Data.SqlClient.SqlParameter("@VersNr", System.Data.SqlDbType.VarChar, 0, "VersNr"),
            new System.Data.SqlClient.SqlParameter("@Status", System.Data.SqlDbType.VarChar, 0, "Status"),
            new System.Data.SqlClient.SqlParameter("@RezDat", System.Data.SqlDbType.DateTime, 0, "RezDat"),
            new System.Data.SqlClient.SqlParameter("@Befreit", System.Data.SqlDbType.Bit, 0, "Befreit"),
            new System.Data.SqlClient.SqlParameter("@ArztNr", System.Data.SqlDbType.VarChar, 0, "ArztNr"),
            new System.Data.SqlClient.SqlParameter("@VK_Bis", System.Data.SqlDbType.VarChar, 0, "VK_Bis"),
            new System.Data.SqlClient.SqlParameter("@Unfall", System.Data.SqlDbType.Bit, 0, "Unfall"),
            new System.Data.SqlClient.SqlParameter("@Hilfsmittel", System.Data.SqlDbType.Int, 0, "Hilfsmittel"),
            new System.Data.SqlClient.SqlParameter("@Erfasst", System.Data.SqlDbType.DateTime, 0, "Erfasst"),
            new System.Data.SqlClient.SqlParameter("@Vorgang", System.Data.SqlDbType.Int, 0, "Vorgang"),
            new System.Data.SqlClient.SqlParameter("@RechNr_Kasse", System.Data.SqlDbType.Int, 0, "RechNr_Kasse"),
            new System.Data.SqlClient.SqlParameter("@RechNr_Mandant", System.Data.SqlDbType.Int, 0, "RechNr_Mandant"),
            new System.Data.SqlClient.SqlParameter("@Image", System.Data.SqlDbType.VarChar, 0, "Image"),
            new System.Data.SqlClient.SqlParameter("@RezArt", System.Data.SqlDbType.SmallInt, 0, "RezArt"),
            new System.Data.SqlClient.SqlParameter("@Pauschale", System.Data.SqlDbType.Money, 0, "Pauschale"),
            new System.Data.SqlClient.SqlParameter("@Diagnose", System.Data.SqlDbType.VarChar, 0, "Diagnose"),
            new System.Data.SqlClient.SqlParameter("@Besonderheit", System.Data.SqlDbType.SmallInt, 0, "Besonderheit"),
            new System.Data.SqlClient.SqlParameter("@Genehmigung_KZ", System.Data.SqlDbType.VarChar, 0, "Genehmigung_KZ"),
            new System.Data.SqlClient.SqlParameter("@Genehmigung_Datum", System.Data.SqlDbType.DateTime, 0, "Genehmigung_Datum"),
            new System.Data.SqlClient.SqlParameter("@Indikation", System.Data.SqlDbType.VarChar, 0, "Indikation")});
      for (int i = 0; i < row.ItemArray.Length; i++)
        if (row.Table.Columns[i].ColumnName != "ID")
          cmd.Parameters["@" + row.Table.Columns[i].ColumnName].Value = row[i];
      cmd.Transaction = tran;
      return cmd;
    }

    private SqlCommand GetRezepteUpdateCommand(string tableName, SqlDataReader reader, DataRow row, SqlTransaction tran)
    {
      string cmdText = "UPDATE [" + tableName + "] " +
        "SET Mandant = @Mandant, IK = @IK, AbrCode = @AbrCode, Tarif = @Tarif, " +
        "Name = @Name, Vorname = @Vorname, GebDat = @GebDat, Strasse = @Strasse, " +
        "Plz = @Plz, Ort = @Ort, VersNr = @VersNr, Status = @Status, RezDat = @RezDat, " +
        "Befreit = @Befreit, ArztNr = @ArztNr, VK_Bis = @VK_Bis, Unfall = @Unfall, " +
        "Hilfsmittel = @Hilfsmittel, Erfasst = @Erfasst, Vorgang = @Vorgang, " +
        "RechNr_Kasse = @RechNr_Kasse, RechNr_Mandant = @RechNr_Mandant, " +
        "Image = @Image, RezArt = @RezArt, Pauschale = @Pauschale, " +
        "Diagnose = @Diagnose, Besonderheit = @Besonderheit, " +
        "Genehmigung_KZ = @Genehmigung_KZ, Genehmigung_Datum = @Genehmigung_Datum, " +
        "Indikation = @Indikation " +
        "WHERE (ID = @Original_ID) AND (Mandant = @Original_Mandant) AND " +
        "(IK = @Original_IK) AND (AbrCode = @Original_AbrCode) AND " +
        "(Tarif = @Original_Tarif) AND (Name = @Original_Name) AND " +
        "(Vorname = @Original_Vorname) AND (@IsNull_GebDat = 1 AND GebDat IS NULL OR " +
        "GebDat = @Original_GebDat) AND (@IsNull_Strasse = 1 AND Strasse IS NULL OR " +
        "Strasse = @Original_Strasse) AND (@IsNull_Plz = 1 AND Plz IS NULL OR " +
        "Plz = @Original_Plz) AND (@IsNull_Ort = 1 AND Ort IS NULL OR " +
        "Ort = @Original_Ort) AND (VersNr = @Original_VersNr) AND " +
        "(Status = @Original_Status) AND (RezDat = @Original_RezDat) AND " +
        "(@IsNull_Befreit = 1 AND Befreit IS NULL OR Befreit = @Original_Befreit) " +
        "AND (@IsNull_ArztNr = 1 AND ArztNr IS NULL OR ArztNr = @Original_ArztNr) " +
        "AND (@IsNull_VK_Bis = 1 AND VK_Bis IS NULL OR VK_Bis = @Original_VK_Bis) " +
        "AND (@IsNull_Unfall = 1 AND Unfall IS NULL OR Unfall = @Original_Unfall) " +
        "AND (@IsNull_Hilfsmittel = 1 AND Hilfsmittel IS NULL OR " +
        "Hilfsmittel = @Original_Hilfsmittel) AND (@IsNull_Erfasst = 1 AND " +
        "Erfasst IS NULL OR Erfasst = @Original_Erfasst) AND (@IsNull_Vorgang = 1 " +
        "AND Vorgang IS NULL OR Vorgang = @Original_Vorgang) AND " +
        "(@IsNull_RechNr_Kasse = 1 AND RechNr_Kasse IS NULL OR " +
        "RechNr_Kasse = @Original_RechNr_Kasse) AND (@IsNull_RechNr_Mandant = 1 AND " +
        "RechNr_Mandant IS NULL OR RechNr_Mandant = @Original_RechNr_Mandant) AND " +
        "(@IsNull_Image = 1 AND Image IS NULL OR Image = @Original_Image) AND " +
        "(@IsNull_RezArt = 1 AND RezArt IS NULL OR RezArt = @Original_RezArt) AND " +
        "(@IsNull_Pauschale = 1 AND Pauschale IS NULL OR " +
        "Pauschale = @Original_Pauschale) AND (@IsNull_Diagnose = 1 AND " +
        "Diagnose IS NULL OR Diagnose = @Original_Diagnose) AND " +
        "(@IsNull_Besonderheit = 1 AND Besonderheit IS NULL OR " +
        "Besonderheit = @Original_Besonderheit) AND (@IsNull_Genehmigung_KZ = 1 AND " +
        "Genehmigung_KZ IS NULL OR Genehmigung_KZ = @Original_Genehmigung_KZ) AND " +
        "(@IsNull_Genehmigung_Datum = 1 AND Genehmigung_Datum IS NULL OR " +
        "Genehmigung_Datum = @Original_Genehmigung_Datum) AND " +
        "(@IsNull_Indikation = 1 AND Indikation IS NULL OR " +
        "Indikation = @Original_Indikation)";
      SqlCommand cmd = new SqlCommand(cmdText);
      cmd.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
                  new System.Data.SqlClient.SqlParameter("@Mandant", System.Data.SqlDbType.VarChar, 0, "Mandant"),
                  new System.Data.SqlClient.SqlParameter("@IK", System.Data.SqlDbType.VarChar, 0, "IK"),
                  new System.Data.SqlClient.SqlParameter("@AbrCode", System.Data.SqlDbType.VarChar, 0, "AbrCode"),
                  new System.Data.SqlClient.SqlParameter("@Tarif", System.Data.SqlDbType.VarChar, 0, "Tarif"),
                  new System.Data.SqlClient.SqlParameter("@Name", System.Data.SqlDbType.VarChar, 0, "Name"),
                  new System.Data.SqlClient.SqlParameter("@Vorname", System.Data.SqlDbType.VarChar, 0, "Vorname"),
                  new System.Data.SqlClient.SqlParameter("@GebDat", System.Data.SqlDbType.DateTime, 0, "GebDat"),
                  new System.Data.SqlClient.SqlParameter("@Strasse", System.Data.SqlDbType.VarChar, 0, "Strasse"),
                  new System.Data.SqlClient.SqlParameter("@Plz", System.Data.SqlDbType.VarChar, 0, "Plz"),
                  new System.Data.SqlClient.SqlParameter("@Ort", System.Data.SqlDbType.VarChar, 0, "Ort"),
                  new System.Data.SqlClient.SqlParameter("@VersNr", System.Data.SqlDbType.VarChar, 0, "VersNr"),
                  new System.Data.SqlClient.SqlParameter("@Status", System.Data.SqlDbType.VarChar, 0, "Status"),
                  new System.Data.SqlClient.SqlParameter("@RezDat", System.Data.SqlDbType.DateTime, 0, "RezDat"),
                  new System.Data.SqlClient.SqlParameter("@Befreit", System.Data.SqlDbType.Bit, 0, "Befreit"),
                  new System.Data.SqlClient.SqlParameter("@ArztNr", System.Data.SqlDbType.VarChar, 0, "ArztNr"),
                  new System.Data.SqlClient.SqlParameter("@VK_Bis", System.Data.SqlDbType.VarChar, 0, "VK_Bis"),
                  new System.Data.SqlClient.SqlParameter("@Unfall", System.Data.SqlDbType.Bit, 0, "Unfall"),
                  new System.Data.SqlClient.SqlParameter("@Hilfsmittel", System.Data.SqlDbType.Int, 0, "Hilfsmittel"),
                  new System.Data.SqlClient.SqlParameter("@Erfasst", System.Data.SqlDbType.DateTime, 0, "Erfasst"),
                  new System.Data.SqlClient.SqlParameter("@Vorgang", System.Data.SqlDbType.Int, 0, "Vorgang"),
                  new System.Data.SqlClient.SqlParameter("@RechNr_Kasse", System.Data.SqlDbType.Int, 0, "RechNr_Kasse"),
                  new System.Data.SqlClient.SqlParameter("@RechNr_Mandant", System.Data.SqlDbType.Int, 0, "RechNr_Mandant"),
                  new System.Data.SqlClient.SqlParameter("@Image", System.Data.SqlDbType.VarChar, 0, "Image"),
                  new System.Data.SqlClient.SqlParameter("@RezArt", System.Data.SqlDbType.SmallInt, 0, "RezArt"),
                  new System.Data.SqlClient.SqlParameter("@Pauschale", System.Data.SqlDbType.Money, 0, "Pauschale"),
                  new System.Data.SqlClient.SqlParameter("@Diagnose", System.Data.SqlDbType.VarChar, 0, "Diagnose"),
                  new System.Data.SqlClient.SqlParameter("@Besonderheit", System.Data.SqlDbType.SmallInt, 0, "Besonderheit"),
                  new System.Data.SqlClient.SqlParameter("@Genehmigung_KZ", System.Data.SqlDbType.VarChar, 0, "Genehmigung_KZ"),
                  new System.Data.SqlClient.SqlParameter("@Genehmigung_Datum", System.Data.SqlDbType.DateTime, 0, "Genehmigung_Datum"),
                  new System.Data.SqlClient.SqlParameter("@Indikation", System.Data.SqlDbType.VarChar, 0, "Indikation"),
                  new System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "ID", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@Original_Mandant", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Mandant", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@Original_IK", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "IK", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@Original_AbrCode", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "AbrCode", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@Original_Tarif", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Tarif", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@Original_Name", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Name", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@Original_Vorname", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Vorname", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@IsNull_GebDat", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "GebDat", System.Data.DataRowVersion.Original, true, null, "", "", ""),
                  new System.Data.SqlClient.SqlParameter("@Original_GebDat", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "GebDat", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@IsNull_Strasse", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Strasse", System.Data.DataRowVersion.Original, true, null, "", "", ""),
                  new System.Data.SqlClient.SqlParameter("@Original_Strasse", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Strasse", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@IsNull_Plz", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Plz", System.Data.DataRowVersion.Original, true, null, "", "", ""),
                  new System.Data.SqlClient.SqlParameter("@Original_Plz", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Plz", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@IsNull_Ort", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Ort", System.Data.DataRowVersion.Original, true, null, "", "", ""),
                  new System.Data.SqlClient.SqlParameter("@Original_Ort", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Ort", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@Original_VersNr", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "VersNr", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@Original_Status", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Status", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@Original_RezDat", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "RezDat", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@IsNull_Befreit", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Befreit", System.Data.DataRowVersion.Original, true, null, "", "", ""),
                  new System.Data.SqlClient.SqlParameter("@Original_Befreit", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Befreit", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@IsNull_ArztNr", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "ArztNr", System.Data.DataRowVersion.Original, true, null, "", "", ""),
                  new System.Data.SqlClient.SqlParameter("@Original_ArztNr", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "ArztNr", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@IsNull_VK_Bis", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "VK_Bis", System.Data.DataRowVersion.Original, true, null, "", "", ""),
                  new System.Data.SqlClient.SqlParameter("@Original_VK_Bis", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "VK_Bis", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@IsNull_Unfall", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Unfall", System.Data.DataRowVersion.Original, true, null, "", "", ""),
                  new System.Data.SqlClient.SqlParameter("@Original_Unfall", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Unfall", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@IsNull_Hilfsmittel", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Hilfsmittel", System.Data.DataRowVersion.Original, true, null, "", "", ""),
                  new System.Data.SqlClient.SqlParameter("@Original_Hilfsmittel", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Hilfsmittel", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@IsNull_Erfasst", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Erfasst", System.Data.DataRowVersion.Original, true, null, "", "", ""),
                  new System.Data.SqlClient.SqlParameter("@Original_Erfasst", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Erfasst", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@IsNull_Vorgang", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Vorgang", System.Data.DataRowVersion.Original, true, null, "", "", ""),
                  new System.Data.SqlClient.SqlParameter("@Original_Vorgang", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Vorgang", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@IsNull_RechNr_Kasse", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "RechNr_Kasse", System.Data.DataRowVersion.Original, true, null, "", "", ""),
                  new System.Data.SqlClient.SqlParameter("@Original_RechNr_Kasse", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "RechNr_Kasse", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@IsNull_RechNr_Mandant", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "RechNr_Mandant", System.Data.DataRowVersion.Original, true, null, "", "", ""),
                  new System.Data.SqlClient.SqlParameter("@Original_RechNr_Mandant", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "RechNr_Mandant", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@IsNull_Image", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Image", System.Data.DataRowVersion.Original, true, null, "", "", ""),
                  new System.Data.SqlClient.SqlParameter("@Original_Image", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Image", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@IsNull_RezArt", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "RezArt", System.Data.DataRowVersion.Original, true, null, "", "", ""),
                  new System.Data.SqlClient.SqlParameter("@Original_RezArt", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "RezArt", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@IsNull_Pauschale", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Pauschale", System.Data.DataRowVersion.Original, true, null, "", "", ""),
                  new System.Data.SqlClient.SqlParameter("@Original_Pauschale", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Pauschale", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@IsNull_Diagnose", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Diagnose", System.Data.DataRowVersion.Original, true, null, "", "", ""),
                  new System.Data.SqlClient.SqlParameter("@Original_Diagnose", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Diagnose", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@IsNull_Besonderheit", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Besonderheit", System.Data.DataRowVersion.Original, true, null, "", "", ""),
                  new System.Data.SqlClient.SqlParameter("@Original_Besonderheit", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Besonderheit", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@IsNull_Genehmigung_KZ", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Genehmigung_KZ", System.Data.DataRowVersion.Original, true, null, "", "", ""),
                  new System.Data.SqlClient.SqlParameter("@Original_Genehmigung_KZ", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Genehmigung_KZ", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@IsNull_Genehmigung_Datum", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Genehmigung_Datum", System.Data.DataRowVersion.Original, true, null, "", "", ""),
                  new System.Data.SqlClient.SqlParameter("@Original_Genehmigung_Datum", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Genehmigung_Datum", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@IsNull_Indikation", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Indikation", System.Data.DataRowVersion.Original, true, null, "", "", ""),
                  new System.Data.SqlClient.SqlParameter("@Original_Indikation", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Indikation", System.Data.DataRowVersion.Original, null),
                  new System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.Int, 4, "ID")});
      for (int i = 0; i < row.ItemArray.Length; i++)
        cmd.Parameters["@" + row.Table.Columns[i].ColumnName].Value = row[i];
      for (int i = 0; i < reader.FieldCount; i++)
      {
        cmd.Parameters["@Original_" + reader.GetName(i)].Value = reader.GetValue(i);
        if (row.Table.Columns[reader.GetName(i)].AllowDBNull)
          if (reader.IsDBNull(i))
            cmd.Parameters["@IsNull_" + reader.GetName(i)].Value = 1;
          else
            cmd.Parameters["@IsNull_" + reader.GetName(i)].Value = 0;
      }
      cmd.Transaction = tran;
      return cmd;
    }

    private SqlCommand GetRezepteDeleteCommand(string tableName, SqlDataReader reader, SqlTransaction tran)
    {
      SqlCommand cmd = new SqlCommand("DELETE FROM [" + tableName + "] " +
        "WHERE (ID = @Original_ID) AND (Mandant = @Original_Mandant) AND " +
        "(IK = @Original_IK) AND (AbrCode = @Original_AbrCode) AND " +
        "(Tarif = @Original_Tarif) AND (Name = @Original_Name) AND " +
        "(Vorname = @Original_Vorname) AND (@IsNull_GebDat = 1 AND GebDat IS NULL OR " +
        "GebDat = @Original_GebDat) AND (@IsNull_Strasse = 1 AND Strasse IS NULL OR " +
        "Strasse = @Original_Strasse) AND (@IsNull_Plz = 1 AND Plz IS NULL OR " +
        "Plz = @Original_Plz) AND (@IsNull_Ort = 1 AND Ort IS NULL OR " +
        "Ort = @Original_Ort) AND (VersNr = @Original_VersNr) AND " +
        "(Status = @Original_Status) AND (RezDat = @Original_RezDat) AND " +
        "(@IsNull_Befreit = 1 AND Befreit IS NULL OR Befreit = @Original_Befreit) " +
        "AND (@IsNull_ArztNr = 1 AND ArztNr IS NULL OR ArztNr = @Original_ArztNr) AND " +
        "(@IsNull_VK_Bis = 1 AND VK_Bis IS NULL OR VK_Bis = @Original_VK_Bis) AND " +
        "(@IsNull_Unfall = 1 AND Unfall IS NULL OR Unfall = @Original_Unfall) AND " +
        "(@IsNull_Hilfsmittel = 1 AND Hilfsmittel IS NULL OR " +
        "Hilfsmittel = @Original_Hilfsmittel) AND (@IsNull_Erfasst = 1 AND " +
        "Erfasst IS NULL OR Erfasst = @Original_Erfasst) AND (@IsNull_Vorgang = 1 AND " +
        "Vorgang IS NULL OR Vorgang = @Original_Vorgang) AND (@IsNull_RechNr_Kasse = 1 " +
        "AND RechNr_Kasse IS NULL OR RechNr_Kasse = @Original_RechNr_Kasse) AND " +
        "(@IsNull_RechNr_Mandant = 1 AND RechNr_Mandant IS NULL OR " +
        "RechNr_Mandant = @Original_RechNr_Mandant) AND (@IsNull_Image = 1 AND " +
        "Image IS NULL OR Image = @Original_Image) AND (@IsNull_RezArt = 1 AND " +
        "RezArt IS NULL OR RezArt = @Original_RezArt) AND (@IsNull_Pauschale = 1 AND " +
        "Pauschale IS NULL OR Pauschale = @Original_Pauschale) AND (@IsNull_Diagnose = 1 " +
        "AND Diagnose IS NULL OR Diagnose = @Original_Diagnose) AND " +
        "(@IsNull_Besonderheit = 1 AND Besonderheit IS NULL OR " +
        "Besonderheit = @Original_Besonderheit) AND (@IsNull_Genehmigung_KZ = 1 " +
        "AND Genehmigung_KZ IS NULL OR Genehmigung_KZ = @Original_Genehmigung_KZ) AND " +
        "(@IsNull_Genehmigung_Datum = 1 AND Genehmigung_Datum IS NULL OR " +
        "Genehmigung_Datum = @Original_Genehmigung_Datum) AND (@IsNull_Indikation = 1 " +
        "AND Indikation IS NULL OR Indikation = @Original_Indikation)");
      cmd.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "ID", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Mandant", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Mandant", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_IK", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "IK", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_AbrCode", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "AbrCode", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Tarif", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Tarif", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Name", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Name", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Vorname", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Vorname", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_GebDat", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "GebDat", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_GebDat", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "GebDat", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Strasse", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Strasse", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Strasse", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Strasse", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Plz", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Plz", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Plz", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Plz", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Ort", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Ort", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Ort", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Ort", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_VersNr", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "VersNr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Status", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Status", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_RezDat", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "RezDat", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Befreit", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Befreit", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Befreit", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Befreit", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_ArztNr", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "ArztNr", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_ArztNr", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "ArztNr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_VK_Bis", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "VK_Bis", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_VK_Bis", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "VK_Bis", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Unfall", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Unfall", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Unfall", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Unfall", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Hilfsmittel", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Hilfsmittel", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Hilfsmittel", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Hilfsmittel", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Erfasst", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Erfasst", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Erfasst", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Erfasst", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Vorgang", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Vorgang", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Vorgang", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Vorgang", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_RechNr_Kasse", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "RechNr_Kasse", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_RechNr_Kasse", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "RechNr_Kasse", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_RechNr_Mandant", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "RechNr_Mandant", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_RechNr_Mandant", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "RechNr_Mandant", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Image", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Image", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Image", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Image", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_RezArt", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "RezArt", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_RezArt", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "RezArt", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Pauschale", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Pauschale", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Pauschale", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Pauschale", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Diagnose", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Diagnose", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Diagnose", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Diagnose", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Besonderheit", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Besonderheit", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Besonderheit", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Besonderheit", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Genehmigung_KZ", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Genehmigung_KZ", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Genehmigung_KZ", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Genehmigung_KZ", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Genehmigung_Datum", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Genehmigung_Datum", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Genehmigung_Datum", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Genehmigung_Datum", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Indikation", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Indikation", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Indikation", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Indikation", System.Data.DataRowVersion.Original, null)});
      DataTable t = reader.GetSchemaTable();
      for (int i = 0; i < reader.FieldCount; i++)
      {
        cmd.Parameters["@Original_" + reader.GetName(i)].Value = reader.GetValue(i);
        DataRow[] rows = t.Select("ColumnName = '" + reader.GetName(i) + "'");
        if ((bool)rows[0]["AllowDBNull"])
          if (reader.IsDBNull(i))
            cmd.Parameters["@IsNull_" + reader.GetName(i)].Value = 1;
          else
            cmd.Parameters["@IsNull_" + reader.GetName(i)].Value = 0;
      }
      cmd.Transaction = tran;
      return cmd;
    }

    private UpdateResult CheckRezepteRow(DataRow row, SqlConnection con, SqlTransaction tran)
    {
      SqlCommand scmd = new SqlCommand("select * from [" + row.Table.TableName +
        "] where id = @id", con);
      scmd.Transaction = tran;
      SqlDataReader reader = null;
      SqlCommand updateCmd = null;
      string my = string.Empty;
      string db = string.Empty;
      string updated = string.Empty;
      scmd.Parameters.Add("@id", SqlDbType.Int);
      scmd.Parameters["@id"].Value = GetValue(row, "ID");
      object[] dbr = null;

      for (int i = 0; i < row.Table.Columns.Count; i++)
        my += row.Table.Columns[i].ColumnName + ": " + GetValue(row, i).ToString() +
          (i == row.Table.Columns.Count - 1 ? string.Empty : ", ");
      try
      {
        reader = scmd.ExecuteReader();
        while (reader.Read())
        {
          for (int i = 0; i < reader.FieldCount; i++)
          {
            db += reader.GetName(i) + ": " + reader.GetValue(i).ToString() +
              (i == reader.FieldCount - 1 ? string.Empty : ", ");
            if (!reader.GetValue(i).Equals(GetValue(row, i)))
              updated += (updated == string.Empty ? string.Empty : ", ") +
                reader.GetName(i) + " " + GetValue(row, i).ToString() + " " +
                reader.GetValue(i).ToString();
            dbr = new object[reader.FieldCount];
            reader.GetValues(dbr);
          }
          if (row.RowState == DataRowState.Modified)
            updateCmd = GetRezepteUpdateCommand(row.Table.TableName, reader, row, tran);
          if (row.RowState == DataRowState.Deleted)
            updateCmd = GetRezepteDeleteCommand(row.Table.TableName, reader, tran);
        }
      }
      catch (Exception ex)
      {
        if (MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.RetryCancel, MessageBoxIcon.Question) == DialogResult.Retry)
          return UpdateResult.Retry;
        else
          return UpdateResult.Cancel;
      }
      finally
      {
        if (reader != null && !reader.IsClosed)
          reader.Close();
      }
      if (db != string.Empty)
      {
        if (row.RowState == DataRowState.Modified)
          if (MessageBox.Show(row.RowError + " Soll der Datensatz geschpeichert werden?\n" +
          my + "\nin der Datenbank\n" + db + "\ngeändert " + updated,
          this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
          {/////////////////
            updateCmd.Connection = con;
            try
            {
              int c = updateCmd.ExecuteNonQuery();
              if (c == 0)
                throw new Exception("Der Datensatz wurde geändert.");
              row.RowError = string.Empty;
              row.AcceptChanges();
            }
            catch (Exception ex)
            {
              if (MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.RetryCancel, MessageBoxIcon.Question) == DialogResult.Retry)
                return UpdateResult.Retry;
              else
                return UpdateResult.Cancel;
            }
          }////////////////
          else
          {
            for (int i = 0; i < dbr.Length; i++)
              row[i] = dbr[i];
            row.RowError = string.Empty;
            row.AcceptChanges();
            return UpdateResult.Cancel;
          }
        if (row.RowState == DataRowState.Deleted)
          if (MessageBox.Show(row.RowError + " Soll der Datensatz gelöscht werden?\n" +
          my + "\nin der Datenbank\n" + db + "\ngeändert " + updated,
          this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
          {//////////////
            updateCmd.Connection = con;
            try
            {
              int c = updateCmd.ExecuteNonQuery();
              if (c == 0)
                throw new Exception("Der Datensatz wurde geändert.");
              row.RowError = string.Empty;
              row.AcceptChanges();
            }
            catch (Exception ex)
            {
              if (MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.RetryCancel, MessageBoxIcon.Question) == DialogResult.Retry)
                return UpdateResult.Retry;
              else
                return UpdateResult.Cancel;
            }
          }////////////////
          else
          {
            row.RejectChanges();
            for (int i = 0; i < dbr.Length; i++)
              row[i] = dbr[i];
            row.RowError = string.Empty;
            row.AcceptChanges();
            return UpdateResult.Cancel;
          }
      }
      else
      {
        if (row.RowState == DataRowState.Modified &&
          MessageBox.Show("Datensatz wurde gelöscht." + " Soll der Datensatz geschpeichert werden?\n" + my,
          this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
        {
          SqlCommand cmd = GetRezepteInsertCommand(row.Table.TableName, row, tran);
          cmd.Connection = con;
          try
          {
            int newID = (int)cmd.ExecuteScalar();
            row.RowError = string.Empty;
            row["ID"] = newID;
            row.AcceptChanges();
          }
          catch (Exception ex)
          {
            if (MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.RetryCancel, MessageBoxIcon.Question) == DialogResult.Retry)
              return UpdateResult.Retry;
            else
              return UpdateResult.Cancel;
          }
        }
        else
        {
          row.RowError = string.Empty;
          if (row.RowState != DataRowState.Deleted)
            row.Delete();
          row.AcceptChanges();
          return UpdateResult.Cancel;
        }
      }
      return UpdateResult.Updated;
    }

    #endregion

    #region CheckPositionenRow

    private UpdateResult CheckPositionenRow(DataRow row, SqlConnection con, SqlTransaction tran)
    {
      SqlCommand scmd = new SqlCommand("select * from [" + row.Table.TableName +
        "] where auto = @auto", con);
      scmd.Transaction = tran;
      SqlDataReader reader = null;
      SqlCommand updateCmd = null;
      string my = string.Empty;
      string db = string.Empty;
      string updated = string.Empty;
      scmd.Parameters.Add("@auto", SqlDbType.Int);
      scmd.Parameters["@auto"].Value = GetValue(row, "Auto");
      object[] dbr = null;

      for (int i = 0; i < row.Table.Columns.Count; i++)
        my += row.Table.Columns[i].ColumnName + ": " + GetValue(row, i).ToString() +
          (i == row.Table.Columns.Count - 1 ? string.Empty : ", ");
      try
      {
        reader = scmd.ExecuteReader();
        while (reader.Read())
        {
          for (int i = 0; i < reader.FieldCount; i++)
          {
            db += reader.GetName(i) + ": " + reader.GetValue(i).ToString() +
              (i == reader.FieldCount - 1 ? string.Empty : ", ");
            if (!reader.GetValue(i).Equals(GetValue(row, i)))
              updated += (updated == string.Empty ? string.Empty : ", ") +
                reader.GetName(i) + " " + GetValue(row, i).ToString() + " " +
                reader.GetValue(i).ToString();
            dbr = new object[reader.FieldCount];
            reader.GetValues(dbr);
          }
          if (row.RowState == DataRowState.Modified)
            updateCmd = GetPositionenUpdateCommand(row.Table.TableName, reader, row, tran);
          if (row.RowState == DataRowState.Deleted)
            updateCmd = GetPositionenDeleteCommand(row.Table.TableName, reader, tran);
        }
      }
      catch (Exception ex)
      {
        if (MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.RetryCancel, MessageBoxIcon.Question) == DialogResult.Retry)
          return UpdateResult.Retry;
        else
          return UpdateResult.Cancel;
      }
      finally
      {
        if (reader != null && !reader.IsClosed)
          reader.Close();
      }
      //      MessageBox.Show(bldr.GetInsertCommand().CommandText);
      //      MessageBox.Show(bldr.GetUpdateCommand().CommandText);
      if (db != string.Empty)
      {
        if (row.RowState == DataRowState.Modified)
          if (MessageBox.Show(row.RowError + " Soll der Datensatz geschpeichert werden?\n" +
          my + "\nin der Datenbank\n" + db + "\ngeändert " + updated,
          this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
          {/////////////////
            updateCmd.Connection = con;
            try
            {
              int c = updateCmd.ExecuteNonQuery();
              if (c == 0)
                throw new Exception("Der Datensatz wurde geändert.");
              row.RowError = string.Empty;
              row.AcceptChanges();
            }
            catch (Exception ex)
            {
              if (MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.RetryCancel, MessageBoxIcon.Question) == DialogResult.Retry)
                return UpdateResult.Retry;
              else
                return UpdateResult.Cancel;
            }
          }////////////////
          else
          {
            for (int i = 0; i < dbr.Length; i++)
              row[i] = dbr[i];
            row.RowError = string.Empty;
            row.AcceptChanges();
            return UpdateResult.Cancel;
          }
        if (row.RowState == DataRowState.Deleted)
          if (MessageBox.Show(row.RowError + " Soll der Datensatz gelöscht werden?\n" +
          my + "\nin der Datenbank\n" + db + "\ngeändert " + updated,
          this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
          {//////////////
            updateCmd.Connection = con;
            try
            {
              int c = updateCmd.ExecuteNonQuery();
              if (c == 0)
                throw new Exception("Der Datensatz wurde geändert.");
              row.RowError = string.Empty;
              row.AcceptChanges();
            }
            catch (Exception ex)
            {
              if (MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.RetryCancel, MessageBoxIcon.Question) == DialogResult.Retry)
                return UpdateResult.Retry;
              else
                return UpdateResult.Cancel;
            }
          }////////////////
          else
          {
            row.RejectChanges();
            for (int i = 0; i < dbr.Length; i++)
              row[i] = dbr[i];
            row.RowError = string.Empty;
            row.AcceptChanges();
            return UpdateResult.Cancel;
          }
      }
      else
      {
        SqlCommand parentSelCmd = new SqlCommand("select * from [" + AbrechnungsJahr + "_Rezepte] where id = @id",
          con, tran);
        SqlDataReader parentReader = null;
        object[] parentValues = null;
        parentSelCmd.Parameters.Add("@id", SqlDbType.Int);
        parentSelCmd.Parameters["@id"].Value = row["ID"];
        try
        {
          parentReader = parentSelCmd.ExecuteReader();
          if (parentReader.Read())
          {
            parentValues = new object[parentReader.FieldCount];
            parentReader.GetValues(parentValues);
          }
          if (parentReader != null && !parentReader.IsClosed)
            parentReader.Close();
          if (row.RowState == DataRowState.Modified && parentValues != null &&
            MessageBox.Show("Datensatz wurde gelöscht." + " Soll der Datensatz geschpeichert werden?\n" + my,
            this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
          {
            SqlCommand cmd = GetPositionenInsertCommand(row.Table.TableName, row, tran);
            cmd.Connection = con;
            try
            {
              int newID = (int)cmd.ExecuteScalar();
              row.RowError = string.Empty;
              row["ID"] = newID;
              row.AcceptChanges();
            }
            catch (Exception ex)
            {
              if (MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.RetryCancel, MessageBoxIcon.Question) == DialogResult.Retry)
                return UpdateResult.Retry;
              else
                return UpdateResult.Cancel;
            }

          }
          else
          {
            if (parentValues == null && row.RowState == DataRowState.Modified &&
              MessageBox.Show("Rezept wurde gelöscht. Soll das Rezept gespeichert werden?\n",
              this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
            {
              try
              {
                DataRow parentRow = (voinDataSet.Rezepte.Select("ID = " + row["ID"].ToString()))[0];
                DataRow[] childRows = parentRow.GetChildRows(voinDataSet.Relations["FK_Rezepte_Positionen"]);
                SqlCommand pInsert = GetRezepteInsertCommand(parentRow.Table.TableName, parentRow, tran);
                //DataRow[] unfallRows = parentRow.GetChildRows(voinDataSet.Relations["FK_" + GetJahrName() + "_Rezepte_" + GetJahrName() + "_Unfall"]);
                pInsert.Connection = con;
                int id = (int)pInsert.ExecuteScalar();
                parentRow["ID"] = id;
                foreach (DataRow r in childRows)
                {
                  SqlCommand cInsert = GetPositionenInsertCommand(r.Table.TableName, r, tran);
                  cInsert.Connection = con;
                  int auto = (int)cInsert.ExecuteScalar();
                  r["AUTO"] = auto;
                }
                //if (unfallRows.Length > 0)
                //{
                //  SqlCommand uInsert = GetUnfallInsertCommand(unfallRows[0].Table.TableName, unfallRows[0], tran);
                //  uInsert.Connection = con;
                //  uInsert.ExecuteNonQuery();
                //}
              }
              catch (Exception e)
              {
                throw new Exception("Aktualisiernungsvorgang ist fehlgeschlagen.", e);
              }
            }
            else
            {
              row.RowError = string.Empty;
              if (row.RowState != DataRowState.Deleted)
                row.Delete();
              row.AcceptChanges();
              return UpdateResult.Cancel;
            }
          }
        }
        catch (Exception e)
        {
          if (e.Message == "Aktualisiernungsvorgang ist fehlgeschlagen.")
          {
            throw e;
          }
          if (MessageBox.Show(e.Message, this.Text, MessageBoxButtons.RetryCancel, MessageBoxIcon.Error) == DialogResult.Retry)
            return UpdateResult.Retry;
          else
            return UpdateResult.Cancel;
        }
        finally
        {
          if (parentReader != null && !parentReader.IsClosed)
            parentReader.Close();
        }

      }
      return UpdateResult.Updated;

    }

    private SqlCommand GetPositionenInsertCommand(string tableName, DataRow row, SqlTransaction tran)
    {
      SqlCommand cmd = new SqlCommand("INSERT INTO [" + tableName + "] (ID, " +
        "Tarifkennzeichen, BuPos, Faktor, EPreis, Anzahl, GebArt, Gebuehr, BehDat, " +
        "Mitarbeiter, Mwst_KZ, Mwst, BuPos_Sonder, Hilfsmittel_KZ, " +
        "Hilfsmittel_InvNr, Kilometer, Uhrzeit_von, Uhrzeit_bis, Dauer_Min, " +
        "Versorgung_von, Versorgung_bis, Text, Zuzahlungsart, Zuzahlung, " +
        "Eigenanteil, VZeitraum, Verbrauch) VALUES     (@ID,@Tarifkennzeichen," +
        "@BuPos,@Faktor,@EPreis,@Anzahl,@GebArt,@Gebuehr,@BehDat,@Mitarbeiter," +
        "@Mwst_KZ,@Mwst,@BuPos_Sonder,@Hilfsmittel_KZ,@Hilfsmittel_InvNr," +
        "@Kilometer,@Uhrzeit_von,@Uhrzeit_bis,@Dauer_Min,@Versorgung_von," +
        "@Versorgung_bis,@Text,@Zuzahlungsart,@Zuzahlung,@Eigenanteil,@VZeitraum," +
        "@Verbrauch); SELECT CAST(scope_identity() AS int)");
      cmd.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.Int, 0, "ID"),
            new System.Data.SqlClient.SqlParameter("@Tarifkennzeichen", System.Data.SqlDbType.VarChar, 0, "Tarifkennzeichen"),
            new System.Data.SqlClient.SqlParameter("@BuPos", System.Data.SqlDbType.VarChar, 0, "BuPos"),
            new System.Data.SqlClient.SqlParameter("@Faktor", System.Data.SqlDbType.Money, 0, "Faktor"),
            new System.Data.SqlClient.SqlParameter("@EPreis", System.Data.SqlDbType.Money, 0, "EPreis"),
            new System.Data.SqlClient.SqlParameter("@Anzahl", System.Data.SqlDbType.Money, 0, "Anzahl"),
            new System.Data.SqlClient.SqlParameter("@GebArt", System.Data.SqlDbType.SmallInt, 0, "GebArt"),
            new System.Data.SqlClient.SqlParameter("@Gebuehr", System.Data.SqlDbType.Money, 0, "Gebuehr"),
            new System.Data.SqlClient.SqlParameter("@BehDat", System.Data.SqlDbType.DateTime, 0, "BehDat"),
            new System.Data.SqlClient.SqlParameter("@Mitarbeiter", System.Data.SqlDbType.VarChar, 0, "Mitarbeiter"),
            new System.Data.SqlClient.SqlParameter("@Mwst_KZ", System.Data.SqlDbType.SmallInt, 0, "Mwst_KZ"),
            new System.Data.SqlClient.SqlParameter("@Mwst", System.Data.SqlDbType.Money, 0, "Mwst"),
            new System.Data.SqlClient.SqlParameter("@BuPos_Sonder", System.Data.SqlDbType.VarChar, 0, "BuPos_Sonder"),
            new System.Data.SqlClient.SqlParameter("@Hilfsmittel_KZ", System.Data.SqlDbType.SmallInt, 0, "Hilfsmittel_KZ"),
            new System.Data.SqlClient.SqlParameter("@Hilfsmittel_InvNr", System.Data.SqlDbType.VarChar, 0, "Hilfsmittel_InvNr"),
            new System.Data.SqlClient.SqlParameter("@Kilometer", System.Data.SqlDbType.Money, 0, "Kilometer"),
            new System.Data.SqlClient.SqlParameter("@Uhrzeit_von", System.Data.SqlDbType.VarChar, 0, "Uhrzeit_von"),
            new System.Data.SqlClient.SqlParameter("@Uhrzeit_bis", System.Data.SqlDbType.VarChar, 0, "Uhrzeit_bis"),
            new System.Data.SqlClient.SqlParameter("@Dauer_Min", System.Data.SqlDbType.Int, 0, "Dauer_Min"),
            new System.Data.SqlClient.SqlParameter("@Versorgung_von", System.Data.SqlDbType.DateTime, 0, "Versorgung_von"),
            new System.Data.SqlClient.SqlParameter("@Versorgung_bis", System.Data.SqlDbType.DateTime, 0, "Versorgung_bis"),
            new System.Data.SqlClient.SqlParameter("@Text", System.Data.SqlDbType.VarChar, 0, "Text"),
            new System.Data.SqlClient.SqlParameter("@Zuzahlungsart", System.Data.SqlDbType.SmallInt, 0, "Zuzahlungsart"),
            new System.Data.SqlClient.SqlParameter("@Zuzahlung", System.Data.SqlDbType.Money, 0, "Zuzahlung"),
            new System.Data.SqlClient.SqlParameter("@Eigenanteil", System.Data.SqlDbType.Money, 0, "Eigenanteil"),
            new System.Data.SqlClient.SqlParameter("@VZeitraum", System.Data.SqlDbType.SmallInt, 0, "VZeitraum"),
            new System.Data.SqlClient.SqlParameter("@Verbrauch", System.Data.SqlDbType.SmallInt, 0, "Verbrauch")});

      for (int i = 0; i < row.ItemArray.Length; i++)
        if (row.Table.Columns[i].ColumnName.ToUpper() != "AUTO")
          cmd.Parameters["@" + row.Table.Columns[i].ColumnName].Value = row[i];
      cmd.Transaction = tran;
      return cmd;
    }

    private SqlCommand GetPositionenDeleteCommand(string tableName, SqlDataReader reader, SqlTransaction tran)
    {
      SqlCommand cmd = new SqlCommand("DELETE FROM [" + tableName + "] WHERE " +
        "(AUTO = @Original_AUTO) AND (ID = @Original_ID) AND (Tarifkennzeichen = " +
        "@Original_Tarifkennzeichen) AND (BuPos = @Original_BuPos) AND (Faktor = " +
        "@Original_Faktor) AND (EPreis = @Original_EPreis) AND (Anzahl = " +
        "@Original_Anzahl) AND (GebArt = @Original_GebArt) AND (Gebuehr = " +
        "@Original_Gebuehr) AND (BehDat = @Original_BehDat) AND " +
        "(@IsNull_Mitarbeiter = 1 AND Mitarbeiter IS NULL OR Mitarbeiter = " +
        "@Original_Mitarbeiter) AND (@IsNull_Mwst_KZ = 1 AND Mwst_KZ IS NULL OR " +
        "Mwst_KZ = @Original_Mwst_KZ) AND (@IsNull_Mwst = 1 AND Mwst IS NULL OR " +
        "Mwst = @Original_Mwst) AND (@IsNull_BuPos_Sonder = 1 AND BuPos_Sonder IS " +
        "NULL OR BuPos_Sonder = @Original_BuPos_Sonder) AND " +
        "(@IsNull_Hilfsmittel_KZ = 1 AND Hilfsmittel_KZ IS NULL OR Hilfsmittel_KZ = " +
        "@Original_Hilfsmittel_KZ) AND (@IsNull_Hilfsmittel_InvNr = 1 AND " +
        "Hilfsmittel_InvNr IS NULL OR Hilfsmittel_InvNr = " +
        "@Original_Hilfsmittel_InvNr) AND (@IsNull_Kilometer = 1 AND Kilometer IS " +
        "NULL OR Kilometer = @Original_Kilometer) AND (@IsNull_Uhrzeit_von = 1 AND " +
        "Uhrzeit_von IS NULL OR Uhrzeit_von = @Original_Uhrzeit_von) AND " +
        "(@IsNull_Uhrzeit_bis = 1 AND Uhrzeit_bis IS NULL OR Uhrzeit_bis = " +
        "@Original_Uhrzeit_bis) AND (@IsNull_Dauer_Min = 1 AND Dauer_Min IS NULL OR " +
        "Dauer_Min = @Original_Dauer_Min) AND (@IsNull_Versorgung_von = 1 AND " +
        "Versorgung_von IS NULL OR Versorgung_von = @Original_Versorgung_von) AND " +
        "(@IsNull_Versorgung_bis = 1 AND Versorgung_bis IS NULL OR Versorgung_bis = " +
        "@Original_Versorgung_bis) AND (@IsNull_Text = 1 AND Text IS NULL OR " +
        "Text = @Original_Text) AND (@IsNull_Zuzahlungsart = 1 AND Zuzahlungsart " +
        "IS NULL OR Zuzahlungsart = @Original_Zuzahlungsart) AND (@IsNull_Zuzahlung " +
        "= 1 AND Zuzahlung IS NULL OR Zuzahlung = @Original_Zuzahlung) AND " +
        "(@IsNull_Eigenanteil = 1 AND Eigenanteil IS NULL OR Eigenanteil = " +
        "@Original_Eigenanteil) AND (@IsNull_VZeitraum = 1 AND VZeitraum IS NULL OR " +
        "VZeitraum = @Original_VZeitraum) AND (@IsNull_Verbrauch = 1 AND Verbrauch " +
        "IS NULL OR Verbrauch = @Original_Verbrauch)");
      cmd.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Original_AUTO", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "AUTO", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "ID", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Tarifkennzeichen", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Tarifkennzeichen", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_BuPos", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "BuPos", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Faktor", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Faktor", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_EPreis", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "EPreis", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Anzahl", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Anzahl", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_GebArt", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "GebArt", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Gebuehr", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Gebuehr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_BehDat", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "BehDat", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Mitarbeiter", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Mitarbeiter", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Mitarbeiter", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Mitarbeiter", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Mwst_KZ", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Mwst_KZ", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Mwst_KZ", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Mwst_KZ", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Mwst", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Mwst", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Mwst", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Mwst", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_BuPos_Sonder", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "BuPos_Sonder", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_BuPos_Sonder", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "BuPos_Sonder", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Hilfsmittel_KZ", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Hilfsmittel_KZ", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Hilfsmittel_KZ", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Hilfsmittel_KZ", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Hilfsmittel_InvNr", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Hilfsmittel_InvNr", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Hilfsmittel_InvNr", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Hilfsmittel_InvNr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Kilometer", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Kilometer", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Kilometer", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Kilometer", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Uhrzeit_von", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Uhrzeit_von", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Uhrzeit_von", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Uhrzeit_von", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Uhrzeit_bis", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Uhrzeit_bis", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Uhrzeit_bis", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Uhrzeit_bis", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Dauer_Min", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Dauer_Min", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Dauer_Min", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Dauer_Min", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Versorgung_von", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Versorgung_von", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Versorgung_von", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Versorgung_von", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Versorgung_bis", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Versorgung_bis", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Versorgung_bis", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Versorgung_bis", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Text", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Text", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Text", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Text", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Zuzahlungsart", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Zuzahlungsart", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Zuzahlungsart", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Zuzahlungsart", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Zuzahlung", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Zuzahlung", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Zuzahlung", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Zuzahlung", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Eigenanteil", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Eigenanteil", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Eigenanteil", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Eigenanteil", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_VZeitraum", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "VZeitraum", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_VZeitraum", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "VZeitraum", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Verbrauch", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Verbrauch", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Verbrauch", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Verbrauch", System.Data.DataRowVersion.Original, null)});
      DataTable t = reader.GetSchemaTable();
      for (int i = 0; i < reader.FieldCount; i++)
      {
        cmd.Parameters["@Original_" + reader.GetName(i)].Value = reader.GetValue(i);
        DataRow[] rows = t.Select("ColumnName = '" + reader.GetName(i) + "'");
        if ((bool)rows[0]["AllowDBNull"])
          if (reader.IsDBNull(i))
            cmd.Parameters["@IsNull_" + reader.GetName(i)].Value = 1;
          else
            cmd.Parameters["@IsNull_" + reader.GetName(i)].Value = 0;
      }
      cmd.Transaction = tran;
      return cmd;
    }

    private SqlCommand GetPositionenUpdateCommand(string tableName, SqlDataReader reader, DataRow row, SqlTransaction tran)
    {
      string cmdText = "UPDATE [" + tableName + "] SET ID = @ID, Tarifkennzeichen = " +
        "@Tarifkennzeichen, BuPos = @BuPos, Faktor = @Faktor, EPreis = @EPreis, " +
        "Anzahl = @Anzahl, GebArt = @GebArt, Gebuehr = @Gebuehr, BehDat = @BehDat, " +
        "Mitarbeiter = @Mitarbeiter, Mwst_KZ = @Mwst_KZ, Mwst = @Mwst, " +
        "BuPos_Sonder = @BuPos_Sonder, Hilfsmittel_KZ = @Hilfsmittel_KZ, " +
        "Hilfsmittel_InvNr = @Hilfsmittel_InvNr, Kilometer = @Kilometer, " +
        "Uhrzeit_von = @Uhrzeit_von, Uhrzeit_bis = @Uhrzeit_bis, Dauer_Min = " +
        "@Dauer_Min, Versorgung_von = @Versorgung_von, Versorgung_bis = " +
        "@Versorgung_bis, Text = @Text, Zuzahlungsart = @Zuzahlungsart, Zuzahlung = " +
        "@Zuzahlung, Eigenanteil = @Eigenanteil, VZeitraum = @VZeitraum, Verbrauch " +
        "= @Verbrauch WHERE (AUTO = @Original_AUTO) AND (ID = @Original_ID) AND " +
        "(Tarifkennzeichen = @Original_Tarifkennzeichen) AND (BuPos = " +
        "@Original_BuPos) AND (Faktor = @Original_Faktor) AND (EPreis = " +
        "@Original_EPreis) AND (Anzahl = @Original_Anzahl) AND (GebArt = " +
        "@Original_GebArt) AND (Gebuehr = @Original_Gebuehr) AND (BehDat = " +
        "@Original_BehDat) AND (@IsNull_Mitarbeiter = 1 AND Mitarbeiter IS NULL OR " +
        "Mitarbeiter = @Original_Mitarbeiter) AND (@IsNull_Mwst_KZ = 1 AND Mwst_KZ " +
        "IS NULL OR Mwst_KZ = @Original_Mwst_KZ) AND (@IsNull_Mwst = 1 AND Mwst IS " +
        "NULL OR Mwst = @Original_Mwst) AND (@IsNull_BuPos_Sonder = 1 AND " +
        "BuPos_Sonder IS NULL OR BuPos_Sonder = @Original_BuPos_Sonder) AND " +
        "(@IsNull_Hilfsmittel_KZ = 1 AND Hilfsmittel_KZ IS NULL OR Hilfsmittel_KZ = " +
        "@Original_Hilfsmittel_KZ) AND (@IsNull_Hilfsmittel_InvNr = 1 AND " +
        "Hilfsmittel_InvNr IS NULL OR Hilfsmittel_InvNr = " +
        "@Original_Hilfsmittel_InvNr) AND (@IsNull_Kilometer = 1 AND Kilometer " +
        "IS NULL OR Kilometer = @Original_Kilometer) AND (@IsNull_Uhrzeit_von = 1 " +
        "AND Uhrzeit_von IS NULL OR Uhrzeit_von = @Original_Uhrzeit_von) AND " +
        "(@IsNull_Uhrzeit_bis = 1 AND Uhrzeit_bis IS NULL OR Uhrzeit_bis = " +
        "@Original_Uhrzeit_bis) AND (@IsNull_Dauer_Min = 1 AND Dauer_Min IS NULL OR " +
        "Dauer_Min = @Original_Dauer_Min) AND (@IsNull_Versorgung_von = 1 AND " +
        "Versorgung_von IS NULL OR Versorgung_von = @Original_Versorgung_von) AND " +
        "(@IsNull_Versorgung_bis = 1 AND Versorgung_bis IS NULL OR Versorgung_bis = " +
        "@Original_Versorgung_bis) AND (@IsNull_Text = 1 AND Text IS NULL OR Text = " +
        "@Original_Text) AND (@IsNull_Zuzahlungsart = 1 AND Zuzahlungsart IS NULL OR " +
        "Zuzahlungsart = @Original_Zuzahlungsart) AND (@IsNull_Zuzahlung = 1 AND " +
        "Zuzahlung IS NULL OR Zuzahlung = @Original_Zuzahlung) AND " +
        "(@IsNull_Eigenanteil = 1 AND Eigenanteil IS NULL OR Eigenanteil = " +
        "@Original_Eigenanteil) AND (@IsNull_VZeitraum = 1 AND VZeitraum IS NULL OR " +
        "VZeitraum = @Original_VZeitraum) AND (@IsNull_Verbrauch = 1 AND Verbrauch " +
        "IS NULL OR Verbrauch = @Original_Verbrauch)";
      SqlCommand cmd = new SqlCommand(cmdText);
      cmd.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.Int, 0, "ID"),
            new System.Data.SqlClient.SqlParameter("@Tarifkennzeichen", System.Data.SqlDbType.VarChar, 0, "Tarifkennzeichen"),
            new System.Data.SqlClient.SqlParameter("@BuPos", System.Data.SqlDbType.VarChar, 0, "BuPos"),
            new System.Data.SqlClient.SqlParameter("@Faktor", System.Data.SqlDbType.Money, 0, "Faktor"),
            new System.Data.SqlClient.SqlParameter("@EPreis", System.Data.SqlDbType.Money, 0, "EPreis"),
            new System.Data.SqlClient.SqlParameter("@Anzahl", System.Data.SqlDbType.Money, 0, "Anzahl"),
            new System.Data.SqlClient.SqlParameter("@GebArt", System.Data.SqlDbType.SmallInt, 0, "GebArt"),
            new System.Data.SqlClient.SqlParameter("@Gebuehr", System.Data.SqlDbType.Money, 0, "Gebuehr"),
            new System.Data.SqlClient.SqlParameter("@BehDat", System.Data.SqlDbType.DateTime, 0, "BehDat"),
            new System.Data.SqlClient.SqlParameter("@Mitarbeiter", System.Data.SqlDbType.VarChar, 0, "Mitarbeiter"),
            new System.Data.SqlClient.SqlParameter("@Mwst_KZ", System.Data.SqlDbType.SmallInt, 0, "Mwst_KZ"),
            new System.Data.SqlClient.SqlParameter("@Mwst", System.Data.SqlDbType.Money, 0, "Mwst"),
            new System.Data.SqlClient.SqlParameter("@BuPos_Sonder", System.Data.SqlDbType.VarChar, 0, "BuPos_Sonder"),
            new System.Data.SqlClient.SqlParameter("@Hilfsmittel_KZ", System.Data.SqlDbType.SmallInt, 0, "Hilfsmittel_KZ"),
            new System.Data.SqlClient.SqlParameter("@Hilfsmittel_InvNr", System.Data.SqlDbType.VarChar, 0, "Hilfsmittel_InvNr"),
            new System.Data.SqlClient.SqlParameter("@Kilometer", System.Data.SqlDbType.Money, 0, "Kilometer"),
            new System.Data.SqlClient.SqlParameter("@Uhrzeit_von", System.Data.SqlDbType.VarChar, 0, "Uhrzeit_von"),
            new System.Data.SqlClient.SqlParameter("@Uhrzeit_bis", System.Data.SqlDbType.VarChar, 0, "Uhrzeit_bis"),
            new System.Data.SqlClient.SqlParameter("@Dauer_Min", System.Data.SqlDbType.Int, 0, "Dauer_Min"),
            new System.Data.SqlClient.SqlParameter("@Versorgung_von", System.Data.SqlDbType.DateTime, 0, "Versorgung_von"),
            new System.Data.SqlClient.SqlParameter("@Versorgung_bis", System.Data.SqlDbType.DateTime, 0, "Versorgung_bis"),
            new System.Data.SqlClient.SqlParameter("@Text", System.Data.SqlDbType.VarChar, 0, "Text"),
            new System.Data.SqlClient.SqlParameter("@Zuzahlungsart", System.Data.SqlDbType.SmallInt, 0, "Zuzahlungsart"),
            new System.Data.SqlClient.SqlParameter("@Zuzahlung", System.Data.SqlDbType.Money, 0, "Zuzahlung"),
            new System.Data.SqlClient.SqlParameter("@Eigenanteil", System.Data.SqlDbType.Money, 0, "Eigenanteil"),
            new System.Data.SqlClient.SqlParameter("@VZeitraum", System.Data.SqlDbType.SmallInt, 0, "VZeitraum"),
            new System.Data.SqlClient.SqlParameter("@Verbrauch", System.Data.SqlDbType.SmallInt, 0, "Verbrauch"),
            new System.Data.SqlClient.SqlParameter("@Original_AUTO", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "AUTO", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "ID", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Tarifkennzeichen", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Tarifkennzeichen", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_BuPos", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "BuPos", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Faktor", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Faktor", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_EPreis", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "EPreis", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Anzahl", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Anzahl", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_GebArt", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "GebArt", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Gebuehr", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Gebuehr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_BehDat", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "BehDat", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Mitarbeiter", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Mitarbeiter", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Mitarbeiter", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Mitarbeiter", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Mwst_KZ", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Mwst_KZ", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Mwst_KZ", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Mwst_KZ", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Mwst", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Mwst", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Mwst", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Mwst", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_BuPos_Sonder", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "BuPos_Sonder", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_BuPos_Sonder", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "BuPos_Sonder", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Hilfsmittel_KZ", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Hilfsmittel_KZ", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Hilfsmittel_KZ", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Hilfsmittel_KZ", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Hilfsmittel_InvNr", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Hilfsmittel_InvNr", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Hilfsmittel_InvNr", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Hilfsmittel_InvNr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Kilometer", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Kilometer", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Kilometer", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Kilometer", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Uhrzeit_von", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Uhrzeit_von", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Uhrzeit_von", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Uhrzeit_von", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Uhrzeit_bis", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Uhrzeit_bis", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Uhrzeit_bis", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Uhrzeit_bis", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Dauer_Min", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Dauer_Min", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Dauer_Min", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Dauer_Min", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Versorgung_von", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Versorgung_von", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Versorgung_von", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Versorgung_von", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Versorgung_bis", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Versorgung_bis", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Versorgung_bis", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Versorgung_bis", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Text", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Text", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Text", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Text", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Zuzahlungsart", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Zuzahlungsart", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Zuzahlungsart", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Zuzahlungsart", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Zuzahlung", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Zuzahlung", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Zuzahlung", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Zuzahlung", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Eigenanteil", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Eigenanteil", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Eigenanteil", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Eigenanteil", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_VZeitraum", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "VZeitraum", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_VZeitraum", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "VZeitraum", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Verbrauch", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Verbrauch", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Verbrauch", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Verbrauch", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@AUTO", System.Data.SqlDbType.Int, 4, "AUTO")});
      for (int i = 0; i < row.ItemArray.Length; i++)
        cmd.Parameters["@" + row.Table.Columns[i].ColumnName].Value = row[i];
      for (int i = 0; i < reader.FieldCount; i++)
      {
        cmd.Parameters["@Original_" + reader.GetName(i)].Value = reader.GetValue(i);
        if (row.Table.Columns[reader.GetName(i)].AllowDBNull)
          if (reader.IsDBNull(i))
            cmd.Parameters["@IsNull_" + reader.GetName(i)].Value = 1;
          else
            cmd.Parameters["@IsNull_" + reader.GetName(i)].Value = 0;
      }
      cmd.Transaction = tran;
      return cmd;
    }

    #endregion

    private void befreitCheckBox_Click(object sender, EventArgs e)
    {
      if (rezepteBindingSource.Count > 0 && rezepteBindingSource.Current != null)
      {
        DataRow row = ((DataRowView)rezepteBindingSource.Current).Row;
        if (row.RowState != DataRowState.Detached && row.RowState != DataRowState.Deleted)
          rezepteBindingSource.EndEdit();
        UpdatePreis(true);
      }
    }

    private void nrComboBox_Enter(object sender, EventArgs e)
    {
      ValidateRezept(true);
    }

    private bool ValidateRezept(bool savePosition)
    {
      bool canceled;
      return ValidateRezept(savePosition, out canceled);
    }

    private bool ValidateRezept(bool savePosition, out bool canceled)
    {
      bool res = true;
      canceled = false;
      if (rezeptValidating)
      {
        try
        {
          errorProvider.Clear();
          SaveCurrent(savePosition);
        }
        catch (Exception ex)
        {
          if (MessageBox.Show(ex.Message + "\nSoll der aktuelle Bearbeitungsvorgang abgebrochen werden?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
          {
            if (rezepteBindingSource.Current != null)
            {
              canceled = true;
              DeleteFromNamesList((int)(rezepteBindingSource.Current as DataRowView).Row["id"]);
              rezepteBindingSource.CancelEdit();
            }
          }
          else
          {
            bool f = true;
            res = false;
            if (ex.Message.IndexOf("'") != -1)
            {
              string spalte = ex.Message.Substring(ex.Message.IndexOf("'") + 1, ex.Message.IndexOf("'", ex.Message.IndexOf("'") + 1) - ex.Message.IndexOf("'") - 1);
              for (int i = 0; i < panel4.Controls.Count && f; i++)
              {
                Control c = panel4.Controls[i];
                foreach (Binding b in c.DataBindings)
                {
                  if (b.BindingMemberInfo.BindingField == spalte && c.Enabled)
                  {
                    errorProvider.SetError(c, ex.Message);
                    f = false;
                    c.Focus();
                    break;
                  }
                }
              }
              if (f && spalte == "Status")
                if (status1TB.Text == string.Empty && status1TB.Enabled)
                {
                  errorProvider.SetError(status1TB, ex.Message);
                  f = false;
                  status1TB.Focus();
                }
                else if (status2TB.Text == string.Empty && status2TB.Enabled)
                {
                  errorProvider.SetError(status2TB, ex.Message);
                  f = false;
                  status2TB.Focus();
                }
            }
            if (f && nameTB.Enabled) nameTB.Focus();
            else if (f && ikTB.Enabled) ikTB.Focus();
          }
        }
      }
      return res;
    }

    private void positionenPanel_Enter(object sender, EventArgs e)
    {
      ValidateRezept(true);
    }

    private void splitContainer1_Panel1_Enter(object sender, EventArgs e)
    {
      ValidateRezept(true);
    }

    private void rezepteBindingSource_ListChanged(object sender, ListChangedEventArgs e)
    {
      UpdateBindingNavigator(sender as BindingSource);
    }

    private void UpdateBindingNavigator(BindingSource bs)
    {
      if (bs != null)
      {
        if (bs.Count > 0)
        {
          if (bs.Position > 0)
          {
            bindingNavigatorMoveFirstItem.Enabled = true;
            bindingNavigatorMovePreviousItem.Enabled = true;
          }
          else
          {
            bindingNavigatorMoveFirstItem.Enabled = false;
            bindingNavigatorMovePreviousItem.Enabled = false;
          }
          bindingNavigatorPositionItem.Text = (bs.Position + 1).ToString();
          bindingNavigatorPositionItem.Enabled = true;
          if (bs.Position < bs.Count - 1)
          {
            bindingNavigatorMoveNextItem.Enabled = true;
            bindingNavigatorMoveLastItem.Enabled = true;
          }
          else
          {
            bindingNavigatorMoveNextItem.Enabled = false;
            bindingNavigatorMoveLastItem.Enabled = false;
          }
          bindingNavigatorDeleteItem.Enabled = true;
        }
        else
        {
          bindingNavigatorMoveFirstItem.Enabled = false;
          bindingNavigatorMovePreviousItem.Enabled = false;
          bindingNavigatorPositionItem.Enabled = false;
          bindingNavigatorPositionItem.Text = "0";
          bindingNavigatorMoveNextItem.Enabled = false;
          bindingNavigatorMoveLastItem.Enabled = false;
          bindingNavigatorDeleteItem.Enabled = false;
        }
      }
    }

    bool positionChanging = false;

    private void rezepteBindingSource_PositionChanged(object sender, EventArgs e)
    {
      if (!positionChanging)
        try
        {
          positionChanging = true;
          //MessageBox.Show("PositionChanged");
          bm_CurrentChanged(((BindingSource)sender).CurrencyManager, e);
          UpdateBindingNavigator(sender as BindingSource);
        }
        finally
        {
          positionChanging = false;
        }
    }

    private void bindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
    {
      if (ValidatePosition() && ValidateRezept(true))
      {
        rezepteBindingSource.Position = 0;
      }
    }

    private void bindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
    {
      if (ValidatePosition() && ValidateRezept(true))
      {
        rezepteBindingSource.Position--;
      }
    }

    private void bindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
    {
      if (ValidatePosition() && ValidateRezept(true))
      {
        rezepteBindingSource.Position++;
      }
    }

    private void bindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
    {
      if (ValidatePosition() && ValidateRezept(true))
      {
        rezepteBindingSource.Position = rezepteBindingSource.Count - 1;
      }
    }

    private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
    {
      bool firePositionChanged = false;
      if (rezepteBindingSource.Count > 0 && rezepteBindingSource.Current != null &&
        rezepteBindingSource.Position != rezepteBindingSource.Count - 1)
        firePositionChanged = true;
      if (rezepteBindingSource.Count > 0 && rezepteBindingSource.Current != null &&
        (rezepteBindingSource.Current as DataRowView).Row.RowState == DataRowState.Detached)
      {
        errorProvider.Clear();
        DeleteFromNamesList((int)(rezepteBindingSource.Current as DataRowView).Row["id"]);
        rezepteBindingSource.CancelEdit();
      }
      else if (ValidatePosition() && ValidateRezept(true))
      {
        errorProvider.Clear();
        rezepteBindingSource.RemoveCurrent();
      }
      if (firePositionChanged)
        rezepteBindingSource_PositionChanged(rezepteBindingSource, EventArgs.Empty);
    }

    private void bindingNavigatorPositionItem_Validating(object sender, CancelEventArgs e)
    {
      try
      {
        int pos = int.Parse(bindingNavigatorPositionItem.Text) - 1;
        if (rezepteBindingSource.Position != pos && ValidatePosition() && ValidateRezept(true))
          rezepteBindingSource.Position = pos;
      }
      catch
      {
      }
      bindingNavigatorPositionItem.Text = (rezepteBindingSource.Position + 1).ToString();
    }

    private void abrechnenButton_Click(object sender, EventArgs e)
    {
      if (namesListBox.SelectedItems.Count > 0 && ValidateRezept(false) &&
        MessageBox.Show("Sollen die ausgewählten Rezepte abgerechnet werden?", this.Text,
        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
      {
        foreach (Rezept rezept in namesListBox.SelectedItems)
        {
          DataRow[] rows = voinDataSet.Rezepte.Select("ID = " + rezept.ID.ToString());
          if (rows.Length > 0 && rows[0].RowState != DataRowState.Deleted &&
            rows[0].RowState != DataRowState.Detached)
            rows[0]["Vorgang"] = 0;
        }
        UpdateDataSet(true);
      }
    }

    private void BesonderheitenBtn_Click(object sender, EventArgs e)
    {
      if (mandantenBindingSource.Count > 0 && mandantenBindingSource.Current != null)
        using (BesonderheitenForm form = new BesonderheitenForm())
        {
          form.Mandant = ((DataRowView)mandantenBindingSource.Current).Row;
          form.DataSet = voinDataSet;
          form.DataAdapter = mandantenBesDA;
          form.ShowDialog();
        }
    }

    private void maskedTextBox_TextChanged(object sender, EventArgs e)
    {
      MaskedTextBox mtb = sender as MaskedTextBox;
      int selStart = mtb.SelectionStart;
      int selLen = mtb.SelectionLength;
      mtb.Text = mtb.Lines[0];
      mtb.SelectionStart = selStart;
      mtb.SelectionLength = selLen;
    }

    private void positionsnummerMTBs_Validating(object sender, CancelEventArgs e)
    {
      UpdatePositionsnummer((MaskedTextBox)sender);
    }

    private void UpdatePositionsnummer(MaskedTextBox mtb)
    {
      DataRow[] rows = new DataRow[0];
      while (mtb.Text.Length < mtb.Mask.Length && mtb.Text.Length != 0)
        mtb.Text = "0" + mtb.Text;
      if (mtb == gruppeMTB)
      {
        gruppeLabel.Text = string.Empty;
        if (gruppeMTB.Text != string.Empty)
        {
          rows = voinDataSet._10_GRUPPE.Select("GRUPPE = " + gruppeMTB.Text);
          if (rows.Length != 0)
            gruppeLabel.Text = (string)rows[0]["Bezeichnung"];
        }
      }
      if (mtb == ortMTB)
      {
        ortLabel.Text = string.Empty;
        if (ortMTB.Text != string.Empty)
        {
          rows = voinDataSet._10_ORT.Select("ORT = " + ortMTB.Text);
          if (rows.Length != 0)
            ortLabel.Text = (string)rows[0]["Bezeichnung"];
        }
      }
      if (mtb == unterGruppeMTB || mtb == gruppeMTB || mtb == ortMTB)
      {
        unterGruppeLabel.Text = string.Empty;
        if (gruppeMTB.Text != string.Empty && ortMTB.Text != string.Empty &&
          unterGruppeMTB.Text != string.Empty)
        {
          rows = voinDataSet._10_UNTER.Select("GRUPPE = " + gruppeMTB.Text +
            " AND ORT = " + ortMTB.Text + " AND UNTER = " + unterGruppeMTB.Text);
          if (rows.Length != 0)
            unterGruppeLabel.Text = (string)rows[0]["Bezeichnung"];
        }
      }
      if (mtb == artMTB || mtb == gruppeMTB || mtb == ortMTB || mtb == unterGruppeMTB)
      {
        artLabel.Text = string.Empty;
        if (gruppeMTB.Text != string.Empty && ortMTB.Text != string.Empty &&
          unterGruppeMTB.Text != string.Empty && artMTB.Text != string.Empty)
        {
          rows = voinDataSet._10_ART.Select("GRUPPE = " + gruppeMTB.Text + " AND ORT = " +
            ortMTB.Text + " AND UNTER = " + unterGruppeMTB.Text + " AND ART = " +
            artMTB.Text);
          if (rows.Length != 0)
            artLabel.Text = (string)rows[0]["Bezeichnung"];
        }
      }
      // Produkt
      produktLabel.Text = string.Empty;
      if (gruppeMTB.Text != string.Empty && ortMTB.Text != string.Empty &&
        unterGruppeMTB.Text != string.Empty && artMTB.Text != string.Empty &&
        produktMTB.Text != string.Empty)
      {
        rows = voinDataSet._10_PRODUKT.Select(" GRUPPE = " + gruppeMTB.Text +
          " AND ORT = " + ortMTB.Text + " AND UNTER = " + unterGruppeMTB.Text +
          " AND Art = " + artMTB.Text + " AND PRODUKT = " + produktMTB.Text);
        if (rows.Length != 0)
          produktLabel.Text = (string)rows[0]["Bezeichnung"];
      }
    }

    private void SearchPosition()
    {
      if (mandantenBindingSource.Count > 0 && mandantenBindingSource.Current != null)
        using (PositionsSearchForm form = new PositionsSearchForm())
        {
          form.DataSet = voinDataSet;
          form.Mandant = (int)((DataRowView)mandantenBindingSource.Current).Row["Nr"];
          form.TarifKZ = tarifSchlusselAbrechnCodeTB.Text;
          if (form.ShowDialog(this) == DialogResult.OK)
            if (form.HMP != string.Empty)
            {
              string hmp = form.HMP;
              gruppeMTB.Text = Substring(hmp, 0, 2);
              ortMTB.Text = Substring(hmp, 2, 2);
              unterGruppeMTB.Text = Substring(hmp, 4, 2);
              artMTB.Text = Substring(hmp, 6, 1);
              produktMTB.Text = Substring(hmp, 7, 3);
              UpdatePositionsnummer(gruppeMTB);
              UpdatePositionsnummer(ortMTB);
            }
        }
    }

    private string Substring(string str, int pos, int len)
    {
      string res = string.Empty;
      if (str != null && str.Length > pos)
      {
        int l = str.Length >= pos + len ? len : str.Length - pos;
        res = str.Substring(pos, l);
      }
      return res;
    }

    private void positionenMTBs_TextChanged(object sender, EventArgs e)
    {
      MaskedTextBox mtb = (MaskedTextBox)sender;
      if (mtb.Text.Length == mtb.Mask.Length && mtb.Focused)
        mtb.Parent.SelectNextControl(mtb, true, true, true, true);
    }

    private void rechnerBtn_Click(object sender, EventArgs e)
    {
      Process p = new Process();
      try
      {
        p.StartInfo.FileName = "calc.exe";
        p.Start();
      }
      catch (Win32Exception ex)
      {
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void UpdatePreis(bool calcZuzalung)
    {
      decimal preis;
      decimal anteil;
      decimal st;
      decimal forderung;
      decimal zuzahlung;
      MehrWertSteuer mwst;
      bool befreit = false;
      DataRow currentRezept = null;
      short zuzahlungsArt = 0;
      bool verbrauch = false;

      if (rezepteBindingSource.Count != 0 && rezepteBindingSource.Current != null)
      {
        currentRezept = ((DataRowView)rezepteBindingSource.Current).Row;
        befreit = (bool)currentRezept["Befreit"];
      }

      if (positionenBindingSource.Count > 0 && positionenBindingSource.Current != null)
      {
        DataRow row = ((DataRowView)positionenBindingSource.Current).Row;
        if (row["Verbrauch"] == DBNull.Value || (short)row["Verbrauch"] == 0)
          verbrauch = false;
        else
          verbrauch = true;
      }

      try { preis = Math.Round(decimal.Parse(ePreisTB.Text), 2, MidpointRounding.AwayFromZero); }
      catch { preis = 0; }

      try 
      {
        if (eigenanteilCB.Checked)
          anteil = Math.Round(decimal.Parse(eigenAnteilTB.Text), 2, MidpointRounding.AwayFromZero);
        else
          anteil = preis - Math.Round(decimal.Parse(eigenAnteilTB.Text), 2, MidpointRounding.AwayFromZero);
      }
      catch { anteil = 0; }

      mwst = (MehrWertSteuer)mwstCB.SelectedItem;
      if (nettoCB.Checked)
        st = Math.Round(preis * (mwst.MWSt / 100m), 2, MidpointRounding.AwayFromZero);
      else
        st = Math.Round(preis * (mwst.MWSt / 100m) / (1m + (mwst.MWSt / 100m)), 2, MidpointRounding.AwayFromZero);
      mwstTB.Text = st.ToString("N2");
      if (calcZuzalung)
      {
        zuzahlung = Math.Round(CalcZuzahlung(preis - anteil + (nettoCB.Checked ?
          st - anteil * mwst.MWSt / 100m : 0), befreit, verbrauch, out zuzahlungsArt), 2, MidpointRounding.AwayFromZero);
      }
      else
      {
        try { zuzahlung = Math.Round(decimal.Parse(zuzahlungTB.Text), 2, MidpointRounding.AwayFromZero); }
        catch { zuzahlung = 0; }
        short art1;
        decimal zuzahlung1 = Math.Round(CalcZuzahlung(preis - anteil + (nettoCB.Checked ?
          st - anteil * mwst.MWSt / 100m : 0), befreit, verbrauch, out art1), 2, MidpointRounding.AwayFromZero);
        if (nettoCB.Checked)
        {
          zuzahlung1 = Math.Round(zuzahlung1 / (1m + (mwst.MWSt / 100m)), 2, MidpointRounding.AwayFromZero);
        }
        if (zuzahlung == zuzahlung1) zuzahlungsArt = art1;
      }
      if (calcZuzalung && nettoCB.Checked)
      {
        zuzahlung = Math.Round(zuzahlung / (1m + (mwst.MWSt / 100m)), 2, MidpointRounding.AwayFromZero);
      }
      zuzahlungTB.Text = zuzahlung.ToString("N2");
      ShowZuzahlungsArt(zuzahlungsArt);
      forderung = preis - anteil - zuzahlung;
      //if (bruttoCB.Checked) forderung += st;
      forderung *= anzahlNUD.Value;
      forderungLabel.Text = Math.Round(forderung, 2, MidpointRounding.AwayFromZero).ToString("N2");
    }

    private void ShowZuzahlungsArt(short zuzahlungsArt)
    {
      switch (zuzahlungsArt)
      {
        case 0:
          zuzahlungsArtLabel.Text = string.Empty;
          zuzahlungsArtLabel.Tag = 0;
          zuzahlungsArtTT.SetToolTip(zuzahlungsArtLabel, string.Empty);
          break;
        case 1:
          zuzahlungsArtLabel.Text = "1 - Prozentuale Zzg §61";
          zuzahlungsArtLabel.Tag = 1;
          zuzahlungsArtTT.SetToolTip(zuzahlungsArtLabel, "Prozentuale Zuzahlung gemäß §61 Satz 1 SGB V");
          break;
        case 2:
          zuzahlungsArtLabel.Text = "2 - Zuzahlungsgrenzübertrag";
          zuzahlungsArtLabel.Tag = 2;
          zuzahlungsArtTT.SetToolTip(zuzahlungsArtLabel, "Grenzübertrag");
          break;
        case 3:
          zuzahlungsArtLabel.Text = "3 - Prozentuale Zuzg. Verbrauchszeitraum";
          zuzahlungsArtLabel.Tag = 3;
          zuzahlungsArtTT.SetToolTip(zuzahlungsArtLabel, "Prozentuale Zuzahlung für " +
            "den Verbrauchszeitraum gem. §33 Abs. 2, Satz 4, letzter Halbsatz SGB V " +
            "falls das Heilmittel zum Verbrauch bestimmt ist.");
          break;
        case 4:
          zuzahlungsArtLabel.Text = "4 - Max. Zuzg. Verbrauchszeitraum";
          zuzahlungsArtLabel.Tag = 4;
          zuzahlungsArtTT.SetToolTip(zuzahlungsArtLabel, string.Empty);
          break;
      }
      //if(positionenBindingSource.Count != 0 && positionenBindingSource.Current != null && 
      //  positionenBindingSource.AllowEdit)
      //{
      //  DataRow row = ((DataRowView)positionenBindingSource.Current).Row;
      //  if (row["Zuzahlungsart"] == DBNull.Value || (short)row["Zuzahlungsart"] != (int)zuzahlungsArtLabel.Tag)
      //    row["Zuzahlungsart"] = (short)(int)zuzahlungsArtLabel.Tag;
      //}
    }

    private decimal CalcZuzahlung(decimal preis, bool befreit, bool verbrauch, out short zuzahlungsArt)
    {
      decimal res = 0;
      if (verbrauch)
      {
        if (preis / 10 > 10) { res = 10; zuzahlungsArt = 4; }
        else { res = preis / 10; zuzahlungsArt = 3; }
      }
      else
      {
        if (preis / 10 < 5) { res = 5; zuzahlungsArt = 2; }
        else if (preis / 10 > 10) { res = 10; zuzahlungsArt = 2; }
        else { res = preis / 10; zuzahlungsArt = 1; }
      }
      if (res > preis) res = preis;
      if (befreit) { res = 0; zuzahlungsArt = 0; }
      return res;
    }

    private void nettoCB_CheckedChanged(object sender, EventArgs e)
    {
      if (nettoCB.Checked) nettoCB.Text = "Netto";
      else nettoCB.Text = "Brutto";
      decimal ePreis = 0;
      decimal eigenAnteil = 0;
      decimal zuzahlung = 0;
      MehrWertSteuer mwst = (MehrWertSteuer)mwstCB.SelectedItem;
      try { ePreis = decimal.Parse(ePreisTB.Text); }
      catch { }
      try 
      { 
        if(eigenanteilCB.Checked)
          eigenAnteil = decimal.Parse(eigenAnteilTB.Text); 
        else
          eigenAnteil = ePreis - decimal.Parse(eigenAnteilTB.Text); 
      }
      catch { }
      try { zuzahlung = decimal.Parse(zuzahlungTB.Text); }
      catch { }
      if (nettoCB.Checked)
      {
        ePreis /= 1 + mwst.MWSt / 100m;
        eigenAnteil /= 1 + mwst.MWSt / 100m;
        zuzahlung /= 1 + mwst.MWSt / 100m;
      }
      else
      {
        ePreis *= 1 + mwst.MWSt / 100m;
        eigenAnteil *= 1 + mwst.MWSt / 100m;
        zuzahlung *= 1 + mwst.MWSt / 100m;
      }
      ePreisTB.Text = ePreis.ToString("N2");
      if (eigenanteilCB.Checked)
        eigenAnteilTB.Text = eigenAnteil.ToString("N2");
      else
        eigenAnteilTB.Text = (ePreis - eigenAnteil).ToString("N2");
      zuzahlungTB.Text = zuzahlung.ToString("N2");
      UpdatePreis(false);
    }

    private void mwstCB_SelectedValueChanged(object sender, EventArgs e)
    {
      if(!initialization)
        UpdatePreis(true);
    }

    private void eigenAntailTB_Validating(object sender, CancelEventArgs e)
    {
      try
      {
        eigenAnteilTB.Text = Math.Round(decimal.Parse(eigenAnteilTB.Text), 2, MidpointRounding.AwayFromZero).ToString("N2");
      }
      catch
      {
        eigenAnteilTB.Text = "0,00";
      }
      UpdatePreis(true);
    }

    private void selectAllTB_Enter(object sender, EventArgs e)
    {
      TextBoxBase tb = (TextBoxBase)sender;
      this.BeginInvoke((MethodInvoker)delegate()
      {
        tb.SelectAll();
      });
    }

    private void ePreisTB_Validating(object sender, CancelEventArgs e)
    {
      decimal ePreis = 0;
      try 
      { 
        ePreisTB.Text = Math.Round(decimal.Parse(ePreisTB.Text), 2, MidpointRounding.AwayFromZero).ToString("N2");
        ePreis = decimal.Parse(ePreisTB.Text);
      }
      catch { ePreisTB.Text = "0,00"; }
      if (!eigenanteilCB.Checked)
      {
        eigenAnteilTB.Text = ePreisTB.Text;
        //decimal eigenAnteil = 0;
        //try { eigenAnteil = decimal.Parse(eigenAnteilTB.Text); }
        //catch { }
        //if (eigenAnteil == 0)
        //  eigenAnteilTB.Text = ePreisTB.Text;
        //if (ePreis < eigenAnteil)
        //  eigenAnteilTB.Text = ePreisTB.Text;
      }
      else
      {
        eigenAnteilTB.Text = "0,00";
        //decimal eigenAnteil = 0;
        //try { eigenAnteil = decimal.Parse(eigenAnteilTB.Text); }
        //catch { }
        //if (ePreis < eigenAnteil)
        //  eigenAnteilTB.Text = ePreisTB.Text;
      }
      UpdatePreis(true);
    }

    private void FillJahrList()
    {
      jahrCB.Items.Clear();
      int cy = 0;
      if (jahr2MenuItem.Checked)
        cy = int.Parse(jahr2MenuItem.Text);
      else
        cy = int.Parse(jahr1MenuItem.Text);
      jahrCB.Items.Add(cy.ToString());
      jahrCB.Items.Add((cy - 1).ToString());
      jahrCB.Items.Add((cy - 2).ToString());
      jahrCB.SelectedIndex = 0;
    }

    private void neuePositionBtn_Click(object sender, EventArgs e)
    {
      if (neuePositionBtn.Text == "&Neu")
      {
        neuePositionBtn.Text = "&Abbrechen";
        AddNeuePosition();
      }
      else
      {
        neuePositionBtn.Text = "&Neu";
        if (positionenBindingSource.Count != 0 && positionenBindingSource.Current != null)
          positionenBindingSource.RemoveCurrent();
      }
    }

    private void AddNeuePosition()
    {
      if (positionenBindingSource.AllowNew && positionenBindingSource.DataSource != null &&
        ValidatePosition())
      {
        positionenBindingSource.AddNew();
        if (positionenBindingSource.Count == 1)
          tagMonatMTB.Focus();
        else
          anzahlNUD.Focus();
      }
    }

    bool changingCurrentPosition = false;
    private void positionenBindingSource_CurrentChanged(object sender, EventArgs e)
    {
      if (!changingCurrentPosition)
      {
        changingCurrentPosition = true;
        try
        {
          ShowCurrentPosition();
        }
        finally
        {
          CalcGesForderung();
          changingCurrentPosition = false;
        }
      }
    }

    private void ShowCurrentPosition()
    {
      try
      {
        if (positionenBindingSource.Count != 0 && positionenBindingSource.Current != null)
        {
          EnabelPositionenPanel();
          DataRow row = ((DataRowView)positionenBindingSource.Current).Row;
          DataRow rezept = ((DataRowView)rezepteBindingSource.Current).Row;
          if (row["Verbrauch"] == DBNull.Value || (short)row["Verbrauch"] == 0)
            verbrauchLabel.Visible = false;
          else
            verbrauchLabel.Visible = true;
          DateTime d = (DateTime)rezept["RezDat"];
          if (row["BehDat"] != DBNull.Value)
            d = (DateTime)row["BehDat"];
          else
          {
            DataRow[] pos = voinDataSet.Positionen.Select("", "", DataViewRowState.CurrentRows);
            if (pos.Length != 0)
              d = (DateTime)pos[0]["BehDat"];
          }
          tagMonatMTB.Text = d.ToString("dd.MM.yyyy").Substring(0, 6);
          int idx = jahrCB.Items.IndexOf(d.ToString("dd.MM.yyyy").Substring(6, 4));
          if (idx != -1)
            jahrCB.SelectedIndex = idx;
          else
            jahrCB.SelectedIndex = jahrCB.Items.Count - 1;
          anzahlNUD.Value = (row["Anzahl"] != DBNull.Value ? (decimal)row["Anzahl"] : 1m);
          anzahlEigenanteilLabel.Text = anzahlNUD.Value.ToString("N0") + " X";
          anzahlZuzahlungLabel.Text = anzahlNUD.Value.ToString("N0") + " X";
          string buPos = (row["BuPos"] != DBNull.Value ? (string)row["BuPos"] : string.Empty);
          gruppeMTB.Text = Substring(buPos, 0, 2);
          ortMTB.Text = Substring(buPos, 2, 2);
          unterGruppeMTB.Text = Substring(buPos, 4, 2);
          artMTB.Text = Substring(buPos, 6, 1);
          produktMTB.Text = Substring(buPos, 7, 3);
          if(row["Mwst_KZ"] == DBNull.Value)
          {
            if (mwstCB.SelectedIndex == -1)
              mwstCB.SelectedValue = (short)1;
          }
          else
          {
            mwstCB.SelectedValue = (short)row["Mwst_KZ"];
          }
          mwstTB.Text = (row["Mwst"] == DBNull.Value ? "0,00" :
            Math.Round((decimal)row["Mwst"], 2, MidpointRounding.AwayFromZero).ToString("N2"));
          decimal ePreis = (row["EPreis"] != DBNull.Value ? (decimal)row["EPreis"] : 0m);
          if (!nettoCB.Checked) ePreis += decimal.Parse(mwstTB.Text);
          ePreisTB.Text = ePreis.ToString("N2");
          decimal eigenAnteil = row["Eigenanteil"] == DBNull.Value ? 0 : (decimal)row["Eigenanteil"];
          if (nettoCB.Checked) eigenAnteil /= (1m + ((MehrWertSteuer)mwstCB.SelectedItem).MWSt /
             100m);
          if (eigenanteilCB.Checked)
            eigenAnteilTB.Text = eigenAnteil.ToString("N2");
          else
            eigenAnteilTB.Text = (ePreis - eigenAnteil).ToString("N2");
          decimal zuzahlung = row["Zuzahlung"] == DBNull.Value ? 0 : (decimal)row["Zuzahlung"];
          if (nettoCB.Checked) zuzahlung /= (1m + ((MehrWertSteuer)mwstCB.SelectedItem).MWSt /
             100m);
          zuzahlungTB.Text = zuzahlung.ToString("N2");
          hilfsMittelKennZeichenCB.SelectedValue = (row["Hilfsmittel_KZ"] == DBNull.Value ?
            (short)0 : (short)row["Hilfsmittel_KZ"]);
          inventarNummerTB.Text = (row["Hilfsmittel_InvNr"] == DBNull.Value ?
            string.Empty : (string)row["Hilfsmittel_InvNr"]);
          zeitVonMTB.Text = (row["Uhrzeit_von"] == DBNull.Value ? string.Empty :
            Substring((string)row["Uhrzeit_von"], 0, 2) + ":" +
            Substring((string)row["Uhrzeit_von"], 2, 2));
          zeitBisMTB.Text = (row["Uhrzeit_bis"] == DBNull.Value ? string.Empty :
            Substring((string)row["Uhrzeit_bis"], 0, 2) + ":" +
            Substring((string)row["Uhrzeit_bis"], 2, 2));
          versorgungVonMTB.Text = (row["Versorgung_von"] == DBNull.Value ? string.Empty :
            ((DateTime)row["Versorgung_von"]).ToString("dd.MM.yyyy"));
          versorgungBisMTB.Text = (row["Versorgung_bis"] == DBNull.Value ? string.Empty :
            ((DateTime)row["Versorgung_bis"]).ToString("dd.MM.yyyy"));
          kilometerTB.Text = (row["Kilometer"] == DBNull.Value ? string.Empty :
            Math.Round((decimal)row["Kilometer"], 2, MidpointRounding.AwayFromZero).ToString());
          dauerTB.Text = (row["Dauer_Min"] == DBNull.Value ? string.Empty :
            ((int)row["Dauer_Min"]).ToString());
          zeitraumTB.Text = (row["VZeitraum"] == DBNull.Value ? string.Empty :
            ((short)row["VZeitraum"]).ToString());
          begruendungTB.Text = (row["Text"] == DBNull.Value ? string.Empty :
            (string)row["Text"]);
          if (focusPosition)
            this.BeginInvoke((MethodInvoker)delegate()
            {
              if (positionenBindingSource.Count == 1)
                tagMonatMTB.Focus();
              else
                anzahlNUD.Focus();
            });
        }
        else
          DisabelPositionenPanel();
      }
      finally
      {
        UpdatePositionsnummer(gruppeMTB);
        UpdatePositionsnummer(ortMTB);
        UpdatePreis(false);
      }
    }

    private void EnabelPositionenPanel()
    {
      foreach (Control c in positionenPanel.Controls)
        if (c != positionenDGV && c != neuePositionBtn)
          c.Enabled = true;
    }

    private void DisabelPositionenPanel()
    {
      if (panel4.Enabled)
        nameTB.Focus();
      else
        ikTB.Focus();
      forderungLabel.Text = string.Empty;
      verbrauchLabel.Visible = false;
      gruppeLabel.Text = string.Empty;
      ortLabel.Text = string.Empty;
      unterGruppeLabel.Text = string.Empty;
      artLabel.Text = string.Empty;
      produktLabel.Text = string.Empty;
      anzahlEigenanteilLabel.Text = string.Empty;
      anzahlZuzahlungLabel.Text = string.Empty;
      zuzahlungsArtLabel.Text = string.Empty;
      gesamteForderungLabel.Text = "Forderung:";
      gesamteZuzahlungLabel.Text = "Zuzahlung:";
      gesamteSummeLabel.Text = "Ges.:";
      foreach (Control c in positionenPanel.Controls)
        if (c != positionenDGV && c != neuePositionBtn)
        {
          c.Enabled = false;
          if (c is TextBoxBase) c.Text = string.Empty;
        }
    }

    private void CalcGesForderung()
    {
      decimal forderung = 0;
      decimal zuzahlung = 0;
      if (positionenBindingSource.DataSource != null)
        foreach (DataRow row in voinDataSet.Positionen.Rows)
          if (row.RowState != DataRowState.Deleted && row.RowState != DataRowState.Detached)
          {
            forderung += CalcForderung(row);
            zuzahlung += (decimal)row["Zuzahlung"] * (decimal)row["Anzahl"];
          }
      forderung = Math.Round(forderung, 2, MidpointRounding.AwayFromZero);
      zuzahlung = Math.Round(zuzahlung, 2, MidpointRounding.AwayFromZero);
      gesamteForderungLabel.Text = "Forderung: " + forderung.ToString("N2");
      gesamteZuzahlungLabel.Text = "Zuzahlung: " + zuzahlung.ToString("N2");
      gesamteSummeLabel.Text = "Ges.: " + (forderung + zuzahlung).ToString("N2");
    }

    private decimal CalcForderung(DataRow row)
    {
      return ((row["EPreis"] != DBNull.Value ? (decimal)row["EPreis"] : 0m) +
        (row["Mwst"] != DBNull.Value ? (decimal)row["Mwst"] : 0m) -
        (row["Eigenanteil"] != DBNull.Value ? (decimal)row["Eigenanteil"] : 0m) -
        (row["Zuzahlung"] != DBNull.Value ? (decimal)row["Zuzahlung"] : 0m)) *
        (row["Anzahl"] != DBNull.Value ? (decimal)row["Anzahl"] : 0m);
    }

    bool focusPosition = true;

    private void LoadPositionen()
    {
      voinDataSet.Positionen.Clear();
      if (rezepteBindingSource.Count > 0 && rezepteBindingSource.Current != null &&
        rezepteBindingSource.DataMember != string.Empty)
      {
        DataRowView drv = (DataRowView)rezepteBindingSource.Current;
        if (drv.Row.RowState != DataRowState.Deleted && drv.Row.RowState != DataRowState.Detached)
        {
          positionenDA.SelectCommand.Parameters["@ID"].Value = drv.Row["ID"];
          positionenDA.Fill(voinDataSet.Positionen);
        }
        ((ISupportInitialize)positionenBindingSource).BeginInit();
        positionenBindingSource.DataSource = voinDataSet;
        positionenBindingSource.DataMember = "Positionen";
        ((ISupportInitialize)positionenBindingSource).EndInit();
        positionenDGV.DataSource = positionenBindingSource;
        BuildPositionenDGVColumns();
        //SetPositionenBindings();
        if (ePreisTB.DataBindings.Count > 0 && !ePreisTB.DataBindings[0].IsBinding)
          positionenBindingSource.ResumeBinding();
      }

    }

    private void BuildPositionenDGVColumns()
    {
      //foreach (DataGridViewColumn c in positionenDGV.Columns)
      //{
      //  c.Visible = false;
      //  if (c.DataPropertyName.Trim().ToUpper() == "BEHDAT")
      //  {
      //    c.Visible = true;
      //    c.DisplayIndex = 0;
      //  }
      //  if (c.DataPropertyName.Trim().ToUpper() == "BUPOS")
      //  {
      //    c.Visible = true;
      //    c.DisplayIndex = 1;
      //  }
      //  if (c.DataPropertyName.Trim().ToUpper() == "ANZAHL")
      //  {
      //    c.Visible = true;
      //    c.DisplayIndex = 2;
      //  }
      //}
      //positionenDGV.Columns.Add(this.forderungColumn);
      //positionenDGV.Columns.Add(this.ePreisColumn);

      positionenDGV.Columns.Clear();
      positionenDGV.Columns.Add(this.forderungColumn);
      positionenDGV.Columns.Add(this.ePreisColumn);
      positionenDGV.Columns.Add(this.anzahlColumn);
      positionenDGV.Columns.Add(this.buPosColumn);
      positionenDGV.Columns.Add(this.datumColumn);
      positionenDGV.Columns["datumColumn"].DisplayIndex = 0;
      positionenDGV.Columns["buPosColumn"].DisplayIndex = 1;
      positionenDGV.Columns["anzahlColumn"].DisplayIndex = 2;
      positionenDGV.Columns["ePreisColumn"].DisplayIndex = 3;
      positionenDGV.Columns["forderungColumn"].DisplayIndex = 4;

    }

    //private void SetPositionenBindings()
    //{
    //  Binding bng;
    //  bng = new Binding("Text", positionenBindingSource, "EPreis");
    //  bng.FormatString = "N2";
    //  bng.FormattingEnabled = true;
    //  ePreisTB.DataBindings.Add(bng);
    //  //anzahlNUD.DataBindings.Add("Value", positionenBindingSource, "Anzahl");
    //  bng = new Binding("Text", positionenBindingSource, "Eigenanteil");
    //  bng.FormatString = "N2";
    //  bng.FormattingEnabled = true;
    //  eigenAnteilTB.DataBindings.Add(bng);
    //  bng = new Binding("Text", positionenBindingSource, "Zuzahlung");
    //  bng.FormatString = "N2";
    //  bng.FormattingEnabled = true;
    //  zuzahlungTB.DataBindings.Add(bng);
    //  mwstCB.DataSource = MehrWertSteuer.MWStList;
    //  mwstCB.ValueMember = "MWStCode";
    //  mwstCB.DisplayMember = "MWStName";
    //  mwstCB.DataBindings.Add("SelectedValue", positionenBindingSource, "Mwst_KZ");
    //}

    //private void ClearPositionenBindings()
    //{
    //  if (positionenBindingSource.Count != 0 && positionenBindingSource.Current != null)
    //    positionenBindingSource.EndEdit();
    //  ePreisTB.DataBindings.Clear();
    //  //anzahlNUD.DataBindings.Clear();
    //  eigenAnteilTB.DataBindings.Clear();
    //  zuzahlungTB.DataBindings.Clear();
    //  mwstCB.DataBindings.Clear();
    //}

    private void positionenDGV_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
    {
      //if (positionenDGV.Columns[e.ColumnIndex].HeaderText == "E-Preis" &&
      //  positionenDGV.Rows.Count > e.RowIndex)
      //{
      //  try
      //  {
      //    DataRow row = ((DataRowView)positionenDGV.Rows[e.RowIndex].DataBoundItem).Row;
      //    decimal ePreis = (row["EPreis"] != DBNull.Value ? (decimal)row["EPreis"] : 0m) +
      //      (row["Mwst"] != DBNull.Value ? (decimal)row["Mwst"] : 0m);
      //    e.Value = Math.Round(ePreis, 2);
      //  }
      //  catch { }
      //}
      if (positionenDGV.Columns[e.ColumnIndex].HeaderText == "Forderung" &&
        positionenDGV.Rows.Count > e.RowIndex)
      {
        try
        {
          DataRow row = ((DataRowView)positionenDGV.Rows[e.RowIndex].DataBoundItem).Row;
          decimal forderung = CalcForderung(row);
          e.Value = Math.Round(forderung, 2, MidpointRounding.AwayFromZero);
        }
        catch { }
      }
      if (positionenDGV.Columns[e.ColumnIndex].HeaderText == "BuPos")
      {
        if (e.Value != DBNull.Value)
        {
          string val = (string)e.Value;
          string res = string.Empty;
          if (Substring(val, 0, 2) != string.Empty)
            res = Substring(val, 0, 2);
          if (Substring(val, 2, 2) != string.Empty)
            res += "." + Substring(val, 2, 2);
          if (Substring(val, 4, 2) != string.Empty)
            res += "." + Substring(val, 4, 2);
          if (Substring(val, 6, 1) != string.Empty)
            res += "." + Substring(val, 6, 1);
          if (Substring(val, 7, 3) != string.Empty)
            res += "." + Substring(val, 7, 3);
          e.Value = res;
        }
        else
          e.Value = string.Empty;
      }
    }

    bool showPositionError = true;

    private bool ValidatePosition()
    {
      bool res = true;
      errorProvider.Clear();
      if (positionenBindingSource.Count != 0 && positionenBindingSource.Current != null)
      {
        DataRow row = ((DataRowView)positionenBindingSource.Current).Row;
        DataRow rezept = ((DataRowView)rezepteBindingSource.Current).Row;
        if (row.RowState == DataRowState.Detached && IsDefaultPosition())
        {
          positionenBindingSource.CancelEdit();
          neuePositionBtn.Text = "&Neu";
          return true;
        }
        if (row["ID"] == DBNull.Value)
          row["ID"] = rezept["ID"];
        if (mandantenBindingSource.Count != 0 && mandantenBindingSource.Current != null)
        {
          //string tkz = (string)((DataRowView)mandantenBindingSource.Current)["Tarifkennzeichen"];
          if (row["Tarifkennzeichen"] == DBNull.Value ||
            (string)row["Tarifkennzeichen"] != tarifSchlusselAbrechnCodeTB.Text)
            row["Tarifkennzeichen"] = tarifSchlusselAbrechnCodeTB.Text;
        }
        if (row["Faktor"] == DBNull.Value) row["Faktor"] = 1m;
        if (row["GebArt"] == DBNull.Value) row["GebArt"] = 1;
        if (row["Gebuehr"] == DBNull.Value) row["Gebuehr"] = 10;
        if (row["Verbrauch"] == DBNull.Value) row["Verbrauch"] = 0;
        if (eigenAnteilTB.Focused || ePreisTB.Focused) UpdatePreis(true);
        if (zuzahlungTB.Focused || anzahlNUD.Focused) UpdatePreis(false);
        try
        {
          MehrWertSteuer steuer = (MehrWertSteuer)mwstCB.SelectedItem;
          //BehDat
          try
          {
            string sd = tagMonatMTB.Text + jahrCB.SelectedItem;
            DateTime d = DateTime.Parse(sd);
            if (row["BehDat"] == DBNull.Value || (DateTime)row["BehDat"] != d)
              row["BehDat"] = d;
            UpdatePositionenDaten(d);
          }
          catch (Exception ex)
          {
            throw new Exception("BehDat", ex);
          }
          //Anzahl
          if (row["Anzahl"] == DBNull.Value || (decimal)row["Anzahl"] != anzahlNUD.Value)
            row["Anzahl"] = anzahlNUD.Value;
          //BuPos
          string buPos = gruppeMTB.Text + ortMTB.Text + unterGruppeMTB.Text + artMTB.Text +
            produktMTB.Text;
          if (row["BuPos"] == DBNull.Value || (string)row["BuPos"] != buPos)
            row["BuPos"] = buPos;
          //Mwst
          decimal mwst = 0;
          if (mwstTB.Text != string.Empty)
            mwst = decimal.Parse(mwstTB.Text);
          if (row["Mwst"] == DBNull.Value || (decimal)row["Mwst"] != mwst)
            row["Mwst"] = mwst;
          //EPreis
          decimal ePreis = 0;
          decimal ePreisBrutto = 0;
          try
          {
            ePreisBrutto = ePreis = decimal.Parse(ePreisTB.Text);
            //            if (!bruttoCB.Checked) ePreis -= mwst;
            if (!nettoCB.Checked) ePreis -= mwst;
            else ePreisBrutto += mwst;
            if (ePreis <= 0m) throw new Exception("Der eingegebene Preis ist falsch.");
            if (row["EPreis"] == DBNull.Value || (decimal)row["EPreis"] != ePreis)
              row["EPreis"] = ePreis;
          }
          catch (Exception ex)
          {
            throw new Exception("EPreis", ex);
          }
          //Mwst_KZ
          if (row["Mwst_KZ"] == DBNull.Value || (short)row["Mwst_KZ"] !=
            (short)mwstCB.SelectedValue) row["Mwst_KZ"] = (short)mwstCB.SelectedValue;
          //Eigenanteil
          try
          {
            decimal eigenAnteil = 0;
            if (eigenAnteilTB.Text != string.Empty)
            {
              eigenAnteil = decimal.Parse(eigenAnteilTB.Text);
            }
            if (nettoCB.Checked)
              eigenAnteil = Math.Round(eigenAnteil * (1 + steuer.MWSt / 100m), 2, MidpointRounding.AwayFromZero);
            if (!eigenanteilCB.Checked)
              eigenAnteil = ePreisBrutto - eigenAnteil;
            if (row["Eigenanteil"] == DBNull.Value || (decimal)row["Eigenanteil"] != eigenAnteil)
              row["Eigenanteil"] = eigenAnteil;
          }
          catch (Exception ex)
          {
            throw new Exception("Eigenanteil", ex);
          }
          //Zuzahlung
          try
          {
            decimal zuzahlung = 0;
            if (zuzahlungTB.Text != string.Empty)
              zuzahlung = decimal.Parse(zuzahlungTB.Text);
            if (nettoCB.Checked)
              zuzahlung = Math.Round(zuzahlung * (1 + steuer.MWSt / 100m), 2, MidpointRounding.AwayFromZero);
            if (row["Zuzahlung"] == DBNull.Value || (decimal)row["Zuzahlung"] != zuzahlung)
              row["Zuzahlung"] = zuzahlung;
          }
          catch (Exception ex)
          {
            throw new Exception("Zuzahlung", ex);
          }
          //Zuzahlungsart
          if (zuzahlungsArtLabel.Tag != null)
          {
            short zuzahlungsArt = (short)(int)zuzahlungsArtLabel.Tag;
            if (row["Zuzahlungsart"] == DBNull.Value || (short)row["Zuzahlungsart"] != zuzahlungsArt)
              row["Zuzahlungsart"] = zuzahlungsArt;

          }
          //Hilfsmittel_KZ
          short hilfsmittel_KZ = (short)hilfsMittelKennZeichenCB.SelectedValue;
          if (row["Hilfsmittel_KZ"] == DBNull.Value || (short)row["Hilfsmittel_KZ"] != hilfsmittel_KZ)
            row["Hilfsmittel_KZ"] = hilfsmittel_KZ;
          //Hilfsmittel_InvNr
          if ((row["Hilfsmittel_InvNr"] == DBNull.Value && inventarNummerTB.Text != string.Empty) ||
            (row["Hilfsmittel_InvNr"] != DBNull.Value && (string)row["Hilfsmittel_InvNr"] !=
            inventarNummerTB.Text))
            row["Hilfsmittel_InvNr"] = inventarNummerTB.Text;
          //Uhrzeit_von
          if (zeitVonMTB.Text == "  :" && row["Uhrzeit_von"] != DBNull.Value)
            row["Uhrzeit_von"] = DBNull.Value;
          if (zeitVonMTB.Text != "  :")
          {
            string uhrzeit_von = Substring(zeitVonMTB.Text, 0, 2) +
              Substring(zeitVonMTB.Text, 3, 2);
            if (row["Uhrzeit_von"] == DBNull.Value || (string)row["Uhrzeit_von"] != uhrzeit_von)
              row["Uhrzeit_von"] = uhrzeit_von;
          }
          //Uhrzeit_bis
          if (zeitVonMTB.Text == "  :" && row["Uhrzeit_bis"] != DBNull.Value)
            row["Uhrzeit_bis"] = DBNull.Value;
          if (zeitVonMTB.Text != "  :")
          {
            string uhrzeit_bis = Substring(zeitBisMTB.Text, 0, 2) +
              Substring(zeitBisMTB.Text, 3, 2);
            if (row["Uhrzeit_bis"] == DBNull.Value || (string)row["Uhrzeit_bis"] != uhrzeit_bis)
              row["Uhrzeit_bis"] = uhrzeit_bis;
          }
          //Versorgung_von
          try
          {
            if (versorgungVonMTB.Text == "  .  ." && row["Versorgung_von"] != DBNull.Value)
              row["Versorgung_von"] = DBNull.Value;
            if (versorgungVonMTB.Text != "  .  .")
            {
              DateTime versorgung_von = DateTime.Parse(versorgungVonMTB.Text);
              if (row["Versorgung_von"] == DBNull.Value || (DateTime)row["Versorgung_von"] !=
                versorgung_von) row["Versorgung_von"] = versorgung_von;
            }
          }
          catch (Exception ex)
          {
            throw new Exception("Versorgung_von", ex);
          }
          //Verorgung_bis
          try
          {
            if (versorgungBisMTB.Text == "  .  ." && row["Versorgung_bis"] != DBNull.Value)
              row["Versorgung_bis"] = DBNull.Value;
            if (versorgungBisMTB.Text != "  .  .")
            {
              DateTime versorgung_bis = DateTime.Parse(versorgungBisMTB.Text);
              if (row["Versorgung_bis"] == DBNull.Value || (DateTime)row["Versorgung_bis"] !=
                versorgung_bis) row["Versorgung_bis"] = versorgung_bis;
            }
          }
          catch (Exception ex)
          {
            throw new Exception("Versorgung_bis", ex);
          }
          //Kilometer
          try
          {
            decimal kilometer = 0;
            if (kilometerTB.Text != string.Empty)
              kilometer = Math.Round(decimal.Parse(kilometerTB.Text), 2, MidpointRounding.AwayFromZero);
            if (kilometerTB.Text == string.Empty && row["Kilometer"] != DBNull.Value)
              row["Kilometer"] = DBNull.Value;
            if (kilometerTB.Text != string.Empty && (row["Kilometer"] == DBNull.Value ||
              (decimal)row["Kilometer"] != kilometer)) row["Kilometer"] = kilometer;
          }
          catch (Exception ex)
          {
            throw new Exception("Kilometer", ex);
          }
          //Dauer_Min
          try
          {
            int dauer_Min = 0;
            if (dauerTB.Text != string.Empty) dauer_Min = int.Parse(dauerTB.Text);
            if (dauerTB.Text == string.Empty && row["Dauer_Min"] != DBNull.Value)
              row["Dauer_Min"] = DBNull.Value;
            if (dauerTB.Text != string.Empty && (row["Dauer_Min"] == DBNull.Value ||
              (int)row["Dauer_Min"] != dauer_Min)) row["Dauer_Min"] = dauer_Min;
          }
          catch (Exception ex)
          {
            throw new Exception("Dauer_Min", ex);
          }
          //VZeitraum
          try
          {
            short vZeitraum = 0;
            if (zeitraumTB.Text != string.Empty) vZeitraum = short.Parse(zeitraumTB.Text);
            if (zeitraumTB.Text == string.Empty && row["VZeitraum"] != DBNull.Value)
              row["VZeitraum"] = DBNull.Value;
            if (zeitraumTB.Text != string.Empty && (row["VZeitraum"] == DBNull.Value ||
              (short)row["VZeitraum"] != vZeitraum)) row["VZeitraum"] = vZeitraum;
          }
          catch (Exception ex)
          {
            throw new Exception("VZeitraum", ex);
          }
          //Text
          if (begruendungTB.Text == string.Empty && row["Text"] != DBNull.Value)
            row["Text"] = DBNull.Value;
          if (begruendungTB.Text != string.Empty && (row["Text"] == DBNull.Value ||
            (string)row["Text"] != begruendungTB.Text)) row["Text"] = begruendungTB.Text;
          positionenBindingSource.EndEdit();
          positionenDGV.Invalidate();
        }
        catch (Exception ex)
        {
          string message = string.Empty;
          switch (ex.Message)
          {
            case "BehDat":
              message = ex.InnerException.Message;
              break;
            case "EPreis":
              message = ex.InnerException.Message;
              break;
            case "Eigenanteil":
              message = ex.InnerException.Message;
              break;
            case "Zuzahlung":
              message = ex.InnerException.Message;
              break;
            case "Versorgung_von":
              message = ex.InnerException.Message;
              break;
            case "Versorgung_bis":
              message = ex.InnerException.Message;
              break;
            case "Kilometer":
              message = ex.InnerException.Message;
              break;
            case "Dauer_Min":
              message = ex.InnerException.Message;
              break;
            case "VZeitraum":
              message = ex.InnerException.Message;
              break;
            default:
              message = ex.Message;
              break;
          }
          if (showPositionError && MessageBox.Show(message + "\nSoll der aktuelle Bearbeitungsvorgang abgebrochen werden?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Error, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
          {
            neuePositionBtn.Text = "&Neu";
            positionenBindingSource.CancelEdit();
            ShowCurrentPosition();
          }
          else
          {
            res = false;
            switch (ex.Message)
            {
              case "BehDat":
                tagMonatMTB.Focus();
                errorProvider.SetError(tagMonatMTB, ex.InnerException.Message);
                break;
              case "EPreis":
                ePreisTB.Focus();
                errorProvider.SetError(ePreisTB, ex.InnerException.Message);
                break;
              case "Eigenanteil":
                eigenAnteilTB.Focus();
                errorProvider.SetError(eigenAnteilTB, ex.InnerException.Message);
                break;
              case "Zuzahlung":
                zuzahlungTB.Focus();
                errorProvider.SetError(zuzahlungTB, ex.InnerException.Message);
                break;
              case "Versorgung_von":
                versorgungVonMTB.Focus();
                errorProvider.SetError(versorgungVonMTB, ex.InnerException.Message);
                break;
              case "Versorgung_bis":
                versorgungBisMTB.Focus();
                errorProvider.SetError(versorgungBisMTB, ex.InnerException.Message);
                break;
              case "Kilometer":
                kilometerTB.Focus();
                errorProvider.SetError(kilometerTB, ex.InnerException.Message);
                break;
              case "Dauer_Min":
                dauerTB.Focus();
                errorProvider.SetError(dauerTB, ex.InnerException.Message);
                break;
              case "VZeitraum":
                zeitraumTB.Focus();
                errorProvider.SetError(zeitraumTB, ex.InnerException.Message);
                break;
              default:
                if (positionenBindingSource.Count == 1)
                  tagMonatMTB.Focus();
                else
                  anzahlNUD.Focus();
                break;
            }
          }

        }
      }
      return res;
    }

    private bool IsDefaultPosition()
    {
      if (anzahlNUD.Value != 1) return false;
      if (gruppeMTB.Text != string.Empty) return false;
      if (ortMTB.Text != string.Empty) return false;
      if (unterGruppeMTB.Text != string.Empty) return false;
      if (artMTB.Text != string.Empty) return false;
      if (produktMTB.Text != string.Empty) return false;
      if (ePreisTB.Text != string.Empty && ePreisTB.Text != (0m).ToString("N2"))
        return false;
      //if ((short)mwstCB.SelectedValue != 1) return false;
      if (eigenAnteilTB.Text != string.Empty && eigenAnteilTB.Text != (0m).ToString("N2"))
        return false;
      if (zuzahlungTB.Text != string.Empty && zuzahlungTB.Text != (0m).ToString("N2"))
        return false;
      if (verbrauchLabel.Visible) return false;
      if ((short)hilfsMittelKennZeichenCB.SelectedValue != 0) return false;
      if (begruendungTB.Text != string.Empty) return false;
      if (inventarNummerTB.Text != string.Empty) return false;
      if (zeitVonMTB.Text != "  :") return false;
      if (zeitBisMTB.Text != "  :") return false;
      if (versorgungVonMTB.Text != "  .  .") return false;
      if (versorgungBisMTB.Text != "  .  .") return false;
      if (kilometerTB.Text != string.Empty) return false;
      if (dauerTB.Text != string.Empty) return false;
      if (zeitraumTB.Text != string.Empty) return false;
      return true;
    }

    private void UpdatePositionenDaten(DateTime date)
    {
      DataRow[] rows = voinDataSet.Positionen.Select("", "",
        DataViewRowState.CurrentRows);
      foreach (DataRow row in rows)
        if (row["BehDat"] == DBNull.Value || (DateTime)row["BehDat"] != date)
          row["BehDat"] = date;
    }

    private void positionenDGV_Enter(object sender, EventArgs e)
    {
      if (ValidatePosition())
        neuePositionBtn.Text = "&Neu";
    }

    private void positionenPanel_Validating(object sender, CancelEventArgs e)
    {
      if (ValidatePosition())
        neuePositionBtn.Text = "&Neu";
    }

    private void positionDeleteBtn_Click(object sender, EventArgs e)
    {
      neuePositionBtn.Text = "&Neu";
      if (positionenBindingSource.Count != 0 && positionenBindingSource.Current != null)
        positionenBindingSource.RemoveCurrent();
    }

    private void tagMonatMTB_Validating(object sender, CancelEventArgs e)
    {
      string ds = tagMonatMTB.Text + jahrCB.SelectedItem;
      DateTime rezDat = DateTime.Today;
      bool f = true;
      try { rezDat = DateTime.Parse(rezeptDatumMTB.Text); }
      catch { f = false; }
      if (f)
        try
        {
          DateTime behDat = DateTime.Parse(ds);
          if (behDat < rezDat)
            MessageBox.Show(ds + " ist kleiner als das Rezeptdatum");
        }
        catch (Exception ex)
        {
          this.BeginInvoke((MethodInvoker)delegate()
          {
            MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
          });
          e.Cancel = true;
        }
    }

    private void zuzahlungTB_Validating(object sender, CancelEventArgs e)
    {
      try { zuzahlungTB.Text = Math.Round(decimal.Parse(zuzahlungTB.Text), 2, MidpointRounding.AwayFromZero).ToString("N2"); }
      catch { zuzahlungTB.Text = "0,00"; }
      UpdatePreis(false);
    }

    private void versorgungDatum_Validating(object sender, CancelEventArgs e)
    {
      MaskedTextBox mtb = (MaskedTextBox)sender;
      try
      {
        if (mtb.Text != "  .  .")
          mtb.Text = DateTime.Parse(mtb.Text).ToString("dd.MM.yyyy");
      }
      catch (Exception ex)
      {
        e.Cancel = true;
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        this.BeginInvoke((MethodInvoker)delegate()
        {
          mtb.SelectAll();
        });
      }
    }

    private void uhrzeit_Validating(object sender, CancelEventArgs e)
    {
      MaskedTextBox mtb = (MaskedTextBox)sender;
      try
      {
        if (mtb.Text != "  :")
        {
          int h = int.Parse(Substring(mtb.Text, 0, 2));
          if (h < 0 || h > 23) throw new Exception();
          int m = int.Parse(Substring(mtb.Text, 3, 2));
          if (m < 0 || m > 59) throw new Exception();
          mtb.Text = (h < 10 ? "0" + h.ToString() : h.ToString()) + ":" + (m < 10 ?
            "0" + m.ToString() : m.ToString());
        }
      }
      catch
      {
        e.Cancel = true;
        MessageBox.Show("Die Zeichenfolge wurde nicht als gültige Uhrzeit erkannt.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        this.BeginInvoke((MethodInvoker)delegate()
        {
          mtb.SelectAll();
        });
      }
    }

    private void kilometerTB_Validating(object sender, CancelEventArgs e)
    {
      try
      {
        if (kilometerTB.Text != string.Empty)
          kilometerTB.Text = Math.Round(decimal.Parse(kilometerTB.Text), 2, MidpointRounding.AwayFromZero).ToString();
      }
      catch (Exception ex)
      {
        e.Cancel = true;
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        this.BeginInvoke((MethodInvoker)delegate()
        {
          kilometerTB.SelectAll();
        });
      }
    }

    private void dauerTB_Validating(object sender, CancelEventArgs e)
    {
      try
      {
        if (dauerTB.Text != string.Empty)
          dauerTB.Text = int.Parse(dauerTB.Text).ToString();
      }
      catch (Exception ex)
      {
        e.Cancel = true;
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        this.BeginInvoke((MethodInvoker)delegate()
        {
          dauerTB.SelectAll();
        });
      }
    }

    private void zeitraumTB_Validating(object sender, CancelEventArgs e)
    {
      try
      {
        if (zeitraumTB.Text != string.Empty)
          zeitraumTB.Text = short.Parse(zeitraumTB.Text).ToString();
      }
      catch (Exception ex)
      {
        e.Cancel = true;
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        this.BeginInvoke((MethodInvoker)delegate()
        {
          zeitraumTB.SelectAll();
        });
      }
    }

    private void posBezeichnungBtn_Click(object sender, EventArgs e)
    {
      string msg = "Gruppe: " + gruppeLabel.Text + "\nOrt: " + ortLabel.Text +
        "\nUntergruppe: " + unterGruppeLabel.Text + "\nArt: " + artLabel.Text +
        "\nProdukt: " + produktLabel.Text + "\nPos.Bez: ";
      string hmp = gruppeMTB.Text + ortMTB.Text + unterGruppeMTB.Text + artMTB.Text +
        produktMTB.Text;
      hmp = hmp.Trim();
      DataRow[] rows = voinDataSet.tab_HMPBezeichnung.Select("HMP LIKE '" + hmp + "'");
      if (rows.Length != 0)
        msg += (string)rows[0]["Bezeichnung"];
      MessageBox.Show(msg, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
    }

    private void anzahlNUD_Enter(object sender, EventArgs e)
    {
      this.BeginInvoke((MethodInvoker)delegate()
      {
        anzahlNUD.Select(0, anzahlNUD.Text.Length);
      });
    }

    private void bindingNavigatorPositionItem_Enter(object sender, EventArgs e)
    {
      ValidatePosition();
      ValidateRezept(true);
    }

    private void verbrauchBtn_Click(object sender, EventArgs e)
    {
      if (positionenBindingSource.Count > 0 && positionenBindingSource.Current != null)
      {
        DataRow row = ((DataRowView)positionenBindingSource.Current).Row;
        if (row["Verbrauch"] == DBNull.Value || (short)row["Verbrauch"] == 0)
        {
          verbrauchLabel.Visible = true;
          row["Verbrauch"] = 1;
        }
        else
        {
          verbrauchLabel.Visible = false;
          row["Verbrauch"] = 0;
        }
        UpdatePreis(false);
      }
    }

    private void anzahlNUD_Validating(object sender, CancelEventArgs e)
    {
      //if (anzahlNUD.Text == string.Empty)
      //{
      //  anzahlNUD.Text = anzahlNUD.Value.ToString("N0");
      //  if (neuePositionBtn.Text == "&Abbrechen")
      //  {
      //    neuePositionBtn.Text = "&Neu";
      //    if (positionenBindingSource.Count != 0 && positionenBindingSource.Current != null)
      //      positionenBindingSource.CancelEdit();
      //    nameTB.Focus();
      //    bindingNavigatorAddNewItem_Click(bindingNavigatorAddNewItem, EventArgs.Empty);
      //  }
      //  else
      //    UpdatePreis(false);
      //}
      //else
      anzahlNUD.Text = anzahlNUD.Value.ToString("N0");
      UpdatePreis(false);
    }

    private void anzahlNUD_ValueChanged(object sender, EventArgs e)
    {
      anzahlEigenanteilLabel.Text = anzahlNUD.Value.ToString("N0") + " X";
      anzahlZuzahlungLabel.Text = anzahlNUD.Value.ToString("N0") + " X";
    }

    private void begruendungTB_Validating(object sender, CancelEventArgs e)
    {
      bool newPos = (neuePositionBtn.Text == "&Abbrechen" ? true : false);
      this.BeginInvoke((MethodInvoker)delegate()
      {
        FocusPosition(newPos);
      });
    }

    private void FocusPosition(bool newPos)
    {
      if (newPos)
      {
        showPositionError = false;
        try
        {
          AddNeuePosition();
          neuePositionBtn.Text = "&Abbrechen";
        }
        finally { showPositionError = true; }
      }
      else if(ValidatePosition())
      {
        CalcGesForderung();
        if (positionenBindingSource.Count != 0)
          if (positionenBindingSource.Count - 1 > positionenBindingSource.Position)
            positionenBindingSource.Position++;
          else
            positionenBindingSource.Position = 0;
      }

      if (positionenBindingSource.Count == 1)
        tagMonatMTB.Focus();
      else
        anzahlNUD.Focus();
    }

    private void geburtsDatumMTB_Validating(object sender, CancelEventArgs e)
    {
      MaskedTextBox tb = sender as MaskedTextBox;
      if (tb.Focused)
        try
        {
          DateTime d = DateTime.Parse(tb.Text);
          if (d > DateTime.Today && tb.Text.Length < 10 && d.AddYears(-100) < DateTime.Today)
          {
            d = d.AddYears(-100);
            tb.Text = d.ToString("dd.MM.yyyy");
          }
          if (d > DateTime.Today)
            throw new Exception("Das Datum ist größer als heute.");
        }
        catch (Exception ex)
        {
          MessageBox.Show(ex.Message + " " + tb.Text, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
          e.Cancel = true;
        }
    }

    private void protokollBtn_Click(object sender, EventArgs e)
    {
      if (ValidatePosition() && ValidateRezept(false))
      {
        UpdateDataSet(false);
        if (mandantenBindingSource.Count != 0 && mandantenBindingSource.Current != null)
        {
          DataRow mandant = ((DataRowView)mandantenBindingSource.Current).Row;
          string rezepteCmd = "SELECT ID, IK, Name, Vorname, GebDat, Befreit, RezDat FROM [" +
            AbrechnungsJahr + "_Rezepte] WHERE (Mandant = @Mandant) AND (Vorgang = 300) " +
            "ORDER BY IK, Name, Vorname";
          string positionenCmd = "SELECT AUTO, Faktor, EPreis, Gebuehr, Zuzahlung, " +
            "BuPos, BehDat, Eigenanteil, Anzahl, Mwst, Mwst_KZ FROM [" + AbrechnungsJahr +
            "_Positionen] WHERE (ID = @ID)";
          SqlDataAdapter rezepteDA = new SqlDataAdapter(rezepteCmd, lastIKConnectionString);
          rezepteDA.SelectCommand.Parameters.Add("@Mandant", SqlDbType.VarChar, 50);
          rezepteDA.SelectCommand.Parameters["@Mandant"].Value = mandant["Nr"].ToString();
          SqlDataAdapter positionenDA = new SqlDataAdapter(positionenCmd, lastIKConnectionString);
          positionenDA.SelectCommand.Parameters.Add("@ID", SqlDbType.Int);
          ReportDataSet dataSet = new ReportDataSet();
          //ListLabel ll = null;

          try
          {
            rezepteDA.Fill(dataSet.Rezepte);

            foreach (DataRow rezept in dataSet.Rezepte.Rows)
            {
              DataRow[] kostenTrs = voinDataSet.Kostentraeger.Select("IK = '" + ((int)rezept["IK"]).ToString() + "'");
              decimal forderung = 0;
              decimal zuzahlung = 0;
              decimal forderungNetto = 0;
              decimal zuzahlungNetto = 0;

              rezept["Mandant"] = mandant["Nr"];
              rezept["MandantenName"] = (string)mandant["Name1"] + " " +
                (string)mandant["Name2"];
              if (kostenTrs.Length != 0)
              {
                rezept["KassenName"] = (kostenTrs[0]["Name1"] != DBNull.Value ?
                  (string)kostenTrs[0]["Name1"] : string.Empty) + " " +
                  (kostenTrs[0]["Name2"] != DBNull.Value ?
                  (string)kostenTrs[0]["Name2"] : string.Empty) + " " +
                  (kostenTrs[0]["Name3"] != DBNull.Value ?
                  (string)kostenTrs[0]["Name3"] : string.Empty) + " " +
                  (kostenTrs[0]["Name4"] != DBNull.Value ?
                  (string)kostenTrs[0]["Name4"] : string.Empty);
              }
              positionenDA.SelectCommand.Parameters["@ID"].Value = (int)rezept["ID"];
              positionenDA.Fill(dataSet.Positionen);
              foreach (DataRow position in dataSet.Positionen.Rows)
              {
                decimal pf = CalcForderung(position);
                decimal pz = (decimal)position["Zuzahlung"] * (decimal)position["Anzahl"];
                decimal mwst = GetMwst(position);
                forderung += Math.Round(pf, 2, MidpointRounding.AwayFromZero);
                zuzahlung += Math.Round(pz, 2, MidpointRounding.AwayFromZero);
                forderungNetto += Math.Round(pf / (1m + mwst / 100m), 2, MidpointRounding.AwayFromZero);
                zuzahlungNetto += Math.Round(pz / (1m + mwst / 100m), 2, MidpointRounding.AwayFromZero);
              }
              rezept["Zuzahlung"] = zuzahlung;
              rezept["Forderung"] = forderung;
              rezept["ZuzahlungNetto"] = zuzahlungNetto;
              rezept["ForderungNetto"] = forderungNetto;
              dataSet.Positionen.Clear();
            }
            //ll = new ListLabel();
            //ll.DataSource = dataSet;
            //ll.DataMember = "Rezepte";
            //ll.AutoShowSelectFile = false;
            //ll.AutoDesignerFile = "Protokol.lst";
            //ll.AutoDestination = LlPrintMode.Export;
            //ll.Print();
            PrintProtokoll(dataSet);
          }
          //catch (LL_User_Aborted_Exception)
          //{
          //}
          catch (Exception ex)
          {
            MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
          }
          finally
          {
            if (rezepteDA != null) rezepteDA.Dispose();
            if (dataSet != null) dataSet.Dispose();
            if (positionenDA != null) positionenDA.Dispose();
            //if (ll != null) ll.Dispose();
          }
        }
      }
    }

    private void PrintProtokoll(ReportDataSet dataSet)
    {
      int err = 0;
      axListLabel1.LlDefineVariableStart();
      axListLabel1.LlDefineFieldStart();
      err = axListLabel1.LlDefineFieldExt("Mandant", "70000",
        LlFieldTypeConstants.LL_NUMERIC);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("MandantenName", "Mandant",
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("IK", "103677438",
        LlFieldTypeConstants.LL_NUMERIC);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("KassenName", "Kasse",
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("Name", "Bauer",
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("Vorname", "Herbert",
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("GebDat", "12.04.1967",
        LlFieldTypeConstants.LL_DATE_DMY);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("RezDat", "12.10.2006",
        LlFieldTypeConstants.LL_DATE_DMY);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("ZuzahlungNetto", "8.55",
        LlFieldTypeConstants.LL_NUMERIC);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("ForderungNetto", "18.55",
        LlFieldTypeConstants.LL_NUMERIC);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("Zuzahlung", "10.55",
        LlFieldTypeConstants.LL_NUMERIC);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("Forderung", "23.55",
        LlFieldTypeConstants.LL_NUMERIC);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

      err = axListLabel1.LlPrintWithBoxStart(LlProjectConstants.LL_PROJECT_LIST,
        Path.Combine(System.Windows.Forms.Application.StartupPath, "Protokoll.lst"),
        LlPrintModeConstants.LL_PRINT_USERSELECT,
        LlBoxTypeConstants.LL_BOXTYPE_BRIDGEMETER, this.Handle.ToInt32(), "");
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlPrintOptionsDialog(this.Handle.ToInt32(), "");
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      int dest = axListLabel1.LlPrintGetOption(
        LlPrintOptionConstants.LL_PRNOPT_PRINTDLG_DEST);
      err = axListLabel1.LlPrint();
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

      for (int i = 0; i < dataSet.Rezepte.Rows.Count; i++)
      {
        ReportDataSet.RezepteRow r = (ReportDataSet.RezepteRow)
          dataSet.Rezepte.Rows[i];

        err = axListLabel1.LlDefineFieldExt("Mandant", r.Mandant.ToString(),
          LlFieldTypeConstants.LL_NUMERIC);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("MandantenName", r.MandantenName,
          LlFieldTypeConstants.LL_TEXT);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("IK", r.IK.ToString(),
          LlFieldTypeConstants.LL_NUMERIC);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("KassenName", r.KassenName,
          LlFieldTypeConstants.LL_TEXT);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("Name", r.Name,
          LlFieldTypeConstants.LL_TEXT);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("Vorname", r.Vorname,
          LlFieldTypeConstants.LL_TEXT);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("GebDat", r.GebDat.ToString("dd.MM.yyyy"),
          LlFieldTypeConstants.LL_DATE_DMY);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("RezDat", r.RezDat.ToString("dd.MM.yyyy"),
          LlFieldTypeConstants.LL_DATE_DMY);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("ZuzahlungNetto", r.ZuzahlungNetto.ToString(),
          LlFieldTypeConstants.LL_NUMERIC);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("ForderungNetto", r.ForderungNetto.ToString(),
          LlFieldTypeConstants.LL_NUMERIC);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("Zuzahlung", r.Zuzahlung.ToString(),
          LlFieldTypeConstants.LL_NUMERIC);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("Forderung", r.Forderung.ToString(),
          LlFieldTypeConstants.LL_NUMERIC);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");


        while (axListLabel1.LlPrintFields() != 0)
          err = axListLabel1.LlPrint();
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlPrintSetBoxText("Drucken", (i + 1) * 100 /
          dataSet.Rezepte.Rows.Count);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      }

      while (axListLabel1.LlPrintFieldsEnd() != 0) ;
      err = axListLabel1.LlPrintEnd(0);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      if (dest == (int)LlDestinationConstants.LL_DESTINATION_PRV)
      {
        err = axListLabel1.LlPreviewDisplay("Protokoll.lst",
          System.Windows.Forms.Application.StartupPath, this.Handle.ToInt32());
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        axListLabel1.LlPreviewDeleteFiles("Protokoll.lst",
          System.Windows.Forms.Application.StartupPath);
      }
    }

    private void protokollMI_Click(object sender, EventArgs e)
    {
      if (ValidatePosition() && ValidateRezept(false))
      {
        UpdateDataSet(false);
        if (mandantenBindingSource.Count != 0 && mandantenBindingSource.Current != null)
        {
          DataRow mandant = ((DataRowView)mandantenBindingSource.Current).Row;
          string rezepteCmd = "SELECT ID, IK, Name, Vorname, GebDat, Befreit, RezDat FROM [" +
            AbrechnungsJahr + "_Rezepte] WHERE (Mandant = @Mandant) AND (Vorgang = 300) " +
            "ORDER BY IK, Name, Vorname";
          string positionenCmd = "SELECT AUTO, Faktor, EPreis, Gebuehr, Zuzahlung, " +
            "BuPos, BehDat, Eigenanteil, Anzahl, Mwst, Mwst_KZ FROM [" + AbrechnungsJahr +
            "_Positionen] WHERE (ID = @ID)";
          SqlDataAdapter rezepteDA = new SqlDataAdapter(rezepteCmd, lastIKConnectionString);
          rezepteDA.SelectCommand.Parameters.Add("@Mandant", SqlDbType.VarChar, 50);
          rezepteDA.SelectCommand.Parameters["@Mandant"].Value = mandant["Nr"].ToString();
          SqlDataAdapter positionenDA = new SqlDataAdapter(positionenCmd, lastIKConnectionString);
          positionenDA.SelectCommand.Parameters.Add("@ID", SqlDbType.Int);
          ReportDataSet dataSet = new ReportDataSet();
          //ListLabel ll = null;
          int err = 0;
          try
          {
            rezepteDA.Fill(dataSet.Rezepte);

            foreach (DataRow rezept in dataSet.Rezepte.Rows)
            {
              DataRow[] kostenTrs = voinDataSet.Kostentraeger.Select("IK = '" + ((int)rezept["IK"]).ToString() + "'");
              decimal forderung = 0;
              decimal zuzahlung = 0;
              decimal forderungNetto = 0;
              decimal zuzahlungNetto = 0;

              rezept["Mandant"] = mandant["Nr"];
              rezept["MandantenName"] = (string)mandant["Name1"] + " " +
                (string)mandant["Name2"];
              if (kostenTrs.Length != 0)
              {
                rezept["KassenName"] = (kostenTrs[0]["Name1"] != DBNull.Value ?
                  (string)kostenTrs[0]["Name1"] : string.Empty) + " " +
                  (kostenTrs[0]["Name2"] != DBNull.Value ?
                  (string)kostenTrs[0]["Name2"] : string.Empty) + " " +
                  (kostenTrs[0]["Name3"] != DBNull.Value ?
                  (string)kostenTrs[0]["Name3"] : string.Empty) + " " +
                  (kostenTrs[0]["Name4"] != DBNull.Value ?
                  (string)kostenTrs[0]["Name4"] : string.Empty);
              }
              positionenDA.SelectCommand.Parameters["@ID"].Value = (int)rezept["ID"];
              positionenDA.Fill(dataSet.Positionen);
              foreach (DataRow position in dataSet.Positionen.Rows)
              {
                decimal pf = CalcForderung(position);
                decimal pz = (decimal)position["Zuzahlung"] * (decimal)position["Anzahl"];
                decimal mwst = GetMwst(position);
                forderung += pf;
                zuzahlung += pz;
                forderungNetto += pf / (1m + mwst / 100m);
                zuzahlungNetto += pz / (1m + mwst / 100m);
              }
              rezept["Zuzahlung"] = zuzahlung;
              rezept["Forderung"] = forderung;
              rezept["ZuzahlungNetto"] = zuzahlungNetto;
              rezept["ForderungNetto"] = forderungNetto;
              dataSet.Positionen.Clear();
            }
            //ll = new ListLabel();
            //ll.DataSource = dataSet;
            //ll.DataMember = "Rezepte";
            //ll.AutoShowSelectFile = false;
            //ll.AutoDesignerFile = "Protokol.lst";
            //ll.Design();
            axListLabel1.LlDefineVariableStart();
            axListLabel1.LlDefineFieldStart();
            err = axListLabel1.LlDefineFieldExt("Mandant", "70000",
              LlFieldTypeConstants.LL_NUMERIC);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("MandantenName", "Mandant",
              LlFieldTypeConstants.LL_TEXT);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("IK", "103677438",
              LlFieldTypeConstants.LL_NUMERIC);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("KassenName", "Kasse",
              LlFieldTypeConstants.LL_TEXT);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("Name", "Bauer",
              LlFieldTypeConstants.LL_TEXT);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("Vorname", "Herbert",
              LlFieldTypeConstants.LL_TEXT);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("GebDat", "12.04.1967",
              LlFieldTypeConstants.LL_DATE_DMY);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("RezDat", "12.10.2006",
              LlFieldTypeConstants.LL_DATE_DMY);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("ZuzahlungNetto", "8.55",
              LlFieldTypeConstants.LL_NUMERIC);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("ForderungNetto", "18.55",
              LlFieldTypeConstants.LL_NUMERIC);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("Zuzahlung", "10.55",
              LlFieldTypeConstants.LL_NUMERIC);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
            err = axListLabel1.LlDefineFieldExt("Forderung", "23.55",
              LlFieldTypeConstants.LL_NUMERIC);
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

            err = axListLabel1.LlDefineLayout(this.Handle.ToInt32(), "", LlProjectConstants.LL_PROJECT_LIST,
              Path.Combine(System.Windows.Forms.Application.StartupPath, "Protokoll.lst"));
            if (err < 0)
              throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          }
          catch (Exception ex)
          {
            MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
          }
          finally
          {
            if (rezepteDA != null) rezepteDA.Dispose();
            if (dataSet != null) dataSet.Dispose();
            if (positionenDA != null) positionenDA.Dispose();
            //if (ll != null) ll.Dispose();
          }
        }
      }
    }

    private decimal GetMwst(DataRow position)
    {
      foreach (MehrWertSteuer mwst in MehrWertSteuer.MWStList)
        if (mwst.MWStCode == (short)position["Mwst_KZ"])
          return mwst.MWSt;
      throw new Exception("Mehrwertsteuerkennzeichne: " +
        ((short)position["Mwst_KZ"]).ToString() + "wurde nicht gefunden.\n");
    }

    private void MainForm_Shown(object sender, EventArgs e)
    {
      BuildPositionenDGVColumns();
    }

    private void iniMI_Click(object sender, EventArgs e)
    {
      if (ValidatePosition() && ValidateRezept(true))
      {
        folderBrowserDialog1.SelectedPath = VOIN10.Properties.Settings.Default.IniPath;
        if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
        {
          if (VOIN10.Properties.Settings.Default.IniPath !=
            folderBrowserDialog1.SelectedPath)
          {
            VOIN10.Properties.Settings.Default.IniPath =
              folderBrowserDialog1.SelectedPath;
            voinDataSet.Clear();
            Initialize();
          }
        }
      }

    }

    private void kvBtn_Click(object sender, EventArgs e)
    {
      try
      {
        if (ValidatePosition() && ValidateRezept(false))
        {
          UpdateDataSet(false);
          if (namesListBox.SelectedItem != null && MessageBox.Show("Soll " + ((Rezept)
            namesListBox.SelectedItem).Name + " einen Kostenvoranschlag erhalten?", this.Text,
            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
          {
            Kostenvoranschlag();
          }
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void Kostenvoranschlag()
    {
      Rezept r = (Rezept)namesListBox.SelectedItem;
      VOINDataSet.RezepteRow rr = (VOINDataSet.RezepteRow)
        voinDataSet.Rezepte.FindByID(r.ID);
      if (rr == null)
        throw new Exception("Rezept wurde nicht gefunden.");
      DataRow[] rows = voinDataSet.Kostentraeger.Select("IK = '" + rr.IK.Trim() + "'");
      if (rows == null || rows.Length == 0)
        throw new Exception("Kostenträger wurde nicht gefunden.");
      VOINDataSet.KostentraegerRow ktr = (VOINDataSet.KostentraegerRow)rows[0];
      rows = voinDataSet.Mandanten.Select("Nr = " + rr.Mandant.ToString());
      if (rows == null || rows.Length == 0)
        throw new Exception("Mandant wurde nicht gefunden.");
      VOINDataSet.MandantenRow mr = (VOINDataSet.MandantenRow)rows[0];
      using (KVAnschriftForm form = new KVAnschriftForm())
      {
        form.Name1 = (ktr.IsName1Null() ? string.Empty : ktr.Name1);
        form.Name2 = (ktr.IsName2Null() ? string.Empty : ktr.Name2);
        form.Name3 = (ktr.IsName3Null() ? string.Empty : ktr.Name3);
        form.Name4 = (ktr.IsName4Null() ? string.Empty : ktr.Name4);
        form.Strasse = (ktr.IsAns_StrasseNull() ? string.Empty : ktr.Ans_Strasse);
        form.PLZ = (ktr.IsAns_plzNull() ? string.Empty : ktr.Ans_plz);
        form.Ort = (ktr.IsAns_OrtNull() ? string.Empty : ktr.Ans_Ort);
        form.IK = ktr.IK.Trim();
        form.DataSet = voinDataSet;
        try
        {
          int.Parse(form.Strasse.Trim());
          form.Strasse = "Postfach " + form.Strasse.Trim(); 
        }
        catch
        {
        }
        if (form.ShowDialog() == DialogResult.OK)
        {
          PrintKostenVoranschlag(form.Name1, form.Name2, form.Name3, form.Name4,
            form.Strasse, form.PLZ, form.Ort, form.IK, rr, mr);
          rr.Vorgang = 150;
          UpdateDataSet(true);
        }
      }
    }

    private void PrintKostenVoranschlag(string name1, string name2, string name3,
      string name4, string strasse, string plz, string ort, string ktIK,
      VOINDataSet.RezepteRow rr, VOINDataSet.MandantenRow mr)
    {
      DataSet dataSet = new DataSet();
      SqlDataAdapter da = new SqlDataAdapter();
      SqlConnection con = new SqlConnection(lastIKConnectionString);
      SqlCommand cmd = new SqlCommand("select * from [" + AbrechnungsJahr +
        "_Positionen] where ID = " + rr.ID.ToString(), con);
      int err = 0;
      da.SelectCommand = cmd;
      try
      {
        da.Fill(dataSet);
      }
      finally
      {
        if (con != null && con.State != ConnectionState.Closed)
          con.Close();
      }
      DataTable table = dataSet.Tables[0];
      if (table.Rows.Count == 0)
        throw new Exception("Rezept enthält keine Positionen.");
      axListLabel1.LlDefineVariableStart();
      axListLabel1.LlDefineFieldStart();

      err = axListLabel1.LlDefineVariableExt("Kopf1", (mr.IsKopf1Null() ? string.Empty :
        mr.Kopf1), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("Kopf2", (mr.IsKopf2Null() ? string.Empty :
        mr.Kopf2), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("Kopf3", (mr.IsKopf3Null() ? string.Empty :
        mr.Kopf3), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("KasName1", name1,
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("KasName2", name2,
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("KasName3", name3,
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("KasName4", name4,
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("KasStrasse", strasse,
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("KasPLZ", plz,
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("KasOrt", ort,
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("Telefon", mr.Tel,
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("Fax", (mr.IsFaxNull() ? string.Empty :
        mr.Fax), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("IK", mr.IK.ToString(),
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("USTIDNR", (mr.IsUStidnrNull() ?
        string.Empty : mr.UStidnr), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("EMail", (mr.IsEMailNull() ?
        string.Empty : mr.EMail), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("HomePage", (mr.IsinetNull() ?
        string.Empty : mr.inet), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("Bank1", (mr.IsBeh_BankNull() ?
        string.Empty : mr.Beh_Bank), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("BLZ1", (mr.IsBeh_BLZNull() ? string.Empty :
        mr.Beh_BLZ), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("KTN1", (mr.IsBeh_KontoNrNull() ?
        string.Empty : mr.Beh_KontoNr), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("Zusatz1", (mr.Iszusatz1Null() ?
        string.Empty : mr.zusatz1), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("Zusatz2", (mr.Iszusatz2Null() ?
        string.Empty : mr.zusatz2), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("RezNr", AbrechnungsJahr + "/" +
        rr.ID.ToString(), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("PazName", rr.Name,
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("PazVorname", rr.Vorname,
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("GebDat", (rr.IsGebDatNull() ?
        string.Empty : rr.GebDat.ToString("dd.MM.yyyy")), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("VersNr", rr.VersNr,
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("PazStrasse", (rr.IsStrasseNull() ?
        string.Empty : rr.Strasse), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("PazPLZ", (rr.IsPlzNull() ? string.Empty :
        rr.Plz), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("PazOrt", (rr.IsOrtNull() ? string.Empty :
        rr.Ort), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("RezDat", rr.RezDat.ToString("dd.MM.yyyy"),
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("ArztNr", (rr.IsArztNrNull() ? string.Empty :
        rr.ArztNr), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("Befreit", (rr.IsBefreitNull() ||
        !rr.Befreit ? "false" : "true"), LlFieldTypeConstants.LL_BOOLEAN);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineVariableExt("FussZeile", (mr.IsFusszeileNull() ?
        string.Empty : mr.Fusszeile), LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

      err = axListLabel1.LlDefineFieldExt("Anzahl", "2",
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("PosNr", "24.38.72.2.473",
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("EPreis", "383,55",
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlDefineFieldExt("Bezeichnung", "Einzelgymnastik",
        LlFieldTypeConstants.LL_TEXT);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");


      string fileName = rr.ID.ToString();
      while (fileName.Length < 8) fileName = "0" + fileName;
      fileName = "A" + fileName + "_2_" + ktIK + "_" +
        DateTime.Today.ToString("yyyyMMdd") + ".pdf";
      string pdfPath = Path.Combine(mandantenPath, mr.Nr.ToString());
      pdfPath = Path.Combine(pdfPath, DateTime.Now.ToString("yyMM"));
      pdfPath = Path.Combine(pdfPath, "Pdf");
      if (!Directory.Exists(pdfPath))
        Directory.CreateDirectory(pdfPath);

      err = axListLabel1.LlXSetParameter(LlExtensionTypeConstants.LL_LLX_EXTENSIONTYPE_EXPORT,
        "PDF", "Export.File", fileName);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlXSetParameter(LlExtensionTypeConstants.LL_LLX_EXTENSIONTYPE_EXPORT, "PDF",
        "Export.Path", pdfPath);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlXSetParameter(LlExtensionTypeConstants.LL_LLX_EXTENSIONTYPE_EXPORT,
        "PDF", "Export.Quiet", "1");
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlPrintWithBoxStart(LlProjectConstants.LL_PROJECT_LIST,
        Path.Combine(System.Windows.Forms.Application.StartupPath, "Kostenvoranschlag.lst"),
        LlPrintModeConstants.LL_PRINT_EXPORT, LlBoxTypeConstants.LL_BOXTYPE_BRIDGEMETER,
        this.Handle.ToInt32(), "Export");
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      err = axListLabel1.LlPrintSetOptionString(LlPrintOptionStringConstants.LL_PRNOPTSTR_EXPORT, "PDF");
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

      err = axListLabel1.LlPrint();
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

      for (int i = 0; i < table.Rows.Count; i++)
      {
        DataRow r = table.Rows[i];

        decimal mwst = GetMwst(r);
        decimal ePreis = Math.Round((decimal)r["EPreis"] * (1m + mwst / 100m), 2, MidpointRounding.AwayFromZero);

        err = axListLabel1.LlDefineFieldExt("Anzahl",
          ((decimal)r["Anzahl"]).ToString("N0"), LlFieldTypeConstants.LL_TEXT);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("PosNr", (string)r["BuPos"],
          LlFieldTypeConstants.LL_TEXT);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlDefineFieldExt("EPreis",
          ePreis.ToString("N2"), LlFieldTypeConstants.LL_TEXT);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        DataRow[] rows = voinDataSet.tab_HMPBezeichnung.Select("TRIM(HMP) LIKE '" +
          ((string)r["BuPos"]).Trim() + "' and Mandant = " + mr.Nr.ToString());
        string bez = string.Empty;
        if (rows != null && rows.Length > 0)
          bez = (rows[0]["Bezeichnung"] != DBNull.Value ? (string)rows[0]["Bezeichnung"] :
            string.Empty);
        err = axListLabel1.LlDefineFieldExt("Bezeichnung", bez,
          LlFieldTypeConstants.LL_TEXT);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

        while (axListLabel1.LlPrintFields() != 0)
          err = axListLabel1.LlPrint();
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
        err = axListLabel1.LlPrintSetBoxText("Drucken", (i + 1) * 100 /
          table.Rows.Count);
        if (err < 0)
          throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      }

      while (axListLabel1.LlPrintFieldsEnd() != 0) ;
      err = axListLabel1.LlPrintEnd(0);
      if (err < 0)
        throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
      Process.Start(Path.Combine(pdfPath, fileName));
    }

    private void kostenvoranschlagMI_Click(object sender, EventArgs e)
    {
      try
      {
        if (ValidatePosition() && ValidateRezept(false))
        {
          UpdateDataSet(false);
          int err = 0;

          axListLabel1.LlDefineVariableStart();
          axListLabel1.LlDefineFieldStart();

          err = axListLabel1.LlDefineVariableExt("Kopf1", "Allergopharmr",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Kopf2", "Joachim Ganzer KG",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Kopf3", "Reinbek",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("KasName1", "Verband der Angestellten",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("KasName2", "Krankenkassen eV",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("KasName3", "Landesvertretung",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("KasName4", "Mecklenburg-Vorpommern",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("KasStrasse", "Wismarschenstr. 142",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("KasPLZ", "19053",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("KasOrt", "Schwerin",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Telefon", "05144 987243",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Fax", "05144 987239",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("IK", "590330566",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("USTIDNR", "",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("EMail", "info@allergopharma.de",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("HomePage", "www.allergopharma.de",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Bank1", "Commerzbank AG Reinbek",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("BLZ1", "20040000",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("KTN1", "2 640 001",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Zusatz1", "Zusatz1",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Zusatz2", "Zusatz2",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("RezNr", "06/37344",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("PazName", "Maier",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("PazVorname", "Anna",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("GebDat", "12.04.1967",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("VersNr", "238388293",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("PazStrasse", "Oberbuch 1a",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("PazPLZ", "91344",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("PazOrt", "Fuchshof",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("RezDat", "10.10.2006",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("ArztNr", "3443788",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("Befreit", "true",
            LlFieldTypeConstants.LL_BOOLEAN);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineVariableExt("FussZeile", "Allergopharma * Joachim Ganzer KG",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

          err = axListLabel1.LlDefineFieldExt("Anzahl", "2",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineFieldExt("PosNr", "24.38.72.2.473",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineFieldExt("EPreis", "383,55",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");
          err = axListLabel1.LlDefineFieldExt("Bezeichnung", "Einzelgymnastik",
            LlFieldTypeConstants.LL_TEXT);
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

          err = axListLabel1.LlDefineLayout(this.Handle.ToInt32(), "", LlProjectConstants.LL_PROJECT_LIST,
            Path.Combine(System.Windows.Forms.Application.StartupPath, "Kostenvoranschlag.lst"));
          if (err < 0)
            throw new Exception("Während des Druckens ist ein Fehler aufgetreten.\n");

        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (tabControl1.SelectedTab == kostenvoranschlagTabPage)
      {
        FillKostJahrCB();
      }
    }

    private void FillKostJahrCB()
    {
      try
      {
        int jahr = int.Parse(AbrechnungsJahr);
        kostJahrCB.Items.Clear();
        kostJahrCB.Items.Add(jahr);
        kostJahrCB.Items.Add(jahr - 1);
        kostJahrCB.SelectedIndex = 0;
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    int searchYear = 0;

    private void kostSuchenBtn_Click(object sender, EventArgs e)
    {
      SqlConnection con = new SqlConnection(lastIKConnectionString);
      SqlCommand cmd = new SqlCommand();
      SqlDataAdapter da = new SqlDataAdapter();
      VOINDataSet.RezepteDataTable dt = new VOINDataSet.RezepteDataTable();
      try
      {
        searchYear = (int)kostJahrCB.SelectedItem;
        cmd.CommandText = "SELECT * FROM [" + kostJahrCB.SelectedItem.ToString() +
          "_Rezepte] WHERE Vorgang <> 300 AND Vorgang <> 299";
        if (genehmigungRB.Checked)
          cmd.CommandText += " AND Vorgang = 150";
        if (kostRezNrTB.Text.Trim() != string.Empty)
        {
          if (!cmd.CommandText.ToUpper().Contains("WHERE"))
            cmd.CommandText += " WHERE";
          else
            cmd.CommandText += " AND";
          cmd.CommandText += " ID LIKE '%" + kostRezNrTB.Text.Trim() + "%'";
        }
        if (kostNameTB.Text != string.Empty)
        {
          if (!cmd.CommandText.ToUpper().Contains("WHERE"))
            cmd.CommandText += " WHERE";
          else
            cmd.CommandText += " AND";
          cmd.CommandText += " Name LIKE '%" + kostNameTB.Text.ToUpper().Trim() + "%'";
        }
        if (kostVornameTB.Text != string.Empty)
        {
          if (!cmd.CommandText.ToUpper().Contains("WHERE"))
            cmd.CommandText += " WHERE";
          else
            cmd.CommandText += " AND";
          cmd.CommandText += " Vorname LIKE '%" + kostVornameTB.Text.ToUpper().Trim() + "%'";
        }
        cmd.Connection = con;
        da.SelectCommand = cmd;
        da.Fill(dt);
        DataView dv = new DataView(dt, string.Empty, "Name, Vorname, GebDat",
          DataViewRowState.CurrentRows);
        kostenvoranschlagDGV.DataSource = dv;
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      finally
      {
        if (con != null && con.State != ConnectionState.Closed)
          con.Close();
      }
    }

    private void kostenvoranschlagDGV_MouseDoubleClick(object sender, MouseEventArgs e)
    {
      DataGridView.HitTestInfo info = kostenvoranschlagDGV.HitTest(e.X, e.Y);
      if ((info.Type == DataGridViewHitTestType.Cell || info.Type ==
        DataGridViewHitTestType.RowHeader) &&
        kostenvoranschlagDGV.SelectedRows.Count > 0)
      {
        DataRow row = ((DataRowView)((DataGridViewRow)
          kostenvoranschlagDGV.SelectedRows[0]).DataBoundItem).Row;
        bool doIt = false;
        if (genehmigungRB.Checked && MessageBox.Show("Genehmigung für diesen Patienten eingeben?", this.Text,
          MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
          doIt = true;
        if (korrekturRB.Checked)
        {
          using (KorrekturTypeForm form = new KorrekturTypeForm())
          {
            if (form.ShowDialog() == DialogResult.OK)
              doIt = true;
          }
        }
        if (doIt)
        {
          if (korrekturRB.Checked)//Korrektur
          {
            try
            {
              SetVorgang(row, searchYear, 300);
              AbrechnungsJahr = searchYear.ToString();
              int mandant = (int)row["Mandant"];
              int id = (int)row["ID"];
              int p = mandantenBindingSource.Find("Nr", mandant);
              row.Delete();
              if (p < 0)
                throw new Exception("Mandant wurde nicht gefunden.");
              if (mandantenBindingSource.Position != p)
                mandantenBindingSource.Position = p;
              else
                LoadMandanten(mandant);
              p = rezepteBindingSource.Find("ID", id);
              if (p < 0)
                throw new Exception("Rezept wurde nicht gefunden.");
              rezepteBindingSource.Position = p;
              tabControl1.SelectedTab = rezepteTabPage;
            }
            catch (Exception ex)
            {
              MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK,
                MessageBoxIcon.Error);
            }
          }
          else //Genehmigung
          {
            CopyRezept((int)row["ID"], searchYear, 300);
          }
          kostenvoranschlagDGV.DataSource = null;
        }

      }
    }

    private void CopyRezept(int id, int searchYear, int vorgang)
    {
      SqlDataAdapter rezDA = new SqlDataAdapter();
      DataTable rezTbl = new DataTable();
      SqlDataAdapter posDA = new SqlDataAdapter();
      DataTable posTbl = new DataTable();
      rezDA.SelectCommand = new SqlCommand("select * from [" + searchYear.ToString() +
        "_Rezepte] where id = @Original_ID", rezepteDA.SelectCommand.Connection);
      rezDA.SelectCommand.Parameters.Add("@Original_ID", SqlDbType.Int);
      rezDA.SelectCommand.Parameters["@Original_ID"].Value = id;
      rezDA.InsertCommand = rezepteDA.InsertCommand.Clone();
      posDA.SelectCommand = positionenDA.SelectCommand.Clone();
      posDA.SelectCommand.Parameters["@ID"].Value = id;
      posDA.InsertCommand = positionenDA.InsertCommand.Clone();
      posDA.SelectCommand.CommandText =
        posDA.SelectCommand.CommandText.Replace("[" + AbrechnungsJahr + "_",
        "[" + searchYear.ToString() + "_");
      try
      {
        rezDA.Fill(rezTbl);
        posDA.Fill(posTbl);
        if (rezTbl.Rows.Count == 0)
          throw new Exception("Rezept wurde nicht gefunden.");
        rezTbl.Rows[0].SetAdded();
        rezTbl.Rows[0]["Vorgang"] = vorgang;
        foreach (DataRow r in posTbl.Rows)
          r.SetAdded();
        rezDA.Update(rezTbl);
        int nID = (int)rezTbl.Rows[0]["ID"];
        foreach (DataRow r in posTbl.Rows)
          r["ID"] = nID;
        posDA.Update(posTbl);
        int p = mandantenBindingSource.Find("Nr", int.Parse((string)rezTbl.Rows[0]["Mandant"]));
        if (p < 0)
          throw new Exception("Mandant wurde nicht gefunden.");
        if (mandantenBindingSource.Position != p)
          mandantenBindingSource.Position = p;
        else
          LoadMandanten(int.Parse((string)rezTbl.Rows[0]["Mandant"]));
        p = rezepteBindingSource.Find("ID", nID);
        if (p < 0)
          throw new Exception("Rezept wurde nicht gefunden.");
        rezepteBindingSource.Position = p;
        tabControl1.SelectedTab = rezepteTabPage;
        if (rezepteBindingSource.IsBindingSuspended)
          rezepteBindingSource.ResumeBinding();
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      finally
      {
        if (rezDA.SelectCommand.Connection.State != ConnectionState.Closed)
          rezDA.SelectCommand.Connection.Close();
        if (posDA.SelectCommand.Connection.State != ConnectionState.Closed)
          posDA.SelectCommand.Connection.Close();
        rezDA.Dispose();
        rezTbl.Dispose();
        posDA.Dispose();
        posTbl.Dispose();
      }
    }

    private void SetVorgang(DataRow row, int year, int vorgang)
    {
      SqlConnection con = new SqlConnection(lastIKConnectionString);
      SqlCommand cmd = new SqlCommand("UPDATE [" + year.ToString() + "_Rezepte] SET " +
        "Vorgang = " + vorgang.ToString() + " WHERE ID = " + ((int)row["ID"]).ToString(),
        con);
      try
      {
        con.Open();
        cmd.ExecuteNonQuery();
      }
      finally
      {
        if (con != null && con.State != ConnectionState.Closed)
          con.Close();
      }
    }

    private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
    {
      if (e.TabPage != rezepteTabPage && (!ValidatePosition() || !ValidateRezept(true)))
      {
        e.Cancel = true;
        splitContainer1.Panel2.Select();
      }
    }

    private void eigenanteilCB_CheckedChanged(object sender, EventArgs e)
    {
      if (eigenanteilCB.Checked) eigenanteilCB.Text = "Eigenanteil";
      else eigenanteilCB.Text = "Übernahme";
      decimal ePreis = 0;
      decimal eigenAnteil = 0;
      try { ePreis = decimal.Parse(ePreisTB.Text); }
      catch { }
      try
      {
        eigenAnteil = decimal.Parse(eigenAnteilTB.Text);
      }
      catch { }
      eigenAnteil = ePreis - eigenAnteil;
      eigenAnteilTB.Text = eigenAnteil.ToString("N2");
    }

  }
}