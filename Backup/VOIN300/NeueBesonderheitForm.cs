﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace VOIN10
{
  public partial class NeueBesonderheitForm : Form
  {
    public RichTextBox RichText
    {
      get { return richTextBox; }
    }

    public NeueBesonderheitForm()
    {
      InitializeComponent();
    }

    private void boldBtn_Click(object sender, EventArgs e)
    {
      richTextBox.SelectionFont = new Font(richTextBox.SelectionFont, richTextBox.SelectionFont.Style ^ FontStyle.Bold);
      boldBtn.Checked = richTextBox.SelectionFont.Bold;
    }

    private void italicBtn_Click(object sender, EventArgs e)
    {
      richTextBox.SelectionFont = new Font(richTextBox.SelectionFont, richTextBox.SelectionFont.Style ^ FontStyle.Italic);
      italicBtn.Checked = richTextBox.SelectionFont.Italic;
    }

    private void underlineBtn_Click(object sender, EventArgs e)
    {
      richTextBox.SelectionFont = new Font(richTextBox.SelectionFont, richTextBox.SelectionFont.Style ^ FontStyle.Underline);
      underlineBtn.Checked = richTextBox.SelectionFont.Underline;
    }

    private void richTextBox_SelectionChanged(object sender, EventArgs e)
    {
      boldBtn.Checked = richTextBox.SelectionFont.Bold;
      italicBtn.Checked = richTextBox.SelectionFont.Italic;
      underlineBtn.Checked = richTextBox.SelectionFont.Underline;
    }
  }
}