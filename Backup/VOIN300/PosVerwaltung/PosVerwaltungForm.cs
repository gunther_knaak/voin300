using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using SwS.Data;

namespace VOIN10.PosVerwaltung
{
  public partial class PosVerwaltungForm : Form
  {
    #region Fields And Properties

    private int mandant = 0;
    public int Mandant
    {
      get { return mandant; }
      set { mandant = value; }
    }

    private string mTarifKZ = string.Empty;
    public string MTarifKZ
    {
      get { return mTarifKZ; }
      set { mTarifKZ = value; }
    }

    private string kTarif = string.Empty;
    public string KTarif
    {
      get { return kTarif; }
      set { kTarif = value; }
    }

    private PosVerwaltungDataSet.tab_HMPBezeichnungRow curPosition = null;

    public string ConnectionString
    {
      get { return sqlConnection.ConnectionString; }
      set { sqlConnection.ConnectionString = value; }
    }

    private bool errorsOccurred = false;

    #endregion

    public PosVerwaltungForm()
    {
      InitializeComponent();
      UpdateBindingNavigator(positionenBS);
      posVerwaltungDS.tab_HMPBezeichnung.TableNewRow += new DataTableNewRowEventHandler(tab_HMPBezeichnung_TableNewRow);
    }

    void tab_HMPBezeichnung_TableNewRow(object sender, DataTableNewRowEventArgs e)
    {
      e.Row["Mandant"] = Mandant;
      e.Row["TarifKZ"] = MTarifKZ + KTarif;
    }

    #region Logic

    private void LoadData()
    {
      try
      {
        posVerwaltungDS.BeginInit();
        posVerwaltungDS.tab_HMPBezeichnung.BeginLoadData();
        posVerwaltungDS.tab_HMPBezeichnung.Clear();
        hmpBezeichnungDA.SelectCommand.Parameters["@Mandant"].Value = Mandant;
        hmpBezeichnungDA.Fill(posVerwaltungDS.tab_HMPBezeichnung);
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message + "\r\n" + ex.StackTrace, this.Text,
          MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      finally
      {
        if (sqlConnection.State != ConnectionState.Closed)
          sqlConnection.Close();
        posVerwaltungDS.tab_HMPBezeichnung.EndLoadData();
        posVerwaltungDS.EndInit();
      }
    }

    private bool CanHide()
    {
      bool res = Validate();
      if (res)
      {
        try
        {
          positionenBS.EndEdit();
          PosVerwaltungDataSet.tab_HMPBezeichnungRow pos = null;
          if (positionenBS.Count > 0 && positionenBS.Position >= 0)
            pos = (PosVerwaltungDataSet.tab_HMPBezeichnungRow)
              ((DataRowView)positionenBS.Current).Row;
          res = UpdatePosition(pos);
        }
        catch (NoNullAllowedException ex)
        {
          if (MessageBox.Show(ex.Message +
            "\r\nWollen Sie den aktuellen Bearbeitungsvorgang abbrechen?", this.Text,
            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            res = false;
          else
            positionenBS.CancelEdit();
        }
        catch (Exception ex)
        {
          if (MessageBox.Show(ex.Message + "\r\n" + ex.StackTrace +
            "\r\nWollen Sie den aktuellen Bearbeitungsvorgang abbrechen?", this.Text,
            MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.No)
            res = false;
          else
            positionenBS.CancelEdit();
        }
      }
      return res;
    }

    private bool UpdatePosition(PosVerwaltungDataSet.tab_HMPBezeichnungRow row)
    {
      bool res = true;
      if (row != null && (row.RowState == DataRowState.Added ||
        row.RowState == DataRowState.Deleted || row.RowState == DataRowState.Modified))
      {
        SqlTransaction tran = null;
        try
        {
          sqlConnection.Open();
          tran = sqlConnection.BeginTransaction();
          hmpBezeichnungDA.UpdateCommand.Transaction = tran;
          hmpBezeichnungDA.DeleteCommand.Transaction = tran;
          hmpBezeichnungDA.InsertCommand.Transaction = tran;
          errorsOccurred = false;
          hmpBezeichnungDA.Update(new DataRow[] { row });
          if(!errorsOccurred)
          {
            tran.Commit();
            row.AcceptChanges();
          }
          else
          {
            LoadData();
          }
        }
        catch (Exception ex)
        {
          MessageBox.Show(ex.Message + "\r\n" + ex.StackTrace, this.Text, MessageBoxButtons.OK, 
            MessageBoxIcon.Error);
          row.RejectChanges();
          res = false;
        }
        finally
        {
          if (tran != null)
            tran.Dispose();
          if (sqlConnection.State != ConnectionState.Closed)
            sqlConnection.Close();
        }
      }
      return res;
    }

    private void ClearData()
    {
      try
      {
        posVerwaltungDS.BeginInit();
        posVerwaltungDS.tab_HMPBezeichnung.BeginLoadData();
        posVerwaltungDS.tab_HMPBezeichnung.Clear();
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message + "\r\n" + ex.StackTrace, this.Text,
          MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      finally
      {
        posVerwaltungDS.tab_HMPBezeichnung.EndLoadData();
        posVerwaltungDS.EndInit();
      }
    }

    private void UpdateBindingNavigator(BindingSource bs)
    {
      if (bs != null)
      {
        if (bs.Count > 0)
        {
          if (bs.Position > 0)
          {
            bindingNavigatorMoveFirstItem.Enabled = true;
            bindingNavigatorMovePreviousItem.Enabled = true;
          }
          else
          {
            bindingNavigatorMoveFirstItem.Enabled = false;
            bindingNavigatorMovePreviousItem.Enabled = false;
          }
          bindingNavigatorPositionItem.Text = (bs.Position + 1).ToString();
          bindingNavigatorPositionItem.Enabled = true;
          if (bs.Position < bs.Count - 1)
          {
            bindingNavigatorMoveNextItem.Enabled = true;
            bindingNavigatorMoveLastItem.Enabled = true;
          }
          else
          {
            bindingNavigatorMoveNextItem.Enabled = false;
            bindingNavigatorMoveLastItem.Enabled = false;
          }
          bindingNavigatorDeleteItem.Enabled = true;
        }
        else
        {
          bindingNavigatorMoveFirstItem.Enabled = false;
          bindingNavigatorMovePreviousItem.Enabled = false;
          bindingNavigatorPositionItem.Enabled = false;
          bindingNavigatorPositionItem.Text = "0";
          bindingNavigatorMoveNextItem.Enabled = false;
          bindingNavigatorMoveLastItem.Enabled = false;
          bindingNavigatorDeleteItem.Enabled = false;
        }
      }
    }

    private void OnNavigatorException(Exception ex)
    {
      MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
    }

    #endregion

    #region Event Handlers

    private void PosVerwaltungForm_Load(object sender, EventArgs e)
    {
      LoadData();
    }

    private void hmpBezeichnungDA_RowUpdated(object sender, SqlRowUpdatedEventArgs e)
    {
      if (e.RecordsAffected == 0)
      {
        e.Status = UpdateStatus.SkipCurrentRow;
        errorsOccurred = true;
      }
    }

    private void PosVerwaltungForm_FormClosing(object sender, FormClosingEventArgs e)
    {
      e.Cancel = !CanHide();
    }

    private void PosVerwaltungForm_FormClosed(object sender, FormClosedEventArgs e)
    {
      ClearData();
    }

    private void positionenBS_CurrentChanged(object sender, EventArgs e)
    {
      PosVerwaltungDataSet.tab_HMPBezeichnungRow newRow = null;
      if (positionenBS.Count > 0 && positionenBS.Position >= 0)
        newRow = (PosVerwaltungDataSet.tab_HMPBezeichnungRow)
          ((DataRowView)positionenBS.Current).Row;
      if (newRow != curPosition)
      {
        PosVerwaltungDataSet.tab_HMPBezeichnungRow oldRow = curPosition;
        curPosition = newRow;
        if (oldRow != null)
          this.BeginInvoke((MethodInvoker)delegate()
          {
            if (oldRow != null)
              UpdatePosition(oldRow);
          });
      }
      UpdateBindingNavigator(positionenBS);
    }

    private void bindingNavigatorMoveFirstItem_Click(object sender, EventArgs e)
    {
      try
      {
        positionenBS.MoveFirst();
      }
      catch (Exception ex)
      {
        OnNavigatorException(ex);
      }
    }

    private void bindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
    {
      try
      {
        positionenBS.MovePrevious();
      }
      catch (Exception ex)
      {
        OnNavigatorException(ex);
      }
    }

    private void bindingNavigatorMoveNextItem_Click(object sender, EventArgs e)
    {
      try
      {
        positionenBS.MoveNext();
      }
      catch (Exception ex)
      {
        OnNavigatorException(ex);
      }
    }

    private void bindingNavigatorMoveLastItem_Click(object sender, EventArgs e)
    {
      try
      {
        positionenBS.MoveLast();
      }
      catch (Exception ex)
      {
        OnNavigatorException(ex);
      }

    }

    private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
    {
      try
      {
        positionenBS.AddNew();
      }
      catch (Exception ex)
      {
        OnNavigatorException(ex);
      }

    }

    private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
    {
      try
      {
        positionenBS.RemoveCurrent();
      }
      catch (Exception ex)
      {
        OnNavigatorException(ex);
      }

    }

    private void bindingNavigatorPositionItem_Validating(object sender, CancelEventArgs e)
    {
      try
      {
        int p = positionenBS.Position;
        try { p = int.Parse(bindingNavigatorPositionItem.Text); }
        catch { }
        positionenBS.Position = p;
      }
      catch (Exception ex)
      {
        OnNavigatorException(ex);
      }
      finally
      {
        bindingNavigatorPositionItem.Text = positionenBS.Position.ToString();
      }
    }

    private void positionenDGV_DataError(object sender, DataGridViewDataErrorEventArgs e)
    {
      MessageBox.Show(e.Exception.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
    }

    #endregion


  }
}