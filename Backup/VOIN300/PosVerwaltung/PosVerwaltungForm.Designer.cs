﻿namespace VOIN10.PosVerwaltung
{
  partial class PosVerwaltungForm
  {
    /// <summary>
    /// Erforderliche Designervariable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Verwendete Ressourcen bereinigen.
    /// </summary>
    /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Vom Windows Form-Designer generierter Code

    /// <summary>
    /// Erforderliche Methode für die Designerunterstützung.
    /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PosVerwaltungForm));
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      this.hmpBezeichnungSelectCommand = new System.Data.SqlClient.SqlCommand();
      this.sqlConnection = new System.Data.SqlClient.SqlConnection();
      this.hmpBezeichnungInsertCommand = new System.Data.SqlClient.SqlCommand();
      this.hmpBezeichnungUpdateCommand = new System.Data.SqlClient.SqlCommand();
      this.hmpBezeichnungDeleteCommand = new System.Data.SqlClient.SqlCommand();
      this.hmpBezeichnungDA = new System.Data.SqlClient.SqlDataAdapter();
      this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
      this.positionenBS = new System.Windows.Forms.BindingSource(this.components);
      this.posVerwaltungDS = new VOIN10.PosVerwaltung.PosVerwaltungDataSet();
      this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
      this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
      this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
      this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
      this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
      this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
      this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
      this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
      this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
      this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
      this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
      this.positionenDGV = new System.Windows.Forms.DataGridView();
      this.hMPDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.mandantDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.bezeichnungDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.artikelNrDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.ePreisDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.tarifKZDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
      this.bindingNavigator1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.positionenBS)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.posVerwaltungDS)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.positionenDGV)).BeginInit();
      this.SuspendLayout();
      // 
      // hmpBezeichnungSelectCommand
      // 
      this.hmpBezeichnungSelectCommand.CommandText = "SELECT     HMP, Mandant, Bezeichnung, ArtikelNr, EPreis, TarifKZ\r\nFROM         ta" +
          "b_HMPBezeichnung\r\nWHERE     (Mandant = @Mandant)";
      this.hmpBezeichnungSelectCommand.Connection = this.sqlConnection;
      this.hmpBezeichnungSelectCommand.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Mandant", System.Data.SqlDbType.Int, 4, "Mandant")});
      // 
      // sqlConnection
      // 
      this.sqlConnection.ConnectionString = "Data Source=.\\SQLEXPRESS;AttachDbFilename=C:\\stamm4.mdf;Integrated Security=True;" +
          "Connect Timeout=30;User Instance=True";
      this.sqlConnection.FireInfoMessageEventOnUserErrors = false;
      // 
      // hmpBezeichnungInsertCommand
      // 
      this.hmpBezeichnungInsertCommand.CommandText = resources.GetString("hmpBezeichnungInsertCommand.CommandText");
      this.hmpBezeichnungInsertCommand.Connection = this.sqlConnection;
      this.hmpBezeichnungInsertCommand.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@HMP", System.Data.SqlDbType.VarChar, 0, "HMP"),
            new System.Data.SqlClient.SqlParameter("@Mandant", System.Data.SqlDbType.Int, 0, "Mandant"),
            new System.Data.SqlClient.SqlParameter("@Bezeichnung", System.Data.SqlDbType.VarChar, 0, "Bezeichnung"),
            new System.Data.SqlClient.SqlParameter("@ArtikelNr", System.Data.SqlDbType.VarChar, 0, "ArtikelNr"),
            new System.Data.SqlClient.SqlParameter("@EPreis", System.Data.SqlDbType.Money, 0, "EPreis"),
            new System.Data.SqlClient.SqlParameter("@TarifKZ", System.Data.SqlDbType.VarChar, 0, "TarifKZ")});
      // 
      // hmpBezeichnungUpdateCommand
      // 
      this.hmpBezeichnungUpdateCommand.CommandText = resources.GetString("hmpBezeichnungUpdateCommand.CommandText");
      this.hmpBezeichnungUpdateCommand.Connection = this.sqlConnection;
      this.hmpBezeichnungUpdateCommand.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@HMP", System.Data.SqlDbType.VarChar, 0, "HMP"),
            new System.Data.SqlClient.SqlParameter("@Mandant", System.Data.SqlDbType.Int, 0, "Mandant"),
            new System.Data.SqlClient.SqlParameter("@Bezeichnung", System.Data.SqlDbType.VarChar, 0, "Bezeichnung"),
            new System.Data.SqlClient.SqlParameter("@ArtikelNr", System.Data.SqlDbType.VarChar, 0, "ArtikelNr"),
            new System.Data.SqlClient.SqlParameter("@EPreis", System.Data.SqlDbType.Money, 0, "EPreis"),
            new System.Data.SqlClient.SqlParameter("@TarifKZ", System.Data.SqlDbType.VarChar, 0, "TarifKZ"),
            new System.Data.SqlClient.SqlParameter("@Original_HMP", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "HMP", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Mandant", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Mandant", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_TarifKZ", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "TarifKZ", System.Data.DataRowVersion.Original, null)});
      // 
      // hmpBezeichnungDeleteCommand
      // 
      this.hmpBezeichnungDeleteCommand.CommandText = "DELETE FROM [tab_HMPBezeichnung] WHERE (([HMP] = @Original_HMP) AND ([Mandant] = " +
          "@Original_Mandant) AND ([TarifKZ] = @Original_TarifKZ))";
      this.hmpBezeichnungDeleteCommand.Connection = this.sqlConnection;
      this.hmpBezeichnungDeleteCommand.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Original_HMP", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "HMP", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Mandant", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Mandant", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_TarifKZ", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "TarifKZ", System.Data.DataRowVersion.Original, null)});
      // 
      // hmpBezeichnungDA
      // 
      this.hmpBezeichnungDA.AcceptChangesDuringUpdate = false;
      this.hmpBezeichnungDA.ContinueUpdateOnError = true;
      this.hmpBezeichnungDA.DeleteCommand = this.hmpBezeichnungDeleteCommand;
      this.hmpBezeichnungDA.InsertCommand = this.hmpBezeichnungInsertCommand;
      this.hmpBezeichnungDA.SelectCommand = this.hmpBezeichnungSelectCommand;
      this.hmpBezeichnungDA.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "tab_HMPBezeichnung", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("HMP", "HMP"),
                        new System.Data.Common.DataColumnMapping("Mandant", "Mandant"),
                        new System.Data.Common.DataColumnMapping("Bezeichnung", "Bezeichnung"),
                        new System.Data.Common.DataColumnMapping("ArtikelNr", "ArtikelNr"),
                        new System.Data.Common.DataColumnMapping("EPreis", "EPreis"),
                        new System.Data.Common.DataColumnMapping("TarifKZ", "TarifKZ")})});
      this.hmpBezeichnungDA.UpdateCommand = this.hmpBezeichnungUpdateCommand;
      this.hmpBezeichnungDA.RowUpdated += new System.Data.SqlClient.SqlRowUpdatedEventHandler(this.hmpBezeichnungDA_RowUpdated);
      // 
      // bindingNavigator1
      // 
      this.bindingNavigator1.AddNewItem = null;
      this.bindingNavigator1.BindingSource = this.positionenBS;
      this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem;
      this.bindingNavigator1.DeleteItem = null;
      this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem});
      this.bindingNavigator1.Location = new System.Drawing.Point(0, 0);
      this.bindingNavigator1.MoveFirstItem = null;
      this.bindingNavigator1.MoveLastItem = null;
      this.bindingNavigator1.MoveNextItem = null;
      this.bindingNavigator1.MovePreviousItem = null;
      this.bindingNavigator1.Name = "bindingNavigator1";
      this.bindingNavigator1.PositionItem = null;
      this.bindingNavigator1.Size = new System.Drawing.Size(697, 25);
      this.bindingNavigator1.TabIndex = 0;
      this.bindingNavigator1.Text = "bindingNavigator1";
      // 
      // positionenBS
      // 
      this.positionenBS.DataMember = "tab_HMPBezeichnung";
      this.positionenBS.DataSource = this.posVerwaltungDS;
      this.positionenBS.CurrentChanged += new System.EventHandler(this.positionenBS_CurrentChanged);
      // 
      // posVerwaltungDS
      // 
      this.posVerwaltungDS.DataSetName = "PosVerwaltungDataSet";
      this.posVerwaltungDS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
      // 
      // bindingNavigatorCountItem
      // 
      this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
      this.bindingNavigatorCountItem.Size = new System.Drawing.Size(44, 22);
      this.bindingNavigatorCountItem.Text = "von {0}";
      this.bindingNavigatorCountItem.ToolTipText = "Die Gesamtanzahl der Elemente.";
      // 
      // bindingNavigatorMoveFirstItem
      // 
      this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
      this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
      this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
      this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
      this.bindingNavigatorMoveFirstItem.Text = "Erste verschieben";
      this.bindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.bindingNavigatorMoveFirstItem_Click);
      // 
      // bindingNavigatorMovePreviousItem
      // 
      this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
      this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
      this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
      this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
      this.bindingNavigatorMovePreviousItem.Text = "Vorherige verschieben";
      this.bindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.bindingNavigatorMovePreviousItem_Click);
      // 
      // bindingNavigatorSeparator
      // 
      this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
      this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
      // 
      // bindingNavigatorPositionItem
      // 
      this.bindingNavigatorPositionItem.AccessibleName = "Position";
      this.bindingNavigatorPositionItem.AutoSize = false;
      this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
      this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
      this.bindingNavigatorPositionItem.Text = "0";
      this.bindingNavigatorPositionItem.ToolTipText = "Aktuelle Position";
      this.bindingNavigatorPositionItem.Validating += new System.ComponentModel.CancelEventHandler(this.bindingNavigatorPositionItem_Validating);
      // 
      // bindingNavigatorSeparator1
      // 
      this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
      this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
      // 
      // bindingNavigatorMoveNextItem
      // 
      this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
      this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
      this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
      this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
      this.bindingNavigatorMoveNextItem.Text = "Nächste verschieben";
      this.bindingNavigatorMoveNextItem.Click += new System.EventHandler(this.bindingNavigatorMoveNextItem_Click);
      // 
      // bindingNavigatorMoveLastItem
      // 
      this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
      this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
      this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
      this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
      this.bindingNavigatorMoveLastItem.Text = "Letzte verschieben";
      this.bindingNavigatorMoveLastItem.Click += new System.EventHandler(this.bindingNavigatorMoveLastItem_Click);
      // 
      // bindingNavigatorSeparator2
      // 
      this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
      this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
      // 
      // bindingNavigatorAddNewItem
      // 
      this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
      this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
      this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
      this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
      this.bindingNavigatorAddNewItem.Text = "Neu hinzufügen";
      this.bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
      // 
      // bindingNavigatorDeleteItem
      // 
      this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
      this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
      this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
      this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
      this.bindingNavigatorDeleteItem.Text = "Löschen";
      this.bindingNavigatorDeleteItem.Click += new System.EventHandler(this.bindingNavigatorDeleteItem_Click);
      // 
      // positionenDGV
      // 
      this.positionenDGV.AllowUserToDeleteRows = false;
      this.positionenDGV.AutoGenerateColumns = false;
      this.positionenDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
      this.positionenDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.positionenDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.hMPDataGridViewTextBoxColumn,
            this.mandantDataGridViewTextBoxColumn,
            this.bezeichnungDataGridViewTextBoxColumn,
            this.artikelNrDataGridViewTextBoxColumn,
            this.ePreisDataGridViewTextBoxColumn,
            this.tarifKZDataGridViewTextBoxColumn});
      this.positionenDGV.DataSource = this.positionenBS;
      this.positionenDGV.Dock = System.Windows.Forms.DockStyle.Fill;
      this.positionenDGV.Location = new System.Drawing.Point(0, 25);
      this.positionenDGV.MultiSelect = false;
      this.positionenDGV.Name = "positionenDGV";
      this.positionenDGV.Size = new System.Drawing.Size(697, 311);
      this.positionenDGV.TabIndex = 1;
      this.positionenDGV.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.positionenDGV_DataError);
      // 
      // hMPDataGridViewTextBoxColumn
      // 
      this.hMPDataGridViewTextBoxColumn.DataPropertyName = "HMP";
      this.hMPDataGridViewTextBoxColumn.HeaderText = "HMP";
      this.hMPDataGridViewTextBoxColumn.Name = "hMPDataGridViewTextBoxColumn";
      // 
      // mandantDataGridViewTextBoxColumn
      // 
      this.mandantDataGridViewTextBoxColumn.DataPropertyName = "Mandant";
      this.mandantDataGridViewTextBoxColumn.HeaderText = "Mandant";
      this.mandantDataGridViewTextBoxColumn.Name = "mandantDataGridViewTextBoxColumn";
      // 
      // bezeichnungDataGridViewTextBoxColumn
      // 
      this.bezeichnungDataGridViewTextBoxColumn.DataPropertyName = "Bezeichnung";
      this.bezeichnungDataGridViewTextBoxColumn.HeaderText = "Bezeichnung";
      this.bezeichnungDataGridViewTextBoxColumn.Name = "bezeichnungDataGridViewTextBoxColumn";
      // 
      // artikelNrDataGridViewTextBoxColumn
      // 
      this.artikelNrDataGridViewTextBoxColumn.DataPropertyName = "ArtikelNr";
      this.artikelNrDataGridViewTextBoxColumn.HeaderText = "ArtikelNr";
      this.artikelNrDataGridViewTextBoxColumn.Name = "artikelNrDataGridViewTextBoxColumn";
      // 
      // ePreisDataGridViewTextBoxColumn
      // 
      this.ePreisDataGridViewTextBoxColumn.DataPropertyName = "EPreis";
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
      dataGridViewCellStyle1.Format = "C2";
      dataGridViewCellStyle1.NullValue = null;
      this.ePreisDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle1;
      this.ePreisDataGridViewTextBoxColumn.HeaderText = "EPreis";
      this.ePreisDataGridViewTextBoxColumn.Name = "ePreisDataGridViewTextBoxColumn";
      // 
      // tarifKZDataGridViewTextBoxColumn
      // 
      this.tarifKZDataGridViewTextBoxColumn.DataPropertyName = "TarifKZ";
      this.tarifKZDataGridViewTextBoxColumn.HeaderText = "TarifKZ";
      this.tarifKZDataGridViewTextBoxColumn.Name = "tarifKZDataGridViewTextBoxColumn";
      // 
      // PosVerwaltungForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(697, 336);
      this.Controls.Add(this.positionenDGV);
      this.Controls.Add(this.bindingNavigator1);
      this.Name = "PosVerwaltungForm";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Positionenverwaltung";
      this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.PosVerwaltungForm_FormClosed);
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PosVerwaltungForm_FormClosing);
      this.Load += new System.EventHandler(this.PosVerwaltungForm_Load);
      ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
      this.bindingNavigator1.ResumeLayout(false);
      this.bindingNavigator1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.positionenBS)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.posVerwaltungDS)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.positionenDGV)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Data.SqlClient.SqlCommand hmpBezeichnungSelectCommand;
    private System.Data.SqlClient.SqlCommand hmpBezeichnungInsertCommand;
    private System.Data.SqlClient.SqlCommand hmpBezeichnungUpdateCommand;
    private System.Data.SqlClient.SqlCommand hmpBezeichnungDeleteCommand;
    private System.Data.SqlClient.SqlDataAdapter hmpBezeichnungDA;
    private System.Data.SqlClient.SqlConnection sqlConnection;
    private PosVerwaltungDataSet posVerwaltungDS;
    private System.Windows.Forms.BindingSource positionenBS;
    private System.Windows.Forms.BindingNavigator bindingNavigator1;
    private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
    private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
    private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
    private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
    private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
    private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
    private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
    private System.Windows.Forms.DataGridView positionenDGV;
    private System.Windows.Forms.DataGridViewTextBoxColumn hMPDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn mandantDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn bezeichnungDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn artikelNrDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn ePreisDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn tarifKZDataGridViewTextBoxColumn;
  }
}