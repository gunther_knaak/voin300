﻿namespace VOIN10
{
  partial class NeueBesonderheitForm
  {
    /// <summary>
    /// Erforderliche Designervariable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Verwendete Ressourcen bereinigen.
    /// </summary>
    /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Vom Windows Form-Designer generierter Code

    /// <summary>
    /// Erforderliche Methode für die Designerunterstützung.
    /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NeueBesonderheitForm));
      this.toolStrip1 = new System.Windows.Forms.ToolStrip();
      this.boldBtn = new System.Windows.Forms.ToolStripButton();
      this.italicBtn = new System.Windows.Forms.ToolStripButton();
      this.underlineBtn = new System.Windows.Forms.ToolStripButton();
      this.buttonsPanel = new System.Windows.Forms.Panel();
      this.panel1 = new System.Windows.Forms.Panel();
      this.abbrechenBtn = new System.Windows.Forms.Button();
      this.okBtn = new System.Windows.Forms.Button();
      this.rtfPanel = new System.Windows.Forms.Panel();
      this.richTextBox = new System.Windows.Forms.RichTextBox();
      this.toolStrip1.SuspendLayout();
      this.buttonsPanel.SuspendLayout();
      this.panel1.SuspendLayout();
      this.rtfPanel.SuspendLayout();
      this.SuspendLayout();
      // 
      // toolStrip1
      // 
      this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.boldBtn,
            this.italicBtn,
            this.underlineBtn});
      this.toolStrip1.Location = new System.Drawing.Point(0, 0);
      this.toolStrip1.Name = "toolStrip1";
      this.toolStrip1.Size = new System.Drawing.Size(635, 25);
      this.toolStrip1.TabIndex = 0;
      this.toolStrip1.Text = "toolStrip1";
      // 
      // boldBtn
      // 
      this.boldBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.boldBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.boldBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.boldBtn.Name = "boldBtn";
      this.boldBtn.Size = new System.Drawing.Size(23, 22);
      this.boldBtn.Text = "F";
      this.boldBtn.Click += new System.EventHandler(this.boldBtn_Click);
      // 
      // italicBtn
      // 
      this.italicBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.italicBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.italicBtn.Image = ((System.Drawing.Image)(resources.GetObject("italicBtn.Image")));
      this.italicBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.italicBtn.Name = "italicBtn";
      this.italicBtn.Size = new System.Drawing.Size(23, 22);
      this.italicBtn.Text = "K";
      this.italicBtn.Click += new System.EventHandler(this.italicBtn_Click);
      // 
      // underlineBtn
      // 
      this.underlineBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.underlineBtn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.underlineBtn.Image = ((System.Drawing.Image)(resources.GetObject("underlineBtn.Image")));
      this.underlineBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
      this.underlineBtn.Name = "underlineBtn";
      this.underlineBtn.Size = new System.Drawing.Size(23, 22);
      this.underlineBtn.Text = "U";
      this.underlineBtn.Click += new System.EventHandler(this.underlineBtn_Click);
      // 
      // buttonsPanel
      // 
      this.buttonsPanel.Controls.Add(this.panel1);
      this.buttonsPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.buttonsPanel.Location = new System.Drawing.Point(0, 235);
      this.buttonsPanel.Name = "buttonsPanel";
      this.buttonsPanel.Size = new System.Drawing.Size(635, 31);
      this.buttonsPanel.TabIndex = 2;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.abbrechenBtn);
      this.panel1.Controls.Add(this.okBtn);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
      this.panel1.Location = new System.Drawing.Point(463, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(172, 31);
      this.panel1.TabIndex = 2;
      // 
      // abbrechenBtn
      // 
      this.abbrechenBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.abbrechenBtn.Location = new System.Drawing.Point(94, 3);
      this.abbrechenBtn.Name = "abbrechenBtn";
      this.abbrechenBtn.Size = new System.Drawing.Size(75, 23);
      this.abbrechenBtn.TabIndex = 1;
      this.abbrechenBtn.Text = "Abbrechen";
      this.abbrechenBtn.UseVisualStyleBackColor = true;
      // 
      // okBtn
      // 
      this.okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.okBtn.Location = new System.Drawing.Point(13, 3);
      this.okBtn.Name = "okBtn";
      this.okBtn.Size = new System.Drawing.Size(75, 23);
      this.okBtn.TabIndex = 0;
      this.okBtn.Text = "OK";
      this.okBtn.UseVisualStyleBackColor = true;
      // 
      // rtfPanel
      // 
      this.rtfPanel.Controls.Add(this.richTextBox);
      this.rtfPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.rtfPanel.Location = new System.Drawing.Point(0, 25);
      this.rtfPanel.Name = "rtfPanel";
      this.rtfPanel.Size = new System.Drawing.Size(635, 210);
      this.rtfPanel.TabIndex = 1;
      // 
      // richTextBox
      // 
      this.richTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
      this.richTextBox.Location = new System.Drawing.Point(0, 0);
      this.richTextBox.Name = "richTextBox";
      this.richTextBox.Size = new System.Drawing.Size(635, 210);
      this.richTextBox.TabIndex = 0;
      this.richTextBox.Text = "";
      this.richTextBox.SelectionChanged += new System.EventHandler(this.richTextBox_SelectionChanged);
      // 
      // NeueBesonderheitForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(635, 266);
      this.Controls.Add(this.rtfPanel);
      this.Controls.Add(this.buttonsPanel);
      this.Controls.Add(this.toolStrip1);
      this.Name = "NeueBesonderheitForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Neue Besonderheit";
      this.toolStrip1.ResumeLayout(false);
      this.toolStrip1.PerformLayout();
      this.buttonsPanel.ResumeLayout(false);
      this.panel1.ResumeLayout(false);
      this.rtfPanel.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ToolStrip toolStrip1;
    private System.Windows.Forms.ToolStripButton boldBtn;
    private System.Windows.Forms.ToolStripButton italicBtn;
    private System.Windows.Forms.ToolStripButton underlineBtn;
    private System.Windows.Forms.Panel buttonsPanel;
    private System.Windows.Forms.Button abbrechenBtn;
    private System.Windows.Forms.Button okBtn;
    private System.Windows.Forms.Panel rtfPanel;
    private System.Windows.Forms.RichTextBox richTextBox;
    private System.Windows.Forms.Panel panel1;
  }
}