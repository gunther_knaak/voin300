﻿namespace VOIN10.PatientenSuchen
{
  partial class PatientenSuchenForm
  {
    /// <summary>
    /// Erforderliche Designervariable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Verwendete Ressourcen bereinigen.
    /// </summary>
    /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Vom Windows Form-Designer generierter Code

    /// <summary>
    /// Erforderliche Methode für die Designerunterstützung.
    /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.panel1 = new System.Windows.Forms.Panel();
      this.cancelBtn = new System.Windows.Forms.Button();
      this.okBtn = new System.Windows.Forms.Button();
      this.textBox1 = new System.Windows.Forms.TextBox();
      this.resultsDGV = new System.Windows.Forms.DataGridView();
      this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.vornameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.gebDatDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.strasseDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.plzDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.ortDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.mandantDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.iKDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.abrCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.tarifDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.versNrDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.statusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.rezDatDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.befreitDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
      this.arztNrDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.vKBisDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.unfallDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
      this.hilfsmittelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.erfasstDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.vorgangDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.rechNrKasseDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.rechNrMandantDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.imageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.rezArtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.pauschaleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.diagnoseDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.besonderheitDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.genehmigungKZDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.genehmigungDatumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.indikationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.patientenSuchenBS = new System.Windows.Forms.BindingSource(this.components);
      this.patientenSuchenDS = new VOIN10.PatientenSuchen.PatientenSuchenDataSet();
      this.searchSelectCommand = new System.Data.SqlClient.SqlCommand();
      this.sqlConnection = new System.Data.SqlClient.SqlConnection();
      this.searchDA = new System.Data.SqlClient.SqlDataAdapter();
      this.panel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.resultsDGV)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.patientenSuchenBS)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.patientenSuchenDS)).BeginInit();
      this.SuspendLayout();
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.cancelBtn);
      this.panel1.Controls.Add(this.okBtn);
      this.panel1.Controls.Add(this.textBox1);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(219, 398);
      this.panel1.TabIndex = 0;
      // 
      // cancelBtn
      // 
      this.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.cancelBtn.Location = new System.Drawing.Point(13, 68);
      this.cancelBtn.Name = "cancelBtn";
      this.cancelBtn.Size = new System.Drawing.Size(196, 23);
      this.cancelBtn.TabIndex = 3;
      this.cancelBtn.Text = "Abbrechen";
      this.cancelBtn.UseVisualStyleBackColor = true;
      // 
      // okBtn
      // 
      this.okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.okBtn.Location = new System.Drawing.Point(12, 38);
      this.okBtn.Name = "okBtn";
      this.okBtn.Size = new System.Drawing.Size(197, 23);
      this.okBtn.TabIndex = 2;
      this.okBtn.Text = "OK";
      this.okBtn.UseVisualStyleBackColor = true;
      // 
      // textBox1
      // 
      this.textBox1.Location = new System.Drawing.Point(12, 12);
      this.textBox1.Name = "textBox1";
      this.textBox1.Size = new System.Drawing.Size(197, 20);
      this.textBox1.TabIndex = 0;
      // 
      // resultsDGV
      // 
      this.resultsDGV.AllowUserToAddRows = false;
      this.resultsDGV.AllowUserToDeleteRows = false;
      this.resultsDGV.AutoGenerateColumns = false;
      this.resultsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.resultsDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn,
            this.vornameDataGridViewTextBoxColumn,
            this.gebDatDataGridViewTextBoxColumn,
            this.strasseDataGridViewTextBoxColumn,
            this.plzDataGridViewTextBoxColumn,
            this.ortDataGridViewTextBoxColumn,
            this.mandantDataGridViewTextBoxColumn,
            this.iKDataGridViewTextBoxColumn,
            this.abrCodeDataGridViewTextBoxColumn,
            this.tarifDataGridViewTextBoxColumn,
            this.versNrDataGridViewTextBoxColumn,
            this.statusDataGridViewTextBoxColumn,
            this.rezDatDataGridViewTextBoxColumn,
            this.befreitDataGridViewCheckBoxColumn,
            this.arztNrDataGridViewTextBoxColumn,
            this.vKBisDataGridViewTextBoxColumn,
            this.unfallDataGridViewCheckBoxColumn,
            this.hilfsmittelDataGridViewTextBoxColumn,
            this.erfasstDataGridViewTextBoxColumn,
            this.vorgangDataGridViewTextBoxColumn,
            this.rechNrKasseDataGridViewTextBoxColumn,
            this.rechNrMandantDataGridViewTextBoxColumn,
            this.imageDataGridViewTextBoxColumn,
            this.rezArtDataGridViewTextBoxColumn,
            this.pauschaleDataGridViewTextBoxColumn,
            this.diagnoseDataGridViewTextBoxColumn,
            this.besonderheitDataGridViewTextBoxColumn,
            this.genehmigungKZDataGridViewTextBoxColumn,
            this.genehmigungDatumDataGridViewTextBoxColumn,
            this.indikationDataGridViewTextBoxColumn});
      this.resultsDGV.DataSource = this.patientenSuchenBS;
      this.resultsDGV.Dock = System.Windows.Forms.DockStyle.Fill;
      this.resultsDGV.Location = new System.Drawing.Point(219, 0);
      this.resultsDGV.Name = "resultsDGV";
      this.resultsDGV.ReadOnly = true;
      this.resultsDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.resultsDGV.Size = new System.Drawing.Size(438, 398);
      this.resultsDGV.TabIndex = 1;
      this.resultsDGV.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.resultsDGV_MouseDoubleClick);
      // 
      // iDDataGridViewTextBoxColumn
      // 
      this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
      this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
      this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
      this.iDDataGridViewTextBoxColumn.ReadOnly = true;
      this.iDDataGridViewTextBoxColumn.Visible = false;
      // 
      // nameDataGridViewTextBoxColumn
      // 
      this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
      this.nameDataGridViewTextBoxColumn.HeaderText = "Name";
      this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
      this.nameDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // vornameDataGridViewTextBoxColumn
      // 
      this.vornameDataGridViewTextBoxColumn.DataPropertyName = "Vorname";
      this.vornameDataGridViewTextBoxColumn.HeaderText = "Vorname";
      this.vornameDataGridViewTextBoxColumn.Name = "vornameDataGridViewTextBoxColumn";
      this.vornameDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // gebDatDataGridViewTextBoxColumn
      // 
      this.gebDatDataGridViewTextBoxColumn.DataPropertyName = "GebDat";
      this.gebDatDataGridViewTextBoxColumn.HeaderText = "GebDat";
      this.gebDatDataGridViewTextBoxColumn.Name = "gebDatDataGridViewTextBoxColumn";
      this.gebDatDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // strasseDataGridViewTextBoxColumn
      // 
      this.strasseDataGridViewTextBoxColumn.DataPropertyName = "Strasse";
      this.strasseDataGridViewTextBoxColumn.HeaderText = "Strasse";
      this.strasseDataGridViewTextBoxColumn.Name = "strasseDataGridViewTextBoxColumn";
      this.strasseDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // plzDataGridViewTextBoxColumn
      // 
      this.plzDataGridViewTextBoxColumn.DataPropertyName = "Plz";
      this.plzDataGridViewTextBoxColumn.HeaderText = "Plz";
      this.plzDataGridViewTextBoxColumn.Name = "plzDataGridViewTextBoxColumn";
      this.plzDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // ortDataGridViewTextBoxColumn
      // 
      this.ortDataGridViewTextBoxColumn.DataPropertyName = "Ort";
      this.ortDataGridViewTextBoxColumn.HeaderText = "Ort";
      this.ortDataGridViewTextBoxColumn.Name = "ortDataGridViewTextBoxColumn";
      this.ortDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // mandantDataGridViewTextBoxColumn
      // 
      this.mandantDataGridViewTextBoxColumn.DataPropertyName = "Mandant";
      this.mandantDataGridViewTextBoxColumn.HeaderText = "Mandant";
      this.mandantDataGridViewTextBoxColumn.Name = "mandantDataGridViewTextBoxColumn";
      this.mandantDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // iKDataGridViewTextBoxColumn
      // 
      this.iKDataGridViewTextBoxColumn.DataPropertyName = "IK";
      this.iKDataGridViewTextBoxColumn.HeaderText = "IK";
      this.iKDataGridViewTextBoxColumn.Name = "iKDataGridViewTextBoxColumn";
      this.iKDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // abrCodeDataGridViewTextBoxColumn
      // 
      this.abrCodeDataGridViewTextBoxColumn.DataPropertyName = "AbrCode";
      this.abrCodeDataGridViewTextBoxColumn.HeaderText = "AbrCode";
      this.abrCodeDataGridViewTextBoxColumn.Name = "abrCodeDataGridViewTextBoxColumn";
      this.abrCodeDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // tarifDataGridViewTextBoxColumn
      // 
      this.tarifDataGridViewTextBoxColumn.DataPropertyName = "Tarif";
      this.tarifDataGridViewTextBoxColumn.HeaderText = "Tarif";
      this.tarifDataGridViewTextBoxColumn.Name = "tarifDataGridViewTextBoxColumn";
      this.tarifDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // versNrDataGridViewTextBoxColumn
      // 
      this.versNrDataGridViewTextBoxColumn.DataPropertyName = "VersNr";
      this.versNrDataGridViewTextBoxColumn.HeaderText = "VersNr";
      this.versNrDataGridViewTextBoxColumn.Name = "versNrDataGridViewTextBoxColumn";
      this.versNrDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // statusDataGridViewTextBoxColumn
      // 
      this.statusDataGridViewTextBoxColumn.DataPropertyName = "Status";
      this.statusDataGridViewTextBoxColumn.HeaderText = "Status";
      this.statusDataGridViewTextBoxColumn.Name = "statusDataGridViewTextBoxColumn";
      this.statusDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // rezDatDataGridViewTextBoxColumn
      // 
      this.rezDatDataGridViewTextBoxColumn.DataPropertyName = "RezDat";
      this.rezDatDataGridViewTextBoxColumn.HeaderText = "RezDat";
      this.rezDatDataGridViewTextBoxColumn.Name = "rezDatDataGridViewTextBoxColumn";
      this.rezDatDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // befreitDataGridViewCheckBoxColumn
      // 
      this.befreitDataGridViewCheckBoxColumn.DataPropertyName = "Befreit";
      this.befreitDataGridViewCheckBoxColumn.HeaderText = "Befreit";
      this.befreitDataGridViewCheckBoxColumn.Name = "befreitDataGridViewCheckBoxColumn";
      this.befreitDataGridViewCheckBoxColumn.ReadOnly = true;
      // 
      // arztNrDataGridViewTextBoxColumn
      // 
      this.arztNrDataGridViewTextBoxColumn.DataPropertyName = "ArztNr";
      this.arztNrDataGridViewTextBoxColumn.HeaderText = "ArztNr";
      this.arztNrDataGridViewTextBoxColumn.Name = "arztNrDataGridViewTextBoxColumn";
      this.arztNrDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // vKBisDataGridViewTextBoxColumn
      // 
      this.vKBisDataGridViewTextBoxColumn.DataPropertyName = "VK_Bis";
      this.vKBisDataGridViewTextBoxColumn.HeaderText = "VK_Bis";
      this.vKBisDataGridViewTextBoxColumn.Name = "vKBisDataGridViewTextBoxColumn";
      this.vKBisDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // unfallDataGridViewCheckBoxColumn
      // 
      this.unfallDataGridViewCheckBoxColumn.DataPropertyName = "Unfall";
      this.unfallDataGridViewCheckBoxColumn.HeaderText = "Unfall";
      this.unfallDataGridViewCheckBoxColumn.Name = "unfallDataGridViewCheckBoxColumn";
      this.unfallDataGridViewCheckBoxColumn.ReadOnly = true;
      // 
      // hilfsmittelDataGridViewTextBoxColumn
      // 
      this.hilfsmittelDataGridViewTextBoxColumn.DataPropertyName = "Hilfsmittel";
      this.hilfsmittelDataGridViewTextBoxColumn.HeaderText = "Hilfsmittel";
      this.hilfsmittelDataGridViewTextBoxColumn.Name = "hilfsmittelDataGridViewTextBoxColumn";
      this.hilfsmittelDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // erfasstDataGridViewTextBoxColumn
      // 
      this.erfasstDataGridViewTextBoxColumn.DataPropertyName = "Erfasst";
      this.erfasstDataGridViewTextBoxColumn.HeaderText = "Erfasst";
      this.erfasstDataGridViewTextBoxColumn.Name = "erfasstDataGridViewTextBoxColumn";
      this.erfasstDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // vorgangDataGridViewTextBoxColumn
      // 
      this.vorgangDataGridViewTextBoxColumn.DataPropertyName = "Vorgang";
      this.vorgangDataGridViewTextBoxColumn.HeaderText = "Vorgang";
      this.vorgangDataGridViewTextBoxColumn.Name = "vorgangDataGridViewTextBoxColumn";
      this.vorgangDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // rechNrKasseDataGridViewTextBoxColumn
      // 
      this.rechNrKasseDataGridViewTextBoxColumn.DataPropertyName = "RechNr_Kasse";
      this.rechNrKasseDataGridViewTextBoxColumn.HeaderText = "RechNr_Kasse";
      this.rechNrKasseDataGridViewTextBoxColumn.Name = "rechNrKasseDataGridViewTextBoxColumn";
      this.rechNrKasseDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // rechNrMandantDataGridViewTextBoxColumn
      // 
      this.rechNrMandantDataGridViewTextBoxColumn.DataPropertyName = "RechNr_Mandant";
      this.rechNrMandantDataGridViewTextBoxColumn.HeaderText = "RechNr_Mandant";
      this.rechNrMandantDataGridViewTextBoxColumn.Name = "rechNrMandantDataGridViewTextBoxColumn";
      this.rechNrMandantDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // imageDataGridViewTextBoxColumn
      // 
      this.imageDataGridViewTextBoxColumn.DataPropertyName = "Image";
      this.imageDataGridViewTextBoxColumn.HeaderText = "Image";
      this.imageDataGridViewTextBoxColumn.Name = "imageDataGridViewTextBoxColumn";
      this.imageDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // rezArtDataGridViewTextBoxColumn
      // 
      this.rezArtDataGridViewTextBoxColumn.DataPropertyName = "RezArt";
      this.rezArtDataGridViewTextBoxColumn.HeaderText = "RezArt";
      this.rezArtDataGridViewTextBoxColumn.Name = "rezArtDataGridViewTextBoxColumn";
      this.rezArtDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // pauschaleDataGridViewTextBoxColumn
      // 
      this.pauschaleDataGridViewTextBoxColumn.DataPropertyName = "Pauschale";
      this.pauschaleDataGridViewTextBoxColumn.HeaderText = "Pauschale";
      this.pauschaleDataGridViewTextBoxColumn.Name = "pauschaleDataGridViewTextBoxColumn";
      this.pauschaleDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // diagnoseDataGridViewTextBoxColumn
      // 
      this.diagnoseDataGridViewTextBoxColumn.DataPropertyName = "Diagnose";
      this.diagnoseDataGridViewTextBoxColumn.HeaderText = "Diagnose";
      this.diagnoseDataGridViewTextBoxColumn.Name = "diagnoseDataGridViewTextBoxColumn";
      this.diagnoseDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // besonderheitDataGridViewTextBoxColumn
      // 
      this.besonderheitDataGridViewTextBoxColumn.DataPropertyName = "Besonderheit";
      this.besonderheitDataGridViewTextBoxColumn.HeaderText = "Besonderheit";
      this.besonderheitDataGridViewTextBoxColumn.Name = "besonderheitDataGridViewTextBoxColumn";
      this.besonderheitDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // genehmigungKZDataGridViewTextBoxColumn
      // 
      this.genehmigungKZDataGridViewTextBoxColumn.DataPropertyName = "Genehmigung_KZ";
      this.genehmigungKZDataGridViewTextBoxColumn.HeaderText = "Genehmigung_KZ";
      this.genehmigungKZDataGridViewTextBoxColumn.Name = "genehmigungKZDataGridViewTextBoxColumn";
      this.genehmigungKZDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // genehmigungDatumDataGridViewTextBoxColumn
      // 
      this.genehmigungDatumDataGridViewTextBoxColumn.DataPropertyName = "Genehmigung_Datum";
      this.genehmigungDatumDataGridViewTextBoxColumn.HeaderText = "Genehmigung_Datum";
      this.genehmigungDatumDataGridViewTextBoxColumn.Name = "genehmigungDatumDataGridViewTextBoxColumn";
      this.genehmigungDatumDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // indikationDataGridViewTextBoxColumn
      // 
      this.indikationDataGridViewTextBoxColumn.DataPropertyName = "Indikation";
      this.indikationDataGridViewTextBoxColumn.HeaderText = "Indikation";
      this.indikationDataGridViewTextBoxColumn.Name = "indikationDataGridViewTextBoxColumn";
      this.indikationDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // patientenSuchenBS
      // 
      this.patientenSuchenBS.DataMember = "Rezepte";
      this.patientenSuchenBS.DataSource = this.patientenSuchenDS;
      // 
      // patientenSuchenDS
      // 
      this.patientenSuchenDS.DataSetName = "PatientenSuchenDataSet";
      this.patientenSuchenDS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
      // 
      // searchSelectCommand
      // 
      this.searchSelectCommand.CommandText = "SELECT     [2007_Rezepte].*\r\nFROM         [2007_Rezepte]";
      this.searchSelectCommand.Connection = this.sqlConnection;
      // 
      // sqlConnection
      // 
      this.sqlConnection.ConnectionString = "Data Source=.\\SQLEXPRESS;AttachDbFilename=C:\\660310038.mdf;Integrated Security=Tr" +
          "ue;Connect Timeout=30;User Instance=True";
      this.sqlConnection.FireInfoMessageEventOnUserErrors = false;
      // 
      // searchDA
      // 
      this.searchDA.SelectCommand = this.searchSelectCommand;
      this.searchDA.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "2007_Rezepte", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID", "ID"),
                        new System.Data.Common.DataColumnMapping("Mandant", "Mandant"),
                        new System.Data.Common.DataColumnMapping("IK", "IK"),
                        new System.Data.Common.DataColumnMapping("AbrCode", "AbrCode"),
                        new System.Data.Common.DataColumnMapping("Tarif", "Tarif"),
                        new System.Data.Common.DataColumnMapping("Name", "Name"),
                        new System.Data.Common.DataColumnMapping("Vorname", "Vorname"),
                        new System.Data.Common.DataColumnMapping("GebDat", "GebDat"),
                        new System.Data.Common.DataColumnMapping("Strasse", "Strasse"),
                        new System.Data.Common.DataColumnMapping("Plz", "Plz"),
                        new System.Data.Common.DataColumnMapping("Ort", "Ort"),
                        new System.Data.Common.DataColumnMapping("VersNr", "VersNr"),
                        new System.Data.Common.DataColumnMapping("Status", "Status"),
                        new System.Data.Common.DataColumnMapping("RezDat", "RezDat"),
                        new System.Data.Common.DataColumnMapping("Befreit", "Befreit"),
                        new System.Data.Common.DataColumnMapping("ArztNr", "ArztNr"),
                        new System.Data.Common.DataColumnMapping("VK_Bis", "VK_Bis"),
                        new System.Data.Common.DataColumnMapping("Unfall", "Unfall"),
                        new System.Data.Common.DataColumnMapping("Hilfsmittel", "Hilfsmittel"),
                        new System.Data.Common.DataColumnMapping("Erfasst", "Erfasst"),
                        new System.Data.Common.DataColumnMapping("Vorgang", "Vorgang"),
                        new System.Data.Common.DataColumnMapping("RechNr_Kasse", "RechNr_Kasse"),
                        new System.Data.Common.DataColumnMapping("RechNr_Mandant", "RechNr_Mandant"),
                        new System.Data.Common.DataColumnMapping("Image", "Image"),
                        new System.Data.Common.DataColumnMapping("RezArt", "RezArt"),
                        new System.Data.Common.DataColumnMapping("Pauschale", "Pauschale"),
                        new System.Data.Common.DataColumnMapping("Diagnose", "Diagnose"),
                        new System.Data.Common.DataColumnMapping("Besonderheit", "Besonderheit"),
                        new System.Data.Common.DataColumnMapping("Genehmigung_KZ", "Genehmigung_KZ"),
                        new System.Data.Common.DataColumnMapping("Genehmigung_Datum", "Genehmigung_Datum"),
                        new System.Data.Common.DataColumnMapping("Indikation", "Indikation")})});
      // 
      // PatientenSuchenForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.cancelBtn;
      this.ClientSize = new System.Drawing.Size(657, 398);
      this.Controls.Add(this.resultsDGV);
      this.Controls.Add(this.panel1);
      this.KeyPreview = true;
      this.Name = "PatientenSuchenForm";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.Text = "Patienten suchen";
      this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
      this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PatientenSuchenForm_KeyDown);
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.resultsDGV)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.patientenSuchenBS)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.patientenSuchenDS)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Button cancelBtn;
    private System.Windows.Forms.Button okBtn;
    private System.Windows.Forms.TextBox textBox1;
    private System.Windows.Forms.DataGridView resultsDGV;
    private System.Data.SqlClient.SqlCommand searchSelectCommand;
    private System.Data.SqlClient.SqlConnection sqlConnection;
    private System.Data.SqlClient.SqlDataAdapter searchDA;
    private System.Windows.Forms.BindingSource patientenSuchenBS;
    private PatientenSuchenDataSet patientenSuchenDS;
    private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn vornameDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn gebDatDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn strasseDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn plzDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn ortDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn mandantDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn iKDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn abrCodeDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn tarifDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn versNrDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn rezDatDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewCheckBoxColumn befreitDataGridViewCheckBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn arztNrDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn vKBisDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewCheckBoxColumn unfallDataGridViewCheckBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn hilfsmittelDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn erfasstDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn vorgangDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn rechNrKasseDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn rechNrMandantDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn imageDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn rezArtDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn pauschaleDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn diagnoseDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn besonderheitDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn genehmigungKZDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn genehmigungDatumDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn indikationDataGridViewTextBoxColumn;
  }
}