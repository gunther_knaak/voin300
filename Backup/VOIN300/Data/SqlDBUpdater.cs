using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace SwS.Data
{
  public class SqlDBUpdater
  {
    private static string caption = "Aktualisierung";
    private static int recordCount = 0;

    public static void Update(DataRow[] rows, SqlConnection con,
      SqlTransaction tran, SqlDataAdapter da, string tabelName, string idName)
    {
      SqlCommand delCmd = da.DeleteCommand.Clone();
      SqlCommand insCmd = da.InsertCommand.Clone();
      SqlCommand updCmd = da.UpdateCommand.Clone();
      SqlCommand cmd = null;
      insCmd.Connection =
        updCmd.Connection =
        delCmd.Connection = con;
      insCmd.Transaction =
        updCmd.Transaction =
        delCmd.Transaction = tran;
      delCmd.StatementCompleted += new StatementCompletedEventHandler(cmd_StatementCompleted);
      insCmd.StatementCompleted += new StatementCompletedEventHandler(cmd_StatementCompleted);
      updCmd.StatementCompleted += new StatementCompletedEventHandler(cmd_StatementCompleted);
      for (int i = 0; i < rows.Length; i++)
      {
        DataRow row = rows[i];
        bool updated = false;
        do
        {
          switch (row.RowState)
          {
            case DataRowState.Added:
              cmd = insCmd;
              break;
            case DataRowState.Deleted:
              cmd = delCmd;
              break;
            case DataRowState.Modified:
              cmd = updCmd;
              break;
            default:
              cmd = null;
              break;
          }
          if (cmd != null)//wenn Datenzatz ge�ndert
          {
            FillCommandParameters(cmd, row);
            //Added
            if (row.RowState == DataRowState.Added)
            {
              SqlDataReader reader = null;
              try
              {
                recordCount = 0;
                reader = cmd.ExecuteReader();
                if (recordCount > 0)
                {
                  if (reader.Read())
                    UpdateAutoValues(reader, row);
                  else
                    throw new DBConcurrencyException();
                  updated = true;
                }
                else
                  throw new DBConcurrencyException();
              }
              finally
              {
                if (reader != null)
                  reader.Dispose();
              }
            }
            //Modified
            else if (row.RowState == DataRowState.Modified)
            {
              SqlDataReader reader = null;
              try
              {
                recordCount = 0;
                reader = cmd.ExecuteReader();
                if (recordCount > 0)
                {
                  if (reader.Read())
                    UpdateAutoValues(reader, row);
                  else
                    throw new DBConcurrencyException();
                  updated = true;
                }
                else
                {
                  if (reader.Read()) // ge�ndert
                    updated = CheckMRowUpdatedError(reader, row);
                  else
                    updated = CheckMRowDeletedError(row);
                }
              }
              finally
              {
                if (reader != null)
                  reader.Dispose();
              }
            }
            //Deleted
            else if (row.RowState == DataRowState.Deleted)
            {
              recordCount = 0;
              cmd.ExecuteNonQuery();
              if (recordCount > 0)
                updated = true;
              else
              {
                updated = CheckDRowError(row, con, tran, tabelName, idName);
              }
            }
          }
          else
            updated = true;
        } while (!updated);
      }
    }

    private static bool CheckDRowError(DataRow row, SqlConnection con, SqlTransaction tran,
      string tableName, string idName)
    {
      bool res = false;
      SqlCommand selCmd = new SqlCommand("select * from " + tableName + " where " +
        idName + " = @Original_" + idName, con);
      SqlDataReader reader = null;
      selCmd.Parameters.Add("@Original_" + idName, SqlDbType.Int);
      selCmd.Parameters["@Original_" + idName].Value = row[idName, DataRowVersion.Original];
      selCmd.Transaction = tran;
      try
      {
        reader = selCmd.ExecuteReader();
        if (reader.Read())
        {
          bool del = false;
          string[] names = new string[row.Table.Columns.Count];
          object[] values = new object[row.Table.Columns.Count];
          if (MessageBox.Show("Der Datensatz wurde ge�ndert.\n" + " Soll der " +
            "Datensatz gel�scht werden?\n" + GetValuesString(row) +
            "\nin der Datenbank\n" + GetValuesString(reader), caption,
            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
          {
            del = true;
          }
          for (int i = 0; i < names.Length; i++)
          {
            int idx = GetOrdinal(row.Table.Columns[i].ColumnName, reader);
            names[i] = row.Table.Columns[i].ColumnName;
            if (idx >= 0)
              values[i] = reader.GetValue(idx);
            else
              values[i] = row[i, DataRowVersion.Original];
          }
          if (del)
          {
            row.RejectChanges();
            for (int i = 0; i < names.Length; i++)
            {
              SetColumnValue(row, names[i], values[i]);
            }
            row.AcceptChanges();
            row.Delete();
            res = false;
          }
          else
          {
            row.RejectChanges();
            for (int i = 0; i < names.Length; i++)
            {
              SetColumnValue(row, names[i], values[i]);
            }
            row.AcceptChanges();
            res = true;
          }

        }
        else
          res = true;
      }
      finally
      {
        if(reader != null)
          reader.Dispose();
      }
      return res;
    }

    private static bool CheckMRowDeletedError(DataRow row)
    {
      if (MessageBox.Show("Datenzatz wurde gel�scht.\n" +
        "Soll der Datensatz gespeichert werden?\n" + GetValuesString(row),
        caption, MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
      {
        row.AcceptChanges();
        row.SetAdded();
        return false;
      }
      else
      {
        row.Delete();
        return true;
      }
    }

    private static bool CheckMRowUpdatedError(SqlDataReader reader, DataRow row)
    {
      if (MessageBox.Show("Der Datensatz wurde ge�ndert.\n" + " Soll der " +
        "Datensatz gespeichert werden?\n" + GetValuesString(row) +
        "\nin der Datenbank\n" + GetValuesString(reader), caption,
        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
      {
        bool db = true;
        string[] names = new string[row.ItemArray.Length];
        object[] oValues = new object[row.ItemArray.Length];
        object[] cValues = new object[row.ItemArray.Length];
        //TODO: Meldung
        if (MessageBox.Show("Datenbank", caption, MessageBoxButtons.YesNo,
          MessageBoxIcon.Question) == DialogResult.Yes)
        {
          db = true;
        }
        else
        {
          db = false;
        }
        for (int i = 0; i < row.Table.Columns.Count; i++)
        {
          int idx = GetOrdinal(row.Table.Columns[i].ColumnName, reader);
          names[i] = row.Table.Columns[i].ColumnName;
          oValues[i] = row[i, DataRowVersion.Original];
          cValues[i] = row[i, DataRowVersion.Current];
          if (idx >= 0)
          {
            object val = reader.GetValue(idx);
            if (!oValues[i].Equals(val))
            {
              if (cValues[i].Equals(oValues[i]))
              {
                cValues[i] = oValues[i] = val;
              }
              else
              {
                if (db)
                  cValues[i] = oValues[i] = val;
                else
                  oValues[i] = val;
              }
            }
          }
        }
        SetRowValues(row, oValues, cValues, names);
        return false;
      }
      else
      {
        for (int i = 0; i < row.ItemArray.Length; i++)
        {
          int idx = GetOrdinal(row.Table.Columns[i].ColumnName, reader);
          if (idx >= 0)
            SetColumnValue(row, row.Table.Columns[i].ColumnName, reader.GetValue(idx));
        }
        row.AcceptChanges();
        return true;
      }
    }

    private static void SetRowValues(DataRow row, object[] oValues, object[] cValues, string[] names)
    {
      for (int i = 0; i < names.Length; i++)
        SetColumnValue(row, names[i], oValues[i]);
      row.AcceptChanges();
      for (int i = 0; i < names.Length; i++)
        SetColumnValue(row, names[i], cValues[i]);
    }

    private static void SetColumnValue(DataRow row, string name, object value)
    {
      bool ro = row.Table.Columns[name].ReadOnly;
      try
      {
        row.Table.Columns[name].ReadOnly = false;
        row[name] = value;
      }
      finally
      {
        row.Table.Columns[name].ReadOnly = ro;
      }
    }

    private static int GetOrdinal(string cName, SqlDataReader reader)
    {
      try
      {
        return reader.GetOrdinal(cName);
      }
      catch { }
      return -1;
    }

    static void cmd_StatementCompleted(object sender, StatementCompletedEventArgs e)
    {
      recordCount = e.RecordCount;
    }

    private static string GetValuesString(SqlDataReader reader)
    {
      string res = string.Empty;
      for (int i = 0; i < reader.FieldCount; i++)
      {
        res += reader.GetName(i) + ": " + reader.GetValue(i).ToString() +
          (i == reader.FieldCount - 1 ? string.Empty : ", ");
      }
      return res;
    }

    private static string GetValuesString(DataRow row)
    {
      string res = string.Empty;
      foreach (DataColumn c in row.Table.Columns)
        res += (res != string.Empty ? ", " : string.Empty) +
          c.ColumnName + ": " + GetValue(row, c.ColumnName).ToString();
      return res;
    }

    private static object GetValue(DataRow row, string columnName)
    {
      return row.RowState == DataRowState.Deleted ? row[columnName, DataRowVersion.Original] : row[columnName];
    }

    private static void UpdateAutoValues(SqlDataReader reader, DataRow row)
    {
      for (int i = 0; i < reader.FieldCount; i++)
      {
        if (!row.Table.Columns[reader.GetName(i)].ReadOnly)
          row[reader.GetName(i)] = reader.GetValue(i);
        else
        {
          try
          {
            row.Table.Columns[reader.GetName(i)].ReadOnly = false;
            row[reader.GetName(i)] = reader.GetValue(i);
          }
          finally
          {
            row.Table.Columns[reader.GetName(i)].ReadOnly = true;
          }
        }
      }
    }

    public static void FillCommandParameters(SqlCommand cmd, DataRow row)
    {
      foreach (SqlParameter param in cmd.Parameters)
      {
        if (param.SourceColumn.Trim() != string.Empty)
        {
          if (param.SourceColumnNullMapping)
          {
            if (row[param.SourceColumn, param.SourceVersion] == DBNull.Value)
              param.Value = 1;
            else
              param.Value = 0;
          }
          else
          {
            param.Value = row[param.SourceColumn, param.SourceVersion];
          }
        }
      }
    }

  }
}
