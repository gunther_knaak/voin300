﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace VOIN10
{
  public partial class KostentraegerSuchenForm : Form
  {
    private VOINDataSet dataSet;

    public VOINDataSet DataSet
    {
      get
      {
        return dataSet;
      }
      set
      {
        dataSet = value;
      }
    }

    public string KTName
    {
      get
      {
        string res = string.Empty;
        if (dataGridView1.DataSource != null)
        {
          BindingManagerBase bm = this.BindingContext[dataGridView1.DataSource];
          if (bm.Count > 0 && bm.Current != null)
          {
            DataRowView drv = (DataRowView)bm.Current;
            DataRow row = drv.Row;
            res = row["Name1"].ToString() + " " + row["Name2"].ToString() + " " + row["Name3"].ToString() + " " + row["Name4"].ToString();
          }
        }
        return res;
        //                return ikLabel.Text;
      }
    }

    public string IK
    {
      get
      {
        string res = string.Empty;
        if (dataGridView1.DataSource != null)
        {
          BindingManagerBase bm = this.BindingContext[dataGridView1.DataSource];
          if (bm.Count > 0 && bm.Current != null)
          {
            DataRowView drv = (DataRowView)bm.Current;
            DataRow row = drv.Row;
            res = row["IK"].ToString();
          }
        }
        return res;
      }
    }

    public VOINDataSet.Kostentraeger_300Row Row
    {
      get
      {
        VOINDataSet.Kostentraeger_300Row res = null;
        if (dataGridView1.DataSource != null)
        {
          BindingManagerBase bm = this.BindingContext[dataGridView1.DataSource];
          if (bm.Count > 0 && bm.Current != null)
          {
            DataRowView drv = (DataRowView)bm.Current;
            res = (VOINDataSet.Kostentraeger_300Row)drv.Row;
          }
        }
        return res;
      }
    }

    public KostentraegerSuchenForm()
    {
      InitializeComponent();
    }

    private void Suchen()
    {
      DataView view = null;
      ikTB.Text = ikTB.Text.Trim();
      view = new DataView(DataSet.Kostentraeger_300, "Name1 LIKE '*" + ikTB.Text +
        "*' OR Name2 LIKE '*" + ikTB.Text + "*' OR Name3 LIKE '*" + ikTB.Text +
        "*' OR Name4 LIKE '*" + ikTB.Text + "*' OR Ans_Ort LIKE '*" + ikTB.Text +
        "*' OR Kurzbezeichnung LIKE '*" + ikTB.Text + "*' OR IK LIKE '*" +
        ikTB.Text + "*'", "Name1, Name2, Name3, Name4", DataViewRowState.CurrentRows);

      dataGridView1.DataSource = view;
      foreach (DataGridViewColumn c in dataGridView1.Columns)
      {
        if (c.DataPropertyName != "IK" && c.DataPropertyName != "Name1" &&
          c.DataPropertyName != "Name2" && c.DataPropertyName != "Name3" &&
          c.DataPropertyName != "Name4" && c.DataPropertyName != "Kurzbezeichnung" &&
          c.DataPropertyName != "Ans_Ort")
          c.Visible = false;
        else
          switch (c.DataPropertyName)
          {
            case "IK":
              c.DisplayIndex = 0;
              break;
            case "Name1":
              c.DisplayIndex = 1;
              break;
            case "Name2":
              c.DisplayIndex = 2;
              break;
            case "Name3":
              c.DisplayIndex = 3;
              break;
            case "Name4":
              c.DisplayIndex = 4;
              break;
            case "Kurzbezeichnung":
              c.DisplayIndex = 5;
              break;
            case "Ans_Ort":
              c.DisplayIndex = 6;
              break;
          }

      }
      dataGridView1.Focus();
    }

    private void KostentraegerSuchenForm_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter && dataGridView1.ContainsFocus)
      {
        this.DialogResult = DialogResult.OK;
        e.Handled = true;
      }
    }

    private void dataGridView1_MouseDoubleClick(object sender, MouseEventArgs e)
    {
      DataGridView.HitTestInfo info = dataGridView1.HitTest(e.X, e.Y);
      if (info.Type == DataGridViewHitTestType.Cell ||
        info.Type == DataGridViewHitTestType.RowHeader)
        this.DialogResult = DialogResult.OK;
    }

    private void ikTB_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter)
      {
        e.Handled = true;
        Suchen();
      }
    }
  }
}