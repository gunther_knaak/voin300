﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace VOIN10
{
  class RezeptzArt
  {

    private static ArrayList rezeptArtList = new ArrayList();

    public static ArrayList RezeptArtList
    {
      get
      {
        return rezeptArtList;
      }
    }

    static RezeptzArt()
    {
      rezeptArtList.Add(new RezeptzArt(1, "1-Erstverordnung (Regelfall)"));
      rezeptArtList.Add(new RezeptzArt(2, "2-Folgeferordnung (Regelfall)"));
      rezeptArtList.Add(new RezeptzArt(10, "10-Verordnung außerhalb des Regelfalles (Folgeverordnung, auch längerfristige Verordnung)"));
    }

    private RezeptzArt(short rezeptAtr, string rezeptArtName)
    {
      this.rezeptArt = rezeptAtr;
      this.rezeptArtName = rezeptArtName;
    }

    private short rezeptArt;

    public short RezeptArt
    {
      get
      {
        return rezeptArt;
      }
    }

    private string rezeptArtName;

    public string RezeptArtName
    {
      get
      {
        return rezeptArtName;
      }
    }
  }
}
