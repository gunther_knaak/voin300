using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;

namespace VOIN10
{
  class Utils
  {
    [DllImport("Kernel32.dll", CharSet = CharSet.Ansi)]
    public static extern uint GetPrivateProfileString(string appName, string keyName,
      string defaultValue, StringBuilder returnedString, uint size, string fileName);

    [DllImport("Kernel32.dll", CharSet = CharSet.Ansi)]
    public static extern bool WritePrivateProfileString(string appName, string keyName,
      string value, string fileName);

    [DllImport("Kernel32.dll", CharSet = CharSet.Ansi)]
    public static extern uint GetPrivateProfileInt(string appName, string keyName, 
      int defaultValue, string fileName);
  }
}
