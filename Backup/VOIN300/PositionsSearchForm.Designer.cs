﻿namespace VOIN10
{
  partial class PositionsSearchForm
  {
    /// <summary>
    /// Erforderliche Designervariable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Verwendete Ressourcen bereinigen.
    /// </summary>
    /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Vom Windows Form-Designer generierter Code

    /// <summary>
    /// Erforderliche Methode für die Designerunterstützung.
    /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
    /// </summary>
    private void InitializeComponent()
    {
      this.panel1 = new System.Windows.Forms.Panel();
      this.cancelBtn = new System.Windows.Forms.Button();
      this.dataGridView1 = new System.Windows.Forms.DataGridView();
      this.HMPColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.BezeichnungColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.ArtikelNrColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.EPreisColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.TarifKZColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.panel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
      this.SuspendLayout();
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.cancelBtn);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel1.Location = new System.Drawing.Point(0, 236);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(640, 30);
      this.panel1.TabIndex = 1;
      // 
      // cancelBtn
      // 
      this.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.cancelBtn.Location = new System.Drawing.Point(562, 4);
      this.cancelBtn.Name = "cancelBtn";
      this.cancelBtn.Size = new System.Drawing.Size(75, 23);
      this.cancelBtn.TabIndex = 0;
      this.cancelBtn.Text = "Abbrechen";
      this.cancelBtn.UseVisualStyleBackColor = true;
      // 
      // dataGridView1
      // 
      this.dataGridView1.AllowUserToAddRows = false;
      this.dataGridView1.AllowUserToDeleteRows = false;
      this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
      this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.HMPColumn,
            this.BezeichnungColumn,
            this.ArtikelNrColumn,
            this.EPreisColumn,
            this.TarifKZColumn});
      this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dataGridView1.Location = new System.Drawing.Point(0, 0);
      this.dataGridView1.MultiSelect = false;
      this.dataGridView1.Name = "dataGridView1";
      this.dataGridView1.ReadOnly = true;
      this.dataGridView1.RowHeadersVisible = false;
      this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dataGridView1.Size = new System.Drawing.Size(640, 236);
      this.dataGridView1.TabIndex = 0;
      this.dataGridView1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseDoubleClick);
      // 
      // HMPColumn
      // 
      this.HMPColumn.DataPropertyName = "HMP";
      this.HMPColumn.HeaderText = "HMP";
      this.HMPColumn.Name = "HMPColumn";
      this.HMPColumn.ReadOnly = true;
      // 
      // BezeichnungColumn
      // 
      this.BezeichnungColumn.DataPropertyName = "Bezeichnung";
      this.BezeichnungColumn.HeaderText = "Bezeichnung";
      this.BezeichnungColumn.Name = "BezeichnungColumn";
      this.BezeichnungColumn.ReadOnly = true;
      // 
      // ArtikelNrColumn
      // 
      this.ArtikelNrColumn.DataPropertyName = "ArtikelNr";
      this.ArtikelNrColumn.HeaderText = "Art. Nr.";
      this.ArtikelNrColumn.Name = "ArtikelNrColumn";
      this.ArtikelNrColumn.ReadOnly = true;
      // 
      // EPreisColumn
      // 
      this.EPreisColumn.DataPropertyName = "EPreis";
      this.EPreisColumn.HeaderText = "EPreis";
      this.EPreisColumn.Name = "EPreisColumn";
      this.EPreisColumn.ReadOnly = true;
      // 
      // TarifKZColumn
      // 
      this.TarifKZColumn.DataPropertyName = "TarifKZ";
      this.TarifKZColumn.HeaderText = "TarifKZ";
      this.TarifKZColumn.Name = "TarifKZColumn";
      this.TarifKZColumn.ReadOnly = true;
      // 
      // PositionsSearchForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.cancelBtn;
      this.ClientSize = new System.Drawing.Size(640, 266);
      this.Controls.Add(this.dataGridView1);
      this.Controls.Add(this.panel1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.KeyPreview = true;
      this.Name = "PositionsSearchForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Positionsauswahl";
      this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PositionsSearchForm_KeyDown);
      this.panel1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Button cancelBtn;
    private System.Windows.Forms.DataGridView dataGridView1;
    private System.Windows.Forms.DataGridViewTextBoxColumn HMPColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn BezeichnungColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn ArtikelNrColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn EPreisColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn TarifKZColumn;

  }
}