﻿namespace VOIN10
{
    partial class MainForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlCommand = new System.Windows.Forms.Panel();
            this.printVOBtn = new System.Windows.Forms.Button();
            this.kvBtn = new System.Windows.Forms.Button();
            this.protokollBtn = new System.Windows.Forms.Button();
            this.pnlHilfe = new System.Windows.Forms.Panel();
            this.BesonderheitenBtn = new System.Windows.Forms.Button();
            this.abrechnenButton = new System.Windows.Forms.Button();
            this.mainMenu = new System.Windows.Forms.MainMenu(this.components);
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.protokollMI = new System.Windows.Forms.MenuItem();
            this.kostenvoranschlagMI = new System.Windows.Forms.MenuItem();
            this.verordnungMI = new System.Windows.Forms.MenuItem();
            this.iniMI = new System.Windows.Forms.MenuItem();
            this.posVerwaltungMI = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.jahr1MenuItem = new System.Windows.Forms.MenuItem();
            this.jahr2MenuItem = new System.Windows.Forms.MenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.namesDGV = new System.Windows.Forms.DataGridView();
            this.NameVorname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sortOrderPanel = new System.Windows.Forms.Panel();
            this.sortCB = new System.Windows.Forms.ComboBox();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.pnlMainRight = new System.Windows.Forms.Panel();
            this.picImage = new System.Windows.Forms.PictureBox();
            this.pnlMainLeft = new System.Windows.Forms.Panel();
            this.rezepteBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.rezepteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.neuRechTSBtn = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.copyTSBtn = new System.Windows.Forms.ToolStripButton();
            this.positionenPanel = new System.Windows.Forms.Panel();
            this.label39 = new System.Windows.Forms.Label();
            this.IdentnrCB = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.posTKZTB = new System.Windows.Forms.TextBox();
            this.eigenanteilCB = new System.Windows.Forms.CheckBox();
            this.anzahlEigenanteilLabel = new System.Windows.Forms.Label();
            this.inventarNummerTB = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.verbrauchLabel = new System.Windows.Forms.Label();
            this.ePreisTB = new System.Windows.Forms.TextBox();
            this.kilometerTB = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.begruendungTB = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.gesamteZuzahlungLabel = new System.Windows.Forms.Label();
            this.gesamteSummeLabel = new System.Windows.Forms.Label();
            this.gesamteForderungLabel = new System.Windows.Forms.Label();
            this.positionDeleteBtn = new System.Windows.Forms.Button();
            this.neuePositionBtn = new System.Windows.Forms.Button();
            this.zuzahlungsArtLabel = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.produktLabel = new System.Windows.Forms.Label();
            this.artLabel = new System.Windows.Forms.Label();
            this.unterGruppeLabel = new System.Windows.Forms.Label();
            this.ortLabel = new System.Windows.Forms.Label();
            this.gruppeLabel = new System.Windows.Forms.Label();
            this.hilfsMittelKennZeichenCB = new System.Windows.Forms.ComboBox();
            this.label35 = new System.Windows.Forms.Label();
            this.verbrauchBtn = new System.Windows.Forms.Button();
            this.forderungLabel = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.zuzahlungTB = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.eigenAnteilTB = new System.Windows.Forms.TextBox();
            this.mwstCB = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.nettoCB = new System.Windows.Forms.CheckBox();
            this.mwstTB = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.rechnerBtn = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.produktMTB = new System.Windows.Forms.MaskedTextBox();
            this.artMTB = new System.Windows.Forms.MaskedTextBox();
            this.unterGruppeMTB = new System.Windows.Forms.MaskedTextBox();
            this.ortMTB = new System.Windows.Forms.MaskedTextBox();
            this.gruppeMTB = new System.Windows.Forms.MaskedTextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.anzahlNUD = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this.jahrCB = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.tagMonatMTB = new System.Windows.Forms.MaskedTextBox();
            this.positionenDGV = new System.Windows.Forms.DataGridView();
            this.datumColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buPosColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.anzahlColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ePreisColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forderungColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label20 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dauerTB = new System.Windows.Forms.TextBox();
            this.dauerCB = new System.Windows.Forms.CheckBox();
            this.label54 = new System.Windows.Forms.Label();
            this.zeitBisMTB = new System.Windows.Forms.MaskedTextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.zeitVonMTB = new System.Windows.Forms.MaskedTextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.posBezeichnungBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.zeitraumCB = new System.Windows.Forms.CheckBox();
            this.versorgungVonMTB = new System.Windows.Forms.MaskedTextBox();
            this.versorgungBisMTB = new System.Windows.Forms.MaskedTextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.zeitraumTB = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.anzahlZuzahlungLabel = new System.Windows.Forms.Label();
            this.pnlRezept = new System.Windows.Forms.Panel();
            this.pnlRezeptImage = new System.Windows.Forms.Panel();
            this.picImage2 = new System.Windows.Forms.PictureBox();
            this.pnlRezeptEingabe = new System.Windows.Forms.Panel();
            this.btnArtznrSuchen = new System.Windows.Forms.Button();
            this.label43 = new System.Windows.Forms.Label();
            this.lstImages = new System.Windows.Forms.ListBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.rechTB = new System.Windows.Forms.TextBox();
            this.imgTB = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.vkBisTB = new System.Windows.Forms.MaskedTextBox();
            this.patientenSuchenBtn = new System.Windows.Forms.Button();
            this.arztNummerTB = new System.Windows.Forms.MaskedTextBox();
            this.rezeptDatumMTB = new System.Windows.Forms.MaskedTextBox();
            this.geburtsDatumMTB = new System.Windows.Forms.MaskedTextBox();
            this.genVonMTB = new System.Windows.Forms.MaskedTextBox();
            this.plzMTB = new System.Windows.Forms.MaskedTextBox();
            this.versichertenNummerMTB = new System.Windows.Forms.MaskedTextBox();
            this.befreitCheckBox = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.genKennzTB = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.status2TB = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.status1TB = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.rezeptArtCB = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.indikationTB = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.ortTB = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.strasseTB = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.vornameTB = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.nameTB = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlKostenTaeger = new System.Windows.Forms.Panel();
            this.pnlRechnungsOptionenPlaceholderRight = new System.Windows.Forms.Panel();
            this.nameLabel = new System.Windows.Forms.Label();
            this.pnlRechNrOptionen = new System.Windows.Forms.Panel();
            this.einzelnCB = new System.Windows.Forms.CheckBox();
            this.rechNrTB = new System.Windows.Forms.TextBox();
            this.imageTB = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.mitRechNrCB = new System.Windows.Forms.CheckBox();
            this.pnlSuchen = new System.Windows.Forms.Panel();
            this.searchBtn = new System.Windows.Forms.Button();
            this.ikTB = new System.Windows.Forms.TextBox();
            this.topPanel = new System.Windows.Forms.Panel();
            this.imgCB = new System.Windows.Forms.CheckBox();
            this.axListLabel1 = new AxListLabel.AxListLabel();
            this.tarifSchlusselAbrechnCodeTB = new System.Windows.Forms.TextBox();
            this.erfasstTB = new System.Windows.Forms.MaskedTextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.name2Label = new System.Windows.Forms.Label();
            this.name1Label = new System.Windows.Forms.Label();
            this.nrComboBox = new System.Windows.Forms.ComboBox();
            this.mandantenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.voinDataSet = new VOIN10.VOINDataSet();
            this.positionenBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mandantenSelectCommand = new System.Data.SqlClient.SqlCommand();
            this.stamm4SqlConnection = new System.Data.SqlClient.SqlConnection();
            this.mandantenDA = new System.Data.SqlClient.SqlDataAdapter();
            this.mandantenDeleteCommand = new System.Data.SqlClient.SqlCommand();
            this.mandantenUpdateCommand = new System.Data.SqlClient.SqlCommand();
            this.kostentraegerSelectCommand = new System.Data.SqlClient.SqlCommand();
            this.kostentraegerDA = new System.Data.SqlClient.SqlDataAdapter();
            this.lastIKsqlConnection = new System.Data.SqlClient.SqlConnection();
            this.kostentraeder_TarifeSelectCommand = new System.Data.SqlClient.SqlCommand();
            this.kostentraeder_TarifeDA = new System.Data.SqlClient.SqlDataAdapter();
            this.tab_PositionenDA = new System.Data.SqlClient.SqlDataAdapter();
            this.tab_PositionenSelectCommand = new System.Data.SqlClient.SqlCommand();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.mandantenBesSelectCommand = new System.Data.SqlClient.SqlCommand();
            this.mandantenBesSqlConnection = new System.Data.SqlClient.SqlConnection();
            this.mandantenBesInsertCommand = new System.Data.SqlClient.SqlCommand();
            this.mandantenBesUpdateCommand = new System.Data.SqlClient.SqlCommand();
            this.mandantenBesDeleteCommand = new System.Data.SqlClient.SqlCommand();
            this.mandantenBesDA = new System.Data.SqlClient.SqlDataAdapter();
            this.gruppeSqlSelectCommand = new System.Data.SqlClient.SqlCommand();
            this.gruppeDA = new System.Data.SqlClient.SqlDataAdapter();
            this.ortSqlSelectCommand = new System.Data.SqlClient.SqlCommand();
            this.ortDA = new System.Data.SqlClient.SqlDataAdapter();
            this.unterGruppeSqlSelectCommand = new System.Data.SqlClient.SqlCommand();
            this.unterGruppeDA = new System.Data.SqlClient.SqlDataAdapter();
            this.artSqlSelectCommand = new System.Data.SqlClient.SqlCommand();
            this.artDA = new System.Data.SqlClient.SqlDataAdapter();
            this.produktSqlSelectCommand = new System.Data.SqlClient.SqlCommand();
            this.produktDA = new System.Data.SqlClient.SqlDataAdapter();
            this.tab_HMPBezeichnungSqlSelectCommand = new System.Data.SqlClient.SqlCommand();
            this.tab_HMPBezeichnungDA = new System.Data.SqlClient.SqlDataAdapter();
            this.zuzahlungsArtTT = new System.Windows.Forms.ToolTip(this.components);
            this.positionenSqlConnection = new System.Data.SqlClient.SqlConnection();
            this.positionenInsertCommand = new System.Data.SqlClient.SqlCommand();
            this.positionenUpdateCommand = new System.Data.SqlClient.SqlCommand();
            this.positionenDeleteCommand = new System.Data.SqlClient.SqlCommand();
            this.positionenDA = new System.Data.SqlClient.SqlDataAdapter();
            this.positionenSelectCommand = new System.Data.SqlClient.SqlCommand();
            this.leitZahlenSelectCommand = new System.Data.SqlClient.SqlCommand();
            this.leitZahlenDA = new System.Data.SqlClient.SqlDataAdapter();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.rezepteTabPage = new System.Windows.Forms.TabPage();
            this.kostenvoranschlagTabPage = new System.Windows.Forms.TabPage();
            this.kostenvoranschlagDGV = new System.Windows.Forms.DataGridView();
            this.kostenvoranschlagSuchenPanel = new System.Windows.Forms.Panel();
            this.genehmigungRB = new System.Windows.Forms.RadioButton();
            this.korrekturRB = new System.Windows.Forms.RadioButton();
            this.kostJahrCB = new System.Windows.Forms.ComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.kostSuchenBtn = new System.Windows.Forms.Button();
            this.kostVornameTB = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.kostNameTB = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.kostRezNrTB = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tabprimKKassenSelectCommand = new System.Data.SqlClient.SqlCommand();
            this.tabprimKKassenDA = new System.Data.SqlClient.SqlDataAdapter();
            this.kostenTraegerDTASelectCommand = new System.Data.SqlClient.SqlCommand();
            this.kostenTraegerDTADA = new System.Data.SqlClient.SqlDataAdapter();
            this.arztBetrNrDA = new System.Data.SqlClient.SqlDataAdapter();
            this.arztBetrNrInsertCommand = new System.Data.SqlClient.SqlCommand();
            this.arztBetrNrSelectCommand = new System.Data.SqlClient.SqlCommand();
            this.rezepteDeleteCommand = new System.Data.SqlClient.SqlCommand();
            this.rezepteUpdateCommand = new System.Data.SqlClient.SqlCommand();
            this.rezepteInsertCommand = new System.Data.SqlClient.SqlCommand();
            this.rezepteSelectCommand = new System.Data.SqlClient.SqlCommand();
            this.rezepteDA = new System.Data.SqlClient.SqlDataAdapter();
            this.tmrNextImage = new System.Windows.Forms.Timer(this.components);
            this.pnlCommand.SuspendLayout();
            this.pnlHilfe.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.namesDGV)).BeginInit();
            this.sortOrderPanel.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.pnlMainRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).BeginInit();
            this.pnlMainLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rezepteBindingNavigator)).BeginInit();
            this.rezepteBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rezepteBindingSource)).BeginInit();
            this.positionenPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.anzahlNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.positionenDGV)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.pnlRezept.SuspendLayout();
            this.pnlRezeptImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picImage2)).BeginInit();
            this.pnlRezeptEingabe.SuspendLayout();
            this.pnlKostenTaeger.SuspendLayout();
            this.pnlRechnungsOptionenPlaceholderRight.SuspendLayout();
            this.pnlRechNrOptionen.SuspendLayout();
            this.pnlSuchen.SuspendLayout();
            this.topPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axListLabel1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mandantenBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.voinDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.positionenBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.rezepteTabPage.SuspendLayout();
            this.kostenvoranschlagTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kostenvoranschlagDGV)).BeginInit();
            this.kostenvoranschlagSuchenPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlCommand
            // 
            this.pnlCommand.BackColor = System.Drawing.Color.Transparent;
            this.pnlCommand.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlCommand.Controls.Add(this.printVOBtn);
            this.pnlCommand.Controls.Add(this.kvBtn);
            this.pnlCommand.Controls.Add(this.protokollBtn);
            this.pnlCommand.Controls.Add(this.pnlHilfe);
            this.pnlCommand.Controls.Add(this.abrechnenButton);
            this.pnlCommand.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlCommand.Location = new System.Drawing.Point(3, 3);
            this.pnlCommand.Name = "pnlCommand";
            this.pnlCommand.Size = new System.Drawing.Size(85, 549);
            this.pnlCommand.TabIndex = 0;
            // 
            // printVOBtn
            // 
            this.printVOBtn.Location = new System.Drawing.Point(3, 134);
            this.printVOBtn.Name = "printVOBtn";
            this.printVOBtn.Size = new System.Drawing.Size(75, 35);
            this.printVOBtn.TabIndex = 4;
            this.printVOBtn.Text = "VO bedrucken";
            this.printVOBtn.UseVisualStyleBackColor = true;
            this.printVOBtn.Click += new System.EventHandler(this.printVOBtn_Click);
            // 
            // kvBtn
            // 
            this.kvBtn.Location = new System.Drawing.Point(3, 93);
            this.kvBtn.Name = "kvBtn";
            this.kvBtn.Size = new System.Drawing.Size(75, 35);
            this.kvBtn.TabIndex = 2;
            this.kvBtn.Text = "KV";
            this.kvBtn.UseVisualStyleBackColor = true;
            this.kvBtn.Click += new System.EventHandler(this.kvBtn_Click);
            // 
            // protokollBtn
            // 
            this.protokollBtn.Enabled = false;
            this.protokollBtn.Location = new System.Drawing.Point(3, 51);
            this.protokollBtn.Name = "protokollBtn";
            this.protokollBtn.Size = new System.Drawing.Size(75, 35);
            this.protokollBtn.TabIndex = 1;
            this.protokollBtn.Text = "Protokoll";
            this.protokollBtn.UseVisualStyleBackColor = true;
            this.protokollBtn.Click += new System.EventHandler(this.protokollBtn_Click);
            // 
            // pnlHilfe
            // 
            this.pnlHilfe.Controls.Add(this.BesonderheitenBtn);
            this.pnlHilfe.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlHilfe.Location = new System.Drawing.Point(0, 505);
            this.pnlHilfe.Name = "pnlHilfe";
            this.pnlHilfe.Size = new System.Drawing.Size(81, 40);
            this.pnlHilfe.TabIndex = 3;
            // 
            // BesonderheitenBtn
            // 
            this.BesonderheitenBtn.Location = new System.Drawing.Point(3, 3);
            this.BesonderheitenBtn.Name = "BesonderheitenBtn";
            this.BesonderheitenBtn.Size = new System.Drawing.Size(75, 35);
            this.BesonderheitenBtn.TabIndex = 0;
            this.BesonderheitenBtn.Text = "?";
            this.BesonderheitenBtn.UseVisualStyleBackColor = true;
            this.BesonderheitenBtn.Click += new System.EventHandler(this.BesonderheitenBtn_Click);
            // 
            // abrechnenButton
            // 
            this.abrechnenButton.Enabled = false;
            this.abrechnenButton.Location = new System.Drawing.Point(3, 10);
            this.abrechnenButton.Name = "abrechnenButton";
            this.abrechnenButton.Size = new System.Drawing.Size(75, 35);
            this.abrechnenButton.TabIndex = 0;
            this.abrechnenButton.Text = "Abrechnen";
            this.abrechnenButton.UseVisualStyleBackColor = true;
            this.abrechnenButton.Click += new System.EventHandler(this.abrechnenButton_Click);
            // 
            // mainMenu
            // 
            this.mainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1,
            this.menuItem2});
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 0;
            this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem4,
            this.menuItem5,
            this.iniMI,
            this.posVerwaltungMI,
            this.menuItem3});
            this.menuItem1.Text = "Datei";
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 0;
            this.menuItem4.Text = "Aktualisieren";
            this.menuItem4.Click += new System.EventHandler(this.menuItem4_Click);
            // 
            // menuItem5
            // 
            this.menuItem5.Index = 1;
            this.menuItem5.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.protokollMI,
            this.kostenvoranschlagMI,
            this.verordnungMI});
            this.menuItem5.Text = "Designer";
            // 
            // protokollMI
            // 
            this.protokollMI.Index = 0;
            this.protokollMI.Text = "Protokoll";
            this.protokollMI.Click += new System.EventHandler(this.protokollMI_Click);
            // 
            // kostenvoranschlagMI
            // 
            this.kostenvoranschlagMI.Index = 1;
            this.kostenvoranschlagMI.Text = "Kostenvoranschlag";
            this.kostenvoranschlagMI.Click += new System.EventHandler(this.kostenvoranschlagMI_Click);
            // 
            // verordnungMI
            // 
            this.verordnungMI.Index = 2;
            this.verordnungMI.Text = "Verordnung";
            this.verordnungMI.Click += new System.EventHandler(this.verordnungMI_Click);
            // 
            // iniMI
            // 
            this.iniMI.Index = 2;
            this.iniMI.Text = "ini";
            this.iniMI.Click += new System.EventHandler(this.iniMI_Click);
            // 
            // posVerwaltungMI
            // 
            this.posVerwaltungMI.Index = 3;
            this.posVerwaltungMI.Text = "Positionenverwaltung";
            this.posVerwaltungMI.Click += new System.EventHandler(this.posVerwaltungMI_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 4;
            this.menuItem3.Text = "Beenden";
            this.menuItem3.Click += new System.EventHandler(this.menuItem3_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 1;
            this.menuItem2.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.jahr1MenuItem,
            this.jahr2MenuItem});
            this.menuItem2.Text = "Abrechnungsjahr";
            // 
            // jahr1MenuItem
            // 
            this.jahr1MenuItem.Index = 0;
            this.jahr1MenuItem.RadioCheck = true;
            this.jahr1MenuItem.Text = "2005";
            this.jahr1MenuItem.Click += new System.EventHandler(this.jahrMenuItem_Click);
            // 
            // jahr2MenuItem
            // 
            this.jahr2MenuItem.Checked = true;
            this.jahr2MenuItem.Index = 1;
            this.jahr2MenuItem.RadioCheck = true;
            this.jahr2MenuItem.Text = "2006";
            this.jahr2MenuItem.Click += new System.EventHandler(this.jahrMenuItem_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(88, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.namesDGV);
            this.splitContainer1.Panel1.Controls.Add(this.sortOrderPanel);
            this.splitContainer1.Panel1.Enter += new System.EventHandler(this.splitContainer1_Panel1_Enter);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.pnlMain);
            this.splitContainer1.Panel2.Controls.Add(this.pnlKostenTaeger);
            this.splitContainer1.Panel2.Controls.Add(this.topPanel);
            this.splitContainer1.Size = new System.Drawing.Size(917, 549);
            this.splitContainer1.SplitterDistance = 119;
            this.splitContainer1.SplitterWidth = 3;
            this.splitContainer1.TabIndex = 1;
            // 
            // namesDGV
            // 
            this.namesDGV.AllowUserToAddRows = false;
            this.namesDGV.AllowUserToDeleteRows = false;
            this.namesDGV.AllowUserToResizeColumns = false;
            this.namesDGV.AllowUserToResizeRows = false;
            this.namesDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.namesDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.namesDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NameVorname});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.namesDGV.DefaultCellStyle = dataGridViewCellStyle2;
            this.namesDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.namesDGV.Location = new System.Drawing.Point(0, 40);
            this.namesDGV.Name = "namesDGV";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.namesDGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.namesDGV.RowHeadersVisible = false;
            this.namesDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.namesDGV.Size = new System.Drawing.Size(115, 505);
            this.namesDGV.TabIndex = 2;
            // 
            // NameVorname
            // 
            this.NameVorname.DataPropertyName = "Image";
            this.NameVorname.HeaderText = "Name";
            this.NameVorname.Name = "NameVorname";
            // 
            // sortOrderPanel
            // 
            this.sortOrderPanel.Controls.Add(this.sortCB);
            this.sortOrderPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.sortOrderPanel.Location = new System.Drawing.Point(0, 0);
            this.sortOrderPanel.Name = "sortOrderPanel";
            this.sortOrderPanel.Size = new System.Drawing.Size(115, 40);
            this.sortOrderPanel.TabIndex = 1;
            // 
            // sortCB
            // 
            this.sortCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sortCB.FormattingEnabled = true;
            this.sortCB.Items.AddRange(new object[] {
            "Kassen-Nr",
            "Alphabetisch",
            "Eingabe"});
            this.sortCB.Location = new System.Drawing.Point(4, 10);
            this.sortCB.Name = "sortCB";
            this.sortCB.Size = new System.Drawing.Size(108, 21);
            this.sortCB.TabIndex = 0;
            this.sortCB.SelectedIndexChanged += new System.EventHandler(this.sortCB_SelectedIndexChanged);
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.pnlMainRight);
            this.pnlMain.Controls.Add(this.pnlMainLeft);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 93);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(791, 452);
            this.pnlMain.TabIndex = 7;
            // 
            // pnlMainRight
            // 
            this.pnlMainRight.Controls.Add(this.picImage);
            this.pnlMainRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlMainRight.Location = new System.Drawing.Point(791, 0);
            this.pnlMainRight.Name = "pnlMainRight";
            this.pnlMainRight.Size = new System.Drawing.Size(0, 452);
            this.pnlMainRight.TabIndex = 9;
            // 
            // picImage
            // 
            this.picImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picImage.Location = new System.Drawing.Point(0, 0);
            this.picImage.Name = "picImage";
            this.picImage.Size = new System.Drawing.Size(0, 452);
            this.picImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picImage.TabIndex = 36;
            this.picImage.TabStop = false;
            this.picImage.Visible = false;
            // 
            // pnlMainLeft
            // 
            this.pnlMainLeft.Controls.Add(this.rezepteBindingNavigator);
            this.pnlMainLeft.Controls.Add(this.positionenPanel);
            this.pnlMainLeft.Controls.Add(this.pnlRezept);
            this.pnlMainLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMainLeft.Location = new System.Drawing.Point(0, 0);
            this.pnlMainLeft.Name = "pnlMainLeft";
            this.pnlMainLeft.Size = new System.Drawing.Size(791, 452);
            this.pnlMainLeft.TabIndex = 8;
            // 
            // rezepteBindingNavigator
            // 
            this.rezepteBindingNavigator.AddNewItem = null;
            this.rezepteBindingNavigator.BindingSource = this.rezepteBindingSource;
            this.rezepteBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.rezepteBindingNavigator.DeleteItem = null;
            this.rezepteBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.neuRechTSBtn,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.copyTSBtn});
            this.rezepteBindingNavigator.Location = new System.Drawing.Point(0, 567);
            this.rezepteBindingNavigator.MoveFirstItem = null;
            this.rezepteBindingNavigator.MoveLastItem = null;
            this.rezepteBindingNavigator.MoveNextItem = null;
            this.rezepteBindingNavigator.MovePreviousItem = null;
            this.rezepteBindingNavigator.Name = "rezepteBindingNavigator";
            this.rezepteBindingNavigator.PositionItem = null;
            this.rezepteBindingNavigator.Size = new System.Drawing.Size(791, 25);
            this.rezepteBindingNavigator.TabIndex = 9;
            this.rezepteBindingNavigator.Text = "bindingNavigator1";
            // 
            // rezepteBindingSource
            // 
            this.rezepteBindingSource.CurrentChanged += new System.EventHandler(this.rezepteBindingSource_CurrentChanged);
            this.rezepteBindingSource.PositionChanged += new System.EventHandler(this.rezepteBindingSource_PositionChanged);
            this.rezepteBindingSource.ListChanged += new System.ComponentModel.ListChangedEventHandler(this.rezepteBindingSource_ListChanged);
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(36, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Die Gesamtanzahl der Elemente.";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Enabled = false;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Erste verschieben";
            this.bindingNavigatorMoveFirstItem.Click += new System.EventHandler(this.bindingNavigatorMoveFirstItem_Click);
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Enabled = false;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Vorherige verschieben";
            this.bindingNavigatorMovePreviousItem.Click += new System.EventHandler(this.bindingNavigatorMovePreviousItem_Click);
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Enabled = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(65, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Aktuelle Position";
            this.bindingNavigatorPositionItem.Enter += new System.EventHandler(this.bindingNavigatorPositionItem_Enter);
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Enabled = false;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Nächste verschieben";
            this.bindingNavigatorMoveNextItem.Click += new System.EventHandler(this.bindingNavigatorMoveNextItem_Click);
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Enabled = false;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Letzte verschieben";
            this.bindingNavigatorMoveLastItem.Click += new System.EventHandler(this.bindingNavigatorMoveLastItem_Click);
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // neuRechTSBtn
            // 
            this.neuRechTSBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.neuRechTSBtn.Enabled = false;
            this.neuRechTSBtn.Image = ((System.Drawing.Image)(resources.GetObject("neuRechTSBtn.Image")));
            this.neuRechTSBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.neuRechTSBtn.Name = "neuRechTSBtn";
            this.neuRechTSBtn.Size = new System.Drawing.Size(23, 22);
            this.neuRechTSBtn.Text = "Neue Rechnung F7";
            this.neuRechTSBtn.Click += new System.EventHandler(this.neuRechTSBtn_Click);
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Enabled = false;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Neue VO F6";
            this.bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Enabled = false;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Löschen";
            this.bindingNavigatorDeleteItem.Click += new System.EventHandler(this.bindingNavigatorDeleteItem_Click);
            // 
            // copyTSBtn
            // 
            this.copyTSBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.copyTSBtn.Enabled = false;
            this.copyTSBtn.Image = ((System.Drawing.Image)(resources.GetObject("copyTSBtn.Image")));
            this.copyTSBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyTSBtn.Name = "copyTSBtn";
            this.copyTSBtn.Size = new System.Drawing.Size(23, 22);
            this.copyTSBtn.Text = "Kopieren F9";
            this.copyTSBtn.Click += new System.EventHandler(this.copyTSBtn_Click);
            // 
            // positionenPanel
            // 
            this.positionenPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.positionenPanel.Controls.Add(this.label39);
            this.positionenPanel.Controls.Add(this.IdentnrCB);
            this.positionenPanel.Controls.Add(this.label29);
            this.positionenPanel.Controls.Add(this.posTKZTB);
            this.positionenPanel.Controls.Add(this.eigenanteilCB);
            this.positionenPanel.Controls.Add(this.anzahlEigenanteilLabel);
            this.positionenPanel.Controls.Add(this.inventarNummerTB);
            this.positionenPanel.Controls.Add(this.label48);
            this.positionenPanel.Controls.Add(this.verbrauchLabel);
            this.positionenPanel.Controls.Add(this.ePreisTB);
            this.positionenPanel.Controls.Add(this.kilometerTB);
            this.positionenPanel.Controls.Add(this.label49);
            this.positionenPanel.Controls.Add(this.begruendungTB);
            this.positionenPanel.Controls.Add(this.label47);
            this.positionenPanel.Controls.Add(this.gesamteZuzahlungLabel);
            this.positionenPanel.Controls.Add(this.gesamteSummeLabel);
            this.positionenPanel.Controls.Add(this.gesamteForderungLabel);
            this.positionenPanel.Controls.Add(this.positionDeleteBtn);
            this.positionenPanel.Controls.Add(this.neuePositionBtn);
            this.positionenPanel.Controls.Add(this.zuzahlungsArtLabel);
            this.positionenPanel.Controls.Add(this.label42);
            this.positionenPanel.Controls.Add(this.produktLabel);
            this.positionenPanel.Controls.Add(this.artLabel);
            this.positionenPanel.Controls.Add(this.unterGruppeLabel);
            this.positionenPanel.Controls.Add(this.ortLabel);
            this.positionenPanel.Controls.Add(this.gruppeLabel);
            this.positionenPanel.Controls.Add(this.hilfsMittelKennZeichenCB);
            this.positionenPanel.Controls.Add(this.label35);
            this.positionenPanel.Controls.Add(this.verbrauchBtn);
            this.positionenPanel.Controls.Add(this.forderungLabel);
            this.positionenPanel.Controls.Add(this.label33);
            this.positionenPanel.Controls.Add(this.zuzahlungTB);
            this.positionenPanel.Controls.Add(this.label30);
            this.positionenPanel.Controls.Add(this.eigenAnteilTB);
            this.positionenPanel.Controls.Add(this.mwstCB);
            this.positionenPanel.Controls.Add(this.label28);
            this.positionenPanel.Controls.Add(this.nettoCB);
            this.positionenPanel.Controls.Add(this.mwstTB);
            this.positionenPanel.Controls.Add(this.label27);
            this.positionenPanel.Controls.Add(this.rechnerBtn);
            this.positionenPanel.Controls.Add(this.label26);
            this.positionenPanel.Controls.Add(this.label24);
            this.positionenPanel.Controls.Add(this.produktMTB);
            this.positionenPanel.Controls.Add(this.artMTB);
            this.positionenPanel.Controls.Add(this.unterGruppeMTB);
            this.positionenPanel.Controls.Add(this.ortMTB);
            this.positionenPanel.Controls.Add(this.gruppeMTB);
            this.positionenPanel.Controls.Add(this.label23);
            this.positionenPanel.Controls.Add(this.anzahlNUD);
            this.positionenPanel.Controls.Add(this.label22);
            this.positionenPanel.Controls.Add(this.jahrCB);
            this.positionenPanel.Controls.Add(this.label21);
            this.positionenPanel.Controls.Add(this.tagMonatMTB);
            this.positionenPanel.Controls.Add(this.positionenDGV);
            this.positionenPanel.Controls.Add(this.label20);
            this.positionenPanel.Controls.Add(this.groupBox2);
            this.positionenPanel.Controls.Add(this.label36);
            this.positionenPanel.Controls.Add(this.posBezeichnungBtn);
            this.positionenPanel.Controls.Add(this.groupBox1);
            this.positionenPanel.Controls.Add(this.label32);
            this.positionenPanel.Controls.Add(this.anzahlZuzahlungLabel);
            this.positionenPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.positionenPanel.Enabled = false;
            this.positionenPanel.Location = new System.Drawing.Point(0, 236);
            this.positionenPanel.Name = "positionenPanel";
            this.positionenPanel.Size = new System.Drawing.Size(791, 331);
            this.positionenPanel.TabIndex = 8;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Enabled = false;
            this.label39.Location = new System.Drawing.Point(370, 276);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(48, 13);
            this.label39.TabIndex = 70;
            this.label39.Text = "Ident. Nr";
            // 
            // IdentnrCB
            // 
            this.IdentnrCB.Enabled = false;
            this.IdentnrCB.FormattingEnabled = true;
            this.IdentnrCB.Location = new System.Drawing.Point(424, 272);
            this.IdentnrCB.Name = "IdentnrCB";
            this.IdentnrCB.Size = new System.Drawing.Size(49, 21);
            this.IdentnrCB.TabIndex = 69;
            this.IdentnrCB.TabStop = false;
            this.IdentnrCB.SelectedIndexChanged += new System.EventHandler(this.IdentnrCB_SelectedIndexChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Enabled = false;
            this.label29.Location = new System.Drawing.Point(564, 208);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(89, 13);
            this.label29.TabIndex = 68;
            this.label29.Text = "Tarifkennzeichen";
            this.label29.Visible = false;
            // 
            // posTKZTB
            // 
            this.posTKZTB.Enabled = false;
            this.posTKZTB.Location = new System.Drawing.Point(659, 205);
            this.posTKZTB.Name = "posTKZTB";
            this.posTKZTB.Size = new System.Drawing.Size(100, 20);
            this.posTKZTB.TabIndex = 67;
            this.posTKZTB.TabStop = false;
            this.posTKZTB.Visible = false;
            // 
            // eigenanteilCB
            // 
            this.eigenanteilCB.Appearance = System.Windows.Forms.Appearance.Button;
            this.eigenanteilCB.Location = new System.Drawing.Point(659, 46);
            this.eigenanteilCB.Name = "eigenanteilCB";
            this.eigenanteilCB.Size = new System.Drawing.Size(73, 22);
            this.eigenanteilCB.TabIndex = 18;
            this.eigenanteilCB.TabStop = false;
            this.eigenanteilCB.Text = "Übernahme";
            this.eigenanteilCB.UseVisualStyleBackColor = true;
            this.eigenanteilCB.Visible = false;
            this.eigenanteilCB.CheckedChanged += new System.EventHandler(this.eigenanteilCB_CheckedChanged);
            // 
            // anzahlEigenanteilLabel
            // 
            this.anzahlEigenanteilLabel.Enabled = false;
            this.anzahlEigenanteilLabel.Location = new System.Drawing.Point(510, 73);
            this.anzahlEigenanteilLabel.Name = "anzahlEigenanteilLabel";
            this.anzahlEigenanteilLabel.Size = new System.Drawing.Size(26, 17);
            this.anzahlEigenanteilLabel.TabIndex = 30;
            this.anzahlEigenanteilLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.anzahlEigenanteilLabel.Visible = false;
            // 
            // inventarNummerTB
            // 
            this.inventarNummerTB.Enabled = false;
            this.inventarNummerTB.Location = new System.Drawing.Point(424, 296);
            this.inventarNummerTB.MaxLength = 20;
            this.inventarNummerTB.Name = "inventarNummerTB";
            this.inventarNummerTB.Size = new System.Drawing.Size(100, 20);
            this.inventarNummerTB.TabIndex = 43;
            this.inventarNummerTB.TabStop = false;
            this.inventarNummerTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Enabled = false;
            this.label48.Location = new System.Drawing.Point(337, 299);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(83, 13);
            this.label48.TabIndex = 42;
            this.label48.Text = "Inventarnummer";
            // 
            // verbrauchLabel
            // 
            this.verbrauchLabel.AutoSize = true;
            this.verbrauchLabel.Enabled = false;
            this.verbrauchLabel.ForeColor = System.Drawing.Color.Red;
            this.verbrauchLabel.Location = new System.Drawing.Point(732, 104);
            this.verbrauchLabel.Name = "verbrauchLabel";
            this.verbrauchLabel.Size = new System.Drawing.Size(56, 13);
            this.verbrauchLabel.TabIndex = 35;
            this.verbrauchLabel.Text = "Verbrauch";
            this.verbrauchLabel.Visible = false;
            // 
            // ePreisTB
            // 
            this.ePreisTB.Enabled = false;
            this.ePreisTB.Location = new System.Drawing.Point(307, 72);
            this.ePreisTB.Name = "ePreisTB";
            this.ePreisTB.Size = new System.Drawing.Size(65, 20);
            this.ePreisTB.TabIndex = 13;
            this.ePreisTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.ePreisTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            this.ePreisTB.Validating += new System.ComponentModel.CancelEventHandler(this.ePreisTB_Validating);
            // 
            // kilometerTB
            // 
            this.kilometerTB.Enabled = false;
            this.kilometerTB.Location = new System.Drawing.Point(659, 231);
            this.kilometerTB.Name = "kilometerTB";
            this.kilometerTB.Size = new System.Drawing.Size(100, 20);
            this.kilometerTB.TabIndex = 45;
            this.kilometerTB.TabStop = false;
            this.kilometerTB.Visible = false;
            this.kilometerTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            this.kilometerTB.Validating += new System.ComponentModel.CancelEventHandler(this.kilometerTB_Validating);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Enabled = false;
            this.label49.Location = new System.Drawing.Point(588, 234);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(72, 13);
            this.label49.TabIndex = 44;
            this.label49.Text = "gefahrene km";
            this.label49.Visible = false;
            // 
            // begruendungTB
            // 
            this.begruendungTB.Enabled = false;
            this.begruendungTB.Location = new System.Drawing.Point(3, 296);
            this.begruendungTB.MaxLength = 70;
            this.begruendungTB.Name = "begruendungTB";
            this.begruendungTB.Size = new System.Drawing.Size(299, 20);
            this.begruendungTB.TabIndex = 59;
            this.begruendungTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            this.begruendungTB.Validating += new System.ComponentModel.CancelEventHandler(this.begruendungTB_Validating);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Enabled = false;
            this.label47.Location = new System.Drawing.Point(3, 280);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(87, 13);
            this.label47.TabIndex = 60;
            this.label47.Text = "Begründungstext";
            // 
            // gesamteZuzahlungLabel
            // 
            this.gesamteZuzahlungLabel.BackColor = System.Drawing.Color.Silver;
            this.gesamteZuzahlungLabel.Enabled = false;
            this.gesamteZuzahlungLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gesamteZuzahlungLabel.Location = new System.Drawing.Point(155, 251);
            this.gesamteZuzahlungLabel.Name = "gesamteZuzahlungLabel";
            this.gesamteZuzahlungLabel.Size = new System.Drawing.Size(147, 22);
            this.gesamteZuzahlungLabel.TabIndex = 65;
            this.gesamteZuzahlungLabel.Text = "Zuzahlung:";
            this.gesamteZuzahlungLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.gesamteZuzahlungLabel.Visible = false;
            // 
            // gesamteSummeLabel
            // 
            this.gesamteSummeLabel.BackColor = System.Drawing.Color.Silver;
            this.gesamteSummeLabel.Enabled = false;
            this.gesamteSummeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gesamteSummeLabel.Location = new System.Drawing.Point(6, 251);
            this.gesamteSummeLabel.Name = "gesamteSummeLabel";
            this.gesamteSummeLabel.Size = new System.Drawing.Size(147, 22);
            this.gesamteSummeLabel.TabIndex = 63;
            this.gesamteSummeLabel.Text = "Ges.:";
            this.gesamteSummeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.gesamteSummeLabel.Visible = false;
            // 
            // gesamteForderungLabel
            // 
            this.gesamteForderungLabel.BackColor = System.Drawing.Color.Silver;
            this.gesamteForderungLabel.Enabled = false;
            this.gesamteForderungLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gesamteForderungLabel.ForeColor = System.Drawing.Color.Red;
            this.gesamteForderungLabel.Location = new System.Drawing.Point(155, 223);
            this.gesamteForderungLabel.Name = "gesamteForderungLabel";
            this.gesamteForderungLabel.Size = new System.Drawing.Size(147, 22);
            this.gesamteForderungLabel.TabIndex = 64;
            this.gesamteForderungLabel.Text = "Forderung:";
            this.gesamteForderungLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // positionDeleteBtn
            // 
            this.positionDeleteBtn.Enabled = false;
            this.positionDeleteBtn.Location = new System.Drawing.Point(81, 223);
            this.positionDeleteBtn.Name = "positionDeleteBtn";
            this.positionDeleteBtn.Size = new System.Drawing.Size(71, 23);
            this.positionDeleteBtn.TabIndex = 62;
            this.positionDeleteBtn.TabStop = false;
            this.positionDeleteBtn.Text = "&Löschen";
            this.positionDeleteBtn.UseVisualStyleBackColor = true;
            this.positionDeleteBtn.Click += new System.EventHandler(this.positionDeleteBtn_Click);
            // 
            // neuePositionBtn
            // 
            this.neuePositionBtn.Location = new System.Drawing.Point(6, 223);
            this.neuePositionBtn.Name = "neuePositionBtn";
            this.neuePositionBtn.Size = new System.Drawing.Size(71, 23);
            this.neuePositionBtn.TabIndex = 61;
            this.neuePositionBtn.TabStop = false;
            this.neuePositionBtn.Text = "&Neu";
            this.neuePositionBtn.UseVisualStyleBackColor = true;
            this.neuePositionBtn.Click += new System.EventHandler(this.neuePositionBtn_Click);
            // 
            // zuzahlungsArtLabel
            // 
            this.zuzahlungsArtLabel.AutoSize = true;
            this.zuzahlungsArtLabel.Enabled = false;
            this.zuzahlungsArtLabel.Location = new System.Drawing.Point(386, 227);
            this.zuzahlungsArtLabel.Name = "zuzahlungsArtLabel";
            this.zuzahlungsArtLabel.Size = new System.Drawing.Size(0, 13);
            this.zuzahlungsArtLabel.TabIndex = 27;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Enabled = false;
            this.label42.Location = new System.Drawing.Point(588, 168);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(74, 13);
            this.label42.TabIndex = 26;
            this.label42.Text = "Zuzahlungsart";
            this.label42.Visible = false;
            // 
            // produktLabel
            // 
            this.produktLabel.BackColor = System.Drawing.Color.Transparent;
            this.produktLabel.Enabled = false;
            this.produktLabel.Location = new System.Drawing.Point(306, 206);
            this.produktLabel.Name = "produktLabel";
            this.produktLabel.Size = new System.Drawing.Size(213, 15);
            this.produktLabel.TabIndex = 25;
            this.produktLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // artLabel
            // 
            this.artLabel.BackColor = System.Drawing.Color.Transparent;
            this.artLabel.Enabled = false;
            this.artLabel.Location = new System.Drawing.Point(306, 187);
            this.artLabel.Name = "artLabel";
            this.artLabel.Size = new System.Drawing.Size(213, 15);
            this.artLabel.TabIndex = 24;
            this.artLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // unterGruppeLabel
            // 
            this.unterGruppeLabel.BackColor = System.Drawing.Color.Transparent;
            this.unterGruppeLabel.Enabled = false;
            this.unterGruppeLabel.Location = new System.Drawing.Point(306, 168);
            this.unterGruppeLabel.Name = "unterGruppeLabel";
            this.unterGruppeLabel.Size = new System.Drawing.Size(213, 15);
            this.unterGruppeLabel.TabIndex = 23;
            this.unterGruppeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ortLabel
            // 
            this.ortLabel.BackColor = System.Drawing.Color.Transparent;
            this.ortLabel.Enabled = false;
            this.ortLabel.Location = new System.Drawing.Point(306, 149);
            this.ortLabel.Name = "ortLabel";
            this.ortLabel.Size = new System.Drawing.Size(216, 15);
            this.ortLabel.TabIndex = 22;
            this.ortLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gruppeLabel
            // 
            this.gruppeLabel.BackColor = System.Drawing.Color.Transparent;
            this.gruppeLabel.Enabled = false;
            this.gruppeLabel.Location = new System.Drawing.Point(306, 130);
            this.gruppeLabel.Name = "gruppeLabel";
            this.gruppeLabel.Size = new System.Drawing.Size(216, 15);
            this.gruppeLabel.TabIndex = 21;
            this.gruppeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // hilfsMittelKennZeichenCB
            // 
            this.hilfsMittelKennZeichenCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.hilfsMittelKennZeichenCB.Enabled = false;
            this.hilfsMittelKennZeichenCB.FormattingEnabled = true;
            this.hilfsMittelKennZeichenCB.Location = new System.Drawing.Point(424, 248);
            this.hilfsMittelKennZeichenCB.Name = "hilfsMittelKennZeichenCB";
            this.hilfsMittelKennZeichenCB.Size = new System.Drawing.Size(165, 21);
            this.hilfsMittelKennZeichenCB.TabIndex = 41;
            this.hilfsMittelKennZeichenCB.Validating += new System.ComponentModel.CancelEventHandler(this.begruendungTB_Validating);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Enabled = false;
            this.label35.Location = new System.Drawing.Point(306, 251);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(112, 13);
            this.label35.TabIndex = 40;
            this.label35.Text = "Hilfsmittelkennzeichen";
            // 
            // verbrauchBtn
            // 
            this.verbrauchBtn.Enabled = false;
            this.verbrauchBtn.Location = new System.Drawing.Point(735, 126);
            this.verbrauchBtn.Name = "verbrauchBtn";
            this.verbrauchBtn.Size = new System.Drawing.Size(47, 34);
            this.verbrauchBtn.TabIndex = 39;
            this.verbrauchBtn.TabStop = false;
            this.verbrauchBtn.Text = "Verbr.\r\nF10";
            this.verbrauchBtn.UseVisualStyleBackColor = true;
            this.verbrauchBtn.Visible = false;
            this.verbrauchBtn.Click += new System.EventHandler(this.verbrauchBtn_Click);
            // 
            // forderungLabel
            // 
            this.forderungLabel.BackColor = System.Drawing.SystemColors.Control;
            this.forderungLabel.Enabled = false;
            this.forderungLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.forderungLabel.ForeColor = System.Drawing.Color.Red;
            this.forderungLabel.Location = new System.Drawing.Point(428, 104);
            this.forderungLabel.Name = "forderungLabel";
            this.forderungLabel.Size = new System.Drawing.Size(81, 18);
            this.forderungLabel.TabIndex = 38;
            this.forderungLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Enabled = false;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(340, 104);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(83, 16);
            this.label33.TabIndex = 37;
            this.label33.Text = "Forderung:";
            // 
            // zuzahlungTB
            // 
            this.zuzahlungTB.Enabled = false;
            this.zuzahlungTB.Location = new System.Drawing.Point(673, 98);
            this.zuzahlungTB.Name = "zuzahlungTB";
            this.zuzahlungTB.Size = new System.Drawing.Size(55, 20);
            this.zuzahlungTB.TabIndex = 34;
            this.zuzahlungTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.zuzahlungTB.Visible = false;
            this.zuzahlungTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            this.zuzahlungTB.Validating += new System.ComponentModel.CancelEventHandler(this.zuzahlungTB_Validating);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Enabled = false;
            this.label30.Location = new System.Drawing.Point(564, 101);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(57, 13);
            this.label30.TabIndex = 32;
            this.label30.Text = "Zuzahlung";
            this.label30.Visible = false;
            // 
            // eigenAnteilTB
            // 
            this.eigenAnteilTB.Enabled = false;
            this.eigenAnteilTB.Location = new System.Drawing.Point(673, 74);
            this.eigenAnteilTB.Name = "eigenAnteilTB";
            this.eigenAnteilTB.Size = new System.Drawing.Size(55, 20);
            this.eigenAnteilTB.TabIndex = 31;
            this.eigenAnteilTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.eigenAnteilTB.Visible = false;
            this.eigenAnteilTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            this.eigenAnteilTB.Validating += new System.ComponentModel.CancelEventHandler(this.eigenAntailTB_Validating);
            // 
            // mwstCB
            // 
            this.mwstCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.mwstCB.Enabled = false;
            this.mwstCB.FormattingEnabled = true;
            this.mwstCB.Location = new System.Drawing.Point(436, 71);
            this.mwstCB.Name = "mwstCB";
            this.mwstCB.Size = new System.Drawing.Size(71, 21);
            this.mwstCB.TabIndex = 29;
            this.mwstCB.TabStop = false;
            this.mwstCB.SelectedValueChanged += new System.EventHandler(this.mwstCB_SelectedValueChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Enabled = false;
            this.label28.Location = new System.Drawing.Point(429, 55);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(80, 13);
            this.label28.TabIndex = 28;
            this.label28.Text = "Mehrwertsteuer";
            // 
            // nettoCB
            // 
            this.nettoCB.Appearance = System.Windows.Forms.Appearance.Button;
            this.nettoCB.Enabled = false;
            this.nettoCB.Location = new System.Drawing.Point(307, 49);
            this.nettoCB.Name = "nettoCB";
            this.nettoCB.Size = new System.Drawing.Size(57, 22);
            this.nettoCB.TabIndex = 17;
            this.nettoCB.TabStop = false;
            this.nettoCB.Text = "Brutto";
            this.nettoCB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.nettoCB.UseVisualStyleBackColor = true;
            this.nettoCB.CheckedChanged += new System.EventHandler(this.nettoCB_CheckedChanged);
            // 
            // mwstTB
            // 
            this.mwstTB.Enabled = false;
            this.mwstTB.Location = new System.Drawing.Point(377, 72);
            this.mwstTB.Name = "mwstTB";
            this.mwstTB.ReadOnly = true;
            this.mwstTB.Size = new System.Drawing.Size(55, 20);
            this.mwstTB.TabIndex = 16;
            this.mwstTB.TabStop = false;
            this.mwstTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Enabled = false;
            this.label27.Location = new System.Drawing.Point(382, 56);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(37, 13);
            this.label27.TabIndex = 15;
            this.label27.Text = "MWSt";
            // 
            // rechnerBtn
            // 
            this.rechnerBtn.Enabled = false;
            this.rechnerBtn.Location = new System.Drawing.Point(363, 50);
            this.rechnerBtn.Margin = new System.Windows.Forms.Padding(0);
            this.rechnerBtn.Name = "rechnerBtn";
            this.rechnerBtn.Size = new System.Drawing.Size(19, 19);
            this.rechnerBtn.TabIndex = 14;
            this.rechnerBtn.TabStop = false;
            this.rechnerBtn.Text = "R";
            this.rechnerBtn.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.rechnerBtn.UseVisualStyleBackColor = true;
            this.rechnerBtn.Click += new System.EventHandler(this.rechnerBtn_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Enabled = false;
            this.label26.Location = new System.Drawing.Point(593, 7);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(40, 13);
            this.label26.TabIndex = 12;
            this.label26.Text = "E-Preis";
            this.label26.Visible = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Enabled = false;
            this.label24.Location = new System.Drawing.Point(469, 7);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(86, 13);
            this.label24.TabIndex = 6;
            this.label24.Text = "Positionsnummer";
            // 
            // produktMTB
            // 
            this.produktMTB.Enabled = false;
            this.produktMTB.Location = new System.Drawing.Point(568, 23);
            this.produktMTB.Mask = "000";
            this.produktMTB.Name = "produktMTB";
            this.produktMTB.Size = new System.Drawing.Size(31, 20);
            this.produktMTB.TabIndex = 11;
            this.produktMTB.Visible = false;
            this.produktMTB.Validating += new System.ComponentModel.CancelEventHandler(this.positionsnummerMTBs_Validating);
            this.produktMTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            this.produktMTB.TextChanged += new System.EventHandler(this.positionenMTBs_TextChanged);
            // 
            // artMTB
            // 
            this.artMTB.Enabled = false;
            this.artMTB.Location = new System.Drawing.Point(543, 23);
            this.artMTB.Mask = "00";
            this.artMTB.Name = "artMTB";
            this.artMTB.Size = new System.Drawing.Size(25, 20);
            this.artMTB.TabIndex = 10;
            this.artMTB.Validating += new System.ComponentModel.CancelEventHandler(this.positionsnummerMTBs_Validating);
            this.artMTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            this.artMTB.TextChanged += new System.EventHandler(this.positionenMTBs_TextChanged);
            // 
            // unterGruppeMTB
            // 
            this.unterGruppeMTB.Enabled = false;
            this.unterGruppeMTB.Location = new System.Drawing.Point(519, 23);
            this.unterGruppeMTB.Mask = "00";
            this.unterGruppeMTB.Name = "unterGruppeMTB";
            this.unterGruppeMTB.Size = new System.Drawing.Size(25, 20);
            this.unterGruppeMTB.TabIndex = 9;
            this.unterGruppeMTB.Validating += new System.ComponentModel.CancelEventHandler(this.positionsnummerMTBs_Validating);
            this.unterGruppeMTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            this.unterGruppeMTB.TextChanged += new System.EventHandler(this.positionenMTBs_TextChanged);
            // 
            // ortMTB
            // 
            this.ortMTB.Enabled = false;
            this.ortMTB.Location = new System.Drawing.Point(499, 23);
            this.ortMTB.Mask = "00";
            this.ortMTB.Name = "ortMTB";
            this.ortMTB.Size = new System.Drawing.Size(25, 20);
            this.ortMTB.TabIndex = 8;
            this.ortMTB.Validating += new System.ComponentModel.CancelEventHandler(this.positionsnummerMTBs_Validating);
            this.ortMTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            this.ortMTB.TextChanged += new System.EventHandler(this.positionenMTBs_TextChanged);
            // 
            // gruppeMTB
            // 
            this.gruppeMTB.Enabled = false;
            this.gruppeMTB.Location = new System.Drawing.Point(471, 23);
            this.gruppeMTB.Mask = "00";
            this.gruppeMTB.Name = "gruppeMTB";
            this.gruppeMTB.Size = new System.Drawing.Size(25, 20);
            this.gruppeMTB.TabIndex = 7;
            this.gruppeMTB.Validating += new System.ComponentModel.CancelEventHandler(this.positionsnummerMTBs_Validating);
            this.gruppeMTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            this.gruppeMTB.TextChanged += new System.EventHandler(this.positionenMTBs_TextChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Enabled = false;
            this.label23.Location = new System.Drawing.Point(421, 8);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(25, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "Anz";
            // 
            // anzahlNUD
            // 
            this.anzahlNUD.Enabled = false;
            this.anzahlNUD.Location = new System.Drawing.Point(423, 23);
            this.anzahlNUD.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.anzahlNUD.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.anzahlNUD.Name = "anzahlNUD";
            this.anzahlNUD.Size = new System.Drawing.Size(45, 20);
            this.anzahlNUD.TabIndex = 5;
            this.anzahlNUD.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.anzahlNUD.ValueChanged += new System.EventHandler(this.anzahlNUD_ValueChanged);
            this.anzahlNUD.Validating += new System.ComponentModel.CancelEventHandler(this.anzahlNUD_Validating);
            this.anzahlNUD.Enter += new System.EventHandler(this.anzahlNUD_Enter);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(-41, 16);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 13);
            this.label22.TabIndex = 5;
            this.label22.Text = "label22";
            // 
            // jahrCB
            // 
            this.jahrCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.jahrCB.Enabled = false;
            this.jahrCB.FormattingEnabled = true;
            this.jahrCB.Location = new System.Drawing.Point(353, 23);
            this.jahrCB.Name = "jahrCB";
            this.jahrCB.Size = new System.Drawing.Size(65, 21);
            this.jahrCB.TabIndex = 3;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Enabled = false;
            this.label21.Location = new System.Drawing.Point(303, 8);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(38, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "Datum";
            // 
            // tagMonatMTB
            // 
            this.tagMonatMTB.Enabled = false;
            this.tagMonatMTB.Location = new System.Drawing.Point(307, 24);
            this.tagMonatMTB.Mask = "00/00/";
            this.tagMonatMTB.Name = "tagMonatMTB";
            this.tagMonatMTB.Size = new System.Drawing.Size(40, 20);
            this.tagMonatMTB.TabIndex = 2;
            this.tagMonatMTB.Validating += new System.ComponentModel.CancelEventHandler(this.tagMonatMTB_Validating);
            this.tagMonatMTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            // 
            // positionenDGV
            // 
            this.positionenDGV.AllowUserToAddRows = false;
            this.positionenDGV.AllowUserToDeleteRows = false;
            this.positionenDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.positionenDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.positionenDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.positionenDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.datumColumn,
            this.buPosColumn,
            this.anzahlColumn,
            this.ePreisColumn,
            this.forderungColumn});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.positionenDGV.DefaultCellStyle = dataGridViewCellStyle9;
            this.positionenDGV.Location = new System.Drawing.Point(6, 17);
            this.positionenDGV.MultiSelect = false;
            this.positionenDGV.Name = "positionenDGV";
            this.positionenDGV.ReadOnly = true;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.positionenDGV.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.positionenDGV.RowHeadersWidth = 10;
            this.positionenDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.positionenDGV.Size = new System.Drawing.Size(297, 200);
            this.positionenDGV.TabIndex = 66;
            this.positionenDGV.TabStop = false;
            this.positionenDGV.Enter += new System.EventHandler(this.positionenDGV_Enter);
            this.positionenDGV.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.positionenDGV_CellFormatting);
            // 
            // datumColumn
            // 
            this.datumColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.datumColumn.DataPropertyName = "BehDat";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "d";
            dataGridViewCellStyle5.NullValue = null;
            this.datumColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.datumColumn.HeaderText = "Datum";
            this.datumColumn.Name = "datumColumn";
            this.datumColumn.ReadOnly = true;
            this.datumColumn.Width = 63;
            // 
            // buPosColumn
            // 
            this.buPosColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.buPosColumn.DataPropertyName = "BuPos";
            this.buPosColumn.HeaderText = "BuPos";
            this.buPosColumn.Name = "buPosColumn";
            this.buPosColumn.ReadOnly = true;
            this.buPosColumn.Width = 63;
            // 
            // anzahlColumn
            // 
            this.anzahlColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.anzahlColumn.DataPropertyName = "Anzahl";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N0";
            dataGridViewCellStyle6.NullValue = null;
            this.anzahlColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this.anzahlColumn.HeaderText = "#";
            this.anzahlColumn.Name = "anzahlColumn";
            this.anzahlColumn.ReadOnly = true;
            this.anzahlColumn.Width = 24;
            // 
            // ePreisColumn
            // 
            this.ePreisColumn.DataPropertyName = "EPreis";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N2";
            dataGridViewCellStyle7.NullValue = null;
            this.ePreisColumn.DefaultCellStyle = dataGridViewCellStyle7;
            this.ePreisColumn.HeaderText = "E-Preis";
            this.ePreisColumn.Name = "ePreisColumn";
            this.ePreisColumn.ReadOnly = true;
            // 
            // forderungColumn
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N2";
            dataGridViewCellStyle8.NullValue = null;
            this.forderungColumn.DefaultCellStyle = dataGridViewCellStyle8;
            this.forderungColumn.HeaderText = "Forderung";
            this.forderungColumn.Name = "forderungColumn";
            this.forderungColumn.ReadOnly = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(3, 1);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(51, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Hilfsmittel";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dauerTB);
            this.groupBox2.Controls.Add(this.dauerCB);
            this.groupBox2.Controls.Add(this.label54);
            this.groupBox2.Controls.Add(this.zeitBisMTB);
            this.groupBox2.Controls.Add(this.label51);
            this.groupBox2.Controls.Add(this.label50);
            this.groupBox2.Controls.Add(this.zeitVonMTB);
            this.groupBox2.Enabled = false;
            this.groupBox2.Location = new System.Drawing.Point(618, 252);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(147, 34);
            this.groupBox2.TabIndex = 46;
            this.groupBox2.TabStop = false;
            this.groupBox2.Visible = false;
            // 
            // dauerTB
            // 
            this.dauerTB.Location = new System.Drawing.Point(347, 10);
            this.dauerTB.Name = "dauerTB";
            this.dauerTB.Size = new System.Drawing.Size(100, 20);
            this.dauerTB.TabIndex = 52;
            this.dauerTB.TabStop = false;
            // 
            // dauerCB
            // 
            this.dauerCB.AutoSize = true;
            this.dauerCB.Location = new System.Drawing.Point(10, 12);
            this.dauerCB.Name = "dauerCB";
            this.dauerCB.Size = new System.Drawing.Size(15, 14);
            this.dauerCB.TabIndex = 53;
            this.dauerCB.TabStop = false;
            this.dauerCB.UseVisualStyleBackColor = true;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(305, 13);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(36, 13);
            this.label54.TabIndex = 51;
            this.label54.Text = "Dauer";
            // 
            // zeitBisMTB
            // 
            this.zeitBisMTB.Location = new System.Drawing.Point(193, 10);
            this.zeitBisMTB.Mask = "00:00";
            this.zeitBisMTB.Name = "zeitBisMTB";
            this.zeitBisMTB.Size = new System.Drawing.Size(43, 20);
            this.zeitBisMTB.TabIndex = 49;
            this.zeitBisMTB.TabStop = false;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(169, 13);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(20, 13);
            this.label51.TabIndex = 48;
            this.label51.Text = "bis";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(55, 13);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(61, 13);
            this.label50.TabIndex = 46;
            this.label50.Text = "Uhrzeit von";
            // 
            // zeitVonMTB
            // 
            this.zeitVonMTB.Location = new System.Drawing.Point(121, 10);
            this.zeitVonMTB.Mask = "00:00";
            this.zeitVonMTB.Name = "zeitVonMTB";
            this.zeitVonMTB.Size = new System.Drawing.Size(43, 20);
            this.zeitVonMTB.TabIndex = 47;
            this.zeitVonMTB.TabStop = false;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Enabled = false;
            this.label36.Location = new System.Drawing.Point(303, 51);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(93, 13);
            this.label36.TabIndex = 19;
            this.label36.Text = "Pos. Bezeichnung";
            this.label36.Visible = false;
            // 
            // posBezeichnungBtn
            // 
            this.posBezeichnungBtn.Enabled = false;
            this.posBezeichnungBtn.Location = new System.Drawing.Point(399, 47);
            this.posBezeichnungBtn.Margin = new System.Windows.Forms.Padding(0);
            this.posBezeichnungBtn.Name = "posBezeichnungBtn";
            this.posBezeichnungBtn.Size = new System.Drawing.Size(24, 24);
            this.posBezeichnungBtn.TabIndex = 20;
            this.posBezeichnungBtn.TabStop = false;
            this.posBezeichnungBtn.Text = "...";
            this.posBezeichnungBtn.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.posBezeichnungBtn.UseVisualStyleBackColor = true;
            this.posBezeichnungBtn.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.zeitraumCB);
            this.groupBox1.Controls.Add(this.versorgungVonMTB);
            this.groupBox1.Controls.Add(this.versorgungBisMTB);
            this.groupBox1.Controls.Add(this.label53);
            this.groupBox1.Controls.Add(this.label52);
            this.groupBox1.Controls.Add(this.label55);
            this.groupBox1.Controls.Add(this.zeitraumTB);
            this.groupBox1.Enabled = false;
            this.groupBox1.Location = new System.Drawing.Point(613, 287);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(152, 34);
            this.groupBox1.TabIndex = 53;
            this.groupBox1.TabStop = false;
            this.groupBox1.Visible = false;
            // 
            // zeitraumCB
            // 
            this.zeitraumCB.AutoSize = true;
            this.zeitraumCB.Location = new System.Drawing.Point(10, 14);
            this.zeitraumCB.Name = "zeitraumCB";
            this.zeitraumCB.Size = new System.Drawing.Size(15, 14);
            this.zeitraumCB.TabIndex = 59;
            this.zeitraumCB.TabStop = false;
            this.zeitraumCB.UseVisualStyleBackColor = true;
            // 
            // versorgungVonMTB
            // 
            this.versorgungVonMTB.Location = new System.Drawing.Point(121, 10);
            this.versorgungVonMTB.Mask = "00/00/0000";
            this.versorgungVonMTB.Name = "versorgungVonMTB";
            this.versorgungVonMTB.Size = new System.Drawing.Size(67, 20);
            this.versorgungVonMTB.TabIndex = 54;
            this.versorgungVonMTB.TabStop = false;
            this.versorgungVonMTB.ValidatingType = typeof(System.DateTime);
            // 
            // versorgungBisMTB
            // 
            this.versorgungBisMTB.Location = new System.Drawing.Point(220, 10);
            this.versorgungBisMTB.Mask = "00/00/0000";
            this.versorgungBisMTB.Name = "versorgungBisMTB";
            this.versorgungBisMTB.Size = new System.Drawing.Size(67, 20);
            this.versorgungBisMTB.TabIndex = 56;
            this.versorgungBisMTB.TabStop = false;
            this.versorgungBisMTB.ValidatingType = typeof(System.DateTime);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(194, 13);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(20, 13);
            this.label53.TabIndex = 55;
            this.label53.Text = "bis";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(33, 13);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(82, 13);
            this.label52.TabIndex = 53;
            this.label52.Text = "Versorgung von";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(293, 13);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(48, 13);
            this.label55.TabIndex = 57;
            this.label55.Text = "Zeitraum";
            // 
            // zeitraumTB
            // 
            this.zeitraumTB.Location = new System.Drawing.Point(347, 10);
            this.zeitraumTB.Name = "zeitraumTB";
            this.zeitraumTB.Size = new System.Drawing.Size(100, 20);
            this.zeitraumTB.TabIndex = 58;
            this.zeitraumTB.TabStop = false;
            // 
            // label32
            // 
            this.label32.Enabled = false;
            this.label32.Location = new System.Drawing.Point(305, 89);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(205, 15);
            this.label32.TabIndex = 36;
            this.label32.Text = "_________________________________";
            // 
            // anzahlZuzahlungLabel
            // 
            this.anzahlZuzahlungLabel.Enabled = false;
            this.anzahlZuzahlungLabel.Location = new System.Drawing.Point(615, 100);
            this.anzahlZuzahlungLabel.Name = "anzahlZuzahlungLabel";
            this.anzahlZuzahlungLabel.Size = new System.Drawing.Size(55, 17);
            this.anzahlZuzahlungLabel.TabIndex = 33;
            this.anzahlZuzahlungLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.anzahlZuzahlungLabel.Visible = false;
            // 
            // pnlRezept
            // 
            this.pnlRezept.Controls.Add(this.pnlRezeptImage);
            this.pnlRezept.Controls.Add(this.pnlRezeptEingabe);
            this.pnlRezept.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlRezept.Location = new System.Drawing.Point(0, 0);
            this.pnlRezept.Name = "pnlRezept";
            this.pnlRezept.Size = new System.Drawing.Size(791, 236);
            this.pnlRezept.TabIndex = 1;
            // 
            // pnlRezeptImage
            // 
            this.pnlRezeptImage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlRezeptImage.Controls.Add(this.picImage2);
            this.pnlRezeptImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlRezeptImage.Location = new System.Drawing.Point(374, 0);
            this.pnlRezeptImage.Name = "pnlRezeptImage";
            this.pnlRezeptImage.Size = new System.Drawing.Size(417, 236);
            this.pnlRezeptImage.TabIndex = 2;
            // 
            // picImage2
            // 
            this.picImage2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picImage2.Location = new System.Drawing.Point(0, 0);
            this.picImage2.Name = "picImage2";
            this.picImage2.Size = new System.Drawing.Size(413, 232);
            this.picImage2.TabIndex = 0;
            this.picImage2.TabStop = false;
            // 
            // pnlRezeptEingabe
            // 
            this.pnlRezeptEingabe.BackColor = System.Drawing.Color.Transparent;
            this.pnlRezeptEingabe.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlRezeptEingabe.Controls.Add(this.btnArtznrSuchen);
            this.pnlRezeptEingabe.Controls.Add(this.label43);
            this.pnlRezeptEingabe.Controls.Add(this.lstImages);
            this.pnlRezeptEingabe.Controls.Add(this.label41);
            this.pnlRezeptEingabe.Controls.Add(this.label40);
            this.pnlRezeptEingabe.Controls.Add(this.rechTB);
            this.pnlRezeptEingabe.Controls.Add(this.imgTB);
            this.pnlRezeptEingabe.Controls.Add(this.label38);
            this.pnlRezeptEingabe.Controls.Add(this.vkBisTB);
            this.pnlRezeptEingabe.Controls.Add(this.patientenSuchenBtn);
            this.pnlRezeptEingabe.Controls.Add(this.arztNummerTB);
            this.pnlRezeptEingabe.Controls.Add(this.rezeptDatumMTB);
            this.pnlRezeptEingabe.Controls.Add(this.geburtsDatumMTB);
            this.pnlRezeptEingabe.Controls.Add(this.genVonMTB);
            this.pnlRezeptEingabe.Controls.Add(this.plzMTB);
            this.pnlRezeptEingabe.Controls.Add(this.versichertenNummerMTB);
            this.pnlRezeptEingabe.Controls.Add(this.befreitCheckBox);
            this.pnlRezeptEingabe.Controls.Add(this.label15);
            this.pnlRezeptEingabe.Controls.Add(this.genKennzTB);
            this.pnlRezeptEingabe.Controls.Add(this.label14);
            this.pnlRezeptEingabe.Controls.Add(this.label13);
            this.pnlRezeptEingabe.Controls.Add(this.label12);
            this.pnlRezeptEingabe.Controls.Add(this.status2TB);
            this.pnlRezeptEingabe.Controls.Add(this.label11);
            this.pnlRezeptEingabe.Controls.Add(this.status1TB);
            this.pnlRezeptEingabe.Controls.Add(this.label10);
            this.pnlRezeptEingabe.Controls.Add(this.rezeptArtCB);
            this.pnlRezeptEingabe.Controls.Add(this.label9);
            this.pnlRezeptEingabe.Controls.Add(this.indikationTB);
            this.pnlRezeptEingabe.Controls.Add(this.label8);
            this.pnlRezeptEingabe.Controls.Add(this.ortTB);
            this.pnlRezeptEingabe.Controls.Add(this.label7);
            this.pnlRezeptEingabe.Controls.Add(this.label6);
            this.pnlRezeptEingabe.Controls.Add(this.strasseTB);
            this.pnlRezeptEingabe.Controls.Add(this.label5);
            this.pnlRezeptEingabe.Controls.Add(this.label4);
            this.pnlRezeptEingabe.Controls.Add(this.label3);
            this.pnlRezeptEingabe.Controls.Add(this.vornameTB);
            this.pnlRezeptEingabe.Controls.Add(this.label2);
            this.pnlRezeptEingabe.Controls.Add(this.nameTB);
            this.pnlRezeptEingabe.Controls.Add(this.label1);
            this.pnlRezeptEingabe.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlRezeptEingabe.Enabled = false;
            this.pnlRezeptEingabe.Location = new System.Drawing.Point(0, 0);
            this.pnlRezeptEingabe.Name = "pnlRezeptEingabe";
            this.pnlRezeptEingabe.Size = new System.Drawing.Size(374, 236);
            this.pnlRezeptEingabe.TabIndex = 1;
            // 
            // btnArtznrSuchen
            // 
            this.btnArtznrSuchen.BackColor = System.Drawing.Color.Transparent;
            this.btnArtznrSuchen.Image = ((System.Drawing.Image)(resources.GetObject("btnArtznrSuchen.Image")));
            this.btnArtznrSuchen.Location = new System.Drawing.Point(242, 16);
            this.btnArtznrSuchen.Name = "btnArtznrSuchen";
            this.btnArtznrSuchen.Size = new System.Drawing.Size(24, 23);
            this.btnArtznrSuchen.TabIndex = 39;
            this.btnArtznrSuchen.TabStop = false;
            this.btnArtznrSuchen.UseVisualStyleBackColor = false;
            this.btnArtznrSuchen.Visible = false;
            this.btnArtznrSuchen.Click += new System.EventHandler(this.btnArtznrSuchen_Click);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(13, 43);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(61, 13);
            this.label43.TabIndex = 38;
            this.label43.Text = "Image-Liste";
            // 
            // lstImages
            // 
            this.lstImages.FormattingEnabled = true;
            this.lstImages.Location = new System.Drawing.Point(12, 59);
            this.lstImages.Name = "lstImages";
            this.lstImages.Size = new System.Drawing.Size(167, 160);
            this.lstImages.TabIndex = 37;
            this.lstImages.SelectedIndexChanged += new System.EventHandler(this.lstImages_SelectedIndexChanged);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(269, 42);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(44, 13);
            this.label41.TabIndex = 35;
            this.label41.Text = "RechNr";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(269, 2);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(36, 13);
            this.label40.TabIndex = 34;
            this.label40.Text = "Image";
            // 
            // rechTB
            // 
            this.rechTB.Location = new System.Drawing.Point(268, 59);
            this.rechTB.Name = "rechTB";
            this.rechTB.Size = new System.Drawing.Size(69, 20);
            this.rechTB.TabIndex = 25;
            // 
            // imgTB
            // 
            this.imgTB.Location = new System.Drawing.Point(268, 18);
            this.imgTB.Name = "imgTB";
            this.imgTB.Size = new System.Drawing.Size(96, 20);
            this.imgTB.TabIndex = 24;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(174, 1);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(62, 13);
            this.label38.TabIndex = 33;
            this.label38.Text = "Arztnummer";
            // 
            // vkBisTB
            // 
            this.vkBisTB.Location = new System.Drawing.Point(173, 18);
            this.vkBisTB.Mask = "999999999";
            this.vkBisTB.Name = "vkBisTB";
            this.vkBisTB.ResetOnPrompt = false;
            this.vkBisTB.ResetOnSpace = false;
            this.vkBisTB.Size = new System.Drawing.Size(69, 20);
            this.vkBisTB.TabIndex = 23;
            this.vkBisTB.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.vkBisTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            // 
            // patientenSuchenBtn
            // 
            this.patientenSuchenBtn.BackColor = System.Drawing.Color.Transparent;
            this.patientenSuchenBtn.Image = ((System.Drawing.Image)(resources.GetObject("patientenSuchenBtn.Image")));
            this.patientenSuchenBtn.Location = new System.Drawing.Point(756, 15);
            this.patientenSuchenBtn.Name = "patientenSuchenBtn";
            this.patientenSuchenBtn.Size = new System.Drawing.Size(24, 23);
            this.patientenSuchenBtn.TabIndex = 2;
            this.patientenSuchenBtn.TabStop = false;
            this.patientenSuchenBtn.UseVisualStyleBackColor = false;
            this.patientenSuchenBtn.Visible = false;
            this.patientenSuchenBtn.Click += new System.EventHandler(this.patientenSuchenBtn_Click);
            // 
            // arztNummerTB
            // 
            this.arztNummerTB.Location = new System.Drawing.Point(98, 18);
            this.arztNummerTB.Mask = "000000000";
            this.arztNummerTB.Name = "arztNummerTB";
            this.arztNummerTB.ResetOnPrompt = false;
            this.arztNummerTB.ResetOnSpace = false;
            this.arztNummerTB.Size = new System.Drawing.Size(69, 20);
            this.arztNummerTB.TabIndex = 22;
            this.arztNummerTB.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.arztNummerTB.Validating += new System.ComponentModel.CancelEventHandler(this.arztNummerTB_Validating);
            this.arztNummerTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            // 
            // rezeptDatumMTB
            // 
            this.rezeptDatumMTB.Location = new System.Drawing.Point(12, 18);
            this.rezeptDatumMTB.Mask = "00/00/0000";
            this.rezeptDatumMTB.Name = "rezeptDatumMTB";
            this.rezeptDatumMTB.Size = new System.Drawing.Size(80, 20);
            this.rezeptDatumMTB.TabIndex = 14;
            this.rezeptDatumMTB.ValidatingType = typeof(System.DateTime);
            this.rezeptDatumMTB.Validating += new System.ComponentModel.CancelEventHandler(this.maskedTextBoxDate_Validating);
            this.rezeptDatumMTB.Validated += new System.EventHandler(this.geburtsDatumMTB_Validated);
            this.rezeptDatumMTB.Enter += new System.EventHandler(this.jahrVorgabeMTB_Enter);
            this.rezeptDatumMTB.TextChanged += new System.EventHandler(this.datumMTB_TextChanged);
            // 
            // geburtsDatumMTB
            // 
            this.geburtsDatumMTB.Location = new System.Drawing.Point(526, 134);
            this.geburtsDatumMTB.Mask = "00/00/0000";
            this.geburtsDatumMTB.Name = "geburtsDatumMTB";
            this.geburtsDatumMTB.Size = new System.Drawing.Size(82, 20);
            this.geburtsDatumMTB.TabIndex = 12;
            this.geburtsDatumMTB.ValidatingType = typeof(System.DateTime);
            this.geburtsDatumMTB.Visible = false;
            this.geburtsDatumMTB.Validating += new System.ComponentModel.CancelEventHandler(this.geburtsDatumMTB_Validating);
            this.geburtsDatumMTB.Validated += new System.EventHandler(this.geburtsDatumMTB_Validated);
            this.geburtsDatumMTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            // 
            // genVonMTB
            // 
            this.genVonMTB.Location = new System.Drawing.Point(185, 134);
            this.genVonMTB.Mask = "00/00/0000";
            this.genVonMTB.Name = "genVonMTB";
            this.genVonMTB.Size = new System.Drawing.Size(69, 20);
            this.genVonMTB.TabIndex = 28;
            this.genVonMTB.ValidatingType = typeof(System.DateTime);
            this.genVonMTB.Visible = false;
            this.genVonMTB.Validating += new System.ComponentModel.CancelEventHandler(this.genVonTB_Validating);
            this.genVonMTB.Enter += new System.EventHandler(this.jahrVorgabeMTB_Enter);
            this.genVonMTB.TextChanged += new System.EventHandler(this.datumMTB_TextChanged);
            // 
            // plzMTB
            // 
            this.plzMTB.Location = new System.Drawing.Point(614, 134);
            this.plzMTB.Mask = "00000";
            this.plzMTB.Name = "plzMTB";
            this.plzMTB.ResetOnSpace = false;
            this.plzMTB.Size = new System.Drawing.Size(39, 20);
            this.plzMTB.TabIndex = 8;
            this.plzMTB.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.plzMTB.Visible = false;
            this.plzMTB.Validating += new System.ComponentModel.CancelEventHandler(this.plzTB_Validating);
            this.plzMTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            this.plzMTB.TextChanged += new System.EventHandler(this.plzMTB_TextChanged);
            // 
            // versichertenNummerMTB
            // 
            this.versichertenNummerMTB.Location = new System.Drawing.Point(401, 134);
            this.versichertenNummerMTB.Mask = "000000000000";
            this.versichertenNummerMTB.Name = "versichertenNummerMTB";
            this.versichertenNummerMTB.ResetOnPrompt = false;
            this.versichertenNummerMTB.ResetOnSpace = false;
            this.versichertenNummerMTB.Size = new System.Drawing.Size(121, 20);
            this.versichertenNummerMTB.TabIndex = 16;
            this.versichertenNummerMTB.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.versichertenNummerMTB.Visible = false;
            this.versichertenNummerMTB.Validating += new System.ComponentModel.CancelEventHandler(this.versichertenNummerMTB_Validating);
            this.versichertenNummerMTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            // 
            // befreitCheckBox
            // 
            this.befreitCheckBox.AutoSize = true;
            this.befreitCheckBox.Location = new System.Drawing.Point(534, 21);
            this.befreitCheckBox.Name = "befreitCheckBox";
            this.befreitCheckBox.Size = new System.Drawing.Size(71, 17);
            this.befreitCheckBox.TabIndex = 31;
            this.befreitCheckBox.TabStop = false;
            this.befreitCheckBox.Text = "Befreit F5";
            this.befreitCheckBox.UseVisualStyleBackColor = true;
            this.befreitCheckBox.Visible = false;
            this.befreitCheckBox.Click += new System.EventHandler(this.befreitCheckBox_Click);
            this.befreitCheckBox.CheckedChanged += new System.EventHandler(this.befreitCheckBox_CheckedChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(257, 118);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(66, 13);
            this.label15.TabIndex = 29;
            this.label15.Text = "Gen. Kennz.";
            this.label15.Visible = false;
            // 
            // genKennzTB
            // 
            this.genKennzTB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.genKennzTB.Location = new System.Drawing.Point(260, 134);
            this.genKennzTB.MaxLength = 20;
            this.genKennzTB.Name = "genKennzTB";
            this.genKennzTB.Size = new System.Drawing.Size(135, 20);
            this.genKennzTB.TabIndex = 30;
            this.genKennzTB.Visible = false;
            this.genKennzTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(182, 118);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(51, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "Gen. von";
            this.label14.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(95, 2);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 13);
            this.label13.TabIndex = 21;
            this.label13.Text = "Betriebsst. Nr.";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 2);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(70, 13);
            this.label12.TabIndex = 13;
            this.label12.Text = "Rezeptdatum";
            // 
            // status2TB
            // 
            this.status2TB.Location = new System.Drawing.Point(585, 56);
            this.status2TB.MaxLength = 1;
            this.status2TB.Name = "status2TB";
            this.status2TB.Size = new System.Drawing.Size(22, 20);
            this.status2TB.TabIndex = 20;
            this.status2TB.Visible = false;
            this.status2TB.TextChanged += new System.EventHandler(this.status2TB_TextChanged);
            this.status2TB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            this.status2TB.Validating += new System.ComponentModel.CancelEventHandler(this.status2TB_Validating);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(551, 59);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(28, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "XXX";
            this.label11.Visible = false;
            // 
            // status1TB
            // 
            this.status1TB.Location = new System.Drawing.Point(525, 56);
            this.status1TB.MaxLength = 1;
            this.status1TB.Name = "status1TB";
            this.status1TB.Size = new System.Drawing.Size(19, 20);
            this.status1TB.TabIndex = 18;
            this.status1TB.Visible = false;
            this.status1TB.TextChanged += new System.EventHandler(this.status1TB_TextChanged);
            this.status1TB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            this.status1TB.Validating += new System.ComponentModel.CancelEventHandler(this.status1TB_Validating);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(522, 40);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Status";
            this.label10.Visible = false;
            // 
            // rezeptArtCB
            // 
            this.rezeptArtCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.rezeptArtCB.FormattingEnabled = true;
            this.rezeptArtCB.Location = new System.Drawing.Point(229, 94);
            this.rezeptArtCB.Name = "rezeptArtCB";
            this.rezeptArtCB.Size = new System.Drawing.Size(132, 21);
            this.rezeptArtCB.TabIndex = 24;
            this.rezeptArtCB.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(226, 77);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 23;
            this.label9.Text = "Rezeptart";
            this.label9.Visible = false;
            // 
            // indikationTB
            // 
            this.indikationTB.Location = new System.Drawing.Point(12, 95);
            this.indikationTB.MaxLength = 10;
            this.indikationTB.Name = "indikationTB";
            this.indikationTB.Size = new System.Drawing.Size(211, 20);
            this.indikationTB.TabIndex = 26;
            this.indikationTB.Visible = false;
            this.indikationTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 79);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 13);
            this.label8.TabIndex = 25;
            this.label8.Text = "Indikation";
            this.label8.Visible = false;
            // 
            // ortTB
            // 
            this.ortTB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.ortTB.Location = new System.Drawing.Point(659, 134);
            this.ortTB.MaxLength = 25;
            this.ortTB.Name = "ortTB";
            this.ortTB.Size = new System.Drawing.Size(121, 20);
            this.ortTB.TabIndex = 10;
            this.ortTB.Visible = false;
            this.ortTB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ortTB_KeyDown);
            this.ortTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(659, 118);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Ort";
            this.label7.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(610, 118);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "PLZ";
            this.label6.Visible = false;
            // 
            // strasseTB
            // 
            this.strasseTB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.strasseTB.Location = new System.Drawing.Point(613, 95);
            this.strasseTB.MaxLength = 30;
            this.strasseTB.Name = "strasseTB";
            this.strasseTB.Size = new System.Drawing.Size(167, 20);
            this.strasseTB.TabIndex = 6;
            this.strasseTB.Visible = false;
            this.strasseTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(610, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Strasse";
            this.label5.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(524, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Geburtsdatum";
            this.label4.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(398, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Versichertennummer";
            this.label3.Visible = false;
            // 
            // vornameTB
            // 
            this.vornameTB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.vornameTB.Location = new System.Drawing.Point(613, 56);
            this.vornameTB.MaxLength = 30;
            this.vornameTB.Name = "vornameTB";
            this.vornameTB.Size = new System.Drawing.Size(167, 20);
            this.vornameTB.TabIndex = 4;
            this.vornameTB.Visible = false;
            this.vornameTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(611, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Vorname";
            this.label2.Visible = false;
            // 
            // nameTB
            // 
            this.nameTB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.nameTB.Location = new System.Drawing.Point(613, 17);
            this.nameTB.MaxLength = 47;
            this.nameTB.Name = "nameTB";
            this.nameTB.Size = new System.Drawing.Size(137, 20);
            this.nameTB.TabIndex = 1;
            this.nameTB.Visible = false;
            this.nameTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(610, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            this.label1.Visible = false;
            // 
            // pnlKostenTaeger
            // 
            this.pnlKostenTaeger.BackColor = System.Drawing.Color.Transparent;
            this.pnlKostenTaeger.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlKostenTaeger.Controls.Add(this.pnlRechnungsOptionenPlaceholderRight);
            this.pnlKostenTaeger.Controls.Add(this.pnlRechNrOptionen);
            this.pnlKostenTaeger.Controls.Add(this.pnlSuchen);
            this.pnlKostenTaeger.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlKostenTaeger.Enabled = false;
            this.pnlKostenTaeger.Location = new System.Drawing.Point(0, 50);
            this.pnlKostenTaeger.Name = "pnlKostenTaeger";
            this.pnlKostenTaeger.Size = new System.Drawing.Size(791, 43);
            this.pnlKostenTaeger.TabIndex = 6;
            // 
            // pnlRechnungsOptionenPlaceholderRight
            // 
            this.pnlRechnungsOptionenPlaceholderRight.Controls.Add(this.nameLabel);
            this.pnlRechnungsOptionenPlaceholderRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlRechnungsOptionenPlaceholderRight.Location = new System.Drawing.Point(469, 0);
            this.pnlRechnungsOptionenPlaceholderRight.Name = "pnlRechnungsOptionenPlaceholderRight";
            this.pnlRechnungsOptionenPlaceholderRight.Size = new System.Drawing.Size(318, 39);
            this.pnlRechnungsOptionenPlaceholderRight.TabIndex = 10;
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.Location = new System.Drawing.Point(6, 10);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(0, 20);
            this.nameLabel.TabIndex = 3;
            // 
            // pnlRechNrOptionen
            // 
            this.pnlRechNrOptionen.AutoSize = true;
            this.pnlRechNrOptionen.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlRechNrOptionen.Controls.Add(this.einzelnCB);
            this.pnlRechNrOptionen.Controls.Add(this.rechNrTB);
            this.pnlRechNrOptionen.Controls.Add(this.imageTB);
            this.pnlRechNrOptionen.Controls.Add(this.label17);
            this.pnlRechNrOptionen.Controls.Add(this.label16);
            this.pnlRechNrOptionen.Controls.Add(this.mitRechNrCB);
            this.pnlRechNrOptionen.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlRechNrOptionen.Location = new System.Drawing.Point(165, 0);
            this.pnlRechNrOptionen.Name = "pnlRechNrOptionen";
            this.pnlRechNrOptionen.Size = new System.Drawing.Size(304, 39);
            this.pnlRechNrOptionen.TabIndex = 9;
            // 
            // einzelnCB
            // 
            this.einzelnCB.AutoSize = true;
            this.einzelnCB.Location = new System.Drawing.Point(3, 20);
            this.einzelnCB.Name = "einzelnCB";
            this.einzelnCB.Size = new System.Drawing.Size(122, 17);
            this.einzelnCB.TabIndex = 13;
            this.einzelnCB.TabStop = false;
            this.einzelnCB.Text = "mit Rech.Nr. Einzeln";
            this.einzelnCB.UseVisualStyleBackColor = true;
            this.einzelnCB.Click += new System.EventHandler(this.einzelnCB_Click);
            // 
            // rechNrTB
            // 
            this.rechNrTB.Location = new System.Drawing.Point(218, 16);
            this.rechNrTB.MaxLength = 12;
            this.rechNrTB.Name = "rechNrTB";
            this.rechNrTB.Size = new System.Drawing.Size(83, 20);
            this.rechNrTB.TabIndex = 12;
            this.rechNrTB.Visible = false;
            this.rechNrTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            // 
            // imageTB
            // 
            this.imageTB.Location = new System.Drawing.Point(129, 16);
            this.imageTB.MaxLength = 50;
            this.imageTB.Name = "imageTB";
            this.imageTB.Size = new System.Drawing.Size(83, 20);
            this.imageTB.TabIndex = 11;
            this.imageTB.Visible = false;
            this.imageTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(215, 1);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(47, 13);
            this.label17.TabIndex = 10;
            this.label17.Text = "Rech.Nr";
            this.label17.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(126, 1);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 13);
            this.label16.TabIndex = 9;
            this.label16.Text = "Auftrag";
            this.label16.Visible = false;
            // 
            // mitRechNrCB
            // 
            this.mitRechNrCB.AutoSize = true;
            this.mitRechNrCB.Location = new System.Drawing.Point(3, 1);
            this.mitRechNrCB.Name = "mitRechNrCB";
            this.mitRechNrCB.Size = new System.Drawing.Size(125, 17);
            this.mitRechNrCB.TabIndex = 8;
            this.mitRechNrCB.TabStop = false;
            this.mitRechNrCB.Text = "mit Rech.Nr. Sammel";
            this.mitRechNrCB.UseVisualStyleBackColor = true;
            this.mitRechNrCB.Click += new System.EventHandler(this.mitRechNrCB_Click);
            // 
            // pnlSuchen
            // 
            this.pnlSuchen.Controls.Add(this.searchBtn);
            this.pnlSuchen.Controls.Add(this.ikTB);
            this.pnlSuchen.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlSuchen.Location = new System.Drawing.Point(0, 0);
            this.pnlSuchen.Name = "pnlSuchen";
            this.pnlSuchen.Size = new System.Drawing.Size(165, 39);
            this.pnlSuchen.TabIndex = 8;
            // 
            // searchBtn
            // 
            this.searchBtn.Location = new System.Drawing.Point(84, 8);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(75, 23);
            this.searchBtn.TabIndex = 3;
            this.searchBtn.TabStop = false;
            this.searchBtn.Text = "Suchen";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.searchBtn_Click);
            // 
            // ikTB
            // 
            this.ikTB.Location = new System.Drawing.Point(3, 10);
            this.ikTB.MaxLength = 9;
            this.ikTB.Name = "ikTB";
            this.ikTB.Size = new System.Drawing.Size(75, 20);
            this.ikTB.TabIndex = 2;
            this.ikTB.TextChanged += new System.EventHandler(this.ikTB_TextChanged);
            this.ikTB.Validating += new System.ComponentModel.CancelEventHandler(this.ikTB_Validating);
            // 
            // topPanel
            // 
            this.topPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.topPanel.Controls.Add(this.imgCB);
            this.topPanel.Controls.Add(this.axListLabel1);
            this.topPanel.Controls.Add(this.tarifSchlusselAbrechnCodeTB);
            this.topPanel.Controls.Add(this.erfasstTB);
            this.topPanel.Controls.Add(this.label19);
            this.topPanel.Controls.Add(this.label18);
            this.topPanel.Controls.Add(this.panel2);
            this.topPanel.Controls.Add(this.nrComboBox);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(791, 50);
            this.topPanel.TabIndex = 0;
            // 
            // imgCB
            // 
            this.imgCB.AutoSize = true;
            this.imgCB.Checked = true;
            this.imgCB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.imgCB.Location = new System.Drawing.Point(265, 25);
            this.imgCB.Name = "imgCB";
            this.imgCB.Size = new System.Drawing.Size(55, 17);
            this.imgCB.TabIndex = 10;
            this.imgCB.Text = "Image";
            this.imgCB.UseVisualStyleBackColor = true;
            this.imgCB.Click += new System.EventHandler(this.imgCB_Click);
            // 
            // axListLabel1
            // 
            this.axListLabel1.Location = new System.Drawing.Point(390, 13);
            this.axListLabel1.Margin = new System.Windows.Forms.Padding(4);
            this.axListLabel1.Name = "axListLabel1";
            this.axListLabel1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axListLabel1.OcxState")));
            this.axListLabel1.Size = new System.Drawing.Size(25, 25);
            this.axListLabel1.TabIndex = 9;
            // 
            // tarifSchlusselAbrechnCodeTB
            // 
            this.tarifSchlusselAbrechnCodeTB.Location = new System.Drawing.Point(84, 24);
            this.tarifSchlusselAbrechnCodeTB.Name = "tarifSchlusselAbrechnCodeTB";
            this.tarifSchlusselAbrechnCodeTB.ReadOnly = true;
            this.tarifSchlusselAbrechnCodeTB.Size = new System.Drawing.Size(91, 20);
            this.tarifSchlusselAbrechnCodeTB.TabIndex = 8;
            this.tarifSchlusselAbrechnCodeTB.TabStop = false;
            // 
            // erfasstTB
            // 
            this.erfasstTB.Enabled = false;
            this.erfasstTB.Location = new System.Drawing.Point(183, 24);
            this.erfasstTB.Mask = "00/00/0000";
            this.erfasstTB.Name = "erfasstTB";
            this.erfasstTB.Size = new System.Drawing.Size(76, 20);
            this.erfasstTB.TabIndex = 7;
            this.erfasstTB.ValidatingType = typeof(System.DateTime);
            this.erfasstTB.Enter += new System.EventHandler(this.selectAllTB_Enter);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(84, 6);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(75, 13);
            this.label19.TabIndex = 6;
            this.label19.Text = "Abrechn.Code";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(179, 6);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 13);
            this.label18.TabIndex = 5;
            this.label18.Text = "erfasst am";
            // 
            // panel2
            // 
            this.panel2.AutoSize = true;
            this.panel2.Controls.Add(this.name2Label);
            this.panel2.Controls.Add(this.name1Label);
            this.panel2.Location = new System.Drawing.Point(326, 17);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(57, 31);
            this.panel2.TabIndex = 4;
            // 
            // name2Label
            // 
            this.name2Label.AutoSize = true;
            this.name2Label.Dock = System.Windows.Forms.DockStyle.Left;
            this.name2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name2Label.Location = new System.Drawing.Point(0, 0);
            this.name2Label.Name = "name2Label";
            this.name2Label.Size = new System.Drawing.Size(0, 20);
            this.name2Label.TabIndex = 1;
            // 
            // name1Label
            // 
            this.name1Label.AutoSize = true;
            this.name1Label.Dock = System.Windows.Forms.DockStyle.Left;
            this.name1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name1Label.Location = new System.Drawing.Point(0, 0);
            this.name1Label.Name = "name1Label";
            this.name1Label.Size = new System.Drawing.Size(0, 20);
            this.name1Label.TabIndex = 0;
            // 
            // nrComboBox
            // 
            this.nrComboBox.DataSource = this.mandantenBindingSource;
            this.nrComboBox.DisplayMember = "Nr";
            this.nrComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.nrComboBox.Enabled = false;
            this.nrComboBox.FormattingEnabled = true;
            this.nrComboBox.Location = new System.Drawing.Point(3, 22);
            this.nrComboBox.Name = "nrComboBox";
            this.nrComboBox.Size = new System.Drawing.Size(75, 21);
            this.nrComboBox.TabIndex = 0;
            this.nrComboBox.ValueMember = "Nr";
            this.nrComboBox.Enter += new System.EventHandler(this.nrComboBox_Enter);
            // 
            // mandantenBindingSource
            // 
            this.mandantenBindingSource.DataMember = "Mandanten";
            this.mandantenBindingSource.DataSource = this.voinDataSet;
            this.mandantenBindingSource.CurrentChanged += new System.EventHandler(this.mandantenBindingSource_CurrentChanged);
            // 
            // voinDataSet
            // 
            this.voinDataSet.DataSetName = "VOINDataSet";
            this.voinDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // positionenBindingSource
            // 
            this.positionenBindingSource.CurrentChanged += new System.EventHandler(this.positionenBindingSource_CurrentChanged);
            // 
            // mandantenSelectCommand
            // 
            this.mandantenSelectCommand.CommandText = resources.GetString("mandantenSelectCommand.CommandText");
            this.mandantenSelectCommand.Connection = this.stamm4SqlConnection;
            // 
            // stamm4SqlConnection
            // 
            this.stamm4SqlConnection.ConnectionString = "Data Source=SWS2;Initial Catalog=M-FAKT;User ID=sa";
            this.stamm4SqlConnection.FireInfoMessageEventOnUserErrors = false;
            // 
            // mandantenDA
            // 
            this.mandantenDA.DeleteCommand = this.mandantenDeleteCommand;
            this.mandantenDA.SelectCommand = this.mandantenSelectCommand;
            this.mandantenDA.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Mandanten", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("Nr", "Nr"),
                        new System.Data.Common.DataColumnMapping("IK", "IK"),
                        new System.Data.Common.DataColumnMapping("Name1", "Name1"),
                        new System.Data.Common.DataColumnMapping("Name2", "Name2"),
                        new System.Data.Common.DataColumnMapping("Name3", "Name3"),
                        new System.Data.Common.DataColumnMapping("Vorname", "Vorname"),
                        new System.Data.Common.DataColumnMapping("Straße", "Straße"),
                        new System.Data.Common.DataColumnMapping("PLZ", "PLZ"),
                        new System.Data.Common.DataColumnMapping("Ort", "Ort"),
                        new System.Data.Common.DataColumnMapping("Tel", "Tel"),
                        new System.Data.Common.DataColumnMapping("Fax", "Fax"),
                        new System.Data.Common.DataColumnMapping("EMail", "EMail"),
                        new System.Data.Common.DataColumnMapping("Beginn", "Beginn"),
                        new System.Data.Common.DataColumnMapping("Ende", "Ende"),
                        new System.Data.Common.DataColumnMapping("Beh_Kontotext", "Beh_Kontotext"),
                        new System.Data.Common.DataColumnMapping("Beh_Bank", "Beh_Bank"),
                        new System.Data.Common.DataColumnMapping("Beh_BLZ", "Beh_BLZ"),
                        new System.Data.Common.DataColumnMapping("Beh_KontoNr", "Beh_KontoNr"),
                        new System.Data.Common.DataColumnMapping("Beh_Verw", "Beh_Verw"),
                        new System.Data.Common.DataColumnMapping("VerbArt", "VerbArt"),
                        new System.Data.Common.DataColumnMapping("PfArt", "PfArt"),
                        new System.Data.Common.DataColumnMapping("PfZahlung", "PfZahlung"),
                        new System.Data.Common.DataColumnMapping("Pfvon", "Pfvon"),
                        new System.Data.Common.DataColumnMapping("Pfbis", "Pfbis"),
                        new System.Data.Common.DataColumnMapping("PfGesamtSumme", "PfGesamtSumme"),
                        new System.Data.Common.DataColumnMapping("PffesterBetrag", "PffesterBetrag"),
                        new System.Data.Common.DataColumnMapping("PfProzent", "PfProzent"),
                        new System.Data.Common.DataColumnMapping("PfMin", "PfMin"),
                        new System.Data.Common.DataColumnMapping("PfMax", "PfMax"),
                        new System.Data.Common.DataColumnMapping("Beh_AbtrText", "Beh_AbtrText"),
                        new System.Data.Common.DataColumnMapping("Beh_AbtrBank", "Beh_AbtrBank"),
                        new System.Data.Common.DataColumnMapping("Beh_AbtrBLZ", "Beh_AbtrBLZ"),
                        new System.Data.Common.DataColumnMapping("Beh_AbtrKntNr", "Beh_AbtrKntNr"),
                        new System.Data.Common.DataColumnMapping("Beh_AbtrVerw", "Beh_AbtrVerw"),
                        new System.Data.Common.DataColumnMapping("Beh_AbtrName1", "Beh_AbtrName1"),
                        new System.Data.Common.DataColumnMapping("Beh_AbtrName2", "Beh_AbtrName2"),
                        new System.Data.Common.DataColumnMapping("Beh_AbtrName3", "Beh_AbtrName3"),
                        new System.Data.Common.DataColumnMapping("Beh_AbtrStrasse", "Beh_AbtrStrasse"),
                        new System.Data.Common.DataColumnMapping("Beh_AbtrPLZ", "Beh_AbtrPLZ"),
                        new System.Data.Common.DataColumnMapping("Beh_AbtrOrt", "Beh_AbtrOrt"),
                        new System.Data.Common.DataColumnMapping("Beh_Berufsgrp", "Beh_Berufsgrp"),
                        new System.Data.Common.DataColumnMapping("Beh_Betriebsart", "Beh_Betriebsart"),
                        new System.Data.Common.DataColumnMapping("VerwTage", "VerwTage"),
                        new System.Data.Common.DataColumnMapping("VerwAbrSofort", "VerwAbrSofort"),
                        new System.Data.Common.DataColumnMapping("Beh_DarlKntNr", "Beh_DarlKntNr"),
                        new System.Data.Common.DataColumnMapping("Beh_Darlsatz", "Beh_Darlsatz"),
                        new System.Data.Common.DataColumnMapping("Beh_DarlGrenze", "Beh_DarlGrenze"),
                        new System.Data.Common.DataColumnMapping("Beh_Darlgespart", "Beh_Darlgespart"),
                        new System.Data.Common.DataColumnMapping("Beh_Darlausgez", "Beh_Darlausgez"),
                        new System.Data.Common.DataColumnMapping("Beh_Einzugam", "Beh_Einzugam"),
                        new System.Data.Common.DataColumnMapping("Beh_Einzug", "Beh_Einzug"),
                        new System.Data.Common.DataColumnMapping("letzte Änderung", "letzte Änderung"),
                        new System.Data.Common.DataColumnMapping("Bearbeiter", "Bearbeiter"),
                        new System.Data.Common.DataColumnMapping("Bemerkungen", "Bemerkungen"),
                        new System.Data.Common.DataColumnMapping("MWSt", "MWSt"),
                        new System.Data.Common.DataColumnMapping("Tarifkennzeichen", "Tarifkennzeichen"),
                        new System.Data.Common.DataColumnMapping("Saldovortrag", "Saldovortrag"),
                        new System.Data.Common.DataColumnMapping("Festabzug", "Festabzug"),
                        new System.Data.Common.DataColumnMapping("Festabzug_Anz", "Festabzug_Anz"),
                        new System.Data.Common.DataColumnMapping("Inkasso", "Inkasso"),
                        new System.Data.Common.DataColumnMapping("Kopf1", "Kopf1"),
                        new System.Data.Common.DataColumnMapping("Kopf2", "Kopf2"),
                        new System.Data.Common.DataColumnMapping("Kopf3", "Kopf3"),
                        new System.Data.Common.DataColumnMapping("UStidnr", "UStidnr"),
                        new System.Data.Common.DataColumnMapping("stnr", "stnr"),
                        new System.Data.Common.DataColumnMapping("inet", "inet"),
                        new System.Data.Common.DataColumnMapping("zusatz1", "zusatz1"),
                        new System.Data.Common.DataColumnMapping("zusatz2", "zusatz2"),
                        new System.Data.Common.DataColumnMapping("Fusszeile", "Fusszeile"),
                        new System.Data.Common.DataColumnMapping("AbrIK", "AbrIK"),
                        new System.Data.Common.DataColumnMapping("Zuzrech", "Zuzrech"),
                        new System.Data.Common.DataColumnMapping("Zahlungsziel_Tage", "Zahlungsziel_Tage")})});
            this.mandantenDA.UpdateCommand = this.mandantenUpdateCommand;
            // 
            // mandantenDeleteCommand
            // 
            this.mandantenDeleteCommand.CommandText = resources.GetString("mandantenDeleteCommand.CommandText");
            this.mandantenDeleteCommand.Connection = this.stamm4SqlConnection;
            this.mandantenDeleteCommand.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Original_Nr", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Nr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_IK", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "IK", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Name1", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Name1", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Name2", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Name2", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Name3", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Name3", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Name3", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Name3", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Vorname", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Vorname", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Straße", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Straße", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_PLZ", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "PLZ", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Ort", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Ort", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Tel", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Tel", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Fax", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Fax", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Fax", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Fax", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_EMail", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "EMail", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_EMail", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "EMail", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beginn", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beginn", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beginn", System.Data.SqlDbType.SmallDateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beginn", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Ende", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Ende", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Ende", System.Data.SqlDbType.SmallDateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Ende", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_Kontotext", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_Kontotext", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_Kontotext", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_Kontotext", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_Bank", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_Bank", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_Bank", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_Bank", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_BLZ", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_BLZ", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_BLZ", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_BLZ", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_KontoNr", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_KontoNr", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_KontoNr", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_KontoNr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_Verw", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_Verw", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_Verw", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_Verw", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_VerbArt", System.Data.SqlDbType.TinyInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "VerbArt", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_PfArt", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "PfArt", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_PfArt", System.Data.SqlDbType.TinyInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "PfArt", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_PfZahlung", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "PfZahlung", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_PfZahlung", System.Data.SqlDbType.TinyInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "PfZahlung", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Pfvon", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Pfvon", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Pfvon", System.Data.SqlDbType.SmallDateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Pfvon", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Pfbis", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Pfbis", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Pfbis", System.Data.SqlDbType.SmallDateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Pfbis", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_PfGesamtSumme", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "PfGesamtSumme", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_PfGesamtSumme", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "PfGesamtSumme", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_PffesterBetrag", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "PffesterBetrag", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_PffesterBetrag", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "PffesterBetrag", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_PfProzent", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "PfProzent", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_PfProzent", System.Data.SqlDbType.Real, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "PfProzent", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_PfMin", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "PfMin", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_PfMin", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "PfMin", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_PfMax", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "PfMax", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_PfMax", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "PfMax", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_AbtrText", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_AbtrText", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_AbtrText", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_AbtrText", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_AbtrBank", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_AbtrBank", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_AbtrBank", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_AbtrBank", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_AbtrBLZ", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_AbtrBLZ", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_AbtrBLZ", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_AbtrBLZ", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_AbtrKntNr", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_AbtrKntNr", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_AbtrKntNr", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_AbtrKntNr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_AbtrVerw", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_AbtrVerw", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_AbtrVerw", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_AbtrVerw", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_AbtrName1", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_AbtrName1", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_AbtrName1", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_AbtrName1", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_AbtrName2", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_AbtrName2", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_AbtrName2", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_AbtrName2", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_AbtrName3", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_AbtrName3", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_AbtrName3", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_AbtrName3", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_AbtrStrasse", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_AbtrStrasse", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_AbtrStrasse", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_AbtrStrasse", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_AbtrPLZ", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_AbtrPLZ", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_AbtrPLZ", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_AbtrPLZ", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_AbtrOrt", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_AbtrOrt", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_AbtrOrt", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_AbtrOrt", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_Berufsgrp", System.Data.SqlDbType.TinyInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_Berufsgrp", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_Betriebsart", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_Betriebsart", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_Betriebsart", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_Betriebsart", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_VerwTage", System.Data.SqlDbType.TinyInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "VerwTage", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_VerwAbrSofort", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "VerwAbrSofort", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_DarlKntNr", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_DarlKntNr", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_DarlKntNr", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_DarlKntNr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_Darlsatz", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_Darlsatz", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_Darlsatz", System.Data.SqlDbType.Real, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_Darlsatz", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_DarlGrenze", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_DarlGrenze", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_DarlGrenze", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_DarlGrenze", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_Darlgespart", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_Darlgespart", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_Darlgespart", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_Darlgespart", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_Darlausgez", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_Darlausgez", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_Darlausgez", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_Darlausgez", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_Einzugam", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_Einzugam", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_Einzugam", System.Data.SqlDbType.SmallDateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_Einzugam", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_Einzug", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_Einzug", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_Einzug", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_Einzug", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_letzte_Änderung", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "letzte Änderung", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_letzte_Änderung", System.Data.SqlDbType.SmallDateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "letzte Änderung", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Bearbeiter", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Bearbeiter", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Bearbeiter", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Bearbeiter", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_MWSt", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "MWSt", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Tarifkennzeichen", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Tarifkennzeichen", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Saldovortrag", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Saldovortrag", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Festabzug", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Festabzug", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Festabzug_Anz", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Festabzug_Anz", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Inkasso", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Inkasso", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Kopf1", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Kopf1", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Kopf1", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Kopf1", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Kopf2", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Kopf2", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Kopf2", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Kopf2", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Kopf3", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Kopf3", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Kopf3", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Kopf3", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_UStidnr", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "UStidnr", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_UStidnr", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "UStidnr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_stnr", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "stnr", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_stnr", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "stnr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_inet", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "inet", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_inet", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "inet", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_zusatz1", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "zusatz1", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_zusatz1", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "zusatz1", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_zusatz2", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "zusatz2", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_zusatz2", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "zusatz2", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Fusszeile", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Fusszeile", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Fusszeile", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Fusszeile", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_AbrIK", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "AbrIK", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Zuzrech", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Zuzrech", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Zuzrech", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Zuzrech", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Zahlungsziel_Tage", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Zahlungsziel_Tage", System.Data.DataRowVersion.Original, null)});
            // 
            // mandantenUpdateCommand
            // 
            this.mandantenUpdateCommand.CommandText = resources.GetString("mandantenUpdateCommand.CommandText");
            this.mandantenUpdateCommand.Connection = this.stamm4SqlConnection;
            this.mandantenUpdateCommand.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Nr", System.Data.SqlDbType.Int, 0, "Nr"),
            new System.Data.SqlClient.SqlParameter("@IK", System.Data.SqlDbType.Int, 0, "IK"),
            new System.Data.SqlClient.SqlParameter("@Name1", System.Data.SqlDbType.NVarChar, 0, "Name1"),
            new System.Data.SqlClient.SqlParameter("@Name2", System.Data.SqlDbType.NVarChar, 0, "Name2"),
            new System.Data.SqlClient.SqlParameter("@Name3", System.Data.SqlDbType.NVarChar, 0, "Name3"),
            new System.Data.SqlClient.SqlParameter("@Vorname", System.Data.SqlDbType.NVarChar, 0, "Vorname"),
            new System.Data.SqlClient.SqlParameter("@Straße", System.Data.SqlDbType.NVarChar, 0, "Straße"),
            new System.Data.SqlClient.SqlParameter("@PLZ", System.Data.SqlDbType.NVarChar, 0, "PLZ"),
            new System.Data.SqlClient.SqlParameter("@Ort", System.Data.SqlDbType.NVarChar, 0, "Ort"),
            new System.Data.SqlClient.SqlParameter("@Tel", System.Data.SqlDbType.NVarChar, 0, "Tel"),
            new System.Data.SqlClient.SqlParameter("@Fax", System.Data.SqlDbType.NVarChar, 0, "Fax"),
            new System.Data.SqlClient.SqlParameter("@EMail", System.Data.SqlDbType.VarChar, 0, "EMail"),
            new System.Data.SqlClient.SqlParameter("@Beginn", System.Data.SqlDbType.SmallDateTime, 0, "Beginn"),
            new System.Data.SqlClient.SqlParameter("@Ende", System.Data.SqlDbType.SmallDateTime, 0, "Ende"),
            new System.Data.SqlClient.SqlParameter("@Beh_Kontotext", System.Data.SqlDbType.NVarChar, 0, "Beh_Kontotext"),
            new System.Data.SqlClient.SqlParameter("@Beh_Bank", System.Data.SqlDbType.NVarChar, 0, "Beh_Bank"),
            new System.Data.SqlClient.SqlParameter("@Beh_BLZ", System.Data.SqlDbType.NVarChar, 0, "Beh_BLZ"),
            new System.Data.SqlClient.SqlParameter("@Beh_KontoNr", System.Data.SqlDbType.NVarChar, 0, "Beh_KontoNr"),
            new System.Data.SqlClient.SqlParameter("@Beh_Verw", System.Data.SqlDbType.NVarChar, 0, "Beh_Verw"),
            new System.Data.SqlClient.SqlParameter("@VerbArt", System.Data.SqlDbType.TinyInt, 0, "VerbArt"),
            new System.Data.SqlClient.SqlParameter("@PfArt", System.Data.SqlDbType.TinyInt, 0, "PfArt"),
            new System.Data.SqlClient.SqlParameter("@PfZahlung", System.Data.SqlDbType.TinyInt, 0, "PfZahlung"),
            new System.Data.SqlClient.SqlParameter("@Pfvon", System.Data.SqlDbType.SmallDateTime, 0, "Pfvon"),
            new System.Data.SqlClient.SqlParameter("@Pfbis", System.Data.SqlDbType.SmallDateTime, 0, "Pfbis"),
            new System.Data.SqlClient.SqlParameter("@PfGesamtSumme", System.Data.SqlDbType.Money, 0, "PfGesamtSumme"),
            new System.Data.SqlClient.SqlParameter("@PffesterBetrag", System.Data.SqlDbType.Money, 0, "PffesterBetrag"),
            new System.Data.SqlClient.SqlParameter("@PfProzent", System.Data.SqlDbType.Real, 0, "PfProzent"),
            new System.Data.SqlClient.SqlParameter("@PfMin", System.Data.SqlDbType.Money, 0, "PfMin"),
            new System.Data.SqlClient.SqlParameter("@PfMax", System.Data.SqlDbType.Money, 0, "PfMax"),
            new System.Data.SqlClient.SqlParameter("@Beh_AbtrText", System.Data.SqlDbType.NVarChar, 0, "Beh_AbtrText"),
            new System.Data.SqlClient.SqlParameter("@Beh_AbtrBank", System.Data.SqlDbType.NVarChar, 0, "Beh_AbtrBank"),
            new System.Data.SqlClient.SqlParameter("@Beh_AbtrBLZ", System.Data.SqlDbType.NVarChar, 0, "Beh_AbtrBLZ"),
            new System.Data.SqlClient.SqlParameter("@Beh_AbtrKntNr", System.Data.SqlDbType.NVarChar, 0, "Beh_AbtrKntNr"),
            new System.Data.SqlClient.SqlParameter("@Beh_AbtrVerw", System.Data.SqlDbType.NVarChar, 0, "Beh_AbtrVerw"),
            new System.Data.SqlClient.SqlParameter("@Beh_AbtrName1", System.Data.SqlDbType.NVarChar, 0, "Beh_AbtrName1"),
            new System.Data.SqlClient.SqlParameter("@Beh_AbtrName2", System.Data.SqlDbType.NVarChar, 0, "Beh_AbtrName2"),
            new System.Data.SqlClient.SqlParameter("@Beh_AbtrName3", System.Data.SqlDbType.NVarChar, 0, "Beh_AbtrName3"),
            new System.Data.SqlClient.SqlParameter("@Beh_AbtrStrasse", System.Data.SqlDbType.NVarChar, 0, "Beh_AbtrStrasse"),
            new System.Data.SqlClient.SqlParameter("@Beh_AbtrPLZ", System.Data.SqlDbType.NVarChar, 0, "Beh_AbtrPLZ"),
            new System.Data.SqlClient.SqlParameter("@Beh_AbtrOrt", System.Data.SqlDbType.NVarChar, 0, "Beh_AbtrOrt"),
            new System.Data.SqlClient.SqlParameter("@Beh_Berufsgrp", System.Data.SqlDbType.TinyInt, 0, "Beh_Berufsgrp"),
            new System.Data.SqlClient.SqlParameter("@Beh_Betriebsart", System.Data.SqlDbType.NVarChar, 0, "Beh_Betriebsart"),
            new System.Data.SqlClient.SqlParameter("@VerwTage", System.Data.SqlDbType.TinyInt, 0, "VerwTage"),
            new System.Data.SqlClient.SqlParameter("@VerwAbrSofort", System.Data.SqlDbType.Bit, 0, "VerwAbrSofort"),
            new System.Data.SqlClient.SqlParameter("@Beh_DarlKntNr", System.Data.SqlDbType.NVarChar, 0, "Beh_DarlKntNr"),
            new System.Data.SqlClient.SqlParameter("@Beh_Darlsatz", System.Data.SqlDbType.Real, 0, "Beh_Darlsatz"),
            new System.Data.SqlClient.SqlParameter("@Beh_DarlGrenze", System.Data.SqlDbType.Money, 0, "Beh_DarlGrenze"),
            new System.Data.SqlClient.SqlParameter("@Beh_Darlgespart", System.Data.SqlDbType.Money, 0, "Beh_Darlgespart"),
            new System.Data.SqlClient.SqlParameter("@Beh_Darlausgez", System.Data.SqlDbType.Money, 0, "Beh_Darlausgez"),
            new System.Data.SqlClient.SqlParameter("@Beh_Einzugam", System.Data.SqlDbType.SmallDateTime, 0, "Beh_Einzugam"),
            new System.Data.SqlClient.SqlParameter("@Beh_Einzug", System.Data.SqlDbType.Money, 0, "Beh_Einzug"),
            new System.Data.SqlClient.SqlParameter("@letzte_Änderung", System.Data.SqlDbType.SmallDateTime, 0, "letzte Änderung"),
            new System.Data.SqlClient.SqlParameter("@Bearbeiter", System.Data.SqlDbType.NVarChar, 0, "Bearbeiter"),
            new System.Data.SqlClient.SqlParameter("@Bemerkungen", System.Data.SqlDbType.NText, 0, "Bemerkungen"),
            new System.Data.SqlClient.SqlParameter("@MWSt", System.Data.SqlDbType.Bit, 0, "MWSt"),
            new System.Data.SqlClient.SqlParameter("@Tarifkennzeichen", System.Data.SqlDbType.NVarChar, 0, "Tarifkennzeichen"),
            new System.Data.SqlClient.SqlParameter("@Saldovortrag", System.Data.SqlDbType.Money, 0, "Saldovortrag"),
            new System.Data.SqlClient.SqlParameter("@Festabzug", System.Data.SqlDbType.Money, 0, "Festabzug"),
            new System.Data.SqlClient.SqlParameter("@Festabzug_Anz", System.Data.SqlDbType.Int, 0, "Festabzug_Anz"),
            new System.Data.SqlClient.SqlParameter("@Inkasso", System.Data.SqlDbType.SmallInt, 0, "Inkasso"),
            new System.Data.SqlClient.SqlParameter("@Kopf1", System.Data.SqlDbType.NVarChar, 0, "Kopf1"),
            new System.Data.SqlClient.SqlParameter("@Kopf2", System.Data.SqlDbType.NVarChar, 0, "Kopf2"),
            new System.Data.SqlClient.SqlParameter("@Kopf3", System.Data.SqlDbType.NVarChar, 0, "Kopf3"),
            new System.Data.SqlClient.SqlParameter("@UStidnr", System.Data.SqlDbType.NVarChar, 0, "UStidnr"),
            new System.Data.SqlClient.SqlParameter("@stnr", System.Data.SqlDbType.NVarChar, 0, "stnr"),
            new System.Data.SqlClient.SqlParameter("@inet", System.Data.SqlDbType.NVarChar, 0, "inet"),
            new System.Data.SqlClient.SqlParameter("@zusatz1", System.Data.SqlDbType.NVarChar, 0, "zusatz1"),
            new System.Data.SqlClient.SqlParameter("@zusatz2", System.Data.SqlDbType.NVarChar, 0, "zusatz2"),
            new System.Data.SqlClient.SqlParameter("@Fusszeile", System.Data.SqlDbType.NVarChar, 0, "Fusszeile"),
            new System.Data.SqlClient.SqlParameter("@AbrIK", System.Data.SqlDbType.Int, 0, "AbrIK"),
            new System.Data.SqlClient.SqlParameter("@Zuzrech", System.Data.SqlDbType.SmallInt, 0, "Zuzrech"),
            new System.Data.SqlClient.SqlParameter("@Zahlungsziel_Tage", System.Data.SqlDbType.SmallInt, 0, "Zahlungsziel_Tage"),
            new System.Data.SqlClient.SqlParameter("@Original_Nr", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Nr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_IK", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "IK", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Name1", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Name1", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Name2", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Name2", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Name3", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Name3", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Name3", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Name3", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Vorname", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Vorname", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Straße", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Straße", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_PLZ", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "PLZ", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Ort", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Ort", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Tel", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Tel", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Fax", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Fax", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Fax", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Fax", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_EMail", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "EMail", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_EMail", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "EMail", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beginn", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beginn", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beginn", System.Data.SqlDbType.SmallDateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beginn", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Ende", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Ende", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Ende", System.Data.SqlDbType.SmallDateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Ende", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_Kontotext", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_Kontotext", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_Kontotext", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_Kontotext", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_Bank", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_Bank", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_Bank", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_Bank", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_BLZ", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_BLZ", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_BLZ", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_BLZ", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_KontoNr", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_KontoNr", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_KontoNr", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_KontoNr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_Verw", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_Verw", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_Verw", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_Verw", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_VerbArt", System.Data.SqlDbType.TinyInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "VerbArt", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_PfArt", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "PfArt", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_PfArt", System.Data.SqlDbType.TinyInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "PfArt", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_PfZahlung", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "PfZahlung", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_PfZahlung", System.Data.SqlDbType.TinyInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "PfZahlung", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Pfvon", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Pfvon", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Pfvon", System.Data.SqlDbType.SmallDateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Pfvon", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Pfbis", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Pfbis", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Pfbis", System.Data.SqlDbType.SmallDateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Pfbis", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_PfGesamtSumme", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "PfGesamtSumme", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_PfGesamtSumme", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "PfGesamtSumme", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_PffesterBetrag", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "PffesterBetrag", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_PffesterBetrag", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "PffesterBetrag", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_PfProzent", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "PfProzent", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_PfProzent", System.Data.SqlDbType.Real, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "PfProzent", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_PfMin", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "PfMin", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_PfMin", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "PfMin", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_PfMax", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "PfMax", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_PfMax", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "PfMax", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_AbtrText", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_AbtrText", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_AbtrText", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_AbtrText", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_AbtrBank", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_AbtrBank", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_AbtrBank", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_AbtrBank", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_AbtrBLZ", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_AbtrBLZ", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_AbtrBLZ", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_AbtrBLZ", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_AbtrKntNr", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_AbtrKntNr", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_AbtrKntNr", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_AbtrKntNr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_AbtrVerw", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_AbtrVerw", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_AbtrVerw", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_AbtrVerw", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_AbtrName1", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_AbtrName1", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_AbtrName1", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_AbtrName1", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_AbtrName2", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_AbtrName2", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_AbtrName2", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_AbtrName2", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_AbtrName3", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_AbtrName3", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_AbtrName3", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_AbtrName3", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_AbtrStrasse", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_AbtrStrasse", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_AbtrStrasse", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_AbtrStrasse", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_AbtrPLZ", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_AbtrPLZ", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_AbtrPLZ", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_AbtrPLZ", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_AbtrOrt", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_AbtrOrt", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_AbtrOrt", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_AbtrOrt", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_Berufsgrp", System.Data.SqlDbType.TinyInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_Berufsgrp", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_Betriebsart", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_Betriebsart", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_Betriebsart", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_Betriebsart", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_VerwTage", System.Data.SqlDbType.TinyInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "VerwTage", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_VerwAbrSofort", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "VerwAbrSofort", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_DarlKntNr", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_DarlKntNr", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_DarlKntNr", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_DarlKntNr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_Darlsatz", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_Darlsatz", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_Darlsatz", System.Data.SqlDbType.Real, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_Darlsatz", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_DarlGrenze", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_DarlGrenze", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_DarlGrenze", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_DarlGrenze", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_Darlgespart", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_Darlgespart", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_Darlgespart", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_Darlgespart", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_Darlausgez", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_Darlausgez", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_Darlausgez", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_Darlausgez", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_Einzugam", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_Einzugam", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_Einzugam", System.Data.SqlDbType.SmallDateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_Einzugam", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Beh_Einzug", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Beh_Einzug", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Beh_Einzug", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Beh_Einzug", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_letzte_Änderung", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "letzte Änderung", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_letzte_Änderung", System.Data.SqlDbType.SmallDateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "letzte Änderung", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Bearbeiter", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Bearbeiter", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Bearbeiter", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Bearbeiter", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_MWSt", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "MWSt", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Tarifkennzeichen", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Tarifkennzeichen", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Saldovortrag", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Saldovortrag", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Festabzug", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Festabzug", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Festabzug_Anz", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Festabzug_Anz", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Inkasso", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Inkasso", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Kopf1", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Kopf1", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Kopf1", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Kopf1", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Kopf2", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Kopf2", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Kopf2", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Kopf2", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Kopf3", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Kopf3", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Kopf3", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Kopf3", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_UStidnr", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "UStidnr", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_UStidnr", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "UStidnr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_stnr", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "stnr", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_stnr", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "stnr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_inet", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "inet", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_inet", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "inet", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_zusatz1", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "zusatz1", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_zusatz1", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "zusatz1", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_zusatz2", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "zusatz2", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_zusatz2", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "zusatz2", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Fusszeile", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Fusszeile", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Fusszeile", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Fusszeile", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_AbrIK", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "AbrIK", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Zuzrech", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Zuzrech", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Zuzrech", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Zuzrech", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Zahlungsziel_Tage", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Zahlungsziel_Tage", System.Data.DataRowVersion.Original, null)});
            // 
            // kostentraegerSelectCommand
            // 
            this.kostentraegerSelectCommand.CommandText = "SELECT     Kostentraeger_300.*\r\nFROM         Kostentraeger_300";
            this.kostentraegerSelectCommand.Connection = this.stamm4SqlConnection;
            // 
            // kostentraegerDA
            // 
            this.kostentraegerDA.SelectCommand = this.kostentraegerSelectCommand;
            this.kostentraegerDA.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Kostentraeger", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID", "ID"),
                        new System.Data.Common.DataColumnMapping("Name1", "Name1"),
                        new System.Data.Common.DataColumnMapping("Name2", "Name2"),
                        new System.Data.Common.DataColumnMapping("Name3", "Name3"),
                        new System.Data.Common.DataColumnMapping("Name4", "Name4"),
                        new System.Data.Common.DataColumnMapping("IK", "IK"),
                        new System.Data.Common.DataColumnMapping("Institutionsart", "Institutionsart"),
                        new System.Data.Common.DataColumnMapping("Kurzbezeichnung", "Kurzbezeichnung"),
                        new System.Data.Common.DataColumnMapping("VKNR", "VKNR"),
                        new System.Data.Common.DataColumnMapping("Kontonummer", "Kontonummer"),
                        new System.Data.Common.DataColumnMapping("Bankleitzahl", "Bankleitzahl"),
                        new System.Data.Common.DataColumnMapping("Bankbezeichnung", "Bankbezeichnung"),
                        new System.Data.Common.DataColumnMapping("Kontoinhaber", "Kontoinhaber"),
                        new System.Data.Common.DataColumnMapping("Public Key", "Public Key"),
                        new System.Data.Common.DataColumnMapping("FKT_IK", "FKT_IK"),
                        new System.Data.Common.DataColumnMapping("Rechnungsart", "Rechnungsart"),
                        new System.Data.Common.DataColumnMapping("KKGruppe", "KKGruppe"),
                        new System.Data.Common.DataColumnMapping("letzte_Aenderung", "letzte_Aenderung"),
                        new System.Data.Common.DataColumnMapping("Bearbeiter", "Bearbeiter"),
                        new System.Data.Common.DataColumnMapping("FiBunummer", "FiBunummer"),
                        new System.Data.Common.DataColumnMapping("Mwst", "Mwst"),
                        new System.Data.Common.DataColumnMapping("Edi_RefNr", "Edi_RefNr"),
                        new System.Data.Common.DataColumnMapping("frei", "frei"),
                        new System.Data.Common.DataColumnMapping("EdiSel", "EdiSel"),
                        new System.Data.Common.DataColumnMapping("Is_DAV", "Is_DAV"),
                        new System.Data.Common.DataColumnMapping("EmailAdresse", "EmailAdresse"),
                        new System.Data.Common.DataColumnMapping("FTPAdresse", "FTPAdresse"),
                        new System.Data.Common.DataColumnMapping("PapierIK", "PapierIK"),
                        new System.Data.Common.DataColumnMapping("Me_IK", "Me_IK"),
                        new System.Data.Common.DataColumnMapping("OE_IK", "OE_IK"),
                        new System.Data.Common.DataColumnMapping("Ans_Strasse", "Ans_Strasse"),
                        new System.Data.Common.DataColumnMapping("Ans_plz", "Ans_plz"),
                        new System.Data.Common.DataColumnMapping("Ans_Ort", "Ans_Ort"),
                        new System.Data.Common.DataColumnMapping("Ans_Telefon", "Ans_Telefon"),
                        new System.Data.Common.DataColumnMapping("Ans_FAX", "Ans_FAX"),
                        new System.Data.Common.DataColumnMapping("Ans_Partner", "Ans_Partner"),
                        new System.Data.Common.DataColumnMapping("Pos_Postfach", "Pos_Postfach"),
                        new System.Data.Common.DataColumnMapping("Pos_Plz", "Pos_Plz"),
                        new System.Data.Common.DataColumnMapping("Pos_Ort", "Pos_Ort")})});
            // 
            // lastIKsqlConnection
            // 
            this.lastIKsqlConnection.ConnectionString = "Data Source=SWS;Initial Catalog=660310038;User ID=sa";
            this.lastIKsqlConnection.FireInfoMessageEventOnUserErrors = false;
            // 
            // kostentraeder_TarifeSelectCommand
            // 
            this.kostentraeder_TarifeSelectCommand.CommandText = "SELECT     IK, TarifSchluessel\r\nFROM         Kostentraeger_Tarife";
            this.kostentraeder_TarifeSelectCommand.Connection = this.stamm4SqlConnection;
            // 
            // kostentraeder_TarifeDA
            // 
            this.kostentraeder_TarifeDA.SelectCommand = this.kostentraeder_TarifeSelectCommand;
            this.kostentraeder_TarifeDA.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Kostentraeger_Tarife", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("IK", "IK"),
                        new System.Data.Common.DataColumnMapping("TarifSchluessel", "TarifSchluessel")})});
            // 
            // tab_PositionenDA
            // 
            this.tab_PositionenDA.SelectCommand = this.tab_PositionenSelectCommand;
            this.tab_PositionenDA.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Tab_Positionen", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("Tarifkennzeichen", "Tarifkennzeichen"),
                        new System.Data.Common.DataColumnMapping("BuPos", "BuPos"),
                        new System.Data.Common.DataColumnMapping("Leistung", "Leistung"),
                        new System.Data.Common.DataColumnMapping("PreisNeu", "PreisNeu"),
                        new System.Data.Common.DataColumnMapping("PreisAlt", "PreisAlt"),
                        new System.Data.Common.DataColumnMapping("PreisGueltigAb", "PreisGueltigAb"),
                        new System.Data.Common.DataColumnMapping("PreisBasis", "PreisBasis"),
                        new System.Data.Common.DataColumnMapping("GebuehrArt", "GebuehrArt"),
                        new System.Data.Common.DataColumnMapping("GebuehrProzent", "GebuehrProzent"),
                        new System.Data.Common.DataColumnMapping("GebuehrDM", "GebuehrDM"),
                        new System.Data.Common.DataColumnMapping("Kilometerangabe", "Kilometerangabe"),
                        new System.Data.Common.DataColumnMapping("Uhrzeitangabe", "Uhrzeitangabe"),
                        new System.Data.Common.DataColumnMapping("Begruendung", "Begruendung"),
                        new System.Data.Common.DataColumnMapping("Genehmigung", "Genehmigung"),
                        new System.Data.Common.DataColumnMapping("Publik", "Publik"),
                        new System.Data.Common.DataColumnMapping("StatNr", "StatNr")})});
            // 
            // tab_PositionenSelectCommand
            // 
            this.tab_PositionenSelectCommand.CommandText = "SELECT     Tab_Positionen.*\r\nFROM         Tab_Positionen";
            this.tab_PositionenSelectCommand.Connection = this.stamm4SqlConnection;
            // 
            // errorProvider
            // 
            this.errorProvider.BlinkRate = 500;
            this.errorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink;
            this.errorProvider.ContainerControl = this.splitContainer1;
            // 
            // mandantenBesSelectCommand
            // 
            this.mandantenBesSelectCommand.CommandText = "SELECT     ID, Nr, Bearbeiter, Datum, Besonderheit\r\nFROM         Mandanten_Bes";
            this.mandantenBesSelectCommand.Connection = this.mandantenBesSqlConnection;
            // 
            // mandantenBesSqlConnection
            // 
            this.mandantenBesSqlConnection.ConnectionString = "Data Source=.\\SQLEXPRESS;AttachDbFilename=C:\\stamm4.mdf;Integrated Security=True;" +
                "Connect Timeout=30;User Instance=True";
            this.mandantenBesSqlConnection.FireInfoMessageEventOnUserErrors = false;
            // 
            // mandantenBesInsertCommand
            // 
            this.mandantenBesInsertCommand.CommandText = resources.GetString("mandantenBesInsertCommand.CommandText");
            this.mandantenBesInsertCommand.Connection = this.mandantenBesSqlConnection;
            this.mandantenBesInsertCommand.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Nr", System.Data.SqlDbType.Int, 0, "Nr"),
            new System.Data.SqlClient.SqlParameter("@Bearbeiter", System.Data.SqlDbType.VarChar, 0, "Bearbeiter"),
            new System.Data.SqlClient.SqlParameter("@Datum", System.Data.SqlDbType.DateTime, 0, "Datum"),
            new System.Data.SqlClient.SqlParameter("@Besonderheit", System.Data.SqlDbType.Text, 0, "Besonderheit")});
            // 
            // mandantenBesUpdateCommand
            // 
            this.mandantenBesUpdateCommand.CommandText = resources.GetString("mandantenBesUpdateCommand.CommandText");
            this.mandantenBesUpdateCommand.Connection = this.mandantenBesSqlConnection;
            this.mandantenBesUpdateCommand.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Nr", System.Data.SqlDbType.Int, 0, "Nr"),
            new System.Data.SqlClient.SqlParameter("@Bearbeiter", System.Data.SqlDbType.VarChar, 0, "Bearbeiter"),
            new System.Data.SqlClient.SqlParameter("@Datum", System.Data.SqlDbType.DateTime, 0, "Datum"),
            new System.Data.SqlClient.SqlParameter("@Besonderheit", System.Data.SqlDbType.Text, 0, "Besonderheit"),
            new System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "ID", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Nr", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Nr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Bearbeiter", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Bearbeiter", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Datum", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Datum", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.Int, 4, "ID")});
            // 
            // mandantenBesDeleteCommand
            // 
            this.mandantenBesDeleteCommand.CommandText = "DELETE FROM [Mandanten_Bes] WHERE (([ID] = @Original_ID) AND ([Nr] = @Original_Nr" +
                ") AND ([Bearbeiter] = @Original_Bearbeiter) AND ([Datum] = @Original_Datum))";
            this.mandantenBesDeleteCommand.Connection = this.mandantenBesSqlConnection;
            this.mandantenBesDeleteCommand.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "ID", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Nr", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Nr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Bearbeiter", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Bearbeiter", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Datum", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Datum", System.Data.DataRowVersion.Original, null)});
            // 
            // mandantenBesDA
            // 
            this.mandantenBesDA.DeleteCommand = this.mandantenBesDeleteCommand;
            this.mandantenBesDA.InsertCommand = this.mandantenBesInsertCommand;
            this.mandantenBesDA.SelectCommand = this.mandantenBesSelectCommand;
            this.mandantenBesDA.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Mandanten_Bes", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID", "ID"),
                        new System.Data.Common.DataColumnMapping("Nr", "Nr"),
                        new System.Data.Common.DataColumnMapping("Bearbeiter", "Bearbeiter"),
                        new System.Data.Common.DataColumnMapping("Datum", "Datum"),
                        new System.Data.Common.DataColumnMapping("Besonderheit", "Besonderheit")})});
            this.mandantenBesDA.UpdateCommand = this.mandantenBesUpdateCommand;
            // 
            // gruppeSqlSelectCommand
            // 
            this.gruppeSqlSelectCommand.CommandText = "SELECT     GRUPPE, BEZEICHNUNG, DEFINIT, INDIKAT, FORMULARB, letzte_auslieferung\r" +
                "\nFROM         [10_GRUPPE]";
            this.gruppeSqlSelectCommand.Connection = this.stamm4SqlConnection;
            // 
            // gruppeDA
            // 
            this.gruppeDA.SelectCommand = this.gruppeSqlSelectCommand;
            this.gruppeDA.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "10_GRUPPE", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("GRUPPE", "GRUPPE"),
                        new System.Data.Common.DataColumnMapping("BEZEICHNUNG", "BEZEICHNUNG"),
                        new System.Data.Common.DataColumnMapping("DEFINIT", "DEFINIT"),
                        new System.Data.Common.DataColumnMapping("INDIKAT", "INDIKAT"),
                        new System.Data.Common.DataColumnMapping("FORMULARB", "FORMULARB"),
                        new System.Data.Common.DataColumnMapping("letzte_auslieferung", "letzte_auslieferung")})});
            // 
            // ortSqlSelectCommand
            // 
            this.ortSqlSelectCommand.CommandText = "SELECT     ORT, BEZEICHNUNG\r\nFROM         [10_ORT]";
            this.ortSqlSelectCommand.Connection = this.stamm4SqlConnection;
            // 
            // ortDA
            // 
            this.ortDA.SelectCommand = this.ortSqlSelectCommand;
            this.ortDA.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "10_ORT", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ORT", "ORT"),
                        new System.Data.Common.DataColumnMapping("BEZEICHNUNG", "BEZEICHNUNG")})});
            // 
            // unterGruppeSqlSelectCommand
            // 
            this.unterGruppeSqlSelectCommand.CommandText = "SELECT     GRUPPE, ORT, UNTER, BEZEICHNUNG, MEDIZIN, TECHNIK\r\nFROM         [10_UN" +
                "TER]";
            this.unterGruppeSqlSelectCommand.Connection = this.stamm4SqlConnection;
            // 
            // unterGruppeDA
            // 
            this.unterGruppeDA.SelectCommand = this.unterGruppeSqlSelectCommand;
            this.unterGruppeDA.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "10_UNTER", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("GRUPPE", "GRUPPE"),
                        new System.Data.Common.DataColumnMapping("ORT", "ORT"),
                        new System.Data.Common.DataColumnMapping("UNTER", "UNTER"),
                        new System.Data.Common.DataColumnMapping("BEZEICHNUNG", "BEZEICHNUNG"),
                        new System.Data.Common.DataColumnMapping("MEDIZIN", "MEDIZIN"),
                        new System.Data.Common.DataColumnMapping("TECHNIK", "TECHNIK")})});
            // 
            // artSqlSelectCommand
            // 
            this.artSqlSelectCommand.CommandText = "SELECT     GRUPPE, ORT, UNTER, ART, BEZEICHNUNG, BESCHREIB, INDIKAT\r\nFROM        " +
                " [10_ART]";
            this.artSqlSelectCommand.Connection = this.stamm4SqlConnection;
            // 
            // artDA
            // 
            this.artDA.SelectCommand = this.artSqlSelectCommand;
            this.artDA.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "10_ART", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("GRUPPE", "GRUPPE"),
                        new System.Data.Common.DataColumnMapping("ORT", "ORT"),
                        new System.Data.Common.DataColumnMapping("UNTER", "UNTER"),
                        new System.Data.Common.DataColumnMapping("ART", "ART"),
                        new System.Data.Common.DataColumnMapping("BEZEICHNUNG", "BEZEICHNUNG"),
                        new System.Data.Common.DataColumnMapping("BESCHREIB", "BESCHREIB"),
                        new System.Data.Common.DataColumnMapping("INDIKAT", "INDIKAT")})});
            // 
            // produktSqlSelectCommand
            // 
            this.produktSqlSelectCommand.CommandText = "SELECT     GRUPPE, ORT, UNTER, ART, PRODUKT, LFNR, ARTBEZ, HERSTELLER, BEZEICHNUN" +
                "G, MERKMAL, BEMERKUNG, EPREIS\r\nFROM         [10_PRODUKT]";
            this.produktSqlSelectCommand.Connection = this.stamm4SqlConnection;
            // 
            // produktDA
            // 
            this.produktDA.SelectCommand = this.produktSqlSelectCommand;
            this.produktDA.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "10_PRODUKT", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("GRUPPE", "GRUPPE"),
                        new System.Data.Common.DataColumnMapping("ORT", "ORT"),
                        new System.Data.Common.DataColumnMapping("UNTER", "UNTER"),
                        new System.Data.Common.DataColumnMapping("ART", "ART"),
                        new System.Data.Common.DataColumnMapping("PRODUKT", "PRODUKT"),
                        new System.Data.Common.DataColumnMapping("LFNR", "LFNR"),
                        new System.Data.Common.DataColumnMapping("ARTBEZ", "ARTBEZ"),
                        new System.Data.Common.DataColumnMapping("HERSTELLER", "HERSTELLER"),
                        new System.Data.Common.DataColumnMapping("BEZEICHNUNG", "BEZEICHNUNG"),
                        new System.Data.Common.DataColumnMapping("MERKMAL", "MERKMAL"),
                        new System.Data.Common.DataColumnMapping("BEMERKUNG", "BEMERKUNG"),
                        new System.Data.Common.DataColumnMapping("EPREIS", "EPREIS")})});
            // 
            // tab_HMPBezeichnungSqlSelectCommand
            // 
            this.tab_HMPBezeichnungSqlSelectCommand.CommandText = "SELECT     HMP, Mandant, Bezeichnung, ArtikelNr, EPreis, TarifKZ\r\nFROM         ta" +
                "b_HMPBezeichnung";
            this.tab_HMPBezeichnungSqlSelectCommand.Connection = this.stamm4SqlConnection;
            // 
            // tab_HMPBezeichnungDA
            // 
            this.tab_HMPBezeichnungDA.SelectCommand = this.tab_HMPBezeichnungSqlSelectCommand;
            this.tab_HMPBezeichnungDA.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "tab_HMPBezeichnung", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("HMP", "HMP"),
                        new System.Data.Common.DataColumnMapping("Mandant", "Mandant"),
                        new System.Data.Common.DataColumnMapping("Bezeichnung", "Bezeichnung"),
                        new System.Data.Common.DataColumnMapping("ArtikelNr", "ArtikelNr"),
                        new System.Data.Common.DataColumnMapping("EPreis", "EPreis"),
                        new System.Data.Common.DataColumnMapping("TarifKZ", "TarifKZ")})});
            // 
            // positionenSqlConnection
            // 
            this.positionenSqlConnection.ConnectionString = "Data Source=.\\SQLEXPRESS;AttachDbFilename=C:\\660310038.mdf;Integrated Security=Tr" +
                "ue;Connect Timeout=30;User Instance=True";
            this.positionenSqlConnection.FireInfoMessageEventOnUserErrors = false;
            // 
            // positionenInsertCommand
            // 
            this.positionenInsertCommand.CommandText = resources.GetString("positionenInsertCommand.CommandText");
            this.positionenInsertCommand.Connection = this.positionenSqlConnection;
            this.positionenInsertCommand.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.Int, 0, "ID"),
            new System.Data.SqlClient.SqlParameter("@Tarifkennzeichen", System.Data.SqlDbType.VarChar, 0, "Tarifkennzeichen"),
            new System.Data.SqlClient.SqlParameter("@BuPos", System.Data.SqlDbType.VarChar, 0, "BuPos"),
            new System.Data.SqlClient.SqlParameter("@Faktor", System.Data.SqlDbType.Money, 0, "Faktor"),
            new System.Data.SqlClient.SqlParameter("@EPreis", System.Data.SqlDbType.Money, 0, "EPreis"),
            new System.Data.SqlClient.SqlParameter("@Anzahl", System.Data.SqlDbType.Money, 0, "Anzahl"),
            new System.Data.SqlClient.SqlParameter("@GebArt", System.Data.SqlDbType.SmallInt, 0, "GebArt"),
            new System.Data.SqlClient.SqlParameter("@Gebuehr", System.Data.SqlDbType.Money, 0, "Gebuehr"),
            new System.Data.SqlClient.SqlParameter("@BehDat", System.Data.SqlDbType.DateTime, 0, "BehDat"),
            new System.Data.SqlClient.SqlParameter("@Mitarbeiter", System.Data.SqlDbType.VarChar, 0, "Mitarbeiter"),
            new System.Data.SqlClient.SqlParameter("@Mwst_KZ", System.Data.SqlDbType.SmallInt, 0, "Mwst_KZ"),
            new System.Data.SqlClient.SqlParameter("@Mwst", System.Data.SqlDbType.Money, 0, "Mwst"),
            new System.Data.SqlClient.SqlParameter("@BuPos_Sonder", System.Data.SqlDbType.VarChar, 0, "BuPos_Sonder"),
            new System.Data.SqlClient.SqlParameter("@Hilfsmittel_KZ", System.Data.SqlDbType.SmallInt, 0, "Hilfsmittel_KZ"),
            new System.Data.SqlClient.SqlParameter("@Hilfsmittel_InvNr", System.Data.SqlDbType.VarChar, 0, "Hilfsmittel_InvNr"),
            new System.Data.SqlClient.SqlParameter("@Kilometer", System.Data.SqlDbType.Money, 0, "Kilometer"),
            new System.Data.SqlClient.SqlParameter("@Uhrzeit_von", System.Data.SqlDbType.VarChar, 0, "Uhrzeit_von"),
            new System.Data.SqlClient.SqlParameter("@Uhrzeit_bis", System.Data.SqlDbType.VarChar, 0, "Uhrzeit_bis"),
            new System.Data.SqlClient.SqlParameter("@Dauer_Min", System.Data.SqlDbType.Int, 0, "Dauer_Min"),
            new System.Data.SqlClient.SqlParameter("@Versorgung_von", System.Data.SqlDbType.DateTime, 0, "Versorgung_von"),
            new System.Data.SqlClient.SqlParameter("@Versorgung_bis", System.Data.SqlDbType.DateTime, 0, "Versorgung_bis"),
            new System.Data.SqlClient.SqlParameter("@Text", System.Data.SqlDbType.VarChar, 0, "Text"),
            new System.Data.SqlClient.SqlParameter("@Zuzahlungsart", System.Data.SqlDbType.SmallInt, 0, "Zuzahlungsart"),
            new System.Data.SqlClient.SqlParameter("@Zuzahlung", System.Data.SqlDbType.Money, 0, "Zuzahlung"),
            new System.Data.SqlClient.SqlParameter("@Eigenanteil", System.Data.SqlDbType.Money, 0, "Eigenanteil"),
            new System.Data.SqlClient.SqlParameter("@VZeitraum", System.Data.SqlDbType.SmallInt, 0, "VZeitraum"),
            new System.Data.SqlClient.SqlParameter("@Verbrauch", System.Data.SqlDbType.SmallInt, 0, "Verbrauch"),
            new System.Data.SqlClient.SqlParameter("@LeistungID", System.Data.SqlDbType.Int, 0, "LeistungID")});
            // 
            // positionenUpdateCommand
            // 
            this.positionenUpdateCommand.CommandText = resources.GetString("positionenUpdateCommand.CommandText");
            this.positionenUpdateCommand.Connection = this.positionenSqlConnection;
            this.positionenUpdateCommand.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.Int, 0, "ID"),
            new System.Data.SqlClient.SqlParameter("@Tarifkennzeichen", System.Data.SqlDbType.VarChar, 0, "Tarifkennzeichen"),
            new System.Data.SqlClient.SqlParameter("@BuPos", System.Data.SqlDbType.VarChar, 0, "BuPos"),
            new System.Data.SqlClient.SqlParameter("@Faktor", System.Data.SqlDbType.Money, 0, "Faktor"),
            new System.Data.SqlClient.SqlParameter("@EPreis", System.Data.SqlDbType.Money, 0, "EPreis"),
            new System.Data.SqlClient.SqlParameter("@Anzahl", System.Data.SqlDbType.Money, 0, "Anzahl"),
            new System.Data.SqlClient.SqlParameter("@GebArt", System.Data.SqlDbType.SmallInt, 0, "GebArt"),
            new System.Data.SqlClient.SqlParameter("@Gebuehr", System.Data.SqlDbType.Money, 0, "Gebuehr"),
            new System.Data.SqlClient.SqlParameter("@BehDat", System.Data.SqlDbType.DateTime, 0, "BehDat"),
            new System.Data.SqlClient.SqlParameter("@Mitarbeiter", System.Data.SqlDbType.VarChar, 0, "Mitarbeiter"),
            new System.Data.SqlClient.SqlParameter("@Mwst_KZ", System.Data.SqlDbType.SmallInt, 0, "Mwst_KZ"),
            new System.Data.SqlClient.SqlParameter("@Mwst", System.Data.SqlDbType.Money, 0, "Mwst"),
            new System.Data.SqlClient.SqlParameter("@BuPos_Sonder", System.Data.SqlDbType.VarChar, 0, "BuPos_Sonder"),
            new System.Data.SqlClient.SqlParameter("@Hilfsmittel_KZ", System.Data.SqlDbType.SmallInt, 0, "Hilfsmittel_KZ"),
            new System.Data.SqlClient.SqlParameter("@Hilfsmittel_InvNr", System.Data.SqlDbType.VarChar, 0, "Hilfsmittel_InvNr"),
            new System.Data.SqlClient.SqlParameter("@Kilometer", System.Data.SqlDbType.Money, 0, "Kilometer"),
            new System.Data.SqlClient.SqlParameter("@Uhrzeit_von", System.Data.SqlDbType.VarChar, 0, "Uhrzeit_von"),
            new System.Data.SqlClient.SqlParameter("@Uhrzeit_bis", System.Data.SqlDbType.VarChar, 0, "Uhrzeit_bis"),
            new System.Data.SqlClient.SqlParameter("@Dauer_Min", System.Data.SqlDbType.Int, 0, "Dauer_Min"),
            new System.Data.SqlClient.SqlParameter("@Versorgung_von", System.Data.SqlDbType.DateTime, 0, "Versorgung_von"),
            new System.Data.SqlClient.SqlParameter("@Versorgung_bis", System.Data.SqlDbType.DateTime, 0, "Versorgung_bis"),
            new System.Data.SqlClient.SqlParameter("@Text", System.Data.SqlDbType.VarChar, 0, "Text"),
            new System.Data.SqlClient.SqlParameter("@Zuzahlungsart", System.Data.SqlDbType.SmallInt, 0, "Zuzahlungsart"),
            new System.Data.SqlClient.SqlParameter("@Zuzahlung", System.Data.SqlDbType.Money, 0, "Zuzahlung"),
            new System.Data.SqlClient.SqlParameter("@Eigenanteil", System.Data.SqlDbType.Money, 0, "Eigenanteil"),
            new System.Data.SqlClient.SqlParameter("@VZeitraum", System.Data.SqlDbType.SmallInt, 0, "VZeitraum"),
            new System.Data.SqlClient.SqlParameter("@Verbrauch", System.Data.SqlDbType.SmallInt, 0, "Verbrauch"),
            new System.Data.SqlClient.SqlParameter("@Original_AUTO", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "AUTO", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "ID", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Tarifkennzeichen", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Tarifkennzeichen", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_BuPos", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "BuPos", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Faktor", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Faktor", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_EPreis", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "EPreis", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Anzahl", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Anzahl", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_GebArt", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "GebArt", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Gebuehr", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Gebuehr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_BehDat", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "BehDat", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Mitarbeiter", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Mitarbeiter", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Mitarbeiter", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Mitarbeiter", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Mwst_KZ", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Mwst_KZ", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Mwst_KZ", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Mwst_KZ", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Mwst", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Mwst", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Mwst", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Mwst", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_BuPos_Sonder", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "BuPos_Sonder", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_BuPos_Sonder", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "BuPos_Sonder", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Hilfsmittel_KZ", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Hilfsmittel_KZ", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Hilfsmittel_KZ", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Hilfsmittel_KZ", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Hilfsmittel_InvNr", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Hilfsmittel_InvNr", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Hilfsmittel_InvNr", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Hilfsmittel_InvNr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Kilometer", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Kilometer", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Kilometer", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Kilometer", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Uhrzeit_von", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Uhrzeit_von", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Uhrzeit_von", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Uhrzeit_von", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Uhrzeit_bis", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Uhrzeit_bis", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Uhrzeit_bis", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Uhrzeit_bis", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Dauer_Min", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Dauer_Min", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Dauer_Min", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Dauer_Min", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Versorgung_von", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Versorgung_von", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Versorgung_von", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Versorgung_von", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Versorgung_bis", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Versorgung_bis", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Versorgung_bis", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Versorgung_bis", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Text", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Text", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Text", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Text", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Zuzahlungsart", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Zuzahlungsart", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Zuzahlungsart", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Zuzahlungsart", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Zuzahlung", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Zuzahlung", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Zuzahlung", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Zuzahlung", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Eigenanteil", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Eigenanteil", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Eigenanteil", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Eigenanteil", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_VZeitraum", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "VZeitraum", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_VZeitraum", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "VZeitraum", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Verbrauch", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Verbrauch", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Verbrauch", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Verbrauch", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@AUTO", System.Data.SqlDbType.Int, 4, "AUTO"),
            new System.Data.SqlClient.SqlParameter("@LeistungID", System.Data.SqlDbType.Int, 0, "LeistungID")});
            // 
            // positionenDeleteCommand
            // 
            this.positionenDeleteCommand.CommandText = resources.GetString("positionenDeleteCommand.CommandText");
            this.positionenDeleteCommand.Connection = this.positionenSqlConnection;
            this.positionenDeleteCommand.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Original_AUTO", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "AUTO", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "ID", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Tarifkennzeichen", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Tarifkennzeichen", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_BuPos", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "BuPos", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Faktor", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Faktor", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_EPreis", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "EPreis", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Anzahl", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Anzahl", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_GebArt", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "GebArt", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Gebuehr", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Gebuehr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_BehDat", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "BehDat", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Mitarbeiter", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Mitarbeiter", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Mitarbeiter", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Mitarbeiter", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Mwst_KZ", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Mwst_KZ", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Mwst_KZ", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Mwst_KZ", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Mwst", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Mwst", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Mwst", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Mwst", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_BuPos_Sonder", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "BuPos_Sonder", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_BuPos_Sonder", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "BuPos_Sonder", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Hilfsmittel_KZ", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Hilfsmittel_KZ", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Hilfsmittel_KZ", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Hilfsmittel_KZ", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Hilfsmittel_InvNr", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Hilfsmittel_InvNr", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Hilfsmittel_InvNr", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Hilfsmittel_InvNr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Kilometer", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Kilometer", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Kilometer", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Kilometer", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Uhrzeit_von", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Uhrzeit_von", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Uhrzeit_von", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Uhrzeit_von", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Uhrzeit_bis", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Uhrzeit_bis", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Uhrzeit_bis", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Uhrzeit_bis", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Dauer_Min", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Dauer_Min", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Dauer_Min", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Dauer_Min", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Versorgung_von", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Versorgung_von", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Versorgung_von", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Versorgung_von", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Versorgung_bis", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Versorgung_bis", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Versorgung_bis", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Versorgung_bis", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Text", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Text", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Text", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Text", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Zuzahlungsart", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Zuzahlungsart", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Zuzahlungsart", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Zuzahlungsart", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Zuzahlung", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Zuzahlung", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Zuzahlung", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Zuzahlung", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Eigenanteil", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Eigenanteil", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Eigenanteil", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Eigenanteil", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_VZeitraum", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "VZeitraum", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_VZeitraum", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "VZeitraum", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Verbrauch", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Verbrauch", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Verbrauch", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Verbrauch", System.Data.DataRowVersion.Original, null)});
            // 
            // positionenDA
            // 
            this.positionenDA.AcceptChangesDuringUpdate = false;
            this.positionenDA.DeleteCommand = this.positionenDeleteCommand;
            this.positionenDA.InsertCommand = this.positionenInsertCommand;
            this.positionenDA.SelectCommand = this.positionenSelectCommand;
            this.positionenDA.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "2006_Positionen", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("AUTO", "AUTO"),
                        new System.Data.Common.DataColumnMapping("ID", "ID"),
                        new System.Data.Common.DataColumnMapping("Tarifkennzeichen", "Tarifkennzeichen"),
                        new System.Data.Common.DataColumnMapping("BuPos", "BuPos"),
                        new System.Data.Common.DataColumnMapping("Faktor", "Faktor"),
                        new System.Data.Common.DataColumnMapping("EPreis", "EPreis"),
                        new System.Data.Common.DataColumnMapping("Anzahl", "Anzahl"),
                        new System.Data.Common.DataColumnMapping("GebArt", "GebArt"),
                        new System.Data.Common.DataColumnMapping("Gebuehr", "Gebuehr"),
                        new System.Data.Common.DataColumnMapping("BehDat", "BehDat"),
                        new System.Data.Common.DataColumnMapping("Mitarbeiter", "Mitarbeiter"),
                        new System.Data.Common.DataColumnMapping("Mwst_KZ", "Mwst_KZ"),
                        new System.Data.Common.DataColumnMapping("Mwst", "Mwst"),
                        new System.Data.Common.DataColumnMapping("BuPos_Sonder", "BuPos_Sonder"),
                        new System.Data.Common.DataColumnMapping("Hilfsmittel_KZ", "Hilfsmittel_KZ"),
                        new System.Data.Common.DataColumnMapping("Hilfsmittel_InvNr", "Hilfsmittel_InvNr"),
                        new System.Data.Common.DataColumnMapping("Kilometer", "Kilometer"),
                        new System.Data.Common.DataColumnMapping("Uhrzeit_von", "Uhrzeit_von"),
                        new System.Data.Common.DataColumnMapping("Uhrzeit_bis", "Uhrzeit_bis"),
                        new System.Data.Common.DataColumnMapping("Dauer_Min", "Dauer_Min"),
                        new System.Data.Common.DataColumnMapping("Versorgung_von", "Versorgung_von"),
                        new System.Data.Common.DataColumnMapping("Versorgung_bis", "Versorgung_bis"),
                        new System.Data.Common.DataColumnMapping("Text", "Text"),
                        new System.Data.Common.DataColumnMapping("Zuzahlungsart", "Zuzahlungsart"),
                        new System.Data.Common.DataColumnMapping("Zuzahlung", "Zuzahlung"),
                        new System.Data.Common.DataColumnMapping("Eigenanteil", "Eigenanteil"),
                        new System.Data.Common.DataColumnMapping("VZeitraum", "VZeitraum"),
                        new System.Data.Common.DataColumnMapping("Verbrauch", "Verbrauch")})});
            this.positionenDA.UpdateCommand = this.positionenUpdateCommand;
            this.positionenDA.RowUpdated += new System.Data.SqlClient.SqlRowUpdatedEventHandler(this.rezepte_RowUpdated);
            // 
            // positionenSelectCommand
            // 
            this.positionenSelectCommand.CommandText = resources.GetString("positionenSelectCommand.CommandText");
            this.positionenSelectCommand.Connection = this.positionenSqlConnection;
            this.positionenSelectCommand.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.Int, 4, "ID")});
            // 
            // leitZahlenSelectCommand
            // 
            this.leitZahlenSelectCommand.CommandText = "SELECT     Leitzahl, Bezeichnung\r\nFROM         Leitzahlen\r\nWHERE     (Verzeichnis" +
                " = 0)\r\nORDER BY Bezeichnung";
            this.leitZahlenSelectCommand.Connection = this.stamm4SqlConnection;
            // 
            // leitZahlenDA
            // 
            this.leitZahlenDA.SelectCommand = this.leitZahlenSelectCommand;
            this.leitZahlenDA.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Leitzahlen", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("Leitzahl", "Leitzahl"),
                        new System.Data.Common.DataColumnMapping("Bezeichnung", "Bezeichnung")})});
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.rezepteTabPage);
            this.tabControl1.Controls.Add(this.kostenvoranschlagTabPage);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1016, 581);
            this.tabControl1.TabIndex = 2;
            this.tabControl1.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControl1_Selecting);
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // rezepteTabPage
            // 
            this.rezepteTabPage.Controls.Add(this.splitContainer1);
            this.rezepteTabPage.Controls.Add(this.pnlCommand);
            this.rezepteTabPage.Location = new System.Drawing.Point(4, 22);
            this.rezepteTabPage.Name = "rezepteTabPage";
            this.rezepteTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.rezepteTabPage.Size = new System.Drawing.Size(1008, 555);
            this.rezepteTabPage.TabIndex = 0;
            this.rezepteTabPage.Text = "1";
            this.rezepteTabPage.UseVisualStyleBackColor = true;
            // 
            // kostenvoranschlagTabPage
            // 
            this.kostenvoranschlagTabPage.Controls.Add(this.kostenvoranschlagDGV);
            this.kostenvoranschlagTabPage.Controls.Add(this.kostenvoranschlagSuchenPanel);
            this.kostenvoranschlagTabPage.Location = new System.Drawing.Point(4, 22);
            this.kostenvoranschlagTabPage.Name = "kostenvoranschlagTabPage";
            this.kostenvoranschlagTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.kostenvoranschlagTabPage.Size = new System.Drawing.Size(1008, 555);
            this.kostenvoranschlagTabPage.TabIndex = 1;
            this.kostenvoranschlagTabPage.Text = "2";
            this.kostenvoranschlagTabPage.UseVisualStyleBackColor = true;
            // 
            // kostenvoranschlagDGV
            // 
            this.kostenvoranschlagDGV.AllowUserToAddRows = false;
            this.kostenvoranschlagDGV.AllowUserToDeleteRows = false;
            this.kostenvoranschlagDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.kostenvoranschlagDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.kostenvoranschlagDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.kostenvoranschlagDGV.DefaultCellStyle = dataGridViewCellStyle12;
            this.kostenvoranschlagDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kostenvoranschlagDGV.Location = new System.Drawing.Point(158, 3);
            this.kostenvoranschlagDGV.MultiSelect = false;
            this.kostenvoranschlagDGV.Name = "kostenvoranschlagDGV";
            this.kostenvoranschlagDGV.ReadOnly = true;
            this.kostenvoranschlagDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.kostenvoranschlagDGV.Size = new System.Drawing.Size(847, 549);
            this.kostenvoranschlagDGV.TabIndex = 1;
            this.kostenvoranschlagDGV.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.kostenvoranschlagDGV_MouseDoubleClick);
            // 
            // kostenvoranschlagSuchenPanel
            // 
            this.kostenvoranschlagSuchenPanel.Controls.Add(this.genehmigungRB);
            this.kostenvoranschlagSuchenPanel.Controls.Add(this.korrekturRB);
            this.kostenvoranschlagSuchenPanel.Controls.Add(this.kostJahrCB);
            this.kostenvoranschlagSuchenPanel.Controls.Add(this.label37);
            this.kostenvoranschlagSuchenPanel.Controls.Add(this.kostSuchenBtn);
            this.kostenvoranschlagSuchenPanel.Controls.Add(this.kostVornameTB);
            this.kostenvoranschlagSuchenPanel.Controls.Add(this.label34);
            this.kostenvoranschlagSuchenPanel.Controls.Add(this.kostNameTB);
            this.kostenvoranschlagSuchenPanel.Controls.Add(this.label31);
            this.kostenvoranschlagSuchenPanel.Controls.Add(this.kostRezNrTB);
            this.kostenvoranschlagSuchenPanel.Controls.Add(this.label25);
            this.kostenvoranschlagSuchenPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.kostenvoranschlagSuchenPanel.Location = new System.Drawing.Point(3, 3);
            this.kostenvoranschlagSuchenPanel.Name = "kostenvoranschlagSuchenPanel";
            this.kostenvoranschlagSuchenPanel.Size = new System.Drawing.Size(155, 549);
            this.kostenvoranschlagSuchenPanel.TabIndex = 0;
            // 
            // genehmigungRB
            // 
            this.genehmigungRB.AutoSize = true;
            this.genehmigungRB.Location = new System.Drawing.Point(5, 193);
            this.genehmigungRB.Name = "genehmigungRB";
            this.genehmigungRB.Size = new System.Drawing.Size(91, 17);
            this.genehmigungRB.TabIndex = 17;
            this.genehmigungRB.Text = "Genehmigung";
            this.genehmigungRB.UseVisualStyleBackColor = true;
            // 
            // korrekturRB
            // 
            this.korrekturRB.AutoSize = true;
            this.korrekturRB.Checked = true;
            this.korrekturRB.Location = new System.Drawing.Point(5, 169);
            this.korrekturRB.Name = "korrekturRB";
            this.korrekturRB.Size = new System.Drawing.Size(68, 17);
            this.korrekturRB.TabIndex = 16;
            this.korrekturRB.TabStop = true;
            this.korrekturRB.Text = "Korrektur";
            this.korrekturRB.UseVisualStyleBackColor = true;
            // 
            // kostJahrCB
            // 
            this.kostJahrCB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.kostJahrCB.FormattingEnabled = true;
            this.kostJahrCB.Location = new System.Drawing.Point(5, 25);
            this.kostJahrCB.Name = "kostJahrCB";
            this.kostJahrCB.Size = new System.Drawing.Size(139, 21);
            this.kostJahrCB.TabIndex = 15;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(3, 9);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(30, 13);
            this.label37.TabIndex = 14;
            this.label37.Text = "Jahr:";
            // 
            // kostSuchenBtn
            // 
            this.kostSuchenBtn.Location = new System.Drawing.Point(5, 216);
            this.kostSuchenBtn.Name = "kostSuchenBtn";
            this.kostSuchenBtn.Size = new System.Drawing.Size(141, 23);
            this.kostSuchenBtn.TabIndex = 13;
            this.kostSuchenBtn.Text = "Suchen";
            this.kostSuchenBtn.UseVisualStyleBackColor = true;
            this.kostSuchenBtn.Click += new System.EventHandler(this.kostSuchenBtn_Click);
            // 
            // kostVornameTB
            // 
            this.kostVornameTB.Location = new System.Drawing.Point(5, 143);
            this.kostVornameTB.Name = "kostVornameTB";
            this.kostVornameTB.Size = new System.Drawing.Size(139, 20);
            this.kostVornameTB.TabIndex = 12;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(3, 127);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(52, 13);
            this.label34.TabIndex = 11;
            this.label34.Text = "Vorname:";
            // 
            // kostNameTB
            // 
            this.kostNameTB.Location = new System.Drawing.Point(5, 104);
            this.kostNameTB.Name = "kostNameTB";
            this.kostNameTB.Size = new System.Drawing.Size(139, 20);
            this.kostNameTB.TabIndex = 10;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(3, 88);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(38, 13);
            this.label31.TabIndex = 9;
            this.label31.Text = "Name:";
            // 
            // kostRezNrTB
            // 
            this.kostRezNrTB.Location = new System.Drawing.Point(5, 65);
            this.kostRezNrTB.Name = "kostRezNrTB";
            this.kostRezNrTB.Size = new System.Drawing.Size(139, 20);
            this.kostRezNrTB.TabIndex = 8;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(3, 49);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(46, 13);
            this.label25.TabIndex = 7;
            this.label25.Text = "Rez.Nr.:";
            // 
            // tabprimKKassenSelectCommand
            // 
            this.tabprimKKassenSelectCommand.CommandText = "SELECT     *\r\nFROM         tab_primKKassen\r\nORDER BY ik, Kasse";
            this.tabprimKKassenSelectCommand.Connection = this.stamm4SqlConnection;
            // 
            // tabprimKKassenDA
            // 
            this.tabprimKKassenDA.SelectCommand = this.tabprimKKassenSelectCommand;
            // 
            // kostenTraegerDTASelectCommand
            // 
            this.kostenTraegerDTASelectCommand.CommandText = "SELECT     Kostentraeger_dta.*\r\nFROM         Kostentraeger_dta";
            this.kostenTraegerDTASelectCommand.Connection = this.stamm4SqlConnection;
            // 
            // kostenTraegerDTADA
            // 
            this.kostenTraegerDTADA.SelectCommand = this.kostenTraegerDTASelectCommand;
            this.kostenTraegerDTADA.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "Kostentraeger_dta", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ik", "ik"),
                        new System.Data.Common.DataColumnMapping("abrcode", "abrcode"),
                        new System.Data.Common.DataColumnMapping("fkt_ik", "fkt_ik"),
                        new System.Data.Common.DataColumnMapping("papierik", "papierik"),
                        new System.Data.Common.DataColumnMapping("oe_ik", "oe_ik"),
                        new System.Data.Common.DataColumnMapping("me_ik", "me_ik"),
                        new System.Data.Common.DataColumnMapping("bl", "bl"),
                        new System.Data.Common.DataColumnMapping("bz", "bz")})});
            // 
            // arztBetrNrDA
            // 
            this.arztBetrNrDA.InsertCommand = this.arztBetrNrInsertCommand;
            this.arztBetrNrDA.SelectCommand = this.arztBetrNrSelectCommand;
            this.arztBetrNrDA.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "ArztBetrNr", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID", "ID"),
                        new System.Data.Common.DataColumnMapping("Arztnr", "Arztnr"),
                        new System.Data.Common.DataColumnMapping("Betrnr", "Betrnr")})});
            // 
            // arztBetrNrInsertCommand
            // 
            this.arztBetrNrInsertCommand.CommandText = "INSERT INTO ArztBetrNr\r\n                      (Arztnr, Betrnr)\r\nVALUES     (@Arzt" +
                "nr,@Betrnr)";
            this.arztBetrNrInsertCommand.Connection = this.lastIKsqlConnection;
            this.arztBetrNrInsertCommand.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Arztnr", System.Data.SqlDbType.Variant, 1024, "Arztnr"),
            new System.Data.SqlClient.SqlParameter("@Betrnr", System.Data.SqlDbType.Variant, 1024, "Betrnr")});
            // 
            // arztBetrNrSelectCommand
            // 
            this.arztBetrNrSelectCommand.CommandText = "SELECT     *\r\nFROM         ArztBetrNr\r\nWHERE     (Betrnr = @Betrnr)\r\nORDER BY Arz" +
                "tnr";
            this.arztBetrNrSelectCommand.Connection = this.lastIKsqlConnection;
            this.arztBetrNrSelectCommand.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Betrnr", System.Data.SqlDbType.Variant, 1024)});
            // 
            // rezepteDeleteCommand
            // 
            this.rezepteDeleteCommand.CommandText = resources.GetString("rezepteDeleteCommand.CommandText");
            this.rezepteDeleteCommand.Connection = this.lastIKsqlConnection;
            this.rezepteDeleteCommand.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "ID", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Mandant", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Mandant", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_IK", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "IK", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_AbrCode", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "AbrCode", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Tarif", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Tarif", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Name", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Name", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Vorname", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Vorname", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_GebDat", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "GebDat", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_GebDat", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "GebDat", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Strasse", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Strasse", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Strasse", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Strasse", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Plz", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Plz", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Plz", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Plz", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Ort", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Ort", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Ort", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Ort", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_VersNr", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "VersNr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Status", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Status", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_RezDat", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "RezDat", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Befreit", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Befreit", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Befreit", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Befreit", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_ArztNr", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "ArztNr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_VK_Bis", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "VK_Bis", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_VK_Bis", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "VK_Bis", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Unfall", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Unfall", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Unfall", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Unfall", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Hilfsmittel", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Hilfsmittel", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Hilfsmittel", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Hilfsmittel", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Erfasst", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Erfasst", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Erfasst", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Erfasst", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Vorgang", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Vorgang", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Vorgang", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Vorgang", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_RechNr_Kasse", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "RechNr_Kasse", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_RechNr_Kasse", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "RechNr_Kasse", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_RechNr_Mandant", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "RechNr_Mandant", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_RechNr_Mandant", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "RechNr_Mandant", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Image", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Image", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Image", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Image", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_RezArt", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "RezArt", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_RezArt", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "RezArt", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Pauschale", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Pauschale", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Pauschale", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Pauschale", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Diagnose", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Diagnose", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Diagnose", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Diagnose", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Besonderheit", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Besonderheit", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Besonderheit", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Besonderheit", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Genehmigung_KZ", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Genehmigung_KZ", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Genehmigung_KZ", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Genehmigung_KZ", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Genehmigung_Datum", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Genehmigung_Datum", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Genehmigung_Datum", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Genehmigung_Datum", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Indikation", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Indikation", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Indikation", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Indikation", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Betriebsstaettennummer", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Betriebsstaettennummer", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Betriebsstaettennummer", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Betriebsstaettennummer", System.Data.DataRowVersion.Original, null)});
            // 
            // rezepteUpdateCommand
            // 
            this.rezepteUpdateCommand.CommandText = resources.GetString("rezepteUpdateCommand.CommandText");
            this.rezepteUpdateCommand.Connection = this.lastIKsqlConnection;
            this.rezepteUpdateCommand.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Mandant", System.Data.SqlDbType.VarChar, 0, "Mandant"),
            new System.Data.SqlClient.SqlParameter("@IK", System.Data.SqlDbType.VarChar, 0, "IK"),
            new System.Data.SqlClient.SqlParameter("@AbrCode", System.Data.SqlDbType.VarChar, 0, "AbrCode"),
            new System.Data.SqlClient.SqlParameter("@Tarif", System.Data.SqlDbType.VarChar, 0, "Tarif"),
            new System.Data.SqlClient.SqlParameter("@Name", System.Data.SqlDbType.VarChar, 0, "Name"),
            new System.Data.SqlClient.SqlParameter("@Vorname", System.Data.SqlDbType.VarChar, 0, "Vorname"),
            new System.Data.SqlClient.SqlParameter("@GebDat", System.Data.SqlDbType.DateTime, 0, "GebDat"),
            new System.Data.SqlClient.SqlParameter("@Strasse", System.Data.SqlDbType.VarChar, 0, "Strasse"),
            new System.Data.SqlClient.SqlParameter("@Plz", System.Data.SqlDbType.VarChar, 0, "Plz"),
            new System.Data.SqlClient.SqlParameter("@Ort", System.Data.SqlDbType.VarChar, 0, "Ort"),
            new System.Data.SqlClient.SqlParameter("@VersNr", System.Data.SqlDbType.VarChar, 0, "VersNr"),
            new System.Data.SqlClient.SqlParameter("@Status", System.Data.SqlDbType.VarChar, 0, "Status"),
            new System.Data.SqlClient.SqlParameter("@RezDat", System.Data.SqlDbType.DateTime, 0, "RezDat"),
            new System.Data.SqlClient.SqlParameter("@Befreit", System.Data.SqlDbType.Bit, 0, "Befreit"),
            new System.Data.SqlClient.SqlParameter("@ArztNr", System.Data.SqlDbType.VarChar, 0, "ArztNr"),
            new System.Data.SqlClient.SqlParameter("@VK_Bis", System.Data.SqlDbType.VarChar, 0, "VK_Bis"),
            new System.Data.SqlClient.SqlParameter("@Unfall", System.Data.SqlDbType.Bit, 0, "Unfall"),
            new System.Data.SqlClient.SqlParameter("@Hilfsmittel", System.Data.SqlDbType.Int, 0, "Hilfsmittel"),
            new System.Data.SqlClient.SqlParameter("@Erfasst", System.Data.SqlDbType.DateTime, 0, "Erfasst"),
            new System.Data.SqlClient.SqlParameter("@Vorgang", System.Data.SqlDbType.Int, 0, "Vorgang"),
            new System.Data.SqlClient.SqlParameter("@RechNr_Kasse", System.Data.SqlDbType.Int, 0, "RechNr_Kasse"),
            new System.Data.SqlClient.SqlParameter("@RechNr_Mandant", System.Data.SqlDbType.Int, 0, "RechNr_Mandant"),
            new System.Data.SqlClient.SqlParameter("@Image", System.Data.SqlDbType.VarChar, 0, "Image"),
            new System.Data.SqlClient.SqlParameter("@RezArt", System.Data.SqlDbType.SmallInt, 0, "RezArt"),
            new System.Data.SqlClient.SqlParameter("@Pauschale", System.Data.SqlDbType.Money, 0, "Pauschale"),
            new System.Data.SqlClient.SqlParameter("@Diagnose", System.Data.SqlDbType.VarChar, 0, "Diagnose"),
            new System.Data.SqlClient.SqlParameter("@Besonderheit", System.Data.SqlDbType.SmallInt, 0, "Besonderheit"),
            new System.Data.SqlClient.SqlParameter("@Genehmigung_KZ", System.Data.SqlDbType.VarChar, 0, "Genehmigung_KZ"),
            new System.Data.SqlClient.SqlParameter("@Genehmigung_Datum", System.Data.SqlDbType.DateTime, 0, "Genehmigung_Datum"),
            new System.Data.SqlClient.SqlParameter("@Indikation", System.Data.SqlDbType.VarChar, 0, "Indikation"),
            new System.Data.SqlClient.SqlParameter("@Betriebsstaettennummer", System.Data.SqlDbType.NVarChar, 0, "Betriebsstaettennummer"),
            new System.Data.SqlClient.SqlParameter("@Original_ID", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "ID", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Mandant", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Mandant", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_IK", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "IK", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_AbrCode", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "AbrCode", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Tarif", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Tarif", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Name", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Name", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Vorname", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Vorname", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_GebDat", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "GebDat", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_GebDat", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "GebDat", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Strasse", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Strasse", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Strasse", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Strasse", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Plz", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Plz", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Plz", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Plz", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Ort", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Ort", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Ort", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Ort", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_VersNr", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "VersNr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Status", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Status", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_RezDat", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "RezDat", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Befreit", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Befreit", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Befreit", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Befreit", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_ArztNr", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "ArztNr", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_VK_Bis", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "VK_Bis", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_VK_Bis", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "VK_Bis", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Unfall", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Unfall", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Unfall", System.Data.SqlDbType.Bit, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Unfall", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Hilfsmittel", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Hilfsmittel", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Hilfsmittel", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Hilfsmittel", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Erfasst", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Erfasst", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Erfasst", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Erfasst", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Vorgang", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Vorgang", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Vorgang", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Vorgang", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_RechNr_Kasse", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "RechNr_Kasse", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_RechNr_Kasse", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "RechNr_Kasse", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_RechNr_Mandant", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "RechNr_Mandant", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_RechNr_Mandant", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "RechNr_Mandant", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@Original_Image", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Image", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_RezArt", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "RezArt", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_RezArt", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "RezArt", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Pauschale", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Pauschale", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Pauschale", System.Data.SqlDbType.Money, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Pauschale", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Diagnose", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Diagnose", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Diagnose", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Diagnose", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Besonderheit", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Besonderheit", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Besonderheit", System.Data.SqlDbType.SmallInt, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Besonderheit", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Genehmigung_KZ", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Genehmigung_KZ", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Genehmigung_KZ", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Genehmigung_KZ", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Genehmigung_Datum", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Genehmigung_Datum", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Genehmigung_Datum", System.Data.SqlDbType.DateTime, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Genehmigung_Datum", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Indikation", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Indikation", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Indikation", System.Data.SqlDbType.VarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Indikation", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@IsNull_Betriebsstaettennummer", System.Data.SqlDbType.Int, 0, System.Data.ParameterDirection.Input, ((byte)(0)), ((byte)(0)), "Betriebsstaettennummer", System.Data.DataRowVersion.Original, true, null, "", "", ""),
            new System.Data.SqlClient.SqlParameter("@Original_Betriebsstaettennummer", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Betriebsstaettennummer", System.Data.DataRowVersion.Original, null),
            new System.Data.SqlClient.SqlParameter("@ID", System.Data.SqlDbType.Int, 4, "ID"),
            new System.Data.SqlClient.SqlParameter("@KD_Auftrag", System.Data.SqlDbType.VarChar, 0, "KD_Auftrag"),
            new System.Data.SqlClient.SqlParameter("@KD_RechNr", System.Data.SqlDbType.VarChar, 0, "KD_RechNr"),
            new System.Data.SqlClient.SqlParameter("@Vorderseite", System.Data.SqlDbType.VarChar, 0, "Vorderseite"),
            new System.Data.SqlClient.SqlParameter("@Original_Vorderseite", System.Data.SqlDbType.NVarChar, 0, System.Data.ParameterDirection.Input, false, ((byte)(0)), ((byte)(0)), "Vorderseite", System.Data.DataRowVersion.Original, null)});
            // 
            // rezepteInsertCommand
            // 
            this.rezepteInsertCommand.CommandText = resources.GetString("rezepteInsertCommand.CommandText");
            this.rezepteInsertCommand.Connection = this.lastIKsqlConnection;
            this.rezepteInsertCommand.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Mandant", System.Data.SqlDbType.VarChar, 0, "Mandant"),
            new System.Data.SqlClient.SqlParameter("@IK", System.Data.SqlDbType.VarChar, 0, "IK"),
            new System.Data.SqlClient.SqlParameter("@AbrCode", System.Data.SqlDbType.VarChar, 0, "AbrCode"),
            new System.Data.SqlClient.SqlParameter("@Tarif", System.Data.SqlDbType.VarChar, 0, "Tarif"),
            new System.Data.SqlClient.SqlParameter("@Name", System.Data.SqlDbType.VarChar, 0, "Name"),
            new System.Data.SqlClient.SqlParameter("@Vorname", System.Data.SqlDbType.VarChar, 0, "Vorname"),
            new System.Data.SqlClient.SqlParameter("@GebDat", System.Data.SqlDbType.DateTime, 0, "GebDat"),
            new System.Data.SqlClient.SqlParameter("@Strasse", System.Data.SqlDbType.VarChar, 0, "Strasse"),
            new System.Data.SqlClient.SqlParameter("@Plz", System.Data.SqlDbType.VarChar, 0, "Plz"),
            new System.Data.SqlClient.SqlParameter("@Ort", System.Data.SqlDbType.VarChar, 0, "Ort"),
            new System.Data.SqlClient.SqlParameter("@VersNr", System.Data.SqlDbType.VarChar, 0, "VersNr"),
            new System.Data.SqlClient.SqlParameter("@Status", System.Data.SqlDbType.VarChar, 0, "Status"),
            new System.Data.SqlClient.SqlParameter("@RezDat", System.Data.SqlDbType.DateTime, 0, "RezDat"),
            new System.Data.SqlClient.SqlParameter("@Befreit", System.Data.SqlDbType.Bit, 0, "Befreit"),
            new System.Data.SqlClient.SqlParameter("@ArztNr", System.Data.SqlDbType.VarChar, 0, "ArztNr"),
            new System.Data.SqlClient.SqlParameter("@VK_Bis", System.Data.SqlDbType.VarChar, 0, "VK_Bis"),
            new System.Data.SqlClient.SqlParameter("@Unfall", System.Data.SqlDbType.Bit, 0, "Unfall"),
            new System.Data.SqlClient.SqlParameter("@Hilfsmittel", System.Data.SqlDbType.Int, 0, "Hilfsmittel"),
            new System.Data.SqlClient.SqlParameter("@Erfasst", System.Data.SqlDbType.DateTime, 0, "Erfasst"),
            new System.Data.SqlClient.SqlParameter("@Vorgang", System.Data.SqlDbType.Int, 0, "Vorgang"),
            new System.Data.SqlClient.SqlParameter("@RechNr_Kasse", System.Data.SqlDbType.Int, 0, "RechNr_Kasse"),
            new System.Data.SqlClient.SqlParameter("@RechNr_Mandant", System.Data.SqlDbType.Int, 0, "RechNr_Mandant"),
            new System.Data.SqlClient.SqlParameter("@Image", System.Data.SqlDbType.VarChar, 0, "Image"),
            new System.Data.SqlClient.SqlParameter("@RezArt", System.Data.SqlDbType.SmallInt, 0, "RezArt"),
            new System.Data.SqlClient.SqlParameter("@Pauschale", System.Data.SqlDbType.Money, 0, "Pauschale"),
            new System.Data.SqlClient.SqlParameter("@Diagnose", System.Data.SqlDbType.VarChar, 0, "Diagnose"),
            new System.Data.SqlClient.SqlParameter("@Besonderheit", System.Data.SqlDbType.SmallInt, 0, "Besonderheit"),
            new System.Data.SqlClient.SqlParameter("@Genehmigung_KZ", System.Data.SqlDbType.VarChar, 0, "Genehmigung_KZ"),
            new System.Data.SqlClient.SqlParameter("@Genehmigung_Datum", System.Data.SqlDbType.DateTime, 0, "Genehmigung_Datum"),
            new System.Data.SqlClient.SqlParameter("@Indikation", System.Data.SqlDbType.VarChar, 0, "Indikation"),
            new System.Data.SqlClient.SqlParameter("@Betriebsstaettennummer", System.Data.SqlDbType.NVarChar, 0, "Betriebsstaettennummer"),
            new System.Data.SqlClient.SqlParameter("@KD_Auftrag", System.Data.SqlDbType.VarChar, 0, "KD_Auftrag"),
            new System.Data.SqlClient.SqlParameter("@KD_RechNr", System.Data.SqlDbType.VarChar, 0, "KD_RechNr"),
            new System.Data.SqlClient.SqlParameter("Vorderseite", System.Data.SqlDbType.VarChar, 0, "Vorderseite"),
            new System.Data.SqlClient.SqlParameter("Hinterseite", System.Data.SqlDbType.VarChar, 0, "Hinterseite")});
            // 
            // rezepteSelectCommand
            // 
            this.rezepteSelectCommand.CommandText = resources.GetString("rezepteSelectCommand.CommandText");
            this.rezepteSelectCommand.Connection = this.lastIKsqlConnection;
            this.rezepteSelectCommand.Parameters.AddRange(new System.Data.SqlClient.SqlParameter[] {
            new System.Data.SqlClient.SqlParameter("@Mandant", System.Data.SqlDbType.VarChar, 50, "Mandant")});
            // 
            // rezepteDA
            // 
            this.rezepteDA.DeleteCommand = this.rezepteDeleteCommand;
            this.rezepteDA.InsertCommand = this.rezepteInsertCommand;
            this.rezepteDA.SelectCommand = this.rezepteSelectCommand;
            this.rezepteDA.TableMappings.AddRange(new System.Data.Common.DataTableMapping[] {
            new System.Data.Common.DataTableMapping("Table", "2008_Rezepte", new System.Data.Common.DataColumnMapping[] {
                        new System.Data.Common.DataColumnMapping("ID", "ID"),
                        new System.Data.Common.DataColumnMapping("Mandant", "Mandant"),
                        new System.Data.Common.DataColumnMapping("IK", "IK"),
                        new System.Data.Common.DataColumnMapping("AbrCode", "AbrCode"),
                        new System.Data.Common.DataColumnMapping("Tarif", "Tarif"),
                        new System.Data.Common.DataColumnMapping("Name", "Name"),
                        new System.Data.Common.DataColumnMapping("Vorname", "Vorname"),
                        new System.Data.Common.DataColumnMapping("GebDat", "GebDat"),
                        new System.Data.Common.DataColumnMapping("Strasse", "Strasse"),
                        new System.Data.Common.DataColumnMapping("Plz", "Plz"),
                        new System.Data.Common.DataColumnMapping("Ort", "Ort"),
                        new System.Data.Common.DataColumnMapping("VersNr", "VersNr"),
                        new System.Data.Common.DataColumnMapping("Status", "Status"),
                        new System.Data.Common.DataColumnMapping("RezDat", "RezDat"),
                        new System.Data.Common.DataColumnMapping("Befreit", "Befreit"),
                        new System.Data.Common.DataColumnMapping("ArztNr", "ArztNr"),
                        new System.Data.Common.DataColumnMapping("VK_Bis", "VK_Bis"),
                        new System.Data.Common.DataColumnMapping("Unfall", "Unfall"),
                        new System.Data.Common.DataColumnMapping("Hilfsmittel", "Hilfsmittel"),
                        new System.Data.Common.DataColumnMapping("Erfasst", "Erfasst"),
                        new System.Data.Common.DataColumnMapping("Vorgang", "Vorgang"),
                        new System.Data.Common.DataColumnMapping("RechNr_Kasse", "RechNr_Kasse"),
                        new System.Data.Common.DataColumnMapping("RechNr_Mandant", "RechNr_Mandant"),
                        new System.Data.Common.DataColumnMapping("Image", "Image"),
                        new System.Data.Common.DataColumnMapping("RezArt", "RezArt"),
                        new System.Data.Common.DataColumnMapping("Pauschale", "Pauschale"),
                        new System.Data.Common.DataColumnMapping("Diagnose", "Diagnose"),
                        new System.Data.Common.DataColumnMapping("Besonderheit", "Besonderheit"),
                        new System.Data.Common.DataColumnMapping("Genehmigung_KZ", "Genehmigung_KZ"),
                        new System.Data.Common.DataColumnMapping("Genehmigung_Datum", "Genehmigung_Datum"),
                        new System.Data.Common.DataColumnMapping("Indikation", "Indikation"),
                        new System.Data.Common.DataColumnMapping("Betriebsstaettennummer", "Betriebsstaettennummer")})});
            this.rezepteDA.UpdateCommand = this.rezepteUpdateCommand;
            this.rezepteDA.RowUpdated += new System.Data.SqlClient.SqlRowUpdatedEventHandler(this.rezepte_RowUpdated);
            // 
            // tmrNextImage
            // 
            this.tmrNextImage.Tick += new System.EventHandler(this.tmrNextImage_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1016, 581);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Menu = this.mainMenu;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VOIN300";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.pnlCommand.ResumeLayout(false);
            this.pnlHilfe.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.namesDGV)).EndInit();
            this.sortOrderPanel.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            this.pnlMainRight.ResumeLayout(false);
            this.pnlMainRight.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).EndInit();
            this.pnlMainLeft.ResumeLayout(false);
            this.pnlMainLeft.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rezepteBindingNavigator)).EndInit();
            this.rezepteBindingNavigator.ResumeLayout(false);
            this.rezepteBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rezepteBindingSource)).EndInit();
            this.positionenPanel.ResumeLayout(false);
            this.positionenPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.anzahlNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.positionenDGV)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.pnlRezept.ResumeLayout(false);
            this.pnlRezeptImage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picImage2)).EndInit();
            this.pnlRezeptEingabe.ResumeLayout(false);
            this.pnlRezeptEingabe.PerformLayout();
            this.pnlKostenTaeger.ResumeLayout(false);
            this.pnlKostenTaeger.PerformLayout();
            this.pnlRechnungsOptionenPlaceholderRight.ResumeLayout(false);
            this.pnlRechnungsOptionenPlaceholderRight.PerformLayout();
            this.pnlRechNrOptionen.ResumeLayout(false);
            this.pnlRechNrOptionen.PerformLayout();
            this.pnlSuchen.ResumeLayout(false);
            this.pnlSuchen.PerformLayout();
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axListLabel1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mandantenBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.voinDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.positionenBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.rezepteTabPage.ResumeLayout(false);
            this.kostenvoranschlagTabPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kostenvoranschlagDGV)).EndInit();
            this.kostenvoranschlagSuchenPanel.ResumeLayout(false);
            this.kostenvoranschlagSuchenPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

      private System.Windows.Forms.Panel pnlCommand;
        private System.Windows.Forms.MainMenu mainMenu;
      private System.Windows.Forms.SplitContainer splitContainer1;
      private System.Windows.Forms.Panel topPanel;
      private System.Windows.Forms.ComboBox nrComboBox;
      private System.Data.SqlClient.SqlCommand mandantenSelectCommand;
        private System.Data.SqlClient.SqlDataAdapter mandantenDA;
      private System.Data.SqlClient.SqlConnection stamm4SqlConnection;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label name2Label;
        private System.Windows.Forms.Label name1Label;
      private System.Windows.Forms.MenuItem menuItem1;
        private System.Data.SqlClient.SqlCommand kostentraegerSelectCommand;
      private System.Data.SqlClient.SqlDataAdapter kostentraegerDA;
      private System.Windows.Forms.Panel pnlRezeptEingabe;
      private System.Windows.Forms.TextBox vornameTB;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.TextBox nameTB;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.TextBox ortTB;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.TextBox strasseTB;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.TextBox indikationTB;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.TextBox status2TB;
      private System.Windows.Forms.Label label11;
      private System.Windows.Forms.TextBox status1TB;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.ComboBox rezeptArtCB;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.Label label14;
      private System.Windows.Forms.Label label13;
      private System.Windows.Forms.Label label12;
      private System.Windows.Forms.Label label15;
      private System.Windows.Forms.TextBox genKennzTB;
      private System.Data.SqlClient.SqlConnection lastIKsqlConnection;
      private System.Windows.Forms.MenuItem menuItem2;
      private System.Windows.Forms.MenuItem jahr1MenuItem;
        private System.Windows.Forms.MenuItem jahr2MenuItem;
      private VOINDataSet voinDataSet;
      private System.Data.SqlClient.SqlCommand mandantenDeleteCommand;
        private System.Data.SqlClient.SqlCommand mandantenUpdateCommand;
      private System.Windows.Forms.BindingSource mandantenBindingSource;
      private System.Windows.Forms.BindingSource rezepteBindingSource;
      private System.Windows.Forms.CheckBox befreitCheckBox;
      private System.Windows.Forms.MenuItem menuItem3;
      private System.Windows.Forms.MaskedTextBox versichertenNummerMTB;
      private System.Windows.Forms.MaskedTextBox plzMTB;
      private System.Windows.Forms.MaskedTextBox genVonMTB;
      private System.Windows.Forms.MaskedTextBox geburtsDatumMTB;
      private System.Windows.Forms.MaskedTextBox rezeptDatumMTB;
      private System.Windows.Forms.MaskedTextBox arztNummerTB;
      private System.Windows.Forms.Label label19;
      private System.Windows.Forms.Label label18;
      private System.Windows.Forms.MaskedTextBox erfasstTB;
      private System.Windows.Forms.MenuItem menuItem4;
      private System.Windows.Forms.TextBox tarifSchlusselAbrechnCodeTB;
      private System.Data.SqlClient.SqlCommand kostentraeder_TarifeSelectCommand;
      private System.Data.SqlClient.SqlDataAdapter kostentraeder_TarifeDA;
      private System.Data.SqlClient.SqlDataAdapter tab_PositionenDA;
      private System.Data.SqlClient.SqlCommand tab_PositionenSelectCommand;
      private System.Windows.Forms.ErrorProvider errorProvider;
      private System.Windows.Forms.Panel pnlRezept;
      private System.Windows.Forms.Button abrechnenButton;
      private System.Windows.Forms.Panel pnlHilfe;
      private System.Windows.Forms.Button BesonderheitenBtn;
      private System.Data.SqlClient.SqlCommand mandantenBesSelectCommand;
      private System.Data.SqlClient.SqlCommand mandantenBesInsertCommand;
      private System.Data.SqlClient.SqlCommand mandantenBesUpdateCommand;
      private System.Data.SqlClient.SqlCommand mandantenBesDeleteCommand;
      private System.Data.SqlClient.SqlDataAdapter mandantenBesDA;
      private System.Data.SqlClient.SqlConnection mandantenBesSqlConnection;
      private System.Data.SqlClient.SqlCommand gruppeSqlSelectCommand;
      private System.Data.SqlClient.SqlDataAdapter gruppeDA;
      private System.Data.SqlClient.SqlCommand ortSqlSelectCommand;
      private System.Data.SqlClient.SqlDataAdapter ortDA;
      private System.Data.SqlClient.SqlCommand unterGruppeSqlSelectCommand;
      private System.Data.SqlClient.SqlDataAdapter unterGruppeDA;
      private System.Data.SqlClient.SqlCommand artSqlSelectCommand;
      private System.Data.SqlClient.SqlDataAdapter artDA;
      private System.Data.SqlClient.SqlCommand produktSqlSelectCommand;
      private System.Data.SqlClient.SqlDataAdapter produktDA;
      private System.Data.SqlClient.SqlCommand tab_HMPBezeichnungSqlSelectCommand;
      private System.Data.SqlClient.SqlDataAdapter tab_HMPBezeichnungDA;
      private System.Windows.Forms.ToolTip zuzahlungsArtTT;
      private System.Data.SqlClient.SqlCommand positionenInsertCommand;
      private System.Data.SqlClient.SqlCommand positionenUpdateCommand;
      private System.Data.SqlClient.SqlCommand positionenDeleteCommand;
      private System.Data.SqlClient.SqlDataAdapter positionenDA;
      private System.Data.SqlClient.SqlConnection positionenSqlConnection;
      private System.Windows.Forms.BindingSource positionenBindingSource;
      private System.Data.SqlClient.SqlCommand leitZahlenSelectCommand;
      private System.Data.SqlClient.SqlDataAdapter leitZahlenDA;
      private System.Windows.Forms.Button protokollBtn;
      private System.Data.SqlClient.SqlCommand positionenSelectCommand;
      private System.Windows.Forms.MenuItem protokollMI;
      private System.Windows.Forms.MenuItem iniMI;
      private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
      private AxListLabel.AxListLabel axListLabel1;
      private System.Windows.Forms.Button kvBtn;
      private System.Windows.Forms.MenuItem menuItem5;
      private System.Windows.Forms.MenuItem kostenvoranschlagMI;
      private System.Windows.Forms.TabControl tabControl1;
      private System.Windows.Forms.TabPage rezepteTabPage;
      private System.Windows.Forms.TabPage kostenvoranschlagTabPage;
      private System.Windows.Forms.Panel kostenvoranschlagSuchenPanel;
      private System.Windows.Forms.Button kostSuchenBtn;
      private System.Windows.Forms.TextBox kostVornameTB;
      private System.Windows.Forms.Label label34;
      private System.Windows.Forms.TextBox kostNameTB;
      private System.Windows.Forms.Label label31;
      private System.Windows.Forms.TextBox kostRezNrTB;
      private System.Windows.Forms.Label label25;
      private System.Windows.Forms.ComboBox kostJahrCB;
      private System.Windows.Forms.Label label37;
      private System.Windows.Forms.DataGridView kostenvoranschlagDGV;
      private System.Windows.Forms.RadioButton genehmigungRB;
      private System.Windows.Forms.RadioButton korrekturRB;
      private System.Data.SqlClient.SqlCommand tabprimKKassenSelectCommand;
      private System.Data.SqlClient.SqlDataAdapter tabprimKKassenDA;
      private System.Windows.Forms.Panel sortOrderPanel;
      private System.Windows.Forms.ComboBox sortCB;
        private System.Windows.Forms.DataGridView namesDGV;
      private System.Windows.Forms.Button printVOBtn;
      private System.Windows.Forms.MenuItem verordnungMI;
      private System.Windows.Forms.MenuItem posVerwaltungMI;
      private System.Data.SqlClient.SqlCommand kostenTraegerDTASelectCommand;
      private System.Data.SqlClient.SqlDataAdapter kostenTraegerDTADA;
      private System.Windows.Forms.Label label38;
      private System.Windows.Forms.MaskedTextBox vkBisTB;
      private System.Windows.Forms.Label label41;
      private System.Windows.Forms.Label label40;
      private System.Windows.Forms.TextBox rechTB;
      private System.Windows.Forms.TextBox imgTB;
      private System.Windows.Forms.CheckBox imgCB;
      private System.Windows.Forms.Button patientenSuchenBtn;
      private System.Windows.Forms.PictureBox picImage;
      private System.Windows.Forms.ListBox lstImages;
      private System.Windows.Forms.Label label43;
      private System.Windows.Forms.Panel pnlKostenTaeger;
      private System.Windows.Forms.Panel pnlRechnungsOptionenPlaceholderRight;
      private System.Windows.Forms.Label nameLabel;
      private System.Windows.Forms.Panel pnlRechNrOptionen;
      private System.Windows.Forms.CheckBox einzelnCB;
      private System.Windows.Forms.TextBox rechNrTB;
      private System.Windows.Forms.TextBox imageTB;
      private System.Windows.Forms.Label label17;
      private System.Windows.Forms.Label label16;
      private System.Windows.Forms.CheckBox mitRechNrCB;
      private System.Windows.Forms.Panel pnlSuchen;
      private System.Windows.Forms.Button searchBtn;
      private System.Windows.Forms.TextBox ikTB;
      private System.Windows.Forms.Panel pnlMain;
      private System.Windows.Forms.Panel pnlMainLeft;
      private System.Windows.Forms.Panel pnlMainRight;
      private System.Windows.Forms.BindingNavigator rezepteBindingNavigator;
      private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
      private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
      private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
      private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
      private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
      private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
      private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
      private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
      private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
      private System.Windows.Forms.ToolStripButton neuRechTSBtn;
      private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
      private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
      private System.Windows.Forms.ToolStripButton copyTSBtn;
      private System.Windows.Forms.Panel positionenPanel;
      private System.Windows.Forms.Label label39;
      private System.Windows.Forms.ComboBox IdentnrCB;
      private System.Windows.Forms.Label label29;
      private System.Windows.Forms.TextBox posTKZTB;
      private System.Windows.Forms.CheckBox eigenanteilCB;
      private System.Windows.Forms.Label anzahlZuzahlungLabel;
      private System.Windows.Forms.Label anzahlEigenanteilLabel;
      private System.Windows.Forms.Label verbrauchLabel;
      private System.Windows.Forms.TextBox ePreisTB;
      private System.Windows.Forms.TextBox kilometerTB;
      private System.Windows.Forms.Label label49;
      private System.Windows.Forms.TextBox inventarNummerTB;
      private System.Windows.Forms.Label label48;
      private System.Windows.Forms.TextBox begruendungTB;
      private System.Windows.Forms.Label label47;
      private System.Windows.Forms.Label gesamteZuzahlungLabel;
      private System.Windows.Forms.Label gesamteSummeLabel;
      private System.Windows.Forms.Label gesamteForderungLabel;
      private System.Windows.Forms.Button positionDeleteBtn;
      private System.Windows.Forms.Button neuePositionBtn;
      private System.Windows.Forms.Label zuzahlungsArtLabel;
      private System.Windows.Forms.Label label42;
      private System.Windows.Forms.Label produktLabel;
      private System.Windows.Forms.Label artLabel;
      private System.Windows.Forms.Label unterGruppeLabel;
      private System.Windows.Forms.Label ortLabel;
      private System.Windows.Forms.Label gruppeLabel;
      private System.Windows.Forms.Button posBezeichnungBtn;
      private System.Windows.Forms.Label label36;
      private System.Windows.Forms.ComboBox hilfsMittelKennZeichenCB;
      private System.Windows.Forms.Label label35;
      private System.Windows.Forms.Button verbrauchBtn;
      private System.Windows.Forms.Label forderungLabel;
      private System.Windows.Forms.Label label33;
      private System.Windows.Forms.Label label32;
      private System.Windows.Forms.TextBox zuzahlungTB;
      private System.Windows.Forms.Label label30;
      private System.Windows.Forms.TextBox eigenAnteilTB;
      private System.Windows.Forms.ComboBox mwstCB;
      private System.Windows.Forms.Label label28;
      private System.Windows.Forms.CheckBox nettoCB;
      private System.Windows.Forms.TextBox mwstTB;
      private System.Windows.Forms.Label label27;
      private System.Windows.Forms.Button rechnerBtn;
      private System.Windows.Forms.Label label26;
      private System.Windows.Forms.Label label24;
      private System.Windows.Forms.MaskedTextBox produktMTB;
      private System.Windows.Forms.MaskedTextBox artMTB;
      private System.Windows.Forms.MaskedTextBox unterGruppeMTB;
      private System.Windows.Forms.MaskedTextBox ortMTB;
      private System.Windows.Forms.MaskedTextBox gruppeMTB;
      private System.Windows.Forms.Label label23;
      private System.Windows.Forms.NumericUpDown anzahlNUD;
      private System.Windows.Forms.Label label22;
      private System.Windows.Forms.ComboBox jahrCB;
      private System.Windows.Forms.Label label21;
      private System.Windows.Forms.MaskedTextBox tagMonatMTB;
      private System.Windows.Forms.DataGridView positionenDGV;
      private System.Windows.Forms.DataGridViewTextBoxColumn datumColumn;
      private System.Windows.Forms.DataGridViewTextBoxColumn buPosColumn;
      private System.Windows.Forms.DataGridViewTextBoxColumn anzahlColumn;
      private System.Windows.Forms.DataGridViewTextBoxColumn ePreisColumn;
      private System.Windows.Forms.DataGridViewTextBoxColumn forderungColumn;
      private System.Windows.Forms.Label label20;
      private System.Windows.Forms.GroupBox groupBox2;
      private System.Windows.Forms.TextBox dauerTB;
      private System.Windows.Forms.CheckBox dauerCB;
      private System.Windows.Forms.Label label54;
      private System.Windows.Forms.MaskedTextBox zeitBisMTB;
      private System.Windows.Forms.Label label51;
      private System.Windows.Forms.Label label50;
      private System.Windows.Forms.MaskedTextBox zeitVonMTB;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.CheckBox zeitraumCB;
      private System.Windows.Forms.MaskedTextBox versorgungVonMTB;
      private System.Windows.Forms.MaskedTextBox versorgungBisMTB;
      private System.Windows.Forms.Label label53;
      private System.Windows.Forms.Label label52;
      private System.Windows.Forms.Label label55;
      private System.Windows.Forms.TextBox zeitraumTB;
      private System.Windows.Forms.Panel pnlRezeptImage;
      private System.Windows.Forms.PictureBox picImage2;
      private System.Windows.Forms.Button btnArtznrSuchen;
      private System.Data.SqlClient.SqlDataAdapter arztBetrNrDA;
      private System.Data.SqlClient.SqlCommand arztBetrNrInsertCommand;
      private System.Data.SqlClient.SqlCommand arztBetrNrSelectCommand;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameVorname;
        private System.Data.SqlClient.SqlCommand rezepteDeleteCommand;
        private System.Data.SqlClient.SqlCommand rezepteUpdateCommand;
        private System.Data.SqlClient.SqlCommand rezepteInsertCommand;
        private System.Data.SqlClient.SqlCommand rezepteSelectCommand;
        private System.Data.SqlClient.SqlDataAdapter rezepteDA;
        private System.Windows.Forms.Timer tmrNextImage;

    }
}

