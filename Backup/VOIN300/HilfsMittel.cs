﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace VOIN10
{
  class HilfsMittel
  {
    private static ArrayList hilfsMittelList;

    public static ArrayList HilfsMittelList
    {
      get { return hilfsMittelList; }
    }

    static HilfsMittel()
    {
      hilfsMittelList = new ArrayList();
      HilfsMittelList.Add(new HilfsMittel("00 - Neulieferung", 0));
      HilfsMittelList.Add(new HilfsMittel("01 - Reparatur", 1));
      HilfsMittelList.Add(new HilfsMittel("02 - Wiedereinsatz", 2));
      HilfsMittelList.Add(new HilfsMittel("03 - Miete", 3));
      HilfsMittelList.Add(new HilfsMittel("04 - Nachlieferung", 4));
      HilfsMittelList.Add(new HilfsMittel("05 - Zurichtung", 5));
      HilfsMittelList.Add(new HilfsMittel("06 - Abgabe höherwertiges HM", 6));
      HilfsMittelList.Add(new HilfsMittel("07 - Nachlieferung höherwertiges HM", 7));
      HilfsMittelList.Add(new HilfsMittel("08 - Vergütungspauschale", 8));
      HilfsMittelList.Add(new HilfsMittel("09 - Folgevergütungspauschale", 9));
      HilfsMittelList.Add(new HilfsMittel("10 - Folgeversorgung", 10));
      HilfsMittelList.Add(new HilfsMittel("11 - Ersatzbeschaffung", 11));
      HilfsMittelList.Add(new HilfsMittel("12 - Zubehör", 12));
      HilfsMittelList.Add(new HilfsMittel("13 - Reperaturpaushale", 13));
      HilfsMittelList.Add(new HilfsMittel("14 - Wartung", 14));
      HilfsMittelList.Add(new HilfsMittel("15 - Wartungspauschale", 15));
      HilfsMittelList.Add(new HilfsMittel("16 - Auslieferung", 16));
      HilfsMittelList.Add(new HilfsMittel("17 - Aussonderung", 17));
      HilfsMittelList.Add(new HilfsMittel("18 - Rückholung", 18));
      HilfsMittelList.Add(new HilfsMittel("19 - Abbruch", 19));
      HilfsMittelList.Add(new HilfsMittel("20 - Erprobung", 20));
    }

    private HilfsMittel(string name, short hilfsMittel_KZ)
    {
      this.name = name;
      this.hilfsMittel_KZ = hilfsMittel_KZ;
    }

    private string name;
    public string Name
    {
      get { return name; }
    }

    private short hilfsMittel_KZ;
    public short HilfsMittel_KZ
    {
      get { return hilfsMittel_KZ; }
    }
  }
}
