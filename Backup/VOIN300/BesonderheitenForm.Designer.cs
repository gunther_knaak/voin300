﻿namespace VOIN10
{
  partial class BesonderheitenForm
  {
    /// <summary>
    /// Erforderliche Designervariable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Verwendete Ressourcen bereinigen.
    /// </summary>
    /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Vom Windows Form-Designer generierter Code

    /// <summary>
    /// Erforderliche Methode für die Designerunterstützung.
    /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
    /// </summary>
    private void InitializeComponent()
    {
      this.mandantFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
      this.nummerLabel = new System.Windows.Forms.Label();
      this.ikLabel = new System.Windows.Forms.Label();
      this.nameLabel = new System.Windows.Forms.Label();
      this.vornameLabel = new System.Windows.Forms.Label();
      this.strasseLabel = new System.Windows.Forms.Label();
      this.plzLabel = new System.Windows.Forms.Label();
      this.ortLabel = new System.Windows.Forms.Label();
      this.telefonLabel = new System.Windows.Forms.Label();
      this.faxLabel = new System.Windows.Forms.Label();
      this.eMailLabel = new System.Windows.Forms.Label();
      this.splitter1 = new System.Windows.Forms.Splitter();
      this.besonderheitenPanel = new System.Windows.Forms.Panel();
      this.panel1 = new System.Windows.Forms.Panel();
      this.panel2 = new System.Windows.Forms.Panel();
      this.neuBtn = new System.Windows.Forms.Button();
      this.okBtn = new System.Windows.Forms.Button();
      this.mandantFlowLayoutPanel.SuspendLayout();
      this.panel1.SuspendLayout();
      this.panel2.SuspendLayout();
      this.SuspendLayout();
      // 
      // mandantFlowLayoutPanel
      // 
      this.mandantFlowLayoutPanel.Controls.Add(this.nummerLabel);
      this.mandantFlowLayoutPanel.Controls.Add(this.ikLabel);
      this.mandantFlowLayoutPanel.Controls.Add(this.nameLabel);
      this.mandantFlowLayoutPanel.Controls.Add(this.vornameLabel);
      this.mandantFlowLayoutPanel.Controls.Add(this.strasseLabel);
      this.mandantFlowLayoutPanel.Controls.Add(this.plzLabel);
      this.mandantFlowLayoutPanel.Controls.Add(this.ortLabel);
      this.mandantFlowLayoutPanel.Controls.Add(this.telefonLabel);
      this.mandantFlowLayoutPanel.Controls.Add(this.faxLabel);
      this.mandantFlowLayoutPanel.Controls.Add(this.eMailLabel);
      this.mandantFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
      this.mandantFlowLayoutPanel.Location = new System.Drawing.Point(0, 0);
      this.mandantFlowLayoutPanel.Name = "mandantFlowLayoutPanel";
      this.mandantFlowLayoutPanel.Size = new System.Drawing.Size(630, 65);
      this.mandantFlowLayoutPanel.TabIndex = 0;
      // 
      // nummerLabel
      // 
      this.nummerLabel.AutoSize = true;
      this.nummerLabel.Location = new System.Drawing.Point(3, 0);
      this.nummerLabel.Name = "nummerLabel";
      this.nummerLabel.Size = new System.Drawing.Size(49, 13);
      this.nummerLabel.TabIndex = 0;
      this.nummerLabel.Text = "Nummer:";
      // 
      // ikLabel
      // 
      this.ikLabel.AutoSize = true;
      this.ikLabel.Location = new System.Drawing.Point(58, 0);
      this.ikLabel.Name = "ikLabel";
      this.ikLabel.Size = new System.Drawing.Size(20, 13);
      this.ikLabel.TabIndex = 1;
      this.ikLabel.Text = "IK:";
      // 
      // nameLabel
      // 
      this.nameLabel.AutoSize = true;
      this.nameLabel.Location = new System.Drawing.Point(84, 0);
      this.nameLabel.Name = "nameLabel";
      this.nameLabel.Size = new System.Drawing.Size(38, 13);
      this.nameLabel.TabIndex = 2;
      this.nameLabel.Text = "Name:";
      // 
      // vornameLabel
      // 
      this.vornameLabel.AutoSize = true;
      this.vornameLabel.Location = new System.Drawing.Point(128, 0);
      this.vornameLabel.Name = "vornameLabel";
      this.vornameLabel.Size = new System.Drawing.Size(52, 13);
      this.vornameLabel.TabIndex = 3;
      this.vornameLabel.Text = "Vorname:";
      // 
      // strasseLabel
      // 
      this.strasseLabel.AutoSize = true;
      this.strasseLabel.Location = new System.Drawing.Point(186, 0);
      this.strasseLabel.Name = "strasseLabel";
      this.strasseLabel.Size = new System.Drawing.Size(41, 13);
      this.strasseLabel.TabIndex = 4;
      this.strasseLabel.Text = "Straße:";
      // 
      // plzLabel
      // 
      this.plzLabel.AutoSize = true;
      this.plzLabel.Location = new System.Drawing.Point(233, 0);
      this.plzLabel.Name = "plzLabel";
      this.plzLabel.Size = new System.Drawing.Size(30, 13);
      this.plzLabel.TabIndex = 5;
      this.plzLabel.Text = "PLZ:";
      // 
      // ortLabel
      // 
      this.ortLabel.AutoSize = true;
      this.ortLabel.Location = new System.Drawing.Point(269, 0);
      this.ortLabel.Name = "ortLabel";
      this.ortLabel.Size = new System.Drawing.Size(24, 13);
      this.ortLabel.TabIndex = 6;
      this.ortLabel.Text = "Ort:";
      // 
      // telefonLabel
      // 
      this.telefonLabel.AutoSize = true;
      this.telefonLabel.Location = new System.Drawing.Point(299, 0);
      this.telefonLabel.Name = "telefonLabel";
      this.telefonLabel.Size = new System.Drawing.Size(46, 13);
      this.telefonLabel.TabIndex = 7;
      this.telefonLabel.Text = "Telefon:";
      // 
      // faxLabel
      // 
      this.faxLabel.AutoSize = true;
      this.faxLabel.Location = new System.Drawing.Point(351, 0);
      this.faxLabel.Name = "faxLabel";
      this.faxLabel.Size = new System.Drawing.Size(27, 13);
      this.faxLabel.TabIndex = 8;
      this.faxLabel.Text = "Fax:";
      // 
      // eMailLabel
      // 
      this.eMailLabel.AutoSize = true;
      this.eMailLabel.Location = new System.Drawing.Point(384, 0);
      this.eMailLabel.Name = "eMailLabel";
      this.eMailLabel.Size = new System.Drawing.Size(39, 13);
      this.eMailLabel.TabIndex = 9;
      this.eMailLabel.Text = "E-Mail:";
      // 
      // splitter1
      // 
      this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
      this.splitter1.Location = new System.Drawing.Point(0, 65);
      this.splitter1.Name = "splitter1";
      this.splitter1.Size = new System.Drawing.Size(630, 8);
      this.splitter1.TabIndex = 1;
      this.splitter1.TabStop = false;
      // 
      // besonderheitenPanel
      // 
      this.besonderheitenPanel.AutoScroll = true;
      this.besonderheitenPanel.AutoSize = true;
      this.besonderheitenPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.besonderheitenPanel.Location = new System.Drawing.Point(0, 73);
      this.besonderheitenPanel.Name = "besonderheitenPanel";
      this.besonderheitenPanel.Size = new System.Drawing.Size(630, 262);
      this.besonderheitenPanel.TabIndex = 2;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.panel2);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel1.Location = new System.Drawing.Point(0, 335);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(630, 29);
      this.panel1.TabIndex = 3;
      // 
      // panel2
      // 
      this.panel2.Controls.Add(this.neuBtn);
      this.panel2.Controls.Add(this.okBtn);
      this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
      this.panel2.Location = new System.Drawing.Point(464, 0);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(166, 29);
      this.panel2.TabIndex = 2;
      // 
      // neuBtn
      // 
      this.neuBtn.Location = new System.Drawing.Point(3, 3);
      this.neuBtn.Name = "neuBtn";
      this.neuBtn.Size = new System.Drawing.Size(75, 23);
      this.neuBtn.TabIndex = 0;
      this.neuBtn.Text = "Neu";
      this.neuBtn.UseVisualStyleBackColor = true;
      this.neuBtn.Click += new System.EventHandler(this.neuBtn_Click);
      // 
      // okBtn
      // 
      this.okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.okBtn.Location = new System.Drawing.Point(84, 3);
      this.okBtn.Name = "okBtn";
      this.okBtn.Size = new System.Drawing.Size(75, 23);
      this.okBtn.TabIndex = 1;
      this.okBtn.Text = "OK";
      this.okBtn.UseVisualStyleBackColor = true;
      // 
      // BesonderheitenForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(630, 364);
      this.Controls.Add(this.besonderheitenPanel);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.splitter1);
      this.Controls.Add(this.mandantFlowLayoutPanel);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "BesonderheitenForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Besonderheiten";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BesonderheitenForm_FormClosing);
      this.mandantFlowLayoutPanel.ResumeLayout(false);
      this.mandantFlowLayoutPanel.PerformLayout();
      this.panel1.ResumeLayout(false);
      this.panel2.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.FlowLayoutPanel mandantFlowLayoutPanel;
    private System.Windows.Forms.Label nummerLabel;
    private System.Windows.Forms.Label ikLabel;
    private System.Windows.Forms.Label nameLabel;
    private System.Windows.Forms.Label vornameLabel;
    private System.Windows.Forms.Label strasseLabel;
    private System.Windows.Forms.Label plzLabel;
    private System.Windows.Forms.Label ortLabel;
    private System.Windows.Forms.Label telefonLabel;
    private System.Windows.Forms.Label faxLabel;
    private System.Windows.Forms.Label eMailLabel;
    private System.Windows.Forms.Splitter splitter1;
    private System.Windows.Forms.Panel besonderheitenPanel;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Button okBtn;
    private System.Windows.Forms.Button neuBtn;
    private System.Windows.Forms.Panel panel2;

  }
}