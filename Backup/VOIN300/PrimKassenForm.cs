using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace VOIN10
{
  public partial class PrimKassenForm : Form
  {
    private string ik = string.Empty;
    public string IK
    {
      get { return ik; }
    }

    private VOINDataSet dataSet = null;
    public VOINDataSet DataSet
    {
      get
      {
        return dataSet;
      }
      set
      {
        if (dataSet != value)
        {
          dataSet = value;
          SetDataSet();
        }
      }
    }

    public PrimKassenForm()
    {
      InitializeComponent();
    }

    private void SetDataSet()
    {
      bindingSource1.DataSource = DataSet.Tab_primKKassen;
    }

    private void dataGridView1_KeyDown(object sender, KeyEventArgs e)
    {
      if(e.KeyCode == Keys.Enter && bindingSource1.Position >= 0)
        SetIK();
    }

    private void dataGridView1_MouseDoubleClick(object sender, MouseEventArgs e)
    {
      if (bindingSource1.Position >= 0)
      {
        DataGridView.HitTestInfo info = dataGridView1.HitTest(e.X, e.Y);
        if (info.Type == DataGridViewHitTestType.Cell || info.Type ==
          DataGridViewHitTestType.RowHeader)
          SetIK();
      }
    }

    private void SetIK()
    {
      if (bindingSource1.Position >= 0)
      {
        VOINDataSet.Tab_primKKassenRow row = (VOINDataSet.Tab_primKKassenRow)
          ((DataRowView)bindingSource1.Current).Row;
        ik = row.IK;
        DialogResult = DialogResult.OK;
      }
    }

  }
}