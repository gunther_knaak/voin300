﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace VOIN10
{
  public partial class BesonderheitenForm : Form
  {
    VOINDataSet dataSet;

    public VOINDataSet DataSet
    {
      get { return  dataSet; }
      set 
      {
        if (dataSet != value)
        {
          dataSet = value;
          UpdateList();
        }
      }
    }

    private void UpdateList()
    {
      if (DataSet != null)
      {
        while (besonderheitenPanel.Controls.Count != 0)
        {
          Control c = besonderheitenPanel.Controls[0];
          besonderheitenPanel.Controls.Remove(c);
          c.Dispose();
        }
        //while (nameTLPanel.Controls.Count != 0)
        //{
        //  Control c = nameTLPanel.Controls[0];
        //  nameTLPanel.Controls.Remove(c);
        //  c.Dispose();
        //}
        DataRow[] rows = DataSet.Mandanten_Bes.Select("Nr = " + Mandant["Nr"].ToString(), "Datum DESC");
        int location = 4;
        foreach (DataRow row in rows)
        {
          RichTextBox rtf = new RichTextBox();
          TextBox tb1 = new TextBox();
          TextBox tb2 = new TextBox();
          rtf.Rtf = (string)row["Besonderheit"];
          rtf.ReadOnly = true;
          rtf.Size = new Size(500, 100);
          rtf.Location = new Point(4, location);

          tb1.Text = (string)row["Bearbeiter"];
          tb1.Location = new Point(508, location);
          tb1.Size = new Size(100, tb1.Size.Height);
          tb1.ReadOnly = true;

          tb2.Text = ((DateTime)row["Datum"]).ToString("dd.MM.yyyy hh:mm");
          tb2.Location = new Point(508, location + tb1.Height + 4);
          tb2.Size = new Size(100, tb2.Size.Height);
          tb2.ReadOnly = true;

          location += 104;
          besonderheitenPanel.Controls.Add(rtf);
          besonderheitenPanel.Controls.Add(tb1);
          besonderheitenPanel.Controls.Add(tb2);
          //besonderheitenPanel.RowStyles[besonderheitenTLPanel.RowStyles.Count - 1].SizeType = SizeType.Absolute;
          //besonderheitenPanel.RowStyles[besonderheitenTLPanel.RowStyles.Count - 1].Height = 100;
          //TextBox tb = new TextBox();
          //tb.Text = (string)row["Bearbeiter"];
          //tb.Enabled = false;
          //besonderheitenPanel.Controls.Add(tb);
          //besonderheitenPanel.RowStyles[nameTLPanel.RowStyles.Count - 1].SizeType = SizeType.Absolute;
          //besonderheitenPanel.RowStyles[nameTLPanel.RowStyles.Count - 1].Height = 50;
          //tb = new TextBox();
          //tb.Text = ((DateTime)row["Datum"]).ToString("dd.MM.yyyy");
          //tb.Enabled = false;
          //besonderheitenPanel.Controls.Add(tb);
          //besonderheitenPanel.RowStyles[nameTLPanel.RowStyles.Count - 1].SizeType = SizeType.Absolute;
          //besonderheitenPanel.RowStyles[nameTLPanel.RowStyles.Count - 1].Height = 50;
        }
      }
    }

    SqlDataAdapter dataAdapter;

    public SqlDataAdapter DataAdapter
    {
      get { return dataAdapter; }
      set { dataAdapter = value; }
    }

    DataRow mandant;

    public DataRow Mandant
    {
      get { return mandant; }
      set 
      {
        if (mandant != value)
        {
          mandant = value;
          SetMandant();
        }
      }
    }

    private void SetMandant()
    {
      if (Mandant != null)
      {
        nummerLabel.Text = "Nummer: " + Mandant["Nr"].ToString();
        ikLabel.Text = "IK: " + Mandant["IK"].ToString();
        nameLabel.Text = "Name: " + Mandant["Name1"].ToString() + " " +
          Mandant["Name2"].ToString() + (Mandant["Name3"] != DBNull.Value ? " " +
          Mandant["Name3"].ToString() : string.Empty);
        if (Mandant["Vorname"].ToString() != string.Empty)
          vornameLabel.Text = "Vorname: " + Mandant["Vorname"].ToString();
        else
          vornameLabel.Visible = false;
        strasseLabel.Text = "Straße: " + Mandant["Straße"].ToString();
        plzLabel.Text = "PLZ: " + Mandant["PLZ"].ToString();
        ortLabel.Text = "Ort:" + Mandant["Ort"].ToString();
        telefonLabel.Text = "Telefon: " + Mandant["Tel"].ToString();
        if (Mandant["Fax"] != DBNull.Value)
          faxLabel.Text = "Fax: " + Mandant["Fax"].ToString();
        else
          faxLabel.Visible = false;
        if (Mandant["EMail"] != DBNull.Value)
          eMailLabel.Text = "E-Mail: " + Mandant["EMail"].ToString();
        else
          eMailLabel.Visible = false;
      }
    }

    public BesonderheitenForm()
    {
      InitializeComponent();
    }

    private void neuBtn_Click(object sender, EventArgs e)
    {
      using(NeueBesonderheitForm form = new NeueBesonderheitForm())
      {
        if (form.ShowDialog() == DialogResult.OK && form.RichText.Rtf != string.Empty)
        {
          DataRow row = DataSet.Mandanten_Bes.NewRow();
          row["Nr"] = Mandant["Nr"];
          row["Bearbeiter"] = Environment.UserName;
          row["Datum"] = DateTime.Now;
          //form.Text += "\n" + Environment.UserName + " " +
          //  ((DateTime)row["Datum"]).ToString("dd.MM.yyyy hh:mm");
          //form.RichText.SelectionStart = form.RichText.Text.Length;
          //form.RichText.AppendText("\n" + Environment.UserName + " " +
          //  ((DateTime)row["Datum"]).ToString("dd.MM.yyyy HH:mm"));
          row["Besonderheit"] = form.RichText.Rtf;
          DataSet.Mandanten_Bes.Rows.Add(row);
          UpdateList();
        }
      }
    }

    private void BesonderheitenForm_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (DataAdapter != null)
      {
        bool f = true;
        while (f)
        {
          f = false;
          try
          {
            DataAdapter.Update(DataSet.Mandanten_Bes);
          }
          catch (Exception ex)
          {
            if (MessageBox.Show(ex.Message, "", MessageBoxButtons.RetryCancel,
              MessageBoxIcon.Error) == DialogResult.Retry) f = true;
            if (f) DataSet.AcceptChanges();
          }
        }
      }
    }
  }
}