﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace VOIN10
{
  public partial class PositionsSearchForm : Form
  {
    private VOINDataSet dataSet = null;
    public VOINDataSet DataSet
    {
      get { return dataSet; }
      set
      {
        if (dataSet != value)
        {
          dataSet = value;
          SetDataSet();
        }
      }
    }

    private int mandant = -1;
    public int Mandant
    {
      get { return mandant; }
      set
      {
        if (mandant != value)
        {
          mandant = value;
          SetDataSet();
        }
      }
    }

    private string tarifKZ = string.Empty;
    public string TarifKZ
    {
      get { return tarifKZ; }
      set
      {
        if (tarifKZ != value)
        {
          tarifKZ = value;
          SetDataSet();
        }
      }
    }

    public string HMP
    {
      get
      {
        string res = string.Empty;
        if(dataGridView1.DataSource != null)
        {
          BindingManagerBase bm = this.BindingContext[dataGridView1.DataSource];
          if (bm != null && bm.Count != 0 && bm.Current != null)
            res = (string)((DataRowView)(bm.Current)).Row["HMP"];
        }
        return res;
      }
    }

    public string SelTarifKZ
    {
      get
      {
        string res = string.Empty;
        if (dataGridView1.DataSource != null)
        {
          BindingManagerBase bm = this.BindingContext[dataGridView1.DataSource];
          if (bm != null && bm.Count != 0 && bm.Current != null && 
            ((DataRowView)bm.Current).Row["TarifKZ"] != DBNull.Value)
            res = (string)((DataRowView)(bm.Current)).Row["TarifKZ"];
        }
        return res;
      }
    }

    public object EPreis
    {
      get
      {
        object res = DBNull.Value;
        if (dataGridView1.DataSource != null)
        {
          BindingManagerBase bm = this.BindingContext[dataGridView1.DataSource];
          if (bm != null && bm.Count != 0 && bm.Current != null &&
            ((DataRowView)bm.Current).Row["EPreis"] != DBNull.Value)
            res = ((DataRowView)(bm.Current)).Row["EPreis"];
        }
        return res;
      }
    }

    private void SetDataSet()
    {
      if (DataSet != null && Mandant != -1 && TarifKZ != string.Empty)
      {
        DataView view = new DataView(DataSet.tab_HMPBezeichnung, "Mandant = " + 
          Mandant.ToString() /*+ " AND TarifKZ = '" + TarifKZ + "'"*/, "HMP", 
          DataViewRowState.CurrentRows);
        dataGridView1.DataSource = view;
        //foreach (DataGridViewColumn c in dataGridView1.Columns)
        //  if (c.DataPropertyName != "HMP" && c.DataPropertyName != "Bezeichnung" &&
        //    c.DataPropertyName != "ArtikelNr")
        //    c.Visible = false;
      }
      else
        dataGridView1.DataSource = null;
    }

    public PositionsSearchForm()
    {
      InitializeComponent();
    }

    private void PositionsSearchForm_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter)
      {
        this.DialogResult = DialogResult.OK;
        e.Handled = true;
      }
    }

    private void dataGridView1_MouseDoubleClick(object sender, MouseEventArgs e)
    {
      DataGridView.HitTestInfo info = dataGridView1.HitTest(e.X, e.Y);
      if (info.Type == DataGridViewHitTestType.Cell ||
        info.Type == DataGridViewHitTestType.RowHeader)
        this.DialogResult = DialogResult.OK;
    }
  }
}