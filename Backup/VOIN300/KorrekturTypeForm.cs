using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace VOIN10
{
  public enum KorrekturType
  {
Zuruecksetzen, Nachkorrektur, Vollkorrektur
  }

  public partial class KorrekturTypeForm : Form
  {
    public KorrekturType KorrekturType 
    {
      get
      {
        if(zuruecksetzenRB.Checked)
          return KorrekturType.Zuruecksetzen;
        if(nachkorrekturRB.Checked)
          return KorrekturType.Nachkorrektur;
        return KorrekturType.Vollkorrektur;
      }
      set 
      {
        if(value == KorrekturType.Zuruecksetzen)
          zuruecksetzenRB.Checked = true;
        if(value == KorrekturType.Nachkorrektur)
          nachkorrekturRB.Checked = true;
        vollkorrekturRB.Checked = true;
      }
    }

    public RadioButton ZuruecksetzenRB
    {
      get { return zuruecksetzenRB; }
    }

    public RadioButton NachkorrekturRB
    {
      get { return nachkorrekturRB; }
    }

    public RadioButton VollkorrekturRB
    {
      get { return vollkorrekturRB; }
    }

    public KorrekturTypeForm()
    {
      InitializeComponent();
    }
  }
}